#include <a_samp>
#include <fixchars>
//#include <mysql>
#include <a_mysql>
//#include <JunkBuster>
#include <sscanf2>
#include <zcmd>
#include <audio>
#include <streamer>
//#include <inventory>
#include <foreach>
#include <progress2>
#include <mSelection>
//#pragma unused strtok

#define COLOR_FADE1 0xf9b7ffAA
#define COLOR_FADE2 0xf9b7ffAA
#define COLOR_FADE3 0xf9b7ffAA
#define COLOR_FADE4 0xf9b7ffAA
#define COLOR_FADE5 0xf9b7ffAA
#define COLOR_WHITE  0xFFFFFFFF

#define COLOR_CHAT1 0xFFFFFFFF
#define COLOR_CHAT2 0x8f8982AA
#define COLOR_CHAT3 0x707070AA
#define COLOR_CHAT4 0xFFFFFFFF
#define COLOR_CHAT5 0xFFFFFFFF

#define MAX_3DTEXT_GLOBAL	(1024)
#define DIALOG_KOCSI 101
#define DIALOG_KOCSIK 102
#define DIALOG_PARANCS 103
#define DIALOG_VHOUSE 104
#define DIALOG_MHOUSE 105
#define DIALOG_TASKA 106
#define DIALOG_JELENTES 107
#define DIALOG_BUG 108
#define DIALOG_GAME 109
#define DIALOG_NONRP 110
#define DIALOG_JALARM 111
#define DIALOG_MHOUSEZ 112
#define DIALOG_MHOUSENY 113
#define DIALOG_MHOUSEI 114
#define DIALOG_LEADER 115
#define DIALOG_LEADER1 116
#define DIALOG_LEADER2 117
#define DIALOG_LEADER3 118
#define DIALOG_LEADER4 119
#define DIALOG_LEADER5 120
#define DIALOG_LEADER6 121
#define DIALOG_NETOWRK 122
#define DIALOG_TASKA1 123
#define DIALOG_TASKA2 124
#define DIALOG_TASKA3 125
#define DIALOG_HAZELAD 126
#define DIALOG_BANK1 127
#define DIALOG_BANK2 128
#define DIALOG_BANK3 129
#define DIALOG_BANK4 130
#define DIALOG_BANK5 131
#define DIALOG_BANK6 132
#define DIALOG_BANK7 133
#define DIALOG_BANK8 134
#define DIALOG_BANK9 135
#define DIALOG_BANK10 136
#define DIALOG_BANK11 137
#define DIALOG_BANK12 138
#define DIALOG_BANK13 139
#define DIALOG_BANK14 140
#define DIALOG_BANK15 141
#define DIALOG_ADMIN1 142
#define DIALOG_ADMIN2 143
#define DIALOG_ADMIN3 144
#define DIALOG_ADMIN4 145
#define DIALOG_ADMIN5 146
#define DIALOG_MOBIL1 147
#define DIALOG_MOBIL2 148
#define DIALOG_MOBIL3 149
#define DIALOG_MOBIL4 150
#define DIALOG_MOBIL5 151
#define DIALOG_KULCS1 152
#define DIALOG_KULCS2 153
#define VITEM_DIALOG 154
#define DIALOG_CSOMAGTARTO1 155
#define DIALOG_CSOMAGTARTO2 156
#define DIALOG_MOBIL6 157
#define DIALOG_VBIZZ 158
#define DIALOG_MBIZZ 159
#define DIALOG_SBIZZ 160
#define DIALOG_BIZ1 161
#define DIALOG_BIZ2 162
#define DIALOG_BIZELAD 163
#define DIALOG_ALLEADER1 164
#define DIALOG_ALLEADER2 165
#define DIALOG_ALLEADER3 166
#define DIALOG_ALLEADER4 167
#define DIALOG_ALLEADER5 168
#define DIALOG_KESZTYUTARTO1 169
#define DIALOG_KESZTYUTARTO2 170
#define KITEM_DIALOG 171
#define DIALOG_FELRAK1 172
#define DIALOG_ORFK1 173
#define DIALOG_OMSZ1 174
#define DIALOG_SZERELO1 175
#define DIALOG_SZERELO2 176
#define DIALOG_SZERELO3 177
#define DIALOG_SZERELO4 178
#define DIALOG_SZERELO5 179
#define DIALOG_SZERELO6 180
#define DIALOG_SZERELO7 181
#define DIALOG_SZERELO8 182
#define DIALOG_SZERELO9 183
#define DIALOG_SZERELO10 184
#define DIALOG_SZERELO11 185
#define DIALOG_SZERELO12 186
#define DIALOG_SZERELO13 187
#define DIALOG_TELEPORT 188
#define DIALOG_SZERELO14 189
#define DIALOG_SZERELO15 190
#define DIALOG_SZERELO16 191
#define DIALOG_RENTH1 192
#define DIALOG_RENTH2 193
#define DIALOG_RENTH3 194
#define DIALOG_RENTH4 195
#define DIALOG_CEG1 196
#define DIALOG_CEG2 197
#define DIALOG_CEG3 198
#define DIALOG_CEG4 199
#define DIALOG_CEG5 200
#define DIALOG_CEG6 201
#define DIALOG_CEG7 202
#define DIALOG_CEG8 203
#define DIALOG_CEG9 204
#define DIALOG_KAMION1 205
#define DIALOG_3DLABEL1 206
#define DIALOG_3DLABEL2 207
#define DIALOG_3DLABEL3 208
#define DIALOG_KOCSIATVESZ 209
#define DIALOG_BORTONN 210
#define DIALOG_KRESZ1 211 
#define DIALOG_KRESZ2 212
#define DIALOG_KRESZ3 213
#define DIALOG_KRESZ4 214
#define DIALOG_KRESZ5 215
#define DIALOG_KRESZ6 216
#define DIALOG_KRESZ7 217
#define DIALOG_KRESZ8 218
#define DIALOG_KRESZ9 219
#define DIALOG_BENZINKUT1 220
#define DIALOG_BENZINKUT2 221
#define DIALOG_TASKA4 222
#define DIALOG_TASKA5 223
#define DIALOG_CSOMAGTARTO3 224
#define DIALOG_CSOMAGTARTO4 225
#define DIALOG_KESZTYUTARTO3 226
#define DIALOG_KESZTYUTARTO4 227
#define DIALOG_NOVENY1 228
#define DIALOG_RAKTAR1 229
#define DIALOG_RAKTAR2 230
#define DIALOG_RAKTAR3 231
#define DIALOG_RAKTAR4 232
#define DIALOG_RAKTAR5 233
#define DIALOG_RAKTAR6 234
#define DIALOG_RAKTAR7 235
#define DIALOG_NOVENY2 236
#define DIALOG_NOVENY3 237
#define DIALOG_NOVENY4 238
#define DIALOG_NOVENY5 239
#define DIALOG_RAKTAR8 240
#define DIALOG_RAKTAR9 241
#define DIALOG_RAKTAR10 242
#define DIALOG_RAKTAR11 243
#define DIALOG_DOBOZ1 244
#define DIALOG_AUSZEREL1 245
#define DIALOG_ONKORMANYZAT1 246
#define DIALOG_ONKORMANYZAT2 247
#define DIALOG_ONKORMANYZAT3 248
#define DIALOG_ONKORMANYZAT4 249
#define DIALOG_FIZETES1 250
#define DIALOG_FIZETES2 251
#define DIALOG_FIZETES3 252
#define DIALOG_FIZETES4 253
#define DIALOG_FIZETES5 254
#define DIALOG_ONKORMANYZAT5 255
#define DIALOG_VASAROL 256

#define JARMURENDEL_MENU 1

#define ITEM_DIALOG 5842

#define COLOR_RED 0xff0000AA
#define COLOR_GREEN 0x00ff00AA
#define COLOR_BABA 0x2ffc99AA
#define COLOR_YELLOW 0xbdc225AA
#define COLOR_BLUE 0x4661FEAA
#define COLOR_ORANGE 0xFF8C00AA

#define MAX_HOUSE 500
#define MAX_RANGOK 15
#define MAX_AJTO 500
#define MAX_BIZZ 500
#define MAX_BARAT 150
#define MAX_BEJELENTESEK 100
#define MAX_BEJELENTESTIPUS 3
#define MAX_UTZAR 15
#define MAX_KITEMS 5
#define MAX_ITEMS 20
#define MAX_TUZ 100
#define MAX_MOVEOB 500
#define MAX_RENTH 500
#define MAX_CEG 50
#define MAX_RAKTAR 50
#define MAX_HULLA (MAX_PLAYERS / 2)


#define HULLAIDO 60000

#define CAR 0

#define BIKE 1

#define MOTORBIKE 2

#define BOAT 3

#define PLANE 4

#define RC 5

#define TRAIN 6

#define TRAILER 7

#define HELICOPTER 8

#define PRESSED(%0) \
	(((newkeys & (%0)) == (%0)) && ((oldkeys & (%0)) != (%0)))
	
#define KEY_AIM KEY_HANDBRAKE
#define HOLDING(%0) ((newkeys & (%0)) == (%0))

#define MAGYAR_NYELV 0
#define ANGOL_NYELV 1
#define NEMET_NYELV 2
#define OROSZ_NYELV 3
#define SPANYOL_NYELV 4
#define OLASZ_NYELV 5
#define JAPÁN_NYELV 6


//egyéni eljárások definiálása
forward timerekhez();
forward playertimer(playerid);
forward ajtokbe(playerid);
forward hazbabe(playerid);
forward KickPublic(playerid);
forward LoginPlayer(playerid,const password[]);
forward saveplayer(playerid);
forward ClearChatbox(playerid, lines);
forward jail();
forward ruhavalaszto(playerid);
forward OnVehicleLoseHealth(playerid, vehid, health);
forward ProxDetectorS(Float:radi, playerid, targetid);
forward PosLog(string[]);
forward IsVehicleOccupied(vehicleid);
forward hazpicnull(playerid);
forward Float:GetDistanceBetweenPoints(Float:rx1,Float:ry1,Float:rz1,Float:rx2,Float:ry2,Float:rz2);
forward IsPlayerCop(playerid);
forward kmszamlalo(vehicleid,playerid);
forward playervaltnull(playerid);
forward trafitimerr(playerid);
forward chats(Float:radi,playerid,string[],color,bool:nev);
forward GetClosestPlayer(p1);
forward Float:GetDistanceBetweenPlayers(p1,p2);
forward prlkv(playerid);
forward startmotor(playerid,vehicleid);
forward omszvizsgal(playerid,id);
forward elverzes(playerid);
forward omszlottellat(playerid,id);
forward otptimerek(playerid,vehicleid,tipus);
forward pszerel(playerid,vehicleid,listitem);
forward kocsivwn(vehicleid);
forward pmotorszerel(playerid,vehicleid);
forward atvjatekos(playerid,ids);
forward kamionbepakol(playerid);
forward kamionlepakol(playerid,hely);
forward GetClosestBenzinbiz(playerid);
forward tankoltimer(playerid,levonando,vehid,bool:joe);
forward funyirotimer(playerid);
forward GetPlayerSpeed(playerid, get3d);
forward AddItem(playerid,itemname[],itemammount);
forward bool:osszevonhato(itemname[]);
forward autoinditas(playerid);
forward novenyultet(playerid,noveny,mennyiseg);
forward novenyfejlodes(playerid,novenyid,novenyf,fejlodes);
forward dobozkipakol(playerid);
forward fizetes();
forward afaado(osszeg,&adoosszeg,&adozott);
forward createhulla(playerid,killerid,reason,Float:x,Float:y,Float:z,cvw);
forward destroyhulla(i);
forward LoadVehicle();
forward LoadHouse();
forward LoadDoor();
forward LoadPlayer(playerid);
forward LoadFrakcio(playerid);
forward Load3DLabel();
forward LoadServerPro();
forward PlayerScore(playerid);
forward LoadMoveObject();
forward	LoadLicens(playerid);
forward LoadBizz();
forward LoadRentHouse();
forward LoadRaktar();
forward LoadCeg();
forward LoadFriend(playerid);

//globális változók és eljárások
native WP_Hash(buffer[], len, const str[]);
native IsValidVehicle(vehicleid);

//globális változók
new MySQL:mysql;
new ruhatimer;
new tisztit[MAX_PLAYERS];
new ruhaskin[MAX_PLAYERS];
new kivalaszt[MAX_PLAYERS];
new loadhouse[MAX_HOUSE];
new Text3D:aszolglabel[MAX_PLAYERS];
new Float:tvpos[MAX_PLAYERS][3];
//new nevbe[MAX_PLAYERS];
new Pfreeze[MAX_PLAYERS];
new Float:markpos[MAX_PLAYERS][3];
new markpose[MAX_PLAYERS][2];
new ahelp[MAX_PLAYERS];
new aszolgban[MAX_PLAYERS];
new Float:anhp[MAX_PLAYERS];
new Float:vhealth[MAX_PLAYERS];
new Float:vhp;
new hazelad[MAX_PLAYERS];
new banksz[MAX_PLAYERS];
new bankj[MAX_PLAYERS];
new forcess[MAX_PLAYERS];
new bankssz[MAX_PLAYERS];
new pickupba[MAX_PLAYERS];
new admindia[MAX_PLAYERS];
new sikertelenbe[MAX_PLAYERS];
new telefonsz[MAX_PLAYERS];
new phivasba[MAX_PLAYERS];
new hivta[MAX_PLAYERS];
new akihivo[MAX_PLAYERS];
new hivo[MAX_PLAYERS];
new csorogsz[MAX_PLAYERS];
new smssz[MAX_PLAYERS];
new pcsomag[MAX_PLAYERS];
new berakcsomag[MAX_PLAYERS];
new bilincsben[MAX_PLAYERS];
new pvisz[MAX_PLAYERS];
new kivisz[MAX_PLAYERS];
new sokkolo[MAX_PLAYERS];
new sokkolop[MAX_PLAYERS];
new barat[MAX_PLAYERS][MAX_BARAT];
new betoltb[MAX_PLAYERS];
new chatfix[MAX_PLAYERS];
new bizelad[MAX_PLAYERS];
new aleaderk[MAX_PLAYERS];
new Text3D:global3d[MAX_3DTEXT_GLOBAL];
new bejelentes[MAX_PLAYERS];
new bejelentszam[MAX_BEJELENTESTIPUS];
new Text:sebessegmero[MAX_VEHICLES];
new Text:kmmero[MAX_VEHICLES];
new animban[MAX_PLAYERS];
new animtipus[MAX_PLAYERS];
new rkocsiid[MAX_PLAYERS];
new timerkmhez[MAX_PLAYERS];
new seanpanicszoba[2];
new fkradio[MAX_PLAYERS];
new ajtokbett[MAX_PLAYERS];
new gpanels[MAX_VEHICLES];
new gdoors[MAX_VEHICLES];
new glights[MAX_VEHICLES];
new gtires[MAX_VEHICLES];
new Float:ghealth[MAX_VEHICLES];
new utzar[MAX_UTZAR];
new pkesztyu[MAX_PLAYERS];
new weapons[MAX_PLAYERS][13][2];
new	bool:folyindit[MAX_PLAYERS];
new	bool:inditmen[MAX_PLAYERS];
new	bool:muholdban[MAX_PLAYERS];
new Float:muholdpoz[MAX_PLAYERS][3];
new serulestipus[MAX_PLAYERS];
new elverzest[MAX_PLAYERS];
new bool:lottellatva[MAX_PLAYERS];
new bool:lotttimerbe[MAX_PLAYERS];
new bool:altatva[MAX_PLAYERS];
new mutetfolyamat[MAX_PLAYERS];
new bool:uszivargas[MAX_VEHICLES];
new bool:beszorulva[MAX_PLAYERS];
new bool:tuzeset;
new bool:munkalapfel[MAX_PLAYERS];
new munkalapid[MAX_PLAYERS];
new bool:szerelesben[MAX_PLAYERS];
new tavtimer[MAX_PLAYERS];
new jrendelmodel[MAX_PLAYERS];
new jszin1[MAX_PLAYERS];
new jszin2[MAX_PLAYERS];
new	jfelajid[MAX_PLAYERS];
new jfelajossz[MAX_PLAYERS];
new jfelajtip[MAX_PLAYERS];
new bool:kfelpakolva[MAX_VEHICLES];
new create3dsz[MAX_PLAYERS][512];
new create3dc[MAX_PLAYERS];
new Float:create3dt[MAX_PLAYERS];
new bool:halalk[MAX_PLAYERS];
new bool:halalban[MAX_PLAYERS];
new atveszkocsid[MAX_PLAYERS];
new szerelkolts[MAX_PLAYERS];
new Float:AFA=25.0;
new Float:SZJA = 25.0;
new bool:felszerelesben[MAX_PLAYERS];
new bool:kreszv[MAX_PLAYERS][5][4];
new PlayerVizsgakerdes[MAX_PLAYERS];
new Playervizsgarnd[MAX_PLAYERS][11];
new kreszjovalasz[MAX_PLAYERS];
new kreszkilometer[MAX_PLAYERS];
new bool:kreszgyakorlasban[MAX_PLAYERS];
new bool:kreszvizsgaban[MAX_PLAYERS]; 
new kreszvcp[MAX_PLAYERS];
new PlayerBar:uzemanyagb[MAX_PLAYERS];
new playerbenzin[MAX_PLAYERS][2];
new bool:pizzamunka[MAX_PLAYERS];
new funyirofu[MAX_PLAYERS];
new funyirocpsz[MAX_PLAYERS];
new bool:funyirasban[MAX_PLAYERS];
new TskKiNev[MAX_PLAYERS][32];
new TskKiAmount[MAX_PLAYERS];
new favagoszamlalo[MAX_PLAYERS];
new bool:lancfuresz[MAX_PLAYERS];
new bool:pakolohely[MAX_VEHICLES][4];
new pakoloobj[MAX_VEHICLES][4];
new bool:dobozpakolhato;
new dobozspawnid;
new bool:elsoinditas;
new dobozido;
new dobozpakolasiido;
new bool:PlayerIsLoggedOn[MAX_PLAYERS];
new bool:csipogo[MAX_PLAYERS];
new adomodosit[MAX_PLAYERS];
new fizmodrang[MAX_PLAYERS];
new fizmodfrak[MAX_PLAYERS];
new Float:tmp_vkilometer[MAX_VEHICLES][3];
new bool:playerinbolt[MAX_VEHICLES];

//munka változók
new Float:tisztitocp[][] = {
{-2252.124,178.079,34.876},
{-2190.223,205.190,34.877},
{-2145.516,251.864,34.877},
{-2145.526,361.931,34.877},
{-2140.327,486.368,34.721},
{-2189.149,508.907,34.721},
{-2262.427,510.165,34.721},
{-2377.597,523.622,27.520},
{-2382.697,629.760,33.466},
{-2314.377,665.824,42.950},
{-2255.233,730.640,49.002},
{-2224.883,809.689,49.002},
{-2105.911,806.955,69.119},
{-2018.803,806.333,45.301},
{-2000.907,877.324,45.002},
{-2002.024,1010.644,52.371},
{-1926.890,1053.031,50.181},
{-1833.833,1100.281,44.994},
{-1716.481,1126.317,42.876},
{-1645.568,1189.490,7.033},
{-1608.484,1101.046,6.744},
{-1563.528,942.884,6.749},
{-1715.422,865.917,24.452},
{-1716.900,753.306,24.439},
{-1767.763,611.072,27.986},
{-1886.946,608.114,34.721},
{-2006.255,543.226,34.721},
{-2105.279,505.352,34.721},
{-2300.610,413.430,34.714},
{-2406.074,308.167,34.721},
{-2423.282,28.295,34.727},
{-2422.668,-44.739,34.877},
{-2259.659,-108.992,34.877}
};

new Float:funyirocp[][] = {
{-2299.517,139.855,34.890},
{-2308.939,138.398,34.892},
{-2321.568,135.519,34.892},
{-2332.207,129.762,34.892},
{-2342.590,120.615,34.892},
{-2348.000,109.970,34.892},
{-2351.504,100.179,34.892},
{-2352.406,87.779,34.892},
{-2349.440,85.150,34.892},
{-2348.046,94.385,34.892},
{-2344.308,106.482,34.892},
{-2337.474,117.325,34.892},
{-2332.091,123.253,34.892},
{-2323.444,129.497,34.892},
{-2314.959,133.122,34.892},
{-2301.473,133.808,34.892},
{-2300.013,129.737,34.884},
{-2307.387,127.564,34.892},
{-2318.535,122.634,34.892},
{-2330.498,114.464,34.892},
{-2338.709,104.362,34.892},
{-2342.002,96.354,34.892},
{-2341.559,87.602,34.892},
{-2336.604,86.606,34.892},
{-2335.037,93.936,34.892},
{-2331.887,102.711,34.892},
{-2326.391,112.175,34.892},
{-2317.346,116.525,34.892},
{-2309.633,118.470,34.892},
{-2301.367,117.679,34.884},
{-2300.314,110.886,34.890},
{-2300.215,103.449,34.892},
{-2301.734,92.698,34.892},
{-2309.039,86.759,34.892},
{-2317.977,87.044,34.892},
{-2328.916,86.736,34.892},
{-2338.260,86.788,34.892},
{-2348.029,86.664,34.892},
{-2351.570,91.964,34.892},
{-2344.117,92.085,34.892},
{-2339.214,92.139,34.892},
{-2331.618,92.977,34.892},
{-2317.240,95.391,34.890},
{-2310.111,101.127,34.892},
{-2313.820,105.197,34.892},
{-2318.537,100.228,34.892},
{-2299.494,139.811,34.892}
};

new Float:kreszvizsgacp[][]=
{
	{-2070.298,-67.477,34.749},
	{-2170.266,-67.524,34.750},
	{-2250.645,-35.534,34.750},
	{-2251.377,72.894,34.758},
	{-2250.753,182.774,34.748},
	{-2249.770,301.232,34.744},
	{-2214.253,318.716,34.748},
	{-2133.894,318.545,34.734},
	{-2043.723,318.353,34.594},
	{-2001.325,337.907,34.595},
	{-2000.155,465.699,34.592},
	{-2000.388,535.780,34.593},
	{-1999.939,630.993,36.619},
	{-2000.332,714.184,44.880},
	{-1964.266,728.657,44.873},
	{-1880.171,728.739,44.874},
	{-1815.873,728.768,35.991},
	{-1748.000,728.242,27.891},
	{-1719.124,679.476,24.312},
	{-1767.002,610.806,27.716},
	{-1864.322,610.690,34.592},
	{-1895.082,569.714,34.506},
	{-1843.213,439.903,16.691},
	{-1847.554,407.665,16.591},
	{-1845.833,376.507,16.862},
	{-1882.120,264.133,36.341},
	{-1900.691,78.993,37.749},
	{-1923.224,-186.791,37.820},
	{-1982.647,-305.384,35.731},
	{-2018.848,-308.586,35.007},
	{-2003.364,-192.643,35.288},
	{-2003.477,-99.830,35.171},
	{-2028.177,-68.441,34.751},
	{-2068.798,-83.864,34.742}
};
new Float:tuzcp[][3]=
{
	{-2009.0 ,150.0, 28.0}
};

new Float:benzinkutcp[][]=
{
	{-1664.568 ,416.591 ,7.179},
	{1669.735 ,411.618 ,7.398},
	{-1674.776 ,406.550 ,7.398},
	{-1678.687 ,402.630 ,7.398},
	{-1685.516 ,409.007 ,7.398},
	{-1681.256 ,413.268,7.398},
	{-1675.910 ,418.635,7.398},
	{-1671.649 ,422.898,7.398}
};

new benzinar[][]=
{
	{2,0,95,385},
	{2,0,98,390},
	{2,0,100,410},
	{2,1,0,395}
};

new ruhaskinek[3][1]={
{5},
{6},
{7}
};

new Float:vaghatofa[][]=
{
	{-1643.214, -2260.368, 32.452}
};

new Float:fegyverspawn[][]=
{
	{-1643.214, -2260.368, 32.452}
};

new munkanevek[][]=
{
	{"Nincs"},
	{"Úttisztító"},
	{"Fuvarozó"},
	{"Fegyvernepper"},
	{"Pizzafutár"},
	{"Fûnyíró"},
	{"Favágó"},
	{"Drogtermesztõ"}
};

new fufolyamat[][]=
{
	{"Ültetés"},
	{"Öntözés"},
	{"Párátlanítás"},
	{"Gondozása"},
	{"Melegítés"},
	{"Szedés"},
	{"Szárítás"},
	{"Feldolgozás"}
};

new fegyvernev[][]=
{
	{"Bokszer"},
	{"Golf ütõ"},
	{"Gumibot"},
	{"Kés"},
	{"Baseball ütõ"},
	{"Ásó"},
	{"Biliárd dákó"},
	{"Katana"},
	{"Láncfûrész"},
	{"Rózsaszin dildó"},
	{"Dildó"},
	{"Vibrátor"},
	{"Szürke vibrátor"},
	{"Virág"},
	{"Sétabot"},
	{"Gránát"},
	{"Könnygáz gránát"},
	{"Molotov-koktél"},
	{"-"},
	{"-"},
	{"-"},
	{"Pisztoly"},
	{"Tompított Pisztoly"},
	{"Desert Eagle"},
	{"Shotgun"},
	{"Fûrészelt Csövû Shotgun"},
	{"Combat Shotgun"},
	{"UZI"},
	{"MP5"},
	{"AK-47"},
	{"M4"},
	{"Tec-9"},
	{"Puska"},
	{"Sniper"},
	{"Rakétavetõ"},
	{"Hõkövetõ rakéta"},
	{"Lángszóró"},
	{"Minigun"},
	{"Bomba táska"},
	{"Detonátor"},
	{"Spray"},
	{"Poroltó"},
	{"Kamera"},
	{"Éjjelátó"},
	{"Hõkamera"},
	{"Ejtõernyõ"}

};

new Float:HotelSpawn[8][3] =
{
	{2198.9229,-1157.9158,1029.7969},
	{2188.6079,-1154.3534,1029.7969},
	{2203.0405,-1171.2780,1029.7969},
	{2204.6919,-1195.6965,1029.7969},
	{2224.0806,-1178.1775,1029.7969},
	{2234.7087,-1171.1752,1029.7969},
	{2249.4883,-1159.9591,1029.7969},
	{2233.9563,-1156.2252,1029.7969}
};

new Float:hinterid[][4]=
{
	{271.884979,306.631988,999.148437,2.0},//1
	{140.17,1366.07,1083.65,5.0},
	{2324.53,-1149.54,1050.71,12.0},
	{225.68,1021.45,1084.02,7.0},
	{234.19,1063.73,1084.21,6.0},
	{226.30,1114.24,1080.99,5.0},
	{235.34,1186.68,1080.26,3.0},
	{491.07,1398.50,1080.26,2.0},
	{24.04,1340.17,1084.38,10.0},
	{-283.44,1470.93,1084.38,15.0},
	{-260.49,1456.75,1084.37,4.0},
	{83.03,1322.28,1083.87,9.0},
	{2317.89,-1026.76,1050.22,9.0},
	{2495.98,-1692.08,1014.74,3.0},
	{2807.48,-1174.76,1025.57,8.0},
	{2196.85,-1204.25,1049.02,6.0},
	{377.15,1417.41,1081.33,15.0},
	{2270.38,-1210.35,1047.56,10.0},
	{446.99,1397.07,1084.30,2.0},
	{387.22,1471.70,1080.19,15.0},
	{22.88,1403.33,1084.44,5.0},
	{2365.31,-1135.60,1050.88,8.0},
	{2237.59,-1081.64,1049.02,2.0},
	{295.04,1472.26,1080.26,15.0},
	{261.12,1284.30,1080.26,4.0},
	{221.92,1140.20,1082.61,4.0},
	{-68.81,1351.21,1080.21,6.0},
	{260.85,1237.24,1084.26,9.0},
	{2468.84,-1698.24,1013.51,2.0},
	{223.20,1287.08,1082.14,1.0},
	{2283.04,-1140.28,1050.90,11.0},
	{328.05,1477.73,1084.44,15.0},
	{223.20,1287.08,1082.14,1.0},
	{-42.59,1405.47,1084.43,8.0},
	{446.90, 506.35, 1001.42, 12.0},//35
	{299.78,309.89,1003.30,4.0},
	{2308.77,-1212.94,1049.02,6.0},
	{2233.64,-1115.26,1050.88,5.0},
	{2218.40,-1076.18,1050.48,1.0},
	{243.72,304.91,999.15,1.0},
	{2259.38,-1135.77,1050.64,10.0}
};

/*new Float:binterid[][4]=
{
	{-25.884498,-185.868988,1003.546875,17.0},
	{6.091179,-29.271898,1003.549438,10.0},
	{-30.946699,-89.609596,1003.546875,18.0},
	{-25.132598,-139.066986,1003.546875,16.0},
	{-27.312299,-29.277599,1003.557250,4.0},
	{-26.691598,-55.714897,1003.546875,6.0},
	{286.148986,-40.644397,1001.515625,1.0},
	{286.800994,-82.547599,1001.515625,4.0},
	{296.919982,-108.071998,1001.515625,6.0},
	{314.820983,-141.431991,999.601562,7.0},
	{316.524993,-167.706985,999.593750,6.0},
	{833.269775,10.588416,1004.179687,3.0},
	{-103.559165,-24.225606,1000.718750,3.0},
	{207.737991,-109.019996,1005.132812,15.0},
	{204.332992,-166.694992,1000.523437,14.0},
	{207.054992,-138.804992,1003.507812,3.0},
	{203.777999,-48.492397,1001.804687,1.0},
	{226.293991,-7.431529,1002.210937,5.0},
	{161.391006,-93.159156,1001.804687,18.0},
	{493.390991,-22.722799,1000.679687,17.0},
	{501.980987,-69.150199,998.757812,11.0},
	{-227.027999,1401.229980,27.765625,18.0},
	{457.304748,-88.428497,999.554687,4.0},
	{454.973937,-110.104995,1000.077209,5.0}
};
*/
new vehName[][] =
{
    {"Landstalker"},
	{"Bravura"},
	{"Buffalo"},
	{"Linerunner"},
	{"Perennial"},
	{"Sentinel"},
	{"Dumper"},
	{"Firetruck"},
	{"Trashmaster"},
	{"Stretch"},
	{"Manana"},
	{"Infernus"},
	{"Voodoo"},
	{"Pony"},
	{"Mule"},
	{"Cheetah"},
	{"Ambulance"},
	{"Leviathan"},
	{"Moonbeam"},
	{"Esperanto"},
	{"Taxi"},
	{"Washington"},
	{"Bobcat"},
	{"Whoopee"},
	{"BFInjection"},
	{"Hunter"},
	{"Premier"},
	{"Enforcer"},
	{"Securicar"},
	{"Banshee"},
	{"Predator"},
	{"Bus"},
	{"Rhino"},
	{"Barracks"},
	{"Hotknife"},
	{"Trailer"},
	{"Previon"},
	{"Coach"},
	{"Cabbie"},
	{"Stallion"},
	{"Rumpo"},
	{"RCBandit"},
	{"Romero"},
	{"Packer"},
	{"Monster"},
	{"Admiral"},
	{"Squalo"},
	{"Seasparrow"},
	{"Pizzaboy"},
	{"Tram"},
	{"Trailer"},
	{"Turismo"},
	{"Speeder"},
	{"Reefer"},
	{"Tropic"},
	{"Flatbed"},
	{"Yankee"},
	{"Caddy"},
	{"Solair"},
	{"Berkley'sRCVan"},
	{"Skimmer"},
	{"PCJ600"},
	{"Faggio"},
	{"Freeway"},
	{"RCBaron"},
	{"RCRaider"},
	{"Glendale"},
	{"Oceanic"},
	{"Sanchez"},
	{"Sparrow"},
	{"Patriot"},
	{"Quad"},
	{"Coastguard"},
	{"Dinghy"},
	{"Hermes"},
	{"Sabre"},
	{"Rustler"},
	{"ZR350"},
	{"Walton"},
	{"Regina"},
	{"Comet"},
	{"BMX"},
	{"Burrito"},
	{"Camper"},
	{"Marquis"},
	{"Baggage"},
	{"Dozer"},
	{"Maverick"},
	{"NewsChopper"},
	{"Rancher"},
	{"FBIRancher"},
	{"Virgo"},
	{"Greenwood"},
	{"Jetmax"},
	{"Hotring"},
	{"Sandking"},
	{"BlistaCompact"},
	{"PoliceMaverick"},
	{"Boxvillde"},
	{"Benson"},
	{"Mesa"},
	{"RCGoblin"},
	{"HotringA"},
	{"HotringB"},
	{"Bloodring"},
	{"Rancher"},
	{"SuperGT"},
	{"Elegant"},
	{"Journey"},
	{"Bike"},
	{"Mountain"},
	{"Beagle"},
	{"Cropduster"},
	{"Stunt"},
	{"Tanker"},
	{"Roadtrain"},
	{"Nebula"},
	{"Majestic"},
	{"Buccaneer"},
	{"Shamal"},
	{"Hydra"},
	{"FCR900"},
	{"NRG500"},
	{"HPV1000"},
	{"Cement"},
	{"TowTruck"},
	{"Fortune"},
	{"Cadrona"},
	{"FBITruck"},
	{"Willard"},
	{"Forklift"},
	{"Tractor"},
	{"Combine"},
	{"Feltzer"},
	{"Remington"},
	{"Slamvan"},
	{"Blade"},
	{"Freight"},
	{"Streak"},
	{"Dodgem"},
	{"Vincent"},
	{"Bullet"},
	{"Clover"},
	{"Sadler"},
	{"Firetruck"},
	{"Hustler"},
	{"Intruder"},
	{"Primo"},
	{"Cargobob"},
	{"Tampa"},
	{"Sunrise"},
	{"Merit"},
	{"Utility"},
	{"Nevada"},
	{"Yosemite"},
	{"Windsor"},
	{"MonsterA"},
	{"MonsterB"},
	{"Uranus"},
	{"Jester"},
	{"Sultan"},
	{"Stratium"},
	{"Elegy"},
	{"Raindance"},
	{"RCTiger"},
	{"Flash"},
	{"Tahoma"},
	{"Savanna"},
	{"Bandito"},
	{"FreightFlat"},
	{"StreakCarriage"},
	{"Kart"},
	{"Mower"},
	{"Dune"},
	{"Sweeper"},
	{"Broadway"},
	{"Tornado"},
	{"AT400"},
	{"DFT30"},
	{"Huntley"},
	{"Stafford"},
	{"BF400"},
	{"NewsVan"},
	{"Tug"},
	{"PetrolTrailer"},
	{"Emperor"},
	{"Wayfarer"},
	{"Euros"},
	{"Hotdog"},
	{"Club"},
	{"FreightBox"},
	{"Trailer"},
	{"Andromada"},
	{"Dodo"},
	{"RCCam"},
	{"Launch"},
	{"LSPD"},
	{"SFPD"},
	{"LVPD"},
	{"Ranger"},
	{"Picador"},
	{"SWAT"},
	{"Alpha"},
	{"Phoenix"},
	{"Glendale"},
	{"Sadler"},
	{"Luggage"},
	{"Luggage"},
	{"Stairs"},
	{"Boxville"},
	{"Tiller"},
	{"UtilityTrailer"}
};

new vehdata[][] =
{
/*Landstalker*/		{65,10,3500000},
/*Bravura*/			{45,9,2500000},
/*Buffalo*/			{55,15,8000000},
/*Linerunner*/		{400,30,100000000},
/*Perennial*/		{45,7,800000},
/*Sentinel*/		{65,10,6500000},
/*Dumper*/			{1000,80,0},
/*Firetruck*/		{250,28,0},
/*Trashmaster*/		{200,25,0},
/*Stretch*/			{65,10,45000000},
/*Manana*/			{50,7,3000000},
/*Infernus*/		{55,15,35000000},
/*Voodoo*/			{65,10,4500000},
/*Pony*/			{60,10,3000000},
/*Mule*/			{80,10,2500000},
/*Cheetah*/			{70,18,65000000},
/*Ambulance*/		{90,10,0},
/*Leviathan*/		{800,90,550000000},
/*Moonbeam*/		{65,9,3000000},
/*Esperanto*/		{60,10,7000000},
/*Taxi*/			{55,9,0},
/*Washington*/		{70,10,25000000},
/*Bobcat*/			{55,10,4000000},
/*Whoopee*/			{80,8,0},
/*BFInjection*/		{45,12,3500000},
/*Hunter*/			{650,70,0},
/*Premier*/			{55,9,4500000},
/*Enforcer*/		{130,20,0},
/*Securicar*/		{100,18,0},
/*Banshee*/			{60,15,20000000},
/*Predator*/		{300,35,0},
/*Bus*/				{140,22,0},
/*Rhino*/			{500,100,0},
/*Barracks*/		{300,35,75000000},
/*Hotknife*/		{60,15,35000000},
/*Trailer*/			{1,1,0},
/*Previon*/			{50,7,3000000},
/*Coach*/			{180,25,0},
/*Cabbie*/			{65,9,0},
/*Stallion*/		{55,10,4500000},
/*Rumpo*/			{70,9,3000000},
/*RCBandit*/		{20,10,0},
/*Romero*/			{65,10,0},
/*Packer*/			{250,25,0},
/*Monster*/			{80,30,0},
/*Admiral*/			{60,9,5500000},
/*Squalo*/			{180,35,90000000},
/*Seasparrow*/		{150,35,0},
/*Pizzaboy*/		{6,3,0},
/*Tram*/			{1,1,0},
/*Trailer*/			{1,1,0},
/*Turismo*/			{70,15,120000000},
/*Speeder*/			{120,35,100000000},
/*Reefer*/			{150,35,78000000},
/*Tropic*/			{150,35,130000000},
/*Flatbed*/			{250,30,75000000},
/*Yankee*/			{100,15,8000000},
/*Caddy*/			{20,8,0},
/*Solair*/			{55,7,4000000},
/*Berkley'sRCVan*/	{70,10,6000000},
/*Skimmer*/			{120,25,120000000},
/*PCJ600*/			{14,4,2800000},
/*Faggio*/			{6,3,350000},
/*Freeway*/			{13,4,3000000},
/*RCBaron*/			{20,10,0},
/*RCRaider*/		{20,10,0},
/*Glendale*/		{60,9,900000},
/*Oceanic*/			{60,9,3850000},
/*Sanchez*/			{12,4,1820000},
/*Sparrow*/			{130,35,75000000},
/*Patriot*/			{75,15,45000000},
/*Quad*/			{18,5,1520000},
/*Coastguard*/		{130,30,0},
/*Dinghy*/			{100,25,250000},
/*Hermes*/			{55,9,5800000},
/*Sabre*/			{70,12,8750000},
/*Rustler*/			{80,25,110000000},
/*ZR350*/			{55,9,5800000},
/*Walton*/			{65,10,2600000},
/*Regina*/			{60,7,3000000},
/*Comet*/			{70,15,36500000},
/*BMX*/				{0,0,0},
/*Burrito*/			{60,10,8500000},
/*Camper*/			{55,9,3200000},
/*Marquis*/			{100,25,130000000},
/*Baggage*/			{30,8,0},
/*Dozer*/			{100,20,0},
/*Maverick*/		{400,70,250000000},
/*NewsChopper*/		{400,70,0},
/*Rancher*/			{65,10,10000000},
/*FBIRancher*/		{65,10,0},
/*Virgo*/			{65,10,6500000},
/*Greenwood*/		{60,8,3000000},
/*Jetmax*/			{120,30,180000000},
/*Hotring*/			{110,25,500000000},
/*Sandking*/		{80,15,450000000},
/*BlistaCompact*/	{50,8,4500000},
/*PoliceMaverick*/	{400,70,0},
/*Boxvillde*/		{55,7,2500000},
/*Benson*/			{80,10,3000000},
/*Mesa*/			{60,9,5400000},
/*RCGoblin*/		{20,10,0},
/*HotringA*/		{110,25,500000000},
/*HotringB*/		{110,25,500000000},
/*Bloodring*/		{65,10,28000000},
/*Rancher*/			{65,10,10000000},
/*SuperGT*/			{70,20,140000000},
/*Elegant*/			{60,8,16000000},
/*Journey*/			{90,10,26000000},
/*Bike*/			{0,0,0},
/*Mountain*/		{0,0,0},
/*Beagle*/			{150,30,110000000},
/*Cropduster*/		{90,20,78000000},
/*Stunt*/			{90,20,250000000},
/*Tanker*/			{350,35,120000000},
/*Roadtrain*/		{200,35,150000000},
/*Nebula*/			{60,8,5800000},
/*Majestic*/		{60,8,4200000},
/*Buccaneer*/		{65,10,8700000},
/*Shamal*/			{600,40,750000000},
/*Hydra*/			{650,50,0},
/*FCR900*/			{15,5,3500000},
/*NRG500*/			{18,5,85000000},
/*HPV1000*/			{18,5,0},
/*Cement*/			{250,35,0},
/*TowTruck*/		{90,15,0},
/*Fortune*/			{60,7,9500000},
/*Cadrona*/			{55,7,5500000},
/*FBITruck*/		{90,20,0},
/*Willard*/			{65,8,5000000},
/*Forklift*/		{10,5,0},
/*Tractor*/			{40,10,0},
/*Combine*/			{100,20,0},
/*Feltzer*/			{60,10,11000000},
/*Remington*/		{75,15,30000000},
/*Slamvan*/			{75,20,42500000},
/*Blade*/			{70,10,12000000},
/*Freight*/			{800,100,0},
/*Streak*/			{800,100,0},
/*Dodgem*/			{50,10,0},
/*Vincent*/			{65,9,4200000},
/*Bullet*/			{70,30,130000000},
/*Clover*/			{60,12,1800000},
/*Sadler*/			{65,9,2800000},
/*Firetruck*/		{120,30,0},
/*Hustler*/			{60,15,33100000},
/*Intruder*/		{55,7,4600000},
/*Primo*/			{60,8,2650000},
/*Cargobob*/		{500,80,500000000},
/*Tampa*/			{55,9,850000},
/*Sunrise*/			{65,9,3200000},
/*Merit*/			{65,8,4800000},
/*Utility*/			{90,15,0},
/*Nevada*/			{1200,100,550000000},
/*Yosemite*/		{70,10,18000000},
/*Windsor*/			{70,10,45000000},
/*MonsterA*/		{120,50,0},
/*MonsterB*/		{120,50,0},
/*Uranus*/			{65,10,6500000},
/*Jester*/			{65,10,12000000},
/*Sultan*/			{65,15,25000000},
/*Stratium*/		{70,9,15000000},
/*Elegy*/			{65,10,25000000},
/*Raindance*/		{500,90,0},
/*RCTiger*/			{20,10,0},
/*Flash*/			{60,8,15000000},
/*Tahoma*/			{55,9,5500000},
/*Savanna*/			{60,10,7800000},
/*Bandito*/			{40,10,6800000},
/*FreightFlat*/		{0,0,0},
/*StreakCarriage*/	{0,0,0},
/*Kart*/			{5,5,0},
/*Mower*/			{5,5,0},
/*Dune*/			{80,10,0},
/*Sweeper*/			{40,9,0},
/*Broadway*/		{65,8,7900000},
/*Tornado*/			{65,15,4800000},
/*AT400*/			{3000,500,1500000000},
/*DFT30*/			{120,30,45000000},
/*Huntley*/			{65,10,28000000},
/*Stafford*/		{75,10,85000000},
/*BF400*/			{15,5,2300000},
/*NewsVan*/			{75,10,0},
/*Tug*/				{20,5,0},
/*PetrolTrailer*/	{0,0,0},
/*Emperor*/			{65,8,5800000},
/*Wayfarer*/		{15,5,5300000},
/*Euros*/			{65,9,13500000},
/*Hotdog*/			{70,8,0},
/*Club*/			{65,8,12000000},
/*FreightBox*/		{0,0,0},
/*Trailer*/			{0,0,0},
/*Andromada*/		{5000,500,3000000000},
/*Dodo*/			{100,35,145000000},
/*RCCam*/			{0,0,0},
/*Launch*/			{100,30,150500000},
/*LSPD*/			{55,10,0},
/*SFPD*/			{55,10,0},
/*LVPD*/			{55,10,0},
/*Ranger*/			{65,13,0},
/*Picador*/			{70,10,3200000},
/*SWAT*/			{100,25,0},
/*Alpha*/			{65,10,10000000},
/*Phoenix*/			{60,15,43500000},
/*Glendale*/		{65,10,960000},
/*Sadler*/			{65,10,1235000},
/*Luggage*/			{0,0,0},
/*Luggage*/			{0,0,0},
/*Stairs*/			{0,0,0},
/*Boxville*/		{55,7,3300000},
/*Tiller*/			{0,0,0},
/*UtilityTrailer*/	{0,0,0}
};

new kerdesA[][][] =
{
	{"Az alábbiak közül melyik a helyes megfigyelési technika?","Felváltva figyeljük az út jobb és bal oldalát.\nTekintetünket igyekezzünk az úton közvetlenül a gépjármû elé irányítani.\nTekintetünket vezessük végig azon a pályán, amelyen haladni szeretnénk.","2"},
	{"Az alábbiak közül mikor válik az úttest legveszélyesebben csúszóssá?","Amikor az úttestet az esõvíz már jó ideje áztatja.\nAmikor az esõ elered, mert a víz az úttestet borító porral nyálkás réteget alkot.\nAmikor az esõvíz az úttestrõl a port jól lemosta.","1"},
	{"Segédmotoros kerékpárral közlekedik.Használhat-e kézben tartva mobil mobilt?","Nem.\nCsak ha forgalmi okból megállásra kényszerül.\nIgen.","0"},
	{"Mi teszi lehetõvé a teljes környezet megfigyelését?","A látótér.\nA látás és a visszapillantó tükrök.\nTekintenünk (figyelmünk) vándorlása.","2"},
	{"Hányszoros lesz a fékút jeges úton - száraz úthoz hasonlítva?","Kb. kétszeres.                                                                    .\nkb. háromszoros.\nkb. hatszoros","2"},
	{"Elindulhat-e olyan gépjármûvel, amelynek elromlott az egyik féklámpája?","Igen.\nCsak kis forgalmú idõszakban és mellékútvonalon.\nNem.","2"},
	{"Elsõbbsége van-e a borostyán-sárga villogó használó jármûnek?","Igen.                                                                    .\nNem","1"},
	{"Mit kell figyelembe vennie a kötelezõen elõírt bukósisak használatánál?","Bármilyen bukósisak becsatolás nélkül is védelmet nyújt.\nA fejméretnek megfelelõen legyen beállítva, és jól legyen becsatolva.\nNem fontos a fejmérethez való beállítás, de jól legyen becsatolva.","1"},
	{"Erõteljes fékezéskor a motorkerékpár melyik kereke hajlamos a megcsúszásra?","A hátsó kerék, mert terhelése átmenetileg csökken.\nA megcsúszási hajlam mindkét keréknél egyforma.\nAz elsõ kerék, mert terhelése átmenetileg menõ.","0"},
	{"Hogyan fékezi motorkerékpárját csúszós úton?","Csak kézifékkel, óvatosan fékezve.\nMotorfékkel, mindkét fékkel egyszerre, óvatosan fékezve, megcsúszás határáig.\nCsak lábfékkel, gyengén fékezve.","1"},
	{"Kötelezõ-e gépkocsijában készenlétben tartani hóláncot?","Nem.\nIgen.\nCsak december 15-e és február 28-a között.","0"},
	{"Ha Ön forgalmi sávot változtat, akkor?","az egyenesen haladó jármûveket egyáltalán nem zavarhatja.\naz egyenesen haladó jármûvek részére köteles elsõbbséget adni.","1"},
	{"Mekkora átlagsebesség kell, ha vasúti átjárón halad keresztül?","Legalább 5 km/h.                                                                    .\nLegalább 20 km/h.\nLegalább 10 km/h.","0"},
	{"Kell-e irányjelzést adni másik útra történõ bekanyarodáskor?","Igen, kivéve a körforgalomba való behajtást.\nNem.\nIgen, minden esetben.","0"},
	{"Mit kell tekinteni a jármû kivilágítása szempontjából éjszakának?","Azt az idõszakot, amikor az utcai közvilágítást bekapcsolják.\nAz esti szürkület kezdete a reggeli szürkület megszûnése, terjedõ idõszakot.\nAz erõs esti szürkülettõl a reggeli szürkület kezdetéig terjedõ idõszakot.","1"},
	{"Szabad-e hátramenetet végezni kijelölt gyalogos-átkelõhelyen?","Nem.                                                                    .\nIgen.","1"},
	{"Mekkora a személygépkocsik megengedett legnagyobb sebessége?","Lakott terület 50km/h, autópálya és autóút 100km/h, egyéb úton 80km/h.\nLakott terület 50km/h,autópálya 130km/h,autóút 110km/h, egyéb úton 90km/h.\nLakott terület 50km/h,autópálya 120km/h, lakott területen kívül 80km/h.","1"},
	{"Bukósisakjának a csatja elszakadt. Folytathatja-e az útját a motorkerékpárjával?","Nem.\nIgen, de csak az elsõ olyan helyig, ahol a hibát kijavíthatja.","0"},
	{"Szabad-e a motorkerékpár motorját zárt helyiségben huzamosabb ideig mûködtetni?","Nem,kipufogógázban lévõ szén-monoxid belélegezve fejfájást, majd halált okoz.\nIgen, mert a kipufogógázban nincsenek mérgezõ anyagok.","0"},
	{"Milyen idõközönként kell ellenõrizni a motorkerékpár világító és jelzõberendezéseit?","Naponta, elindulás elõtt.\nHetenként elegendõ, az ápolási munkák során.\nHavonta elegendõ, a karbantartás során.","0"}
};

//lokális player kocsik stb változók vagy állandók	
enum pInfo
{
	Admin,
	Score,
	kivalasztott,
	skin,
	Money,
	dbid,
	inhouse,
	Float:oldx,
	Float:oldy,
	Float:oldz,
	Float:inx,
	Float:iny,
	Float:inz,
	szuletes[128],
	neme[128],
	allamp[128],
	okm[128],
	frakcio,
	rang,
	pfrakcio[128],
	prang[128],
	pleader,
	pconnecttime,
	pszolg,
	fksin,
	pmunka,
	pborton,
	btipus,
	elfogad,
	felajan,
	kival[128],
	iitems[128],
	iamounts,
	pfizetes,
	felfiz,
	fiztipus,
	level,
	test[128],
	phazas[128],
	preg[128],
	kithivsz[128],
	kulcs[128],
	ftipus,
	phfrakcio[128],
	alosztaly,
	ado,
	paleader,
	bool:trafipaxo,
	Float:trx,
	Float:try,
	Float:trz,
	trafilim,
	trafitimer,
	prlk[128],
	cegleader,
	pceg,
	Float:ptchm,
	pakolhat,
	jsorszam[16],
	jogosit[16],
	maonline
};

new PlayerInfo[MAX_PLAYERS][pInfo];

enum cInfo
{
	Id,
	Owner,
	Model,
	Float:PosX,
	Float:PosY,
	Float:PosZ,
	Float:PosFace,
	Color1,
	Color2,
	Plate[11],
	dbid,
	Ownername[64],
	cid,
	vmunka,
	vfrakcio,
	Float:megkilometer,
	Float:uzemanyag,
	uzemanyagt,
	lefoglalva,
	lefoglaltido,
	ktartozas,
	vceg,
	bool:Vvan,
	bool:tankolasban,
	forgalmi
};

new VehicleInfo[MAX_VEHICLES][cInfo];

enum phouse
{
	Id,
	Float:px,
	Float:py,
	Float:pz,
	Owner,
	Cost,
	vw,
	intid,
	Ownername[128],
	Float:enterx,
	Float:entery,
	Float:enterz,
	interior,
	housepick,
	nyitzar,
	hvw,
	bool:hvan,
	Text3D:houselabel
};
new HouseInfo[MAX_HOUSE][phouse];

enum jjato
{
	Id,
	Float:ax,
	Float:ay,
	Float:az,
	Float:fx,
	Float:fy,
	Float:fz,
	Float:face1,
	Float:face2,
	inter,
	virt
};
new AjtoInfo[MAX_AJTO][jjato];

enum pbizz
{
	Id,
	Float:px,
	Float:py,
	Float:pz,
	Owner,
	Cost,
	bvw,
	intid,
	Ownername[128],
	bizname[128],
	btipus,
	Float:enterx,
	Float:entery,
	Float:enterz,
	interior,
	bizzpick,
	kasszapw,
	mapicon,
	Text3D:bizlabel,
	kassza,
	bool:bvan
};
new BizzInfo[MAX_BIZZ][pbizz];

enum bejelent
{
	Id,
	Float:bx,
	Float:by,
	Float:bz
}
new BejelentesInfo[MAX_BEJELENTESTIPUS][MAX_BEJELENTESEK][bejelent];

enum tuzes
{
	ID,
	bool:ures,
	THP,
	OID,
	Float:TX,
	Float:TY,
	Float:TZ
}
new TuzInfo[MAX_TUZ][tuzes];

enum kinfo
{
	dbid,
	model,
	Float:kx,
	Float:ky,
	Float:kz,
	Float:rkx,
	Float:rky,
	Float:rkz,
	kmunka,
	kfrakcio,
	bool:rnyitva,
	kid,
	Float:knx,
	Float:kny,
	Float:knz
}
new KapuInfo[MAX_MOVEOB][kinfo];

enum rinfo
{
	dbid,
	owner,
	ownername[64],
	rcost,
	rido,
	rzarvar,
	Float:rx,
	Float:ry,
	Float:rz,
	rvw,
	pickid,
	bool:rvan,
	Text3D:rlabel
}
new RentInfo[MAX_RENTH][rinfo];

enum ceinfo
{
	dbid,
	cnev[64],
	cowner,
	cownername[64],
	ccost,
	Float:cex,
	Float:cey,
	Float:cez,
	cegpick
}
new CegInfo[MAX_CEG][ceinfo];

enum rainfo
{
	dbid,
	owner,
	Float:rax,
	Float:ray,
	Float:raz,
	ar,
	jelszo,
	fajta,
	bool:ontozorendszer,
	bool:paratlanito,
	bool:parasito,
	benzin,
	Text3D:ralabel,
	rapickupid,
	bool:rkvan
	
}
new RaktarInfo[MAX_RAKTAR][rainfo];

enum Hhulla
{
	hobject[6],
	bool:hvan,
	aldozat[32],
	tettes[32],
	halalok[128],
	huido,
	htimer
}
new HullaInfo[MAX_HULLA][Hhulla];

main( ) { }
public OnGameModeInit()
{
	tuzeset = false;
	elsoinditas = true;
	//ShowPlayerMarkers(2);
	ShowNameTags(true);
	mysql = mysql_connect("192.168.10.2","samp","","samp");
	mysql_log(ERROR | WARNING);
	mysql_set_charset("latin2");
	mysql_tquery(mysql,"SET NAMES latin2");
	
	mysql_tquery(mysql,"SELECT * FROM `serverp`","LoadServerPro");
	mysql_tquery(mysql,"SELECT * FROM `vehicle`","LoadVehicle");
	mysql_tquery(mysql,"SELECT * FROM `house`","LoadHouse");
	mysql_tquery(mysql,"SELECT * FROM `biznisz`","LoadBizz");
	mysql_tquery(mysql,"SELECT * FROM `3dtextlabel`","Load3DLabel");
	mysql_tquery(mysql,"SELECT * FROM `ajtok`","LoadDoor");
	mysql_tquery(mysql,"SELECT * FROM `kapuk`","LoadMoveObject");
	mysql_tquery(mysql,"SELECT * FROM `renthouse`","LoadRentHouse");
	mysql_tquery(mysql,"SELECT * FROM `ceg`","LoadCeg");
	mysql_tquery(mysql,"SELECT * FROM `raktar`","LoadRaktar");
	
	UsePlayerPedAnims();
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(0);
	Audio_SetPack("default_pack",true);
	ManualVehicleEngineAndLights();
	SetTimer("jail", 1000, 1);
	SetTimer("timerekhez",1000,1);
	for(new i; i< MAX_VEHICLES;i++)
	{
		ghealth[i] = 1000.0;
	}
	SendRconCommand("reloadfs objekt");
	//AddStaticVehicleEx(537,1695.2631,-1953.6426,14.8756,89.9202,1,1, 120);
	return 1;
}
public OnGameModeExit()
{
	savecar();
	savehouse();
	saverenth();
	saveceg();
	mysql_close();
	Audio_DestroyTCPServer();
	return 1;
}

public OnPlayerRequestClass( playerid, classid )
{
	SetPlayerColor(playerid,-1);
	if(sikertelenbe[playerid] ==0)
	{
		SetTimerEx("playertimer",1000,1,"u",playerid);
		SetSpawnInfo(playerid, 0, 0, 1958.3783, 1343.1572, 15.3746, 269.1425, 0, 0, 0, 0, 0, 0);
		SpawnPlayer(playerid);
		if(PlayerInfo[playerid][pborton] > 0)
		{
			SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
			bortonbe(playerid);
		}
		else	
		{	
			SetTimerEx("hazbabe",500,false,"u",playerid);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}

public hazbabe(playerid)
{
	SetPlayerPos(playerid,1958.3783, 1343.1572, 15.3746);
	new hid = 0;
	new i = 0;
	new bool:talal = false;
	while(hid < MAX_HOUSE && talal==false)
	{
		if(HouseInfo[hid][Owner] == PlayerInfo[playerid][dbid])
		{
			talal= true;
		}
		else
		{
			talal= false;
			hid++;
		}
	}
	if(talal == true)
	{
		SetPlayerPos(playerid, HouseInfo[hid][enterx],HouseInfo[hid][entery],HouseInfo[hid][enterz]);
		SetPlayerVirtualWorld(playerid, HouseInfo[hid][hvw]);
		SetPlayerInterior(playerid, HouseInfo[hid][interior]);
		SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
		Streamer_UpdateEx(playerid,HouseInfo[hid][enterx],HouseInfo[hid][entery],HouseInfo[hid][enterz],GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));
		SendClientMessage(playerid,-1, "(( Otthon ébredtél! ))");
	}
	else
	{
		while(i < MAX_RENTH && talal==false)
		{
			if(RentInfo[i][owner] == PlayerInfo[playerid][dbid])
			{
				talal= true;
			}
			else
			{
				talal= false;
				i++;
			}
		}
		if(talal == true)
		{
			SetPlayerPos(playerid,266.500, 305.075, 999.148);
			SetPlayerInterior(playerid,2);
			SetPlayerFacingAngle(playerid,270.256);
			SetPlayerVirtualWorld(playerid,i);
			SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
			SendClientMessage(playerid,-1, "(( Otthon ébredtél! ))");
		}
		else
		{
			new id = random(sizeof(HotelSpawn));
			SetPlayerPos(playerid, HotelSpawn[id][0],HotelSpawn[id][1],HotelSpawn[id][2]);
			SetPlayerInterior(playerid,15);
			SetPlayerVirtualWorld(playerid,0);
			SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
			SendClientMessage(playerid,-1, "(( Hajléktalan szállóban ébredtél! ))");
		}
	}
}
public OnPlayerConnect(playerid)
{
	PlayerIsLoggedOn[playerid] = false;
	TogglePlayerSpectating(playerid, true);
	removeobj(playerid);
	playervaltnull(playerid);
	new ip[256];
	new Query[512],pName[24];
	new rowc;
	GetPlayerName(playerid,pName,24);
	format(Query,sizeof(Query),"SELECT `Name` FROM `user` WHERE `Name` = '%s' LIMIT 1;",pName);
	new Cache:result = mysql_query(mysql,Query);
	if(cache_get_row_count(rowc))
	{
		ShowPlayerDialog(playerid, 2, DIALOG_STYLE_PASSWORD, "Belépés", "Írd be a jelszavad!", "Rendben", "Mégse");
	}
	else
	{
		KickWithMessage(playerid, COLOR_RED, "NEM VAGY BEREGISZTRÁLVA!");
	}
	cache_delete(result);
	GetPlayerIp(playerid, ip, sizeof(ip));
	tisztit[playerid] = 0;
	new sql[256],string[128];
	new year,mounth,day,hour,minute,second;
	getdate(year, mounth, day);
	gettime(hour,minute,second);
	format(string,sizeof(string),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
	mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `connectlog`(`ido`, `nev`, `tipus`) VALUES ('%s','%s','felcsatlakozott')",string,Nev(playerid));
	result = mysql_query(mysql,Query);
	cache_delete(result);
	SetPlayerColor(playerid,-1);
	animban[playerid] = 0;
	animtipus[playerid] = 0;
	SetPlayerSkillLevel(playerid,WEAPONSKILL_PISTOL,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_PISTOL_SILENCED,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_DESERT_EAGLE,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_SHOTGUN,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_SAWNOFF_SHOTGUN,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_SPAS12_SHOTGUN,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_MICRO_UZI,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_MP5,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_AK47,999);
	SetPlayerSkillLevel(playerid,WEAPONSKILL_M4,999);
	return 1;
}

public OnPlayerDisconnect(playerid, reason)
{
	SetPlayerColor(playerid,-1);
	saveplayer(playerid);
	jogostivanymentes(playerid);
	new ip[130];
	new string[128];
	GetPlayerIp(playerid, ip, sizeof(ip));
	switch(reason)
	{
		case 0: format(string,sizeof(string),"(( %s crashelt ))",Nev(playerid));
		case 1: format(string,sizeof(string),"(( %s kilépett ))",Nev(playerid));
		case 2: format(string,sizeof(string),"(( %s kickelve ))",Nev(playerid));
	}
	chats(10,playerid,string,COLOR_CHAT1,false);
	new sql[256],string3[128];
	new year,mounth,day,hour,minute,second;
	getdate(year, mounth, day);
	gettime(hour,minute,second);
	format(string3,sizeof(string3),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
	mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `connectlog`(`ido`, `nev`, `tipus`) VALUES ('%s','%s','lecsatlakozott')",string3,Nev(playerid));
	new Cache:res = mysql_query(mysql,sql);
	cache_delete(res);
	KillTimer(ajtokbett[playerid]);
	PlayerIsLoggedOn[playerid] = false;
	return 1;
}


public OnPlayerSpawn(playerid)
{
	SetPlayerColor(playerid,-1);
	anhp[playerid] = 100;
	if(PlayerInfo[playerid][pborton] > 0 && PlayerInfo[playerid][btipus] == 5)
	{
		bortonbe(playerid);
		SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
	}
	SetPlayerColor(playerid,-1);
	for(new i=0;i<MAX_PLAYERS;i++)
	{
		SetPlayerMarkerForPlayer(i,playerid,0xFFFFFF00);
	}
	PlayerIsLoggedOn[playerid] = true;
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	SetPlayerColor(playerid,-1);
	animban[playerid] = 0;
	animtipus[playerid] = 0;
	new string[128];
	new wep[120];
	GetWeaponName(reason, wep, sizeof(wep));
	if(IsPlayerConnected(killerid))
	{
		format(string, sizeof(string), "SCD: %s megölte %s ezzel: %s", Nev(killerid), Nev(playerid), wep);
	}
	else
	{
		format(string, sizeof(string), "SCD: Rendszer megölte %s ezzel: %s", Nev(playerid), wep);
	}
	SendToAdmins(COLOR_RED, string);
	PlayerInfo[playerid][pszolg] = 0;
	sokkolo[playerid] =0;
	sokkolop[playerid] =0;
	KillTimer(elverzest[playerid]);
	lottellatva[playerid] = false;
	lotttimerbe[playerid] = false;
	beszorulva[playerid] = false;
	if(reason == WEAPON_VEHICLE)
	{
		PlayerInfo[playerid][pborton] = 180;
		PlayerInfo[playerid][btipus] =5;
	}
	else if(killerid != INVALID_PLAYER_ID)
	{
		PlayerInfo[playerid][pborton] = 300;
		PlayerInfo[playerid][btipus] =5;
	}
	else
	{
		PlayerInfo[playerid][pborton] = 60;
		PlayerInfo[playerid][btipus] =5;
	}
	halalban[playerid] = false;
	halalk[playerid] = false;	
	
	new Float:dx,Float:dy,Float:dz;
	GetPlayerPos(playerid,dx,dy,dz);
	createhulla(playerid,killerid,reason,dx,dy,dz,GetPlayerVirtualWorld(playerid));
	
	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	UpdateVehicleDamageStatus(vehicleid,gpanels[vehicleid],gdoors[vehicleid],glights[vehicleid],gtires[vehicleid]);
	SetVehicleHealth(vehicleid,ghealth[vehicleid]);
	vscarresp(vehicleid);
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	GetVehicleDamageStatus(vehicleid,gpanels[vehicleid],gdoors[vehicleid],glights[vehicleid],gtires[vehicleid]);
	GetVehicleHealth(vehicleid,ghealth[vehicleid]);
	return 1;
}

public OnPlayerText(playerid,text[])
{
	if(isnull(text))
	return 0;
	new string[128];
	new sendername[256];
	GetPlayerName(playerid, sendername, sizeof(sendername));
	str_replace(sendername, '_', ' ');
	if(aszolgban[playerid] == 1)
	{
		format(string, sizeof(string), "(( %s mondja: %s ))", Nev(playerid),text);
		SetPlayerChatBubble(playerid, text, COLOR_WHITE, 20.0, 8000);
		chats(20,playerid,string,COLOR_CHAT1,false);
	}
	if(strfind(text, ":)", true) != -1)
	{
		Cselekves(playerid,"mosolyog.");
	}
	if(IsPlayerConnected(playerid)&& aszolgban[playerid] == 0)
	{
		format(string, sizeof(string), ": %s", text);
		SetPlayerChatBubble(playerid, text, COLOR_WHITE, 20.0, 8000);
		chats(20,playerid,string,COLOR_CHAT1,true);
	}
	if(phivasba[playerid] ==1)
	{
		if(hivta[playerid] ==1)
		{
			format(string,sizeof(string),"%s telefonba: %s",Nev(playerid),text);
			SendClientMessage(PlayerInfo[playerid][kithivsz],-1,string);
		}
		else
		{
			format(string,sizeof(string),"%s telefonba: %s",Nev(playerid),text);
			SendClientMessage(PlayerInfo[playerid][kithivsz],-1,string);
		}
	}
	if(phivasba[playerid] ==1 && bejelentes[playerid] ==1)
	{
		new Float:posz[3];
		GetPlayerPos(playerid,posz[0],posz[1],posz[2]);
		bejelentszam[1] +=1;
		BejelentesInfo[1][bejelentszam[1]][Id] = bejelentszam[1];
		BejelentesInfo[1][bejelentszam[1]][bx] = posz[0];
		BejelentesInfo[1][bejelentszam[1]][by] = posz[1];
		BejelentesInfo[1][bejelentszam[1]][bz] = posz[2];
		SendClientMessage(playerid,-1,"Bejelentését rögzítettük!");
		format(string,sizeof(string),"Diszpécser: Bejelentes: %s\n szám: %d",text,bejelentszam[1]);
		for(new i;i<MAX_PLAYERS;i++)
		{
			if(IsPlayerCop(i) && PlayerInfo[i][pszolg] ==1)
			{
				SendClientMessage(i,-1,string);
			}
		}
		phivasba[playerid] =0;
	}
	new sql[256],string2[128];
	new year,mounth,day,hour,minute,second;
	getdate(year, mounth, day);
	gettime(hour,minute,second);
	format(string2,sizeof(string2),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
	mysql_format(mysql,sql,sizeof(sql), "INSERT INTO `chatlog`(`ido`, `nev`, `chat`) VALUES ('%s','%s','%s')",string2,Nev(playerid),text);
	new Cache:res = mysql_query(mysql,sql);
	cache_delete(res);
	return 0;
}

//parancsok

CMD:tankol(playerid,params[])
{
	if(!IsPlayerInAnyVehicle(playerid))
	{
		new i=0;
		new bool:talal = false;
		while(talal == false && i < sizeof(benzinkutcp))
		{
			if(IsPlayerInRangeOfPoint(playerid,4.0,benzinkutcp[i][0],benzinkutcp[i][1],benzinkutcp[i][2]))
			{
				talal = true;
			}
			else
			{
				talal = false;
				i++;
			}
		}
		if(talal == true)
		{
			if(GetClosestVehicle(playerid,1.8) != -1)
			{
				new bizid;
				new btip[8];
				new string[128];
				new digtext[1024];
				bizid = GetClosestBenzinbiz(playerid);
				for(new j;j<sizeof(benzinar);j++)
				{
					if(benzinar[j][0] == bizid)
					{
						switch(benzinar[j][1])
						{
							case 0: format(btip,sizeof(btip),"Benzin");
							case 1: format(btip,sizeof(btip),"Diesel");
							case 2: format(btip,sizeof(btip),"Kerozin");
						}
						
						if(benzinar[j][1] == 1)
						{
							format(string,sizeof(string),"%s\t%d FT/L\n",btip,benzinar[j][3]);
						}
						else
						{
							format(string,sizeof(string),"%s %d\t%d FT/L\n",btip,benzinar[j][2],benzinar[j][3]);
						}
						
					}
					strcat(digtext,string);
				}
				ShowPlayerDialog(playerid,DIALOG_BENZINKUT1,DIALOG_STYLE_TABLIST,"Benzinkút",digtext,"Kiválaszt","Mégse");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs jármû a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy benzinkúton! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Kocsiban nem használhatod! ))");
	}
	return 1;
}

CMD:vehdel(playerid, params[]) 
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			new currentveh;
			currentveh = GetPlayerVehicleID(playerid);
			VehicleInfo[currentveh][Vvan] = false;
			if(VehicleInfo[currentveh][Id] != 0)
			{
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"DELETE FROM `samp`.`vehicle` WHERE `vehicle`.`Id` = %d",VehicleInfo[currentveh][Id]);
				new Cache:res = mysql_query(mysql,sql);
				cache_delete(res);
			}
			DestroyVehicle(currentveh);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:money(playerid, params[]) 
{	
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new amounts;
		if(sscanf(params, "ud", ids, amounts))return SendClientMessage(playerid, -1, "(( Használat: /money [ID] [Mennyiség] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			PlayerInfo[ids][Money] += amounts;
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:moneydel(playerid, params[]) 
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat: /moneydel [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			PlayerInfo[ids][Money] = 0;
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:akill(playerid, params[]) 
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat:/akill [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			SetPlayerHealth(ids, -1);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:makeadmin(playerid, params[]) 
{
	if(IsPlayerConnected(playerid))
	{
		if(PlayerInfo[playerid][Admin] >= 1337 || IsPlayerAdmin(playerid))
		{
			new ids;
			new amounts;
			if(sscanf(params, "ud", ids, amounts))return SendClientMessage(playerid, -1, "(( Használat:/makeadmin [ID] [LVL] ))");
			else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
			else
			{
				new string[128],string2[128];
				format(string,sizeof(string),"(( %s kinevezve %d adminnak! ))",Nev(ids),amounts);
				format(string2,sizeof(string2),"(( Kineveztek %d adminnnak! ))",amounts);
				SendClientMessage(playerid, COLOR_GREEN, string);
				SendClientMessage(ids, COLOR_GREEN, string2);
				PlayerInfo[ids][Admin] = amounts;
				saveplayer(ids);
			}
		}
		else
		{
			return 0;
		}
	}
	return 1;
}
CMD:veh(playerid, params[]) 
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new amounts;
		new colork;
		new colorkk;
		new string[6];
		new srn;
		new Float:x,Float:y,Float:z;
		if(sscanf(params, "dddd", amounts, colork, colorkk,srn))return SendClientMessage(playerid, -1, "(( Használat:/veh [ID] [SZIN1] [SZIN2] [Sziréna] ))");
		else if(400 > amounts)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else if(611 < amounts)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			new vid=1;
			new bool:talal;
			while( vid < MAX_VEHICLES && talal == false)
			{
				if(VehicleInfo[vid][Vvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					vid++;
				}
			}
			if(talal == true)
			{
				format(string,sizeof(string),"Admin");
				GetPlayerPos(playerid,x,y,z);
				CreateVehicle(amounts,x+3.0,y,z,0.0,colork,colorkk,-1,srn);
				SetVehicleNumberPlate(vid,string);
				VehicleInfo[vid][PosX] = x;
				VehicleInfo[vid][PosY] = y;
				VehicleInfo[vid][PosZ] = z;
				VehicleInfo[vid][vmunka] = 0;
				VehicleInfo[vid][vfrakcio] = 0;
				VehicleInfo[vid][megkilometer] = 0;
				VehicleInfo[vid][uzemanyag] = 100;
				sscanf(string, "s[32]",VehicleInfo[vid][Plate]);
				SetVehicleToRespawn(vid);
				VehicleInfo[vid][Vvan] = true;
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:fixveh(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{	
		if(IsPlayerInAnyVehicle(playerid))
		{
			RepairVehicle(GetPlayerVehicleID(playerid));
			GetVehicleHealth(GetPlayerVehicleID(playerid), vhp);
			vhealth[playerid] = vhp;
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}

CMD:fixvehid(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		if(sscanf(params, "i", ids))return SendClientMessage(playerid, -1, "(( Használat: /fixvehd [ID] ))");
		else
		{
			RepairVehicle(ids);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:prac(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 2)
	{
		new ids,szoveg[128];
		if(sscanf(params, "us[128]", ids,szoveg))return SendClientMessage(playerid, -1, "(( Használat: /prac [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else if(PlayerInfo[ids][Admin] < 2)return SendClientMessage(playerid, COLOR_RED, "(( Nem írhatsz moderátornak/asnek/playernek ))");
		else
		{
			format(szoveg,sizeof(szoveg),"((*PRAC* %s(%d): %s ))",Nev(playerid),playerid,szoveg);
			SendClientMessage(ids,0x8b1ea6AA,szoveg);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:asc(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1 || ahelp[playerid] == 1)
	{
		new string[126];
		
		if(isnull(params))
			return SendClientMessage(playerid, -1, "(( Használat: /asc [ÜZENET] ))");
		if(PlayerInfo[playerid][Admin] ==0)
		{
			format(string, sizeof(string), "*ASC* Adminsegéd %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 1)
		{
			format(string, sizeof(string), "*ASC* Moderátor %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 2)
		{
			format(string, sizeof(string), "*ASC* %d %s: %s", PlayerInfo[playerid][Admin]-1, Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 5)
		{
			format(string, sizeof(string), "*ASC* Fõadmin %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 1337)
		{
			format(string, sizeof(string), "*ASC* Fejlesztõ %s: %s", Nev(playerid), params);
		}
		SendToAS(0xffa600AA, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:mc(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new string[126];
		
		if(isnull(params))
			return SendClientMessage(playerid, -1, "(( Használat: /mc [ÜZENET] ))");
		
		if(PlayerInfo[playerid][Admin] == 1)
		{
			format(string, sizeof(string), "*MC* Moderátor %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 2)
		{
			format(string, sizeof(string), "*MC* %d %s: %s", PlayerInfo[playerid][Admin]-1, Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 5)
		{
			format(string, sizeof(string), "*MC* Fõadmin %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 1337)
		{
			format(string, sizeof(string), "*MC* Fejlesztõ %s: %s", Nev(playerid), params);
		}
		SendToModerator(0x00fff7AA, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:ac(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 2)
	{
		new string[126];
		
		if(isnull(params))
			return SendClientMessage(playerid, -1, "(( Használat: /ac [ÜZENET] ))");
		
		if(PlayerInfo[playerid][Admin] == 1)
		{
			format(string, sizeof(string), "*AC* Moderátor %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 2)
		{
			format(string, sizeof(string), "*AC* %d %s: %s", PlayerInfo[playerid][Admin]-1, Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 5)
		{
			format(string, sizeof(string), "*AC* Fõadmin %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 1337)
		{
			format(string, sizeof(string), "*AC* Fejlesztõ %s: %s", Nev(playerid), params);
		}
		SendToAdmins(0xF2FF00FF, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:fc(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new string[126];
		
		if(isnull(params))
		return SendClientMessage(playerid, -1, "(( Használat: /fc [ÜZENET] ))");
			
		format(string, sizeof(string), "*FC* Fejlesztõ %s: %s", Nev(playerid), params);
		SendToDevelopment(0x449aebAA, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:goto(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new Float:x,Float:y,Float:z;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat: /gotop [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		new ints = GetPlayerInterior(ids);
		SetPlayerInterior(playerid,ints);
		new avw = GetPlayerVirtualWorld(ids);
		SetPlayerVirtualWorld(playerid,avw);
		GetPlayerPos(ids, x, y,z);
		SetPlayerPos(playerid, x+1, y+1, z);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:gethere(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		new ids;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat: /getp [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		if(!IsPlayerInAnyVehicle(ids))
		{
			new ints = GetPlayerInterior(playerid);
			SetPlayerInterior(ids,ints);
			new avw = GetPlayerVirtualWorld(playerid);
			SetPlayerVirtualWorld(ids,avw);
			GetPlayerPos(playerid, x, y, z);
			SetPlayerPos(ids, x+1, y+1, z);
		}
		else
		{
			GetPlayerPos(playerid, x, y, z);
			SetVehiclePos(GetPlayerVehicleID(ids), x, y+1, z);
		}
		SendClientMessage(ids, COLOR_RED, "(( Teleportálva lettél! ))");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:adminok(playerid, params[])
{
	new string[128];
	if(IsPlayerConnected(playerid))
	{
		SendClientMessage(playerid, 0x26adfcAA, "(( .::Online Adminok::. ))");
		for(new i = 0; i < MAX_PLAYERS; i++)
		{
			if(IsPlayerConnected(i))
			{
				if(PlayerInfo[i][Admin] == 1)
				{
					format(string, sizeof(string), "(( Moderátor %s ))", Nev(i));
					SendClientMessage(playerid,0x26adfcAA,string);
				}
				if(1 < PlayerInfo[i][Admin] && PlayerInfo[i][Admin]<= 4)
				{
					format(string, sizeof(string), "(( %d Admin %s ))", PlayerInfo[i][Admin]-1, Nev(i));
					SendClientMessage(playerid,0x26adfcAA,string);
				}
				if(PlayerInfo[i][Admin] == 5)
				{
					format(string, sizeof(string), "(( Fõadmin %s ))", Nev(i));
					SendClientMessage(playerid,0x26adfcAA,string);
				}
				if(PlayerInfo[i][Admin] == 1337)
				{
					format(string, sizeof(string), "(( Fejlesztõ %s ))", Nev(i));
					SendClientMessage(playerid,0x26adfcAA,string);
				}
			}
		}
	}
	return 1;
}
CMD:atv(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new tipus[64];
		if(sscanf(params, "s[64]d", tipus,ids))return SendClientMessage(playerid, -1, "(( Használat: /atv [TIPUS] [ID] ))");
		GetPlayerPos(playerid, tvpos[playerid][0],tvpos[playerid][1],tvpos[playerid][2]);
		if( strcmp( tipus, "jatekos", true ) == 0 )
		{
			KillTimer(tavtimer[playerid]);
			tavtimer[playerid] = SetTimerEx("atvjatekos",1000,true,"uu",playerid,ids);
		}
		if( strcmp( tipus, "jarmu", true ) == 0 )
		{
			//GetPlayerPos(playerid, tvpos[playerid][0],tvpos[playerid][1],tvpos[playerid][2]);
			TogglePlayerSpectating(playerid, 1);
			PlayerSpectateVehicle(playerid,ids);
		}
	}
	else
	{
		return 0;
	}
		
	return 1;
}
CMD:szkuvi(playerid, params[])
{
	new sendername[ MAX_PLAYER_NAME ];
	new string[128];
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new ids;
		new Float:x,Float:y,Float:z;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat: /szkuvi [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		GetPlayerName(ids, sendername, sizeof(sendername));
		GetPlayerPos(ids, x, y, z);
		CreatePlayerObject(ids,385,x,y,z,0,0,0);
		format(string, 128, "%s plízelve lett", sendername);
		SendClientMessage(playerid, COLOR_RED, string);
	}
	else
	{
		return 0;
	}
		
	return 1;
}
CMD:atvki(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		KillTimer(tavtimer[playerid]);
		TogglePlayerSpectating(playerid, 0);
		SetPlayerPos(playerid, tvpos[playerid][0],tvpos[playerid][1],tvpos[playerid][2]);
		SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:givegun(playerid, params[]) 
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new amounts;
		new ammo;
		if(sscanf(params, "udd", ids, amounts, ammo))return SendClientMessage(playerid, -1, "(( Használat:/givegun [PLAYERID] [ID] [AMMO] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else if(0 > amounts)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else if(46 < amounts)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			GivePlayerWeapon(ids, amounts, ammo);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:parancsok(playerid, params[])
{
	static dialogText[256];
	new File:DFile = fopen("parancsok.txt", io_read), _tmpstr[64];
	dialogText[0] = 0x0;
	if(DFile)
	{
		while(fread(DFile, _tmpstr))
		{
		strcat(dialogText, _tmpstr);
		}
	}
	ShowPlayerDialog(playerid, DIALOG_PARANCS, DIALOG_STYLE_LIST, "Parancsok", dialogText, "Kilépés", "");
	return 1;
}
CMD:adminparancsok(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		static dialogText[256];
		new File:DFile = fopen("adminparancsok.txt", io_read), _tmpstr[64];
		dialogText[0] = 0x0;
		if(DFile)
		{
			while(fread(DFile, _tmpstr))
			{
			strcat(dialogText, _tmpstr);
			}
		}
		ShowPlayerDialog(playerid, DIALOG_PARANCS, DIALOG_STYLE_LIST, "Parancsok", dialogText, "Kilépés", "");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:segitseg(playerid, params[])
{
	static dialogText[256];
	new File:DFile = fopen("segitseg.txt", io_read), _tmpstr[64];
	dialogText[0] = 0x0;
	if(DFile)
	{
		while(fread(DFile, _tmpstr))
		{
		strcat(dialogText, _tmpstr);
		}
	}
	ShowPlayerDialog(playerid, DIALOG_PARANCS, DIALOG_STYLE_MSGBOX, "Leírás", dialogText, "Kilépés", "");
	return 1;
}
CMD:arendszam(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] > 1)
	{
		new vid[128];
		new tip;
		if(sscanf(params, "is[128]",tip,vid))return SendClientMessage(playerid,-1,"(( Használat: /arendszam [Típus 1:Kocsi ID, 2:Rendszám] ))");
		else
		{
			if(tip == 1)
			{
				new ert;
				new string[128];
				ert = strval(vid);
				format(string,sizeof(string),"(( Rendszám: %s Tulajdonos: %d Frakció: %d Model: %d %s DBID: %d ))",VehicleInfo[ert][Plate],VehicleInfo[ert][Owner],VehicleInfo[ert][vfrakcio],VehicleInfo[ert][Model],GetVehicleModelName(VehicleInfo[ert][Model],false),VehicleInfo[ert][Id]);
				SendClientMessage(playerid,-1,string);
			}
			else if(tip == 2)
			{
				for(new i=1;i<MAX_VEHICLES;i++)
				{
					if( (strcmp(VehicleInfo[i][Plate],vid,false) == 0) && (strlen(VehicleInfo[i][Plate]) > 3))
					{
						new string[128];
						format(string,sizeof(string),"(( Rendszám: %s Tulajdonos: %d Model: %d %s DBID: %d %d ))",VehicleInfo[i][Plate],VehicleInfo[i][Owner],VehicleInfo[i][Model],GetVehicleModelName(VehicleInfo[i][Model],false),VehicleInfo[i][Id]);
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			else
			{
				SendClientMessage(playerid,COLOR_RED,"(( Hibás típus! ))");
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:vs(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new plate[32];
		new Query[1024];
		new amount;
		new colork;
		new colorkk;
		new owne;
		new ownen[64];
		new vsmunka;
		new vsfrakcio;
		new uzem;
		new Float:x,Float:y,Float:z,Float:face;
		if(sscanf(params, "ds[64]ddds[32]ddd",owne,ownen,amount,colork,colorkk,plate,vsmunka,vsfrakcio,uzem))return SendClientMessage(playerid, -1, "(( Használat:/vs [Tulaj DBID] [Tulaj neve] [Model] [Szin1] [Szin2] [Rendszám] [Munka] [Frakcio] [üzemanyag]))");
		else if(400 > amount)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else if(611 < amount)return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			GetPlayerPos(playerid, x, y, z);
			GetPlayerFacingAngle(playerid, face);
			mysql_format(mysql,Query,sizeof(Query),"INSERT INTO `vehicle`(Owner,Ownername,Model,PosX,PosY,PosZ,PosFace,Color1,Color2,Plate,munka,frakcio,uzemanyagt) VALUES ('%d','%s','%d','%f','%f','%f','%f','%d','%d','%s','%d','%d','%d')",owne,ownen,amount,x,y,z,face,colork,colorkk,plate,vsmunka,vsfrakcio,uzem);
			new Cache:res = mysql_query(mysql,Query);
			cache_delete(res);
			mysql_format(mysql,Query, sizeof(Query), "SELECT * FROM `vehicle` ORDER BY `Id` DESC LIMIT 1");
			mysql_tquery(mysql,Query,"LoadVehicle");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:createh(playerid,params[])
{	
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new ids,cost;
		new Float:x,Float:y,Float:z;
		new sql[512];
		if(sscanf(params, "dd",ids,cost))return SendClientMessage(playerid, -1, "(( Használat:/createh [interid] [ára] ))");
		else
		{	
			ids--;
			GetPlayerPos(playerid,x,y,z);
			mysql_format(mysql,sql,sizeof(sql),("INSERT INTO `house`(x,y,z,ex,ey,ez,interior,Owner,Ownername,Cost,zarva) VALUES ('%f','%f','%f','%f','%f','%f','%d','0','Nincs','%d','0')"),x,y,z,hinterid[ids][0],hinterid[ids][1],hinterid[ids][2],floatround(hinterid[ids][3]),cost);
			new Cache:res = mysql_query(mysql,sql);
			cache_delete(res);
			
			mysql_format(mysql,sql, sizeof(sql), "SELECT `Id`, `x`, `y`, `z`, `ex`, `ey`, `ez`, `interior`, `Owner`, `Ownername`, `Cost`, `zarva` FROM `house` ORDER BY `Id` DESC LIMIT 1");
			mysql_tquery(mysql,sql,"LoadHouse");		
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:createrenth(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new cost;
		new Float:x,Float:y,Float:z;
		new sql[512];
		if(sscanf(params, "d",cost))return SendClientMessage(playerid, -1, "(( Használat:/createrenth [ára] ))");
		else
		{
			GetPlayerPos(playerid,x,y,z);
			mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `renthouse`(`Owner`, `Ownername`, `cost`, `ido`, `zarva`, `x`, `y`, `z`, `vw`) VALUES ('0','NINCS','%d','0','0','%f','%f','%f','%d')",cost,x,y,z,GetPlayerVirtualWorld(playerid));
			new Cache: res = mysql_query(mysql,sql);
			cache_delete(res);
			mysql_format(mysql,sql, sizeof(sql), "SELECT `ID`, `Owner`, `Ownername`, `cost`, `ido`, `zarva`, `x`, `y`, `z`, `vw` FROM `renthouse` ORDER BY `ID` DESC LIMIT 1");
			mysql_tquery(mysql,sql,"LoadRentHouse");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}	
CMD:reloadvs(playerid, params[])
{	
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		SendClientMessageToAll(COLOR_GREEN,"(( Jármû respawn elindítva! ))");
		savecar();
		new string[64];
		for(new i=0;i<MAX_VEHICLES;i++)
		{
			DestroyVehicle(i);
			VehicleInfo[i][Vvan] = false;
		}
		mysql_tquery(mysql,"SELECT * FROM `vehicle`","LoadVehicle");
		format(string,sizeof(string),"(( %s fejlesztõ respawnolta az összes kocsit! ))",Nev(playerid));
		SendClientMessageToAll(COLOR_GREEN,string);
	}
	else
	{
		return 0;
	}	
	return 1;
}
CMD:reloadhouse(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new House;
		new string[64];
		for(new hid;hid<MAX_HOUSE;hid++)
		{
			DestroyPickup(HouseInfo[hid][housepick]);
			Delete3DTextLabel(HouseInfo[hid][houselabel]);
			HouseInfo[hid][hvan] = false;
		}
		loadhouse[House] =0;
		LoadHouse();
		format(string,sizeof(string),"(( %s fejlesztõ respawnolta az összes házat! ))",Nev(playerid));
		SendClientMessageToAll(COLOR_GREEN,string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:reloadcar(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		for(new i=0;i<MAX_VEHICLES;i++)
		{
			GetVehicleDamageStatus(i,gpanels[i],gdoors[i],glights[i],gtires[i]);
			GetVehicleHealth(i,ghealth[i]);
		}
		for(new vehicleid=0;vehicleid<MAX_VEHICLES; vehicleid++)
		{
			if(!IsVehicleOccupied(vehicleid))
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
				SetVehicleParamsEx(vehicleid, 0, lights, alarm, doors, bonnet, boot, objective);
				vscarresp(vehicleid);
			}
		}
		SendClientMessageToAll(COLOR_GREEN,"(( Összes kocsi respawnlova! ))");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:rtc(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			new vid;
			vid = GetPlayerVehicleID(playerid);
			GetVehicleDamageStatus(vid,gpanels[vid],gdoors[vid],glights[vid],gtires[vid]);
			GetVehicleHealth(vid,ghealth[vid]);
			vscarresp(vid);
		}
		else
		{
			new vid;
			vid = GetClosestVehicle(playerid,3.0);
			GetVehicleDamageStatus(vid,gpanels[vid],gdoors[vid],glights[vid],gtires[vid]);
			GetVehicleHealth(vid,ghealth[vid]);
			vscarresp(vid);
			
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:reloadsp(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new string[128];
		new sql[128];
		mysql_format(mysql,sql,sizeof(sql),"SELECT * FROM `serverp`");
		mysql_tquery(mysql,sql,"LoadServerPro");
		format(string,sizeof(string),"(( %s fejlesztõ respawnlota a szerver beállításokat! ))",Nev(playerid));
		SendClientMessageToAll(COLOR_GREEN,string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:reloadajto(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1337)
	{
		new string[128];
		for(new i=0;i<MAX_AJTO;i++)
		{
			AjtoInfo[i][Id] = 0;
			AjtoInfo[i][ax] = 0;
			AjtoInfo[i][ay] = 0;
			AjtoInfo[i][az] = 0;
			AjtoInfo[i][fx] = 0;
			AjtoInfo[i][fy] = 0;
			AjtoInfo[i][fz] = 0;
			AjtoInfo[i][face2] = 0;
			AjtoInfo[i][face1] = 0;
			AjtoInfo[i][inter] = 0;
			AjtoInfo[i][virt] = 0;
		}
		new sql[128];
		mysql_format(mysql,sql, sizeof(sql), "SELECT * FROM `ajtok`");
		mysql_tquery(mysql,sql,"LoadDoor");
		format(string,sizeof(string),"(( %s fejlesztõ respawnolta az összes ajtót! ))",Nev(playerid));
		SendClientMessageToAll(COLOR_GREEN,string);
	}
	else
	{
		return 0;
	}
	return 1;
}
public KickPublic(playerid)
{
	Kick(playerid);
}
stock KickWithMessage(playerid, color, message[])
{
    SendClientMessage(playerid, color, message);
    SetTimerEx("KickPublic", 1000, 0, "u", playerid);
}
 /*CMD:akick(playerid, params[])
{
	new kickid;
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(sscanf(params, "us[128]", kickid, params))return SendClientMessage(playerid, -1, "(( Használat:/kick [ID] [INDOK] ))");
			else if(!IsPlayerConnected(kickid))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
			else
			{
				
				//KickWithMessage(kickid, 0xFF0000FF, "");
			}
	}
	else
	{
		return 0;
	}
	return 1;
}*/
CMD:repaint(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new vehid,color1,color2;
		if(sscanf(params, "ddd", vehid,color1, color2))return SendClientMessage(playerid, -1, "(( Használat:/repaint [ID] [COLOR1] [COLOR2] ))");
		else
		{
			ChangeVehicleColor(vehid,color1,color2);
			VehicleInfo[vehid][Color1] = color1;
			VehicleInfo[vehid][Color2] = color2;
			SendClientMessage(playerid,-1,"(( Átszinezve! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:replate(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new vehid;
		new rendszam[32];
		if(sscanf(params, "ds[32]", vehid,rendszam))return SendClientMessage(playerid, -1, "(( Használat:/replate [ID] [RENDSZÁM] ))");
		else
		{
			SetVehicleNumberPlate(vehid,rendszam);
			sscanf(rendszam, "s[32]",VehicleInfo[vehid][Plate]);
			SendClientMessage(playerid,-1,"(( Rendszám átírva ))");
			SetVehicleToRespawn(vehid);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:apark(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			new vid;
			new Float:x,Float:y,Float:z,Float:face;
			vid = GetPlayerVehicleID(playerid);
			GetVehiclePos(vid,x,y,z);
			GetVehicleZAngle(vid, face);
			VehicleInfo[vid][PosX] = x;
			VehicleInfo[vid][PosY] = y;
			VehicleInfo[vid][PosZ] = z;
			VehicleInfo[vid][PosFace] = face;
			SendClientMessage(playerid,-1,"(( Sikeresen leparkolva! ))");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
 /*CMD:ban(playerid, params[])
{
	new string[128];
	new sendername[ MAX_PLAYER_NAME ];
	new sendname[ MAX_PLAYER_NAME ];
	new pban[15];
	new Query[128];
	new amounts;
	new ids;
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(sscanf(params, "uis[128]s[128]", ids, amounts, pban, params))return SendClientMessage(playerid, -1, "(( Használat:/ban [ID] [1PERC|2ÓRA|3NAP] [INDOK] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		if( strcmp( pban, "perc", true ) == 0 )
		{
			GetPlayerName(playerid, sendername, sizeof(sendername));
			GetPlayerName(ids, sendname, sizeof(sendname));
			format(string, 128, "%s banolta %s indok: %s", sendername, sendname, params);
			SendClientMessageToAll(-1, string);
			new year, month, day, hour, minute,second;
			new date;
			getdate(year, month, day);
			gettime( hour, minute,second);
			new btime = minute+amounts;
			date = mktime(hour, btime, second, day, month, year);
			format(Query, sizeof(Query), "UPDATE `user` SET `Ban` = '%d', `oka` = '%s' WHERE `Name` = '%s' ", date,params, sendname);
			mysql_query(Query);
			KickWithMessage(ids, 0xFF0000FF, "");
			return 1;
		}
		if( strcmp( pban, "ora", true ) == 0 )
		{
			GetPlayerName(playerid, sendername, sizeof(sendername));
			GetPlayerName(ids, sendname, sizeof(sendname));
			format(string, 128, "%s banolta %s indok: %s", sendername, sendname, params);
			SendClientMessageToAll(-1, string);
			new year, month, day, hour, minute,second;
			new date;
			getdate(year, month, day);
			gettime( hour, minute,second);
			new btime = hour+amounts;
			date = mktime(btime, minute, second, day, month, year);
			format(Query, sizeof(Query), "UPDATE `user` SET `Ban` = '%d', `oka` = '%s' WHERE `Name` = '%s' ", date,params, sendname);
			mysql_query(Query);
			KickWithMessage(ids, 0xFF0000FF, "");
		
		}
		if( strcmp( pban, "nap", true ) == 0 )
		{
			GetPlayerName(playerid, sendername, sizeof(sendername));
			GetPlayerName(ids, sendname, sizeof(sendname));
			format(string, 128, "%s banolta %s indok: %s", sendername, sendname, params);
			SendClientMessageToAll(-1, string);
			new year, month, day, hour, minute,second;
			new date;
			getdate(year, month, day);
			gettime( hour, minute,second);
			new btime = day+amounts;
			date = mktime(hour, minute, second, btime, month, year);
			format(Query, sizeof(Query), "UPDATE `user` SET `Ban` = '%d', `oka` = '%s' WHERE `Name` = '%s' ", date,params, sendname);
			mysql_query(Query);
			KickWithMessage(ids, 0xFF0000FF, "");
		
		}
	}
	else
	{
		return 0;
	}
	return 1;
}*/
CMD:gotocar(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		new ids,virta;
		if(sscanf(params, "i", ids))return SendClientMessage(playerid, -1, "(( Használat:/gotocar [ID] ))");
		GetVehiclePos(ids, x, y, z);
		virta = GetVehicleVirtualWorld(ids);
		SetPlayerVirtualWorld(playerid,virta);
		SetPlayerInterior(playerid,0);
		SetPlayerPos(playerid, x, y, z+1);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:dbgotocar(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		new ids,virta;
		if(sscanf(params, "i", ids))return SendClientMessage(playerid, -1, "(( Használat:/dbgotocar [DBID] ))");
		for(new i;i<MAX_VEHICLES;i++)
		{
			if(VehicleInfo[i][Id] == ids)
			{
				GetVehiclePos(i, x, y, z);
				virta = GetVehicleVirtualWorld(i);
				SetPlayerVirtualWorld(playerid,virta);
				SetPlayerInterior(playerid,0);
				SetPlayerPos(playerid, x, y, z+1);
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:getcar(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		new ids;
		if(sscanf(params, "i", ids))return SendClientMessage(playerid, -1, "(( Használat:/getcar [ID] ))");
		GetPlayerPos(playerid, x, y, z);
		SetVehiclePos(ids, x, y, z);
		SetPlayerPos(playerid, x, y, z+1);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:dbgetcar(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		new ids;
		if(sscanf(params, "i", ids))return SendClientMessage(playerid, -1, "(( Használat:/dbgetcar [DBID] ))");
		for(new i;i<MAX_VEHICLES;i++)
		{
			if(VehicleInfo[i][Id] == ids)
			{
				GetPlayerPos(playerid, x, y, z);
				SetVehiclePos(i, x, y, z);
				SetPlayerPos(playerid, x, y, z+1);
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:slap(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new Float:x,Float:y,Float:z;
		if(sscanf(params, "u", ids))return SendClientMessage(playerid, -1, "(( Használat:/slap [ID] ))");
		GetPlayerPos(ids, x, y, z);
		SetPlayerPos(ids, x, y, z+5);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:restart(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
			SendClientMessageToAll(COLOR_RED, "(( Egy tulajdonos újraindította a szervert! ))");
			SendRconCommand("gmx");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:gotopos(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		if(sscanf(params, "fff", x,y,z))return SendClientMessage(playerid, -1, "(( Használat:/gotopos [X] [Y] [Z] ))");
		SetPlayerPos(playerid, x, y, z);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:getpos(playerid, params[])
{	
	new string[128];
	new Float:face;
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new Float:x,Float:y,Float:z;
		GetPlayerPos(playerid, x, y, z);
		GetPlayerFacingAngle(playerid, face);
		format(string, 128, "(( Jelenlegi pozícidó x:%.3f y:%.3f z:%.3f int:%d vw:%d Face:%.3f ))", x, y, z,GetPlayerInterior(playerid),GetPlayerVirtualWorld(playerid),face);
		SendClientMessage(playerid, -1, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:fm(playerid, params[])
{
	//Audio_Play(playerid, 2);
	if(IsPlayerInAnyVehicle(playerid))
	{
		Audio_PlayStreamed(playerid,"http://79.172.241.238:8000/musicfm.mp3",false,true,true);
	}
	return 1;
}
CMD:hangero(playerid, params[])
{
	new handleid, volume=1;
	if (sscanf(params, "d", volume))
	{
		SendClientMessage(playerid, 0xFFFF00FF, "(( Használat: /hangero <hangero (0-100) ))");
		return 1;
	}
	new string[128];
	format(string, sizeof(string), "(( Hangerõ megváltoztatva %d-ra/re ))", volume);
	SendClientMessage(playerid, 0xFFFF00FF, string);
	Audio_SetVolume(playerid, handleid,volume);
	return 1;
}
CMD:b(playerid, params[])
{
	new string[128];
	if(isnull(params))
	return SendClientMessage(playerid, -1, "(( Használat: /b [ÜZENET] ))");
	format(string,sizeof(string),"(( %s:  %s ))",Nev(playerid),params);
	chats(30.0,playerid,string,COLOR_CHAT3,false);
	return 1;
}
CMD:setskin(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new amounts;
		if(sscanf(params, "ud", ids, amounts))return SendClientMessage(playerid, -1, "(( Használat: /setskin [ID] [SKIN ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			PlayerInfo[ids][skin] = amounts;
			SetPlayerSkin(ids,amounts);
			SendClientMessage(ids, COLOR_RED, "(( Egy admin átálította a skinedet! ))");
		}
	}
	else
	{
		return 0;
	}	
	return 1;
}
CMD:jelentes(playerid, params[])
{
	ShowPlayerDialog(playerid, DIALOG_JELENTES, DIALOG_STYLE_LIST, "Jelentés", "BUG\nJátékkal kapcsolatos kérdések\nSzabály szegõk\nVészhelyzet", "OK","Mégse");
	return 1;
}
CMD:v(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1 || ahelp[playerid] ==1)
	{
		new ids;
		new string[128];
		if(sscanf(params, "us[256]", ids, params ))return SendClientMessage(playerid, -1, "(( Használat: /v(álasz) [ID] [ÜZENET] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
		else
		{
			switch(PlayerInfo[playerid][Admin])
			{
				case 0:
				{
					format(string, sizeof(string), "Adminsegéd %s Válasza: %s",Nev(playerid), params);
					SendClientMessage(ids, COLOR_YELLOW, string);
					format(string, sizeof(string), "Jelentés: %s Adminsegéd válaszolt %s -nak/nek: %s", Nev(playerid), Nev(ids), params);
					SendToAS(0xfc972aAA, string);
				}				
				case 1:
				{
					format(string, sizeof(string), "Moderátor %s Válasza: %s",Nev(playerid), params);
					SendClientMessage(ids, COLOR_YELLOW, string);
					format(string, sizeof(string), "Jelentés: %s Moderátor válaszolt %s -nak/nek: %s", Nev(playerid), Nev(ids), params);
					SendToAS(0xfc972aAA, string);
				}				
				case 2 .. 4:
				{
					format(string, sizeof(string), "%d Admin %s Válasza: %s",PlayerInfo[playerid][Admin]-1,Nev(playerid), params);
					SendClientMessage(ids, COLOR_YELLOW, string);
					format(string, sizeof(string), "Jelentés: %d Admin %s válaszolt %s -nak/nek: %s", PlayerInfo[playerid][Admin]-1,Nev(playerid), Nev(ids), params);
					SendToAS(0xfc972aAA, string);
				}				
				case 5:
				{
					format(string, sizeof(string), " Fõadmin %s Válasza: %s",Nev(playerid), params);
					SendClientMessage(ids, COLOR_YELLOW, string);
					format(string, sizeof(string), "Jelentés: Fõadmin %s válaszolt %s -nak/nek: %s", Nev(playerid), Nev(ids), params);
					SendToAS(0xfc972aAA, string);
				}				
				case 1337:
				{
					format(string, sizeof(string), "Fejlesztõ %s Válasza: %s",Nev(playerid), params);
					SendClientMessage(ids, COLOR_YELLOW, string);
					format(string, sizeof(string), "Jelentés: Fejlesztõ %s válaszolt %s -nak/nek: %s", Nev(playerid), Nev(ids), params);
					SendToAS(0xfc972aAA, string);
				}
			}
		}
	}	
	return 1;
}
CMD:cc(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		    for(new i = 0; i < MAX_PLAYERS; i++)
		    ClearChatbox(i, 100);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:stat(playerid, params[])
{
	new string[128],string2[128];
	new hour, minute, millisecond,contime,munk;
	contime =  gettime() - PlayerInfo[playerid][pconnecttime];
	GetDateAndTimeOfh(millisecond, hour, minute, contime);
	munk = PlayerInfo[playerid][pmunka];
	format(string,sizeof(string),"(( Név: %s Születési idõ: %s Neme: %s Játszott óra: %d Házastárs: %s ))",Nev(playerid),PlayerInfo[playerid][szuletes],PlayerInfo[playerid][neme],hour,PlayerInfo[playerid][phazas]);
	format(string2,sizeof(string2),"(( Munka: %s Frakció: %s Rang: %s Regisztrált: %s ))",munkanevek[munk][0],PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],PlayerInfo[playerid][preg]);
	SendClientMessage(playerid,-1,string);
	SendClientMessage(playerid,-1,string2);
	return 1;
}
CMD:rendszam(playerid, params[])
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		new string[128]; 
		format(string, sizeof(string), "(( %s ))", VehicleInfo[GetPlayerVehicleID(playerid)][Plate]);
		SendClientMessage(playerid, -1, string);
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
	}
	return 1;
}
CMD:indit(playerid, params[])
{
	autoinditas(playerid);
	return 1;
}
CMD:leallit(playerid, params[])
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
		{
			new engine, lights, alarm, doors, bonnet, boot, objective;
			GetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, lights, alarm, doors, bonnet, boot, objective);
			if(engine == 1)
			{
				SetVehicleParamsEx(GetPlayerVehicleID(playerid), 0, lights, alarm, doors, bonnet, boot, objective);
				SendClientMessage(playerid, -1, "(( Leállítva ))");
			}
			else
			{
				SendClientMessage(playerid, -1, "(( Már levan állítva ))");
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "(( Nem te vagy a vezetõ! ))");
		}
	}
	else
	{
		SendClientMessage(playerid, -1, "(( Nem ülsz kocsiban! ))");
	}
	return 1;
}
CMD:lampa(playerid,params[])
{	
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
		{
			new engine, lights, alarm, doors, bonnet, boot, objective;
			GetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, lights, alarm, doors, bonnet, boot, objective);
			if(lights == 0)
			{
				SetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, 1, alarm, doors, bonnet, boot, objective);
			}
			else
			{
				SetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, 0, alarm, doors, bonnet, boot, objective);
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "(( Nem te vagy a vezetõ! ))");
		}
	}
	else
	{
		SendClientMessage(playerid, -1, "(( Nem ülsz kocsiban! ))");
	}
	return 1;
}
CMD:alefoglal(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new vehicleid;
		if(IsPlayerInAnyVehicle(playerid))
		{
			vehicleid = GetPlayerVehicleID(playerid);
		}
		else
		{
			vehicleid = GetClosestVehicle(playerid,6.0);
		}
		new engine,  lights,  alarm,  doors,  bonnet,  boot,  objective;
		PutPlayerInVehicle(playerid,vehicleid,0);
		VehicleInfo[GetPlayerVehicleID(playerid)][uzemanyag] = vehdata[GetVehicleModel(GetPlayerVehicleID(playerid))-400][0];
		if(VehicleInfo[GetPlayerVehicleID(playerid)][uzemanyag] >0.0)
		{
			GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
			SetVehicleParamsEx(vehicleid, 1, lights, alarm, doors, bonnet, boot, objective);
		}
		else
		{
			SendClientMessage(playerid,COLOR_RED,"(( Nincs benne üzemanyag! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:exit(playerid, params[])
{
	new hid;
	hid = GetPlayerVirtualWorld(playerid);
	if(IsPlayerInRangeOfPoint(playerid,10.0,HouseInfo[hid][enterx], HouseInfo[hid][entery], HouseInfo[hid][enterz]))
	{
		SetPlayerPos(playerid,HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]);
		SetPlayerVirtualWorld(playerid,0);
		SetPlayerInterior(playerid,0);
	}
	else if(IsPlayerInRangeOfPoint(playerid,10.0,BizzInfo[hid][enterx], BizzInfo[hid][entery], BizzInfo[hid][enterz]))
	{
		SetPlayerPos(playerid,BizzInfo[hid][px],BizzInfo[hid][py],BizzInfo[hid][pz]);
		SetPlayerVirtualWorld(playerid,0);
		SetPlayerInterior(playerid,0);
		playerinbolt[playerid] = false;
	}
	else if(IsPlayerInRangeOfPoint(playerid,5.0,266.500, 305.075, 999.148))
	{
		SetPlayerPos(playerid,RentInfo[hid][rx],RentInfo[hid][ry],RentInfo[hid][rz]);
		SetPlayerVirtualWorld(playerid,RentInfo[hid][rvw]);
		SetPlayerInterior(playerid,0);
	}
	else if(IsPlayerInRangeOfPoint(playerid,5.0,1606.584,-1624.856,416.389))
	{
		SetPlayerPos(playerid,RaktarInfo[hid][rax],RaktarInfo[hid][ray],RaktarInfo[hid][raz]);
		SetPlayerVirtualWorld(playerid,0);
		SetPlayerInterior(playerid,0);
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy a bejáratnál! ))");
	}
	return 1;
}
CMD:gotoh(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		if(sscanf(params, "d", ids))return SendClientMessage(playerid, -1, "(( Használat: /gotoh [ID] ))");
		else
		{
			SetPlayerPos(playerid, HouseInfo[ids][px],HouseInfo[ids][py],HouseInfo[ids][pz]);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:ajail(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids,amount;
		new string[128],string2[128];
		if(sscanf(params, "uds[64]", ids,amount,params))return SendClientMessage(playerid, -1, "(( Használat: /ajail [ID] [PERC] [OKA] ))");
		else
		{
			PlayerInfo[ids][pborton] = amount*60;
			PlayerInfo[ids][btipus] = 4;
			format(string,sizeof(string),"(( %s berakott AJ-ba idõ:%d indok: %s ))",Nev(playerid),amount,params);
			format(string2,sizeof(string2),"(( SCD:%s berakta %s AJ-ba idõ: %d indok: %s ))",Nev(playerid),Nev(ids),amount,params);
			SendClientMessage(ids,-1,string);
			SendToAdmins(-1,string2);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:unjail(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids,amount;
		new string[128],string2[128];
		if(sscanf(params, "ud", ids,amount))return SendClientMessage(playerid, -1, "(( Használat: /ajail [ID] [PERC] [OKA] ))");
		else
		{
			PlayerInfo[ids][pborton] = 5;
			PlayerInfo[ids][btipus] = 0;
			format(string,sizeof(string),"(( %s kiszedett AJ-bõl ))",Nev(playerid));
			format(string2,sizeof(string2),"SCD: %s kiszedte AJ-bõl %s",Nev(playerid),Nev(ids));
			SendClientMessage(ids,-1,string);
			SendToAdmins(-1,string2);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:helps(playerid, params[])
{
	return 1;
}

CMD:me(playerid, params[])
{
	new string[128];
	if(isnull(params))
	return SendClientMessage(playerid, -1, "(( Használat: /me [cselekvés] ))");
	format(string, sizeof(string)," %s",params);
	chats(30.0,playerid,string,COLOR_FADE1,true);
	return 1;
}
CMD:try(playerid, params[])
{
	new string[128];
	new rand = random(2)+1;
	if(isnull(params))
	return SendClientMessage(playerid, -1, "(( Használat: /try [cselekvés] ))");
	if(rand == 1)
	{
		format(string, sizeof(string),": megpróbál(ja) %s és sikerül neki", params);
	}
	else
	{
		format(string, sizeof(string),": megpróbál(ja) %s de nem sikerült neki", params);
	}
	chats(30.0,playerid,string,COLOR_FADE1,true);
	return 1;
}
CMD:penztarca(playerid, params[])
{
	new string[128];
	format(string, sizeof(string),"(( %d FT van a pénztárcádban))", PlayerInfo[playerid][Money]);
	SendClientMessage(playerid, -1, string);
	Cselekves(playerid, "megnézi a pénztárca tartalmát");
	return 1;
}
CMD:szemelyi(playerid, params[])
{
	return 1;
}
CMD:jogositvany(playerid, params[])
{
	if(strcmp( PlayerInfo[playerid][jogosit], "NINCS", true ) != 0)
	{
		new string[128];
		SendClientMessage(playerid,-1,"(( |_____Jogosítvány_____| ))");
		format(string,sizeof(string),"(( Név: %s ))",Nev(playerid));
		chats(10.0,playerid,string,-1,false);
		format(string,sizeof(string),"(( Neme: %s ))",PlayerInfo[playerid][neme]);
		chats(10.0,playerid,string,-1,false);
		format(string,sizeof(string),"(( Állampolgár: %s))",PlayerInfo[playerid][allamp]);
		chats(10.0,playerid,string,-1,false);
		format(string,sizeof(string),"(( Kategória: %s ))",PlayerInfo[playerid][jogosit]);
		chats(10.0,playerid,string,-1,false);
		format(string,sizeof(string),"(( Sorszám: %s ))",PlayerInfo[playerid][jsorszam]);
		chats(10.0,playerid,string,-1,false);
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nincs jogosítványod! ))");
	}
	return 1;
}
CMD:kresz(playerid,params[])
{
	if(IsPlayerInRangeOfPoint(playerid,3.0,-2032.718,-117.239,1035.171))
	{
		ShowPlayerDialog(playerid,DIALOG_KRESZ1,DIALOG_STYLE_LIST,"Kresz","A\nB,M\nC\nC+E\nD","OK","Mégse");
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy a megfelelõ helyen! ))");
	}
	return 1;
}
CMD:asay(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new string[128];
		if(isnull(params))
		return SendClientMessage(playerid, -1, "(( Használat: /asay [ÜZENET] ))");
		if(PlayerInfo[playerid][Admin] == 1)
		{
			format(string, sizeof(string), "Moderátor: %s", params);
		}
		if(PlayerInfo[playerid][Admin] >= 2)
		{
			format(string, sizeof(string), "%d: %s", PlayerInfo[playerid][Admin]-1, params);
		}
		if(PlayerInfo[playerid][Admin] == 5)
		{
			format(string, sizeof(string), "Fõadmin: %s", params);
		}
		if(PlayerInfo[playerid][Admin] >= 1337)
		{
			format(string, sizeof(string), "Fejlesztõ: %s", params);
		}
		SendClientMessageToAll(0x008c00AA, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:asayn(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new string[128];
		if(isnull(params))
		return SendClientMessage(playerid, -1, "(( Használat: /asayn [ÜZENET] ))");
		if(PlayerInfo[playerid][Admin] == 1)
		{
			format(string, sizeof(string), "Moderátor %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 2)
		{
			format(string, sizeof(string), "%d Admin %s: %s", PlayerInfo[playerid][Admin]-1, Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] == 5)
		{
			format(string, sizeof(string), "Fõadmin %s: %s", Nev(playerid), params);
		}
		if(PlayerInfo[playerid][Admin] >= 1337)
		{
			format(string, sizeof(string), "Fejlesztõ %s: %s", Nev(playerid), params);
		}
		SendClientMessageToAll(0x008c00AA, string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:weatherall(playerid, params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new amount;
		if(sscanf(params, "d", amount))return SendClientMessage(playerid, -1, "(( Használat: /weatherall [ID] ))");
		else
		{
			SetWeather(amount);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:settime(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new amount;
		if(sscanf(params, "d", amount))return SendClientMessage(playerid, -1, "(( Használat: /settime [ÓRA] ))");
		else
		{
			SetWorldTime(amount);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:ido(playerid,params[])
{
	new year,mounth,day,hour,minute,second, string[128],string2[120],millisecond;
	new ahour,aminute,asecond;
	getdate(year, mounth, day);
	gettime(hour,minute,second);
	format(string,sizeof(string),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
	SendClientMessage(playerid,-1,string);
	if(PlayerInfo[playerid][pborton] !=0)
	{
		asecond = PlayerInfo[playerid][pborton];
		GetDateAndTimeOfh(millisecond, ahour, aminute, asecond);
		format(string2,sizeof(string2), "Börtön idõ: %02d:%02d:%02d", ahour,aminute,asecond);
		SendClientMessage(playerid,-1,string2);
	}
	SendClientMessage(playerid,-1,"A pontos idõ!");
	ApplyAnimation(playerid,"COP_AMBIENT","Coplook_watch", 4.1, 0, 0, 0, 0, -1);
	return 1;
}
CMD:create3d(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		ShowPlayerDialog(playerid,DIALOG_3DLABEL1,DIALOG_STYLE_INPUT,"3D LABEL CREATOR","Szöveg","Tovább","Mégse");
		/*new query[256];
		new color, Float:X, Float:Y, Float:Z, Float:DrawDistance, virtualworld;
		if(sscanf(params, "s[32]hf",params,color,DrawDistance))return SendClientMessage(playerid, -1, "(( Használat:/create3d [szöveg] [szin] [látótáv] ))");
		else
		{	
			GetPlayerPos(playerid, X, Y, Z);
			virtualworld = GetPlayerVirtualWorld(playerid);
			format(query,sizeof(query),"INSERT INTO `3dtextlabel` (text,color,x,y,z,distance,vw,testLOS) VALUES ('%s','%h','%.3f','%.3f','%.3f','%.3f','%d','0')",params,color,X,Y,Z,DrawDistance,virtualworld);
			mysql_query(query);
			Create3DTextLabel(params,color,X,Y,Z,DrawDistance,virtualworld,0);
		}*/
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:r(playerid, params[])
{
	if(PlayerInfo[playerid][frakcio] > 0)
	{
		if(isnull(params)) return SendClientMessage(playerid,-1,"(( Használat: /r [szöveg] ))");
		if(fkradio[playerid] ==0) return SendClientMessage(playerid,COLOR_RED,"(( Nem vagy szolgálatban, vagy nem vagy egyetlen frakciónak a tagja ))");
		new string[128];
		format(string,sizeof(string), "[rádió] %s %s %s: %s ,vége.", PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],Nev(playerid),params);
		SendToRadio(playerid,COLOR_BLUE,string);
		format(string,sizeof(string),": *rádióba* %s",params);
		chats(30.0,playerid,string,COLOR_CHAT1,true);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:d(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] > 0)
	{
		if(PlayerInfo[playerid][ftipus] !=1 ) return 0;
		if(PlayerInfo[playerid][pszolg] == 0) return SendClientMessage(playerid,COLOR_RED,"(( Nem vagy szolgálatban! ))");
		if(isnull(params)) return SendClientMessage(playerid,-1,"(( Használat: /d [szöveg] ))");
		new string[128];
		format(string,sizeof(string), "[rádió] %s %s %s: %s ,vége.", PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],Nev(playerid),params);
		SendToRadioD(playerid,COLOR_ORANGE,string);
		format(string,sizeof(string),": *rádióba* %s",params);
		chats(30.0,playerid,string,COLOR_CHAT1,true);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:leader(playerid,params[])
{
	if((PlayerInfo[playerid][pleader] == 1 && PlayerInfo[playerid][frakcio] > 0) || (PlayerInfo[playerid][paleader] == 1  && PlayerInfo[playerid][frakcio] != 0))
	{
		if(PlayerInfo[playerid][alosztaly] ==0 || PlayerInfo[playerid][paleader] == 1)
		{
			if(PlayerInfo[playerid][alosztaly] ==0 || PlayerInfo[playerid][frakcio] == 3)
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája\nFelvétel", "OK", "Mégse");
			}
			else
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája", "OK", "Mégse");
			}
		}
		else
		{
			new sql[128];
			new string[256];
			new dlg[512];
			mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve` FROM `frakcio` WHERE `Alosztaly` = '%d'",PlayerInfo[playerid][alosztaly]);
			new Cache:res = mysql_query(mysql,sql);
			new row;
			cache_get_row_count(row);
			for(new i=0;i<row;i++)
			{
				cache_get_value_name(i,"Hneve",string,sizeof(string));
				format(string,sizeof(string),"\n%s",string);
				strcat(dlg,string);
			}
			cache_delete(res);
			ShowPlayerDialog(playerid,DIALOG_ALLEADER1,DIALOG_STYLE_LIST,"Vezetõség",dlg,"OK","Mégse");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:fizetes(playerid,params[])
{
	if(PlayerInfo[playerid][pleader] == 1)
	{
		if(PlayerInfo[playerid][alosztaly] ==0 || PlayerInfo[playerid][paleader] == 1)
		{
			new query[128];
			new dlg[512];
			new frakok[64];
			new string[64];
			new fizus;
			format(string,sizeof(string),"Rang\tFizetés\n",frakok);
			strcat(dlg,string);
			for(new i = 1; i < MAX_RANGOK; i++)
			{
				mysql_format(mysql,query,sizeof(query),"SELECT `%d`,`%df` FROM `frakcio` WHERE LENGTH(`%d`) > 1 AND Id = '%d'",i,i,i,PlayerInfo[playerid][frakcio]);
				new Cache:res = mysql_query(mysql,query);
				cache_get_value_index(0,0,frakok,sizeof(frakok));
				cache_get_value_index_int(0,1,fizus);
				format(string,sizeof(string),"%s\t%s\n",frakok,cformat(fizus));
				strcat(dlg,string);
				cache_delete(res);
			}
			ShowPlayerDialog(playerid, DIALOG_FIZETES1, DIALOG_STYLE_TABLIST_HEADERS, "Fizetés", dlg, "OK", "Mégse");
		}
		else
		{
			new sql[128];
			new string[256];
			new dlg[512];
			new row;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve` FROM `frakcio` WHERE `Alosztaly` = '%d'",PlayerInfo[playerid][alosztaly]);
			new Cache:res = mysql_query(mysql,sql);
			cache_get_row_count(row);
			for(new i;i<row;i++)
			{
				cache_get_value_name(i,"Hneve",string,sizeof(string));
				format(string,sizeof(string),"\n%s",string);
				strcat(dlg,string);
				
			}
			cache_delete(res);
			ShowPlayerDialog(playerid,DIALOG_FIZETES3,DIALOG_STYLE_LIST,"Fizetés",dlg,"OK","Mégse");
		}
	}
	else
	{
		return 1;
	}
	return 1;
}
CMD:setint(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, amount, string[128];
		if(sscanf(params, "ud",ids,amount))return SendClientMessage(playerid, -1, "(( Használat:/setint [id/név] [ID] ))");
		else
		{
			SetPlayerInterior(ids,amount);
			format(string,sizeof(string), "(( Interior átálítva %d ))", amount);
			SendClientMessage(ids,-1,string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:setvw(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, amount, string[128];
		if(sscanf(params, "ud",ids,amount))return SendClientMessage(playerid, -1, "(( Használat:/setvw [id/név] [ID] ))");
		else
		{
			SetPlayerVirtualWorld(ids,amount);
			format(string,sizeof(string), "(( VirtualWorld átálítva %d ))", amount);
			SendClientMessage(ids,-1,string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:vontat(playerid,params[])
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetVehicleModel(GetPlayerVehicleID(playerid))== 525)
		{
			if(GetVehicleTrailer(GetPlayerVehicleID(playerid)) == 0)
			{
				new vehicleid = GetClosestVehicle(playerid,8.0);
				AttachTrailerToVehicle(vehicleid, GetPlayerVehicleID(playerid));
			}
			else
			{
				DetachTrailerFromVehicle(GetPlayerVehicleID(playerid));
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ülsz vontatós kocsiban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
	}
	return 1;
}
CMD:szolg(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] > 0 && PlayerInfo[playerid][ftipus] < 2)
	{
		if(PlayerInfo[playerid][pszolg] == 0)
		{
			switch(PlayerInfo[playerid][frakcio])
			{
				case 1 .. 5:
				{
					if(IsPlayerInRangeOfPoint(playerid,5, 264.222, 110.534, 1004.617))
					{
						SetPlayerSkin(playerid, PlayerInfo[playerid][fksin]);
						GivePlayerWeapon(playerid,24,14);
						GivePlayerWeapon(playerid,3,1);
						GivePlayerWeapon(playerid,41,200);
						PlayerInfo[playerid][pszolg] = 1;
						Cselekves(playerid, "szolgálatba lépett");
						fkradio[playerid] = 1;
						AddItem(playerid,"Bilincs",1);
						AddItem(playerid,"Csipogó",1);
					}
					else
					{
						SendClientMessage(playerid,COLOR_RED,"(( Nem vagy szolg helyen! ))");
					}
				}
				case 7:
				{
					if(IsPlayerInRangeOfPoint(playerid,3,1130.713,-1348.315,-4.892))
					{
						SetPlayerSkin(playerid, PlayerInfo[playerid][fksin]);
						PlayerInfo[playerid][pszolg] = 1;
						Cselekves(playerid, "szolgálatba lépett");
						fkradio[playerid] = 3;
					}
					else
					{
						SendClientMessage(playerid,COLOR_RED,"(( Nem vagy szolg helyen! ))");
					}
				}
				case 8:
				{
					SetPlayerSkin(playerid, PlayerInfo[playerid][fksin]);
					PlayerInfo[playerid][pszolg] = 1;
					Cselekves(playerid, "szolgálatba lépett");
					fkradio[playerid] = 6;
				}
				case 10:
				{
					if(IsPlayerInRangeOfPoint(playerid,3,358.804, 206.544, 1008.382))
					{
						SetPlayerSkin(playerid, PlayerInfo[playerid][fksin]);
						PlayerInfo[playerid][pszolg] = 1;
						Cselekves(playerid, "szolgálatba lépett");
						fkradio[playerid] = 5;
					}
					else
					{
						SendClientMessage(playerid,COLOR_RED,"(( Nem vagy szolg helyen! ))");
					}
				}
				case 9,13:
				{
					SetPlayerSkin(playerid, PlayerInfo[playerid][fksin]);
					PlayerInfo[playerid][pszolg] = 1;
					Cselekves(playerid, "szolgálatba lépett");
					fkradio[playerid] = 5;
				}
			}
		}
		else
		{
			SetPlayerSkin(playerid, PlayerInfo[playerid][skin]);
			ResetPlayerWeapons (playerid);
			PlayerInfo[playerid][pszolg] = 0;
			Cselekves(playerid, "leadta a szolgálatot");
			felszerelesben[playerid] = false;
			SetPlayerArmour(playerid,0);
		}
	}
	else
	{
		SendClientMessage(playerid,-1, "(( Nem vagy frakicóban! ))");
	}
	return 1;
}
CMD:elfogad(playerid,params[])
{
	new tipus[64];
	if(sscanf(params, "s[64]", tipus))return SendClientMessage(playerid, -1, "(( /elfogad [tipus]))");
	else
	{
		if(PlayerInfo[playerid][felajan] == 1)
		{
			if( strcmp( tipus, "frakcio", true ,7) == 0 )
			{
				PlayerInfo[playerid][frakcio] = PlayerInfo[playerid][elfogad];
				SendClientMessage(playerid,-1,"(( Sikeresen elfogadtad az ajánlatot! ))");
				PlayerInfo[playerid][felajan] = 0;
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[playerid][rang],PlayerInfo[playerid][rang],PlayerInfo[playerid][frakcio]);
				mysql_tquery(mysql, sql, "LoadFrakcio", "d", playerid);
				saveplayer(playerid);
			}
			if( strcmp( tipus, "munkalap", true ,8) == 0 )
			{
				if(munkalapfel[playerid] == true)
				{
					munkalapfel[playerid] = false;
					new vehicleid = munkalapid[playerid];
					VehicleInfo[vehicleid][lefoglalva] = 2;
					VehicleInfo[vehicleid][ktartozas] += szerelkolts[playerid];
					SendClientMessage(playerid,-1,"(( Sikeresen elfogadtad a munkalapot! ))");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ajánlottak fel munkalapot! ))");
				}
			}
			if( strcmp( tipus, "kereskedo", true ,9) == 0 )
			{
				if(PlayerInfo[playerid][Money] >= jfelajossz[playerid])
				{
					if(jfelajtip[playerid] ==1)
					{
						new vehicleid = jfelajid[playerid];
						new name[64];
						GetPlayerName(playerid,name,sizeof(name));
						PlayerInfo[playerid][Money] -= jfelajossz[playerid];
						VehicleInfo[vehicleid][Owner] = PlayerInfo[playerid][dbid];
						sscanf(name, "s[64]", VehicleInfo[vehicleid][Ownername]);
						VehicleInfo[vehicleid][vfrakcio] = 0;
						PlayerInfo[playerid][felajan] = 0;
						new sql[256];
						new ados,afas;
						afaado(jfelajossz[playerid],ados,afas);
						allamibevetel(ados);
						mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`=`penz`+'%d' WHERE `frakcio`='13'",afas);
						new Cache:res = mysql_query(mysql,sql);
						cache_delete(res);
						SendClientMessage(playerid,-1,"(( Sikeresen megvásároltad a jármûvet! ))");
					}
					if(jfelajtip[playerid] == 2)
					{
						new vehicleid = jfelajid[playerid];
						PlayerInfo[playerid][Money] += jfelajossz[playerid];
						VehicleInfo[vehicleid][Owner] = 0;
						VehicleInfo[vehicleid][vfrakcio] = 13;
						sscanf("NINCS", "s[64]", VehicleInfo[vehicleid][Ownername]);
						PlayerInfo[playerid][felajan] = 0;
						new sql[256];
						new ados,afas,bpenz;
						afaado(jfelajossz[playerid],ados,afas);
						allamibevetel(ados);
						mysql_format(mysql,sql,sizeof(sql),"SELECT `penz` FROM `bank` WHERE `frakcio`='13'");
						new Cache:res = mysql_query(mysql,sql);
						cache_get_value_index_int(0,0,bpenz);
						cache_delete(res);
						if(bpenz - afas >= 0)
						{
							mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`=`penz`-'%d' WHERE `frakcio`='13'",afas);
							res = mysql_query(mysql,sql);
							cache_delete(res);
							SendClientMessage(playerid,-1,"(( Sikeresen eladtad a jármûvet! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs elég pénz a bankszámlán! ))");
						}
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs nálad elég pénz! ))");
				}
			}
			if( strcmp( tipus, "ceg", true ,9) == 0 )
			{
				PlayerInfo[playerid][pceg] = PlayerInfo[playerid][elfogad];
				SendClientMessage(playerid,-1,"(( Sikeresen elfogadtad az ajánlatot! ))");
				saveplayer(playerid);
				PlayerInfo[playerid][felajan] = 0;
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ajánlottak fel semmit! ))");
		}
	}
	return 1;
}
CMD:klk(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >0)
	{
		new string[256];
		new vehid;
		if(IsPlayerInAnyVehicle(playerid))
		{
			vehid = GetPlayerVehicleID(playerid);
			format(string,sizeof(string),"ID:%d Tulaj:%d Model:%d Szin1:%d Szin2:%d Rendszám:%s DBID:%s Tulaj neve:%s Munka:%d Frakció:%d",VehicleInfo[vehid][Id],VehicleInfo[vehid][Owner],VehicleInfo[vehid][Model],VehicleInfo[vehid][Color1],VehicleInfo[vehid][Color2],VehicleInfo[vehid][Plate],VehicleInfo[vehid][dbid],VehicleInfo[vehid][Ownername],VehicleInfo[vehid][vmunka],VehicleInfo[vehid][vfrakcio]);
			SendClientMessage(playerid,-1,string);
		}
		else
		{
			if(sscanf(params, "d", vehid))return SendClientMessage(playerid, -1, "(( /klk [ID]))");
			else
			{
			format(string,sizeof(string),"ID:%d Tulaj:%d Model:%d Szin1:%d Szin2:%d Rendszám:%s DBID:%s Tulaj neve:%s Munka:%d Frakció:%d",VehicleInfo[vehid][Id],VehicleInfo[vehid][Owner],VehicleInfo[vehid][Model],VehicleInfo[vehid][Color1],VehicleInfo[vehid][Color2],VehicleInfo[vehid][Plate],VehicleInfo[vehid][dbid],VehicleInfo[vehid][Ownername],VehicleInfo[vehid][vmunka],VehicleInfo[vehid][vfrakcio]);
			SendClientMessage(playerid,-1,string);
			}
		}
		
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:bejelentes(playerid,params[])
{	
	new szam;
	if(PlayerInfo[playerid][pszolg] ==1)
	{
		if(PlayerInfo[playerid][frakcio] !=0)
		{
			if(sscanf(params, "d", szam))return SendClientMessage(playerid, -1, "(( /bejelentes [száma] ))");
			else
			{
				if(IsPlayerCop(playerid))
				{
					for(new i;i<MAX_BEJELENTESEK;i++)
					{
						if(szam == BejelentesInfo[1][i][Id])
						{
							SetPlayerCheckpoint(playerid,BejelentesInfo[1][i][bx],BejelentesInfo[1][i][by],BejelentesInfo[1][i][bz],5.0);
							return 1;
						}
					}
				}
			}
		}
	}
	return 1;
}
CMD:torol(playerid,params[])
{
	DisablePlayerCheckpoint(playerid);
	SendClientMessage(playerid,-1, "(( Összes CP törölve ))");
	return 1;
}
CMD:uttisztitostart(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] ==1)
	{
		if(IsPlayerInAnyVehicle(playerid) && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] ==1 && GetVehicleModel(GetPlayerVehicleID(playerid)) == 574)
		{
			tisztit[playerid] = 1;
			new rand;
			rand = random(sizeof(tisztitocp));
			SetPlayerCheckpoint(playerid, tisztitocp[rand][0],tisztitocp[rand][1],tisztitocp[rand][2],5.0);
			SendClientMessage(playerid,-1, "(( Munkát megkezted! ))");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy úttisztító kocsiban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy úttisztító! ))");
	}
	return 1;
}
CMD:uttisztitostop(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] ==1)
	{
		if(IsPlayerInAnyVehicle(playerid) && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] ==1 && GetVehicleModel(GetPlayerVehicleID(playerid)) == 574)
		{
			tisztit[playerid] = 0;
			DisablePlayerCheckpoint(playerid);
			SendClientMessage(playerid,-1, "(( Munkát befejezted! ))");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy úttisztító kocsiban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy úttisztító! ))");
	}
	return 1;
}
CMD:funyirostart(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 5)
	{
		if(IsPlayerInAnyVehicle(playerid) && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] ==5 && GetVehicleModel(GetPlayerVehicleID(playerid)) == 572)
		{
			funyirasban[playerid] = true;
			funyirocpsz[playerid] = 0;
			SetPlayerCheckpoint(playerid, funyirocp[funyirocpsz[playerid]][0],funyirocp[funyirocpsz[playerid]][1],funyirocp[funyirocpsz[playerid]][2],5.0);
			funyirofu[playerid] = CreatePlayerObject(playerid, 822, funyirocp[funyirocpsz[playerid]][0],funyirocp[funyirocpsz[playerid]][1],funyirocp[funyirocpsz[playerid]][2], 0, 0, 96, 300.0);
			SendClientMessage(playerid,-1, "(( Munkát megkezted! ))");
			funyirocpsz[playerid]++;
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy fûnyíró kocsiban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy kertész! ))");
	}
	return 1;
}
CMD:lancfuresz(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 6)
	{
		if(IsPlayerInRangeOfPoint(playerid,25.0,-1632.728, -2243.010, 31.476))
		{
			if(lancfuresz[playerid] == false)
			{
				lancfuresz[playerid] = true;
				GivePlayerWeapon(playerid,9,1);
				SendClientMessage(playerid,-1,"(( Elkezheted a munkát! ))");
			}
			else
			{
				lancfuresz[playerid] = false;
				ResetPlayerWeapons(playerid);
				SendClientMessage(playerid,-1,"(( Befejezted a munkát! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy a faházban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy Favágó! ))");
	}
	return 1;
}
CMD:fafelpakol(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 6)
	{
		new veh;
		veh = GetClosestVehicle(playerid,3.0);
		if(VehicleInfo[veh][vmunka]==6 && veh > 0)
		{
			if(favagoszamlalo[playerid] == 200)
			{
				for(new i;i<4;i++)
				{
					if(pakolohely[veh][i] == false)
					{
						pakolohely[veh][i] = true;
						favagoszamlalo[playerid] = 0;
						pakoloobj[veh][i] = CreateObject(1271,0,0,0,0,0,0,0);
						if(i == 0)
						{	
							AttachObjectToVehicle(pakoloobj[veh][i],veh,-0.5,-0.85,0.30,0,0,0);
							SendClientMessage(playerid,-1,"(( Sikeresen felpakoltad! ))");
							return 1;
						}
						if(i == 1)
						{	
							AttachObjectToVehicle(pakoloobj[veh][i],veh,0.5,-0.85,0.30,0,0,0);
							SendClientMessage(playerid,-1,"(( Sikeresen felpakoltad! ))");
							return 1;
						}
						if(i == 2)
						{	
							AttachObjectToVehicle(pakoloobj[veh][i],veh,-0.5,-2,0.30,0,0,0);
							SendClientMessage(playerid,-1,"(( Sikeresen felpakoltad! ))");
							return 1;
						}
						if(i == 3)
						{	
							AttachObjectToVehicle(pakoloobj[veh][i],veh,0.5,-2,0.30,0,0,0);
							SendClientMessage(playerid,-1,"(( Sikeresen felpakoltad! ))");
							return 1;
						}
					}
				}
				SendClientMessage(playerid,-1,"(( Jármû tele van! ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs nállad elég fa! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem munkajármû! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy Favágó! ))");
	}
	return 1;
}
CMD:faelad(playerid,params[])
{
	
	if(PlayerInfo[playerid][pmunka] == 6)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			if(VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] == 6)
			{
				if(IsPlayerInRangeOfPoint(playerid,5,-1968.476, -2434.008, 30.621))
				{
					new db = 0;
					for(new i;i<4;i++)
					{
						if(pakolohely[GetPlayerVehicleID(playerid)][i] == true)
						{
							pakolohely[GetPlayerVehicleID(playerid)][i] = false;
							DestroyObject(pakoloobj[GetPlayerVehicleID(playerid)][i]);
							db++;
						}
					}
					PlayerInfo[playerid][pfizetes] += 1 * db;
					SendClientMessage(playerid,-1,"(( Sikeresen leatad a fát!))");
					SendClientMessage(playerid,-1,"(( Pénz hozzáadva a fizetéshez!))");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem vagy a kijelölt helyen! ))");
					SetPlayerCheckpoint(playerid,-1968.476, -2434.008, 30.621,5);
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem munkajármû!))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem ûlsz kocsiban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy Favágó! ))");
	}
	return 1;
}
CMD:novenyek(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 7)
	{
		new i = GetPlayerVirtualWorld(playerid);
		if(RaktarInfo[i][fajta] == 1)
		{
			ShowPlayerDialog(playerid,DIALOG_NOVENY1,DIALOG_STYLE_LIST,"Növények","Ültetés\nGondozás\nEltávolítás","OK","Mégse");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Itt nem tudsz növényeket ûltetni! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:respawn(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		new classid;
		new string[128];
		new string2[128];
		if(sscanf(params,"u",ids))return SendClientMessage(playerid,COLOR_GREEN,"(( /respawn (ID) ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			OnPlayerRequestClass( ids, classid );
			format(string,sizeof(string),"(( SCD: %s respawnolta %s ))",Nev(playerid),Nev(ids));
			format(string2,sizeof(string2),"(( Respawnoltad %s ))",Nev(ids));
			SendClientMessage(playerid,-1,string2);
			SendToAdmins(COLOR_RED, string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:nokorhaz(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids;
		if(sscanf(params,"u",ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Használat: /nokorhaz [ID] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID) ))");
		else if(PlayerInfo[ids][pborton] == 0 && PlayerInfo[ids][btipus] !=5)return SendClientMessage(playerid,COLOR_GREEN,"(( Játékos nincs kórházban ))");
		else
		{
			new string[128],string2[128];
			PlayerInfo[ids][pborton] = 0;
			PlayerInfo[ids][btipus] = 0;
			format(string,sizeof(string),"(( %s kiszedted a kórházból!))",Nev(ids));
			format(string2,sizeof(string2),"(( %s kiszedett a kórházból! ))",Nev(playerid));
			SendClientMessage(playerid,-1,string);
			SendClientMessage(ids,-1,string2);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:ruha(playerid,params[])
{
	new Float:x,Float:y,Float:z,Float:face;
	GetPlayerPos(playerid,x,y,z);
	GetPlayerFacingAngle(playerid,face);
	SetPlayerFacingAngle(playerid,face);
	SetPlayerCameraPos(playerid,x-2,y-2,z+1);
	SetPlayerCameraLookAt(playerid,x+2,y+2,z);
	TogglePlayerControllable(playerid,0);
	ruhatimer = SetTimerEx("ruhavalaszto",1,true,"u",playerid);
	ruhaskin[playerid] = 1;
	return 1;
}
CMD:sethp(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, Float:amount, string[64],string2[64];
		if(sscanf(params,"uf", ids,amount)) return SendClientMessage(playerid,-1,"(( Használat: /sethp [id/név] [érték] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else if(amount > 100)return SendClientMessage(playerid,COLOR_GREEN,"(( Nana csak 100! ))");
		else
		{	
			SetPlayerHealth(ids,amount);
			format(string,sizeof(string),"(( %s megváltoztatta az életed %.1f -ra/re ))", Nev(playerid), amount);
			format(string2,sizeof(string2),"(( Megváltoztattad %s életét %.1f -ra/re ))", Nev(ids), amount);
			SendClientMessage(playerid,-1,string2);
			SendClientMessage(ids,-1,string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:setarmour(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, Float:amount, string[64],string2[64];
		if(sscanf(params,"uf", ids,amount)) return SendClientMessage(playerid,-1,"(( Használat: /setarmour [id/név] [érték] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else if(amount > 100)return SendClientMessage(playerid,COLOR_GREEN,"(( Nana csak 100! ))");
		else
		{	
			SetPlayerArmour(ids,amount);
			format(string,sizeof(string),"(( %s megváltoztatta a pajzsodat %.1f -ra/re ))", Nev(playerid), amount);
			format(string2,sizeof(string2),"(( Megváltoztattad %s pajzsát %.1f -ra/re ))", Nev(ids), amount);
			SendClientMessage(playerid,-1,string2);
			SendClientMessage(ids,-1,string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:forcepayday(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		SendClientMessage(playerid,-1,"Fizetés elindítva!");
		new sql[256];
		mysql_format(mysql,sql,sizeof(sql),"SELECT `Id`,`frakcio`,`frang`,`fizetes`,`fiztipus`,`felfiz`,`fizszam`,`maonline` FROM `user`");
		mysql_tquery(mysql,sql,"fizetes");
		SendClientMessage(playerid,-1,"Fizetés befejezve!");
		
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:makeleader(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, amount;
		new string[64],string2[64];
		if(sscanf(params,"ud", ids,amount)) return SendClientMessage(playerid,-1,"(( Használat: /makeleader [id/név] [frakcioid] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			if(amount > 0)
			{
				PlayerInfo[ids][frakcio] = amount;
				PlayerInfo[ids][pleader] = 1;
				format(string,sizeof(string),"(( %s kinevezett a(z) %d ID frakicó vezetõjének))",Nev(playerid),amount);
				format(string2,sizeof(string2),"(( %s kinevezted a %d ID frakció vezetõjének ))",Nev(ids),amount);
				SendClientMessage(ids,-1,string);
				SendClientMessage(playerid,-1,string2);
				saveplayer(ids);
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
				mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
			}
			else
			{
				PlayerInfo[ids][frakcio] = 0;
				PlayerInfo[ids][pleader] = 0;
				PlayerInfo[ids][prang] = 1;
				format(string,sizeof(string),"(( %s eltávolított a vezetõi pozícióvól))",Nev(playerid));
				format(string2,sizeof(string2),"(( %s eltávolítotad a vezetõi pozícióból ))",Nev(ids));
				SendClientMessage(ids,-1,string);
				SendClientMessage(playerid,-1,string2);	
				saveplayer(ids);
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
				mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:setjob(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new ids, amount;
		if(sscanf(params,"ud", ids,amount)) return SendClientMessage(playerid,-1,"(( Használat: /setjob [id/név] [munka id] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			new string[128];
			PlayerInfo[ids][pmunka] = amount;
			format(string,sizeof(string),"(( %s átálította a munkádat %s -ra/re ))",Nev(playerid),munkanevek[PlayerInfo[ids][pmunka]][0]);
			SendClientMessage(ids,-1,string);
			SendClientMessage(playerid,-1,"(( Sikeresen átálítottad a munkáját! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:networkstatus(playerid,params[])
{
	new stats[400+1];
	GetNetworkStats(stats, sizeof(stats));
	ShowPlayerDialog(playerid, DIALOG_NETOWRK, DIALOG_STYLE_MSGBOX, "A szerver internet státusza:", stats, "Bezár", ""); 
	return 1;
}
CMD:freeze(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new ids;
		new string[128],string2[128];
		if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /freeze [id/név] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			if(Pfreeze[ids] ==0)
			{
				TogglePlayerControllable(ids,false);
				format(string,sizeof(string),"(( %s lefagyasztott!))",Nev(playerid));
				format(string2,sizeof(string2),"(( Lefagyasztottad %s ))",Nev(ids));
				SendClientMessage(ids,-1,string);
				SendClientMessage(playerid,-1,string2);
				Pfreeze[ids] =1;
			}
			else
			{
				TogglePlayerControllable(ids,true);
				format(string,sizeof(string),"(( %s kifagyasztott!))",Nev(playerid));
				format(string2,sizeof(string2),"(( Kifagyasztottad %s ))",Nev(ids));
				SendClientMessage(playerid,-1,string);
				SendClientMessage(ids,-1,string2);
				Pfreeze[ids] =0;
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:awep(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new string[128];
		new weaponss[13][2];
		new wepname[56];
		new ids;
		if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /awep [id/név] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		for (new i = 0; i < 13; i++)
		{
			GetPlayerWeaponData(ids, i, weaponss[i][0], weaponss[i][1]);
			if(weaponss[i][0] >0)
			{
				GetWeaponName(weaponss[i][0],wepname,sizeof(wepname));
				format(string,sizeof(string),"(( %s %d ))",wepname,weaponss[i][1]);
				SendClientMessage(playerid,-1,string);
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:mark(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new string[128];
		GetPlayerPos(playerid,markpos[playerid][0],markpos[playerid][1],markpos[playerid][2]);
		markpose[playerid][0] = GetPlayerVirtualWorld(playerid);
		markpose[playerid][1] = GetPlayerInterior(playerid);
		format(string,sizeof(string),"(( Mark lerakva X:%.3f ,Y:%.3f, Z:%.3f ))",markpos[playerid][0],markpos[playerid][1],markpos[playerid][2]);
		SendClientMessage(playerid,-1,string);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:gotomark(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		SetPlayerVirtualWorld(playerid,markpose[playerid][0]);
		SetPlayerInterior(playerid,markpose[playerid][1]);
		SetPlayerPos(playerid,markpos[playerid][0],markpos[playerid][1],markpos[playerid][2]);
		SendClientMessage(playerid,-1,"(( Mark-hoz gotozva ))");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:makeas(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new ids,string[61],string2[64];
		if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /makeas [id/név] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			ahelp[ids] = 1;
			format(string,sizeof(string),"(( %s kinevezett adminsegédnek! ))", Nev(playerid));
			format(string2,sizeof(string2),"(( SCD: %s kinevezte %s adminsegédnek ))",Nev(playerid),Nev(ids));
			SendClientMessage(ids,-1,string);
			SendToAdmins(COLOR_RED,string2);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:unas(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new ids,string[61],string2[64];
		if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /unas [id/név] ))");
		else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Hibás ID ))");
		else
		{
			ahelp[ids] = 0;
			format(string,sizeof(string),"(( %s elvette az adminsegédedet! ))", Nev(playerid));
			format(string2,sizeof(string2),"(( SCD: %s elvette %s adminsegédjét ))",Nev(playerid),Nev(ids));
			SendClientMessage(ids,-1,string);
			SendToAdmins(COLOR_RED,string2);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:getas(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new string[128];
		SendClientMessage(playerid,0x26adfcAA,"(( Adminsegédek ))");
		for(new i = 0; i < MAX_PLAYERS; i++)
		{
			if(IsPlayerConnected(i))
			{
				if(ahelp[i] ==1)
				{
					format(string, sizeof(string), "(( %s %d ))", Nev(i),i);
					SendClientMessage(playerid,0x26adfcAA,string);
				}
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:aszolg(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		if(aszolgban[playerid] ==0)
		{
			aszolgban[playerid] = 1;
			switch(PlayerInfo[playerid][Admin])
			{
				case 1:
				{
					new string[128];
					format(string,sizeof(string),"Adminszolgálatban\n Moderátor %s",Nev(playerid));
					aszolglabel[playerid] = Create3DTextLabel(string, 0x10decdAA, 30.0, 40.0, 50.0, 40.0, 0);
					Attach3DTextLabelToPlayer(aszolglabel[playerid], playerid, 0.0, 0.0, 0.3);
					SendClientMessage(playerid,-1,"(( Adminszolgálatba léptél! ))");
				}
				case 2 .. 4:
				{
					new string[128];
					format(string,sizeof(string),"Adminszolgálatban\n %d Admin %s",PlayerInfo[playerid][Admin]-1,Nev(playerid));
					aszolglabel[playerid] = Create3DTextLabel(string, 0x10decdAA, 30.0, 40.0, 50.0, 40.0, 0);
					Attach3DTextLabelToPlayer(aszolglabel[playerid], playerid, 0.0, 0.0, 0.3);
					SendClientMessage(playerid,-1,"(( Adminszolgálatba léptél! ))");
				}
				case 5:
				{
					new string[128];
					format(string,sizeof(string),"Adminszolgálatban\n Fõadmin %s",Nev(playerid));
					aszolglabel[playerid] = Create3DTextLabel(string, 0x10decdAA, 30.0, 40.0, 50.0, 40.0, 0);
					Attach3DTextLabelToPlayer(aszolglabel[playerid], playerid, 0.0, 0.0, 0.3);
					SendClientMessage(playerid,-1,"(( Adminszolgálatba léptél! ))");
				}
				case 1337:
				{
					new string[128];
					format(string,sizeof(string),"Adminszolgálatban\n Fejlesztõ %s",Nev(playerid));
					aszolglabel[playerid] = Create3DTextLabel(string, 0x10decdAA, 30.0, 40.0, 50.0, 40.0, 0);
					Attach3DTextLabelToPlayer(aszolglabel[playerid], playerid, 0.0, 0.0, 0.3);
					SendClientMessage(playerid,-1,"(( Adminszolgálatba léptél! ))");
				}
			}
		}
		else
		{
			Delete3DTextLabel(aszolglabel[playerid]);
			SendClientMessage(playerid,-1,"(( Kilépté az adminszolgálatból! ))");
			aszolgban[playerid] = 0;
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:harc(playerid,params[])
{
	new tipus[12];
	if(sscanf(params,"s[12]", tipus)) return SendClientMessage(playerid,-1,"(( Használat: /harc [normal/bob/kungfu/kneehead/grabkick/elbow] ))");
	else
	{
		if( strcmp( tipus, "normal", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_NORMAL);
			SendClientMessage(playerid,-1,"(( A harcod mostantól normál ))");
		}
		if( strcmp( tipus, "box", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_BOXING);
			SendClientMessage(playerid,-1,"(( A harcod mostantól box ))");
		}
		if( strcmp( tipus, "kungfu", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_KUNGFU);
			SendClientMessage(playerid,-1,"(( A harcod mostantól kungfu ))");
		}
		if( strcmp( tipus, "kneehead", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_KNEEHEAD);
			SendClientMessage(playerid,-1,"(( A harcod mostantól kneehead ))");
		}
		if( strcmp( tipus, "grabkick", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_GRABKICK);
			SendClientMessage(playerid,-1,"(( A harcod mostantól grabkick ))");
		}
		if( strcmp( tipus, "elbow", true ) == 0 )
		{
			SetPlayerFightingStyle(playerid,FIGHT_STYLE_ELBOW);
			SendClientMessage(playerid,-1,"(( A harcod mostantól elbow ))");
		}
	}
	return 1;
}
CMD:id(playerid,params[])
{
	new ids;
	new string[32];
	if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /ID [id/név] ))");
	else if(!IsPlayerConnected(ids))return SendClientMessage(playerid,COLOR_GREEN,"(( Nincs ilyen ID/NÉV ))");
	else
	{
		format(string,sizeof(string),"(( %s %d))",Nev(ids),ids);
		SendClientMessage(playerid,-1,string);
	}
	return 1;
}
CMD:asegit(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		new ids;
		new string2[64];
		new string[64];
		if(sscanf(params,"u", ids)) return SendClientMessage(playerid,-1,"(( Használat: /asegit [ID] ))");
		else
		{
			KillTimer(elverzest[playerid]);
			halalban[playerid] = false;
			halalk[playerid] = false;	
			animban[ids] = 0;
			ClearAnimations(ids);
			SetPlayerDrunkLevel(ids,0);
			beszorulva[playerid] = false;
			format(string,sizeof(string),"(( %s felsegítette %s))",Nev(playerid),Nev(ids));
			format(string2,sizeof(string2),"(( %s felsegített ))",Nev(playerid));
			SendClientMessage(ids,-1,string2);
			SendToAdmins(COLOR_GREEN,string);
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:elrak(playerid,params[])
{
	new msg[128],dialogstr[3000];
	new weaponss[13][2];
	new i2 =0;
	for (new i = 0; i < 13; i++)
	{
		GetPlayerWeaponData(playerid, i, weaponss[i][0], weaponss[i][1]);
	}
	for (new i = 0; i < 13; i++)
	{
		if(weaponss[i][0] > 0 && weaponss[i][1] > 0)
		{
			weapons[playerid][i2][0] = weaponss[i][0];
			weapons[playerid][i2][1] = weaponss[i][1];
			format(msg,128,"%s\t\t%d\n",fegyvernev[weapons[playerid][i2][0]-1],weapons[playerid][i2][1]);
			strcat(dialogstr,msg);
			i2++;
		}
	}
	ShowPlayerDialog(playerid,DIALOG_FELRAK1,DIALOG_STYLE_LIST,"Fegyverek",dialogstr,"Választ","Mégsem");
	return 1;
}
CMD:bank(playerid,params[])
{
	ShowPlayerDialog(playerid,DIALOG_BANK1,DIALOG_STYLE_LIST,"Bank","Belépés számlaszámmal\nSaját számlák\nSzámla létrehozása\nPénz küldése\nFizetés","OK","Mégse");
	return 1;
}
CMD:posmentes(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >=1337)
	{
		new tipus[128];
		new Float:x,Float:y,Float:z,Float:angle;
		new string[256];
		if(sscanf(params,"s[32]", tipus)) return SendClientMessage(playerid,-1,"(( Használat: /posmentes [sima/cp] ))");
		if( strcmp( tipus, "sima", true ) == 0 )
		{
			GetPlayerPos(playerid,x,y,z);
			GetPlayerFacingAngle(playerid,angle);
			format(string,sizeof(string),"{%.3f,%.3f,%.3f}",x,y,z,angle);
			PosLog(string);
		}
		if( strcmp( tipus, "cp", true ) == 0 )
		{
			GetPlayerPos(playerid,x,y,z);
			format(string,sizeof(string),"SetPlayerCheckpoint(playerid, %.3f, %.3f, %.3f, 3.0);",x,y,z);
			PosLog(string);
			SendClientMessage(playerid,-1,"(( CP pos mentve ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
/*CMD:mobil(playerid,params[])
{
	new query[256];
	new mszam;
	new string[128];
	new dlg[256];
	format(query,sizeof(query),"SELECT `telefonszam` FROM `mobil` WHERE `tulaj` = '%d'",PlayerInfo[playerid][dbid]);
	mysql_query(query);
	mysql_store_result();
	while(mysql_fetch_row(query))
	{
		sscanf(query, "p<|>d",mszam);
		format(string,sizeof(string),"\n%d",mszam);
		strcat(dlg,string);
		ShowPlayerDialog(playerid, DIALOG_MOBIL1, DIALOG_STYLE_LIST, "Mobil", dlg, "OK", "Mégse");
	}
	return 1;
}*/
CMD:felvesz(playerid,params[])
{
	if(csorogsz[playerid] ==1)
	{
		if(phivasba[playerid] ==0)
		{
			phivasba[playerid] =1;
			phivasba[PlayerInfo[playerid][kithivsz]] =1;
			csorogsz[playerid] =0;
			SendClientMessage(playerid,-1,"(( Felvetted ))");
			SendClientMessage(hivo[playerid],-1,"(( Felvették ))");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Már beszélsz valakivel! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem hív senki! ))");
	}
	return 1;
}
CMD:lerak(playerid,params[])
{
	if(phivasba[playerid] ==1)
	{
		phivasba[playerid] =0;
		phivasba[PlayerInfo[playerid][kithivsz]] =0;
		SendClientMessage(playerid,-1,"(( Leraktad ))");
		SendClientMessage(akihivo[playerid],-1,"(( Lerakták ))");
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem beszélsz senkivel! ))");
	}
	return 1;
}
CMD:giveitem(playerid,params[])
{
	new id,amount,item[128];
	if(sscanf(params,"us[128]i",id,item,amount)) return SendClientMessage(playerid,-1,"(( Használat: /giveitem [id/név] [ItemID] [Darabszám] ))");
	SendClientMessage(playerid,-1,"Megadva");
	return 1;
}
CMD:givekulcs(playerid,params[])
{
	new id,item[128];
	if(sscanf(params,"us[128]",id,item)) return SendClientMessage(playerid,-1,"(( Használat: /givekulcs [id/név] [rendszám] ))");
	format(item,sizeof(item),"Jármûkulcs %s",item);
	AddItem(id,item,1);
	SendClientMessage(playerid,-1,"Megadva");
	return 1;
}
CMD:raktar(playerid,params[])
{
	new tipus[128];
	if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /raktar [tartalom] [berak] [berendezes] [beszerel] ))");
	if( strcmp( tipus, "tartalom", true ) == 0 )
	{
		new string[256];
		new dlg[512];
		new sql[256];
		new menny,rows;
		mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `raktartartalom` WHERE `dbid` = '%d';",RaktarInfo[GetPlayerVirtualWorld(playerid)][dbid]);
		new Cache:res = mysql_query(mysql,sql);
		format(string,sizeof(string),"Név\tMennyiség\n");
		strcat(dlg,string);
		cache_get_row_count(rows);
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name(i,"nev",string,sizeof(string));
			cache_get_value_name_int(i,"mennyiseg",menny);
			format(string,sizeof(string),"%s\t%d\n",string,menny);
			strcat(dlg,string);
			
		}
		cache_delete(res);
		ShowPlayerDialog(playerid,DIALOG_RAKTAR8,DIALOG_STYLE_TABLIST_HEADERS,"Raktár",dlg,"OK","Mégse");
	}
	if( strcmp( tipus, "berak", true ) == 0 )
	{
		new string[256];
		new dlg[512];
		new sql[256];
		new menny,rows;
		mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `inventory` WHERE `dbid` = '%d';",PlayerInfo[playerid][dbid]);
		new Cache:res = mysql_query(mysql,sql);
		format(string,sizeof(string),"Név\tMennyiség\n");
		strcat(dlg,string);
		cache_get_row_count(rows);
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name(i,"nev",string,sizeof(string));
			cache_get_value_name_int(i,"mennyiseg",menny);
			format(string,sizeof(string),"%s\t%d\n",string,menny);
			strcat(dlg,string);
		}
		cache_delete(res);
		ShowPlayerDialog(playerid,DIALOG_RAKTAR10,DIALOG_STYLE_TABLIST_HEADERS,"Táska",dlg,"OK","Mégse");
	}
	return 1;
}
CMD:csomagtarto(playerid,params[])
{
	new tipus[128];
	if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /csomagtarto [nyit] [zár] [tartalom] [berak] ))");
	if( strcmp( tipus, "nyit", true ) == 0 )
	{
		new vehicleid = GetClosestVehicle(playerid,3.0);
		new engine,  lights,  alarm,  doors,  bonnet,  boot,  objective;
		GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
		if(doors==0)
		{
			if(boot==0)
			{
				SetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, 1, objective);
				SendClientMessage(playerid,-1,"(( Kinyitottad a csomagtartót ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Csomagtartó nyitva van! ))");
			}
		}
	}		
	if( strcmp( tipus, "zár", true ) == 0 )
	{
		new vehicleid = GetClosestVehicle(playerid,3.0);
		new engine, lights, alarm, doors, bonnet, boot, objective;
		GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
		if(doors ==0)
		{
			if(boot==1)
			{
				SetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, 0, objective);
				SendClientMessage(playerid,-1,"(( Bezártad a csomagtartót ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Csomagtartó zárva van! ))");
			}
		}
	}
	if( strcmp( tipus, "tartalom", true ) == 0 )
	{
		new vehicleid = GetClosestVehicle(playerid,3.0);
		new engine, lights, alarm, doors, bonnet, boot, objective;
		GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
		if(doors ==0)
		{
			if(boot==1)
			{
				pcsomag[playerid] = vehicleid;
				new string[256];
				new dlg[512];
				new sql[256];
				new menny,rows;
				mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `csomagtarto` WHERE `dbid` = '%d';",VehicleInfo[vehicleid][Id]);
				new Cache:res = mysql_query(mysql,sql);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"nev",string,sizeof(string));
					cache_get_value_name_int(i,"mennyiseg",menny);
					format(string,sizeof(string),"\n%s\t\t%d",string,menny);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid,DIALOG_CSOMAGTARTO1,DIALOG_STYLE_LIST,"Csomagtartó",dlg,"OK","Mégse");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Csomagtartó zárva van! ))");
			}
		}
	}	
	if( strcmp( tipus, "berak", true ) == 0 )
	{
		new vehicleid = GetClosestVehicle(playerid,3.0);
		pcsomag[playerid] = vehicleid;
		new string[256];
		new dlg[512];
		new sql[256];
		new menny,rows;
		mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `inventory` WHERE `dbid` = '%d';",PlayerInfo[playerid][dbid]);
		new Cache:res = mysql_query(mysql,sql);
		cache_get_row_count(rows);
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name(i,"nev",string,sizeof(string));
			cache_get_value_name_int(i,"mennyiseg",menny);
			format(string,sizeof(string),"\n%s\t\t%d",string,menny);
			strcat(dlg,string);
		}
		cache_delete(res);
		ShowPlayerDialog(playerid,DIALOG_CSOMAGTARTO3,DIALOG_STYLE_LIST,"Táska",dlg,"OK","Mégse");
	}
	return 1;
}
CMD:kesztyutarto(playerid,params[])
{
	if(IsPlayerInAnyVehicle(playerid))
	{	
		new tipus[128];
		if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /kesztyutarto [tartalom] [berak] ))");
		else
		{
			if( strcmp( tipus, "tartalom", true ) == 0 )
			{
				new v;
				v = GetPlayerVehicleID(playerid);
				pkesztyu[playerid] = v;
				new string[256];
				new dlg[512];
				new sql[256];
				new menny,rows;
				mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `kesztyutarto` WHERE `dbid` = '%d';",VehicleInfo[v][Id]);
				new Cache:res = mysql_query(mysql,sql);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"nev",string,sizeof(string));
					cache_get_value_name_int(i,"mennyiseg",menny);
					format(string,sizeof(string),"\n%s\t\t%d",string,menny);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid,DIALOG_KESZTYUTARTO1,DIALOG_STYLE_LIST,"Kesztyûtartó",dlg,"OK","Mégse");
			}
			if( strcmp( tipus, "berak", true ) == 0 )
			{
				new vehicleid = GetPlayerVehicleID(playerid);
				pkesztyu[playerid] = vehicleid;
				new string[256];
				new dlg[512];
				new sql[256];
				new menny,rows;
				mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `inventory` WHERE `dbid` = '%d';",PlayerInfo[playerid][dbid]);
				new Cache:res = mysql_query(mysql,sql);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"nev",string,sizeof(string));
					cache_get_value_name_int(i,"mennyiseg",menny);
					format(string,sizeof(string),"\n%s\t\t%d",string,menny);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid,DIALOG_KESZTYUTARTO3,DIALOG_STYLE_LIST,"Táska",dlg,"OK","Mégse");
			}
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban ! ))");
	}
	return 1;
}
CMD:taska(playerid,params[])
{
	new string[256];
	new dlg[512];
	new sql[256];
	new menny,rows;
	mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `inventory` WHERE `dbid` = '%d';",PlayerInfo[playerid][dbid]);
	new Cache:res = mysql_query(mysql,sql);
	cache_get_row_count(rows);
	format(string,sizeof(string),"Név\tMennyiség\n");
	strcat(dlg,string);
	for(new i=0;i<rows;i++)
	{
		cache_get_value_name(i,"nev",string,sizeof(string));
		cache_get_value_name_int(i,"mennyiseg",menny);
		format(string,sizeof(string),"%s\t%d\n",string,menny);
		strcat(dlg,string);
	}
	cache_delete(res);
	ShowPlayerDialog(playerid,DIALOG_TASKA1,DIALOG_STYLE_TABLIST_HEADERS,"Táska",dlg,"OK","Mégse");
	return 1;
}
CMD:vasarol(playerid,params[])
{
	if(playerinbolt[playerid] == true)
	{
		new art,menny,rows;
		new sql[256];
		new dlg[512];
		new string[128];
		mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`ar`,`mennyiseg` FROM `termekek` WHERE `dbid` = '%d' ORDER BY `nev` ASC",BizzInfo[GetPlayerVirtualWorld(playerid)][Id]);
		new Cache:res = mysql_query(mysql,sql);
		format(string,sizeof(string),"Név\tÁr\tMennyiség\n",string,menny);
		strcat(dlg,string);
		cache_get_row_count(rows);
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name(i,"nev",string,sizeof(string));
			cache_get_value_name_int(i,"ar",art);
			cache_get_value_name_int(i,"mennyiseg",menny);
			format(string,sizeof(string),"%s\t%s\t%d\n",string,cformat(art),menny);
			strcat(dlg,string);
		}
		cache_delete(res);
		ShowPlayerDialog(playerid,DIALOG_VASAROL,DIALOG_STYLE_TABLIST_HEADERS,"Táska",dlg,"OK","Mégse");
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy boltban! ))");
	}
	return 1;
}
CMD:bemutatkoz(playerid,params[])
{
	new sql[128];
	new ids = GetClosestPlayer(playerid);
	if(playerid == ids) return 1;
	if(ProxDetectorS(2.0,playerid,ids))
	{
		for(new bari;bari<MAX_BARAT;bari++)
		{
			if(barat[ids][bari] == PlayerInfo[playerid][dbid])
			{
				return SendClientMessage(playerid,-1,"(( Már bemutatkoztál neki! ))");
			}
		}
		mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `barat`(ki,kinek) VALUES ('%d','%d')",PlayerInfo[ids][dbid],PlayerInfo[playerid][dbid]);
		new Cache:res = mysql_query(mysql,sql);
		cache_delete(res);
		SendClientMessage(playerid,-1,"(( Sikeresen bemutatkoztál! ))");
		mysql_format(mysql,sql,sizeof(sql),"SELECT `kinek` FROM `barat` WHERE `ki` = '%d'",PlayerInfo[playerid][dbid]);
		mysql_tquery(mysql, sql, "LoadFriend", "d", playerid);
		mysql_format(mysql,sql,sizeof(sql),"SELECT `kinek` FROM `barat` WHERE `ki` = '%d'",PlayerInfo[ids][dbid]);
		mysql_tquery(mysql, sql, "LoadFriend", "d", ids);
		OnPlayerStreamIn(ids,playerid);
	}
	return 1;
}
CMD:ado(playerid,params[])
{
	new tipus[128];
	new osszeg;
	if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /ado [befizet] [info] ))");
	else if( strcmp( tipus, "befizet", true,7 ) == 0 )
	{
		if(sscanf(params,"s[128]d",tipus,osszeg)) return SendClientMessage(playerid,-1,"(( Használat: /ado [befizet] [összeg]");
		else
		{
			if(PlayerInfo[playerid][ado] >= osszeg)
			{
				if(PlayerInfo[playerid][Money]-osszeg >= 0)
				{
					new string[256];
					PlayerInfo[playerid][ado] -= osszeg;
					PlayerInfo[playerid][Money] -= osszeg;
					allamibevetel(osszeg);
					format(string,sizeof(string),"(( %d adótartozást kifizettél maradt %d ))",osszeg,PlayerInfo[playerid][ado]);
					SendClientMessage(playerid,-1,string);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs ennyi adótartozásod ))");
			}
		}
	}
	else if( strcmp( tipus, "info", true ) == 0 )
	{
		new string[256];
		format(string,sizeof(string),"(( Jelenleg neked %d adótartozásod van ))",PlayerInfo[playerid][ado]);
		SendClientMessage(playerid,-1,string);
	}
	return 1;
}
CMD:adozas(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 10)
	{
		ShowPlayerDialog(playerid,DIALOG_ONKORMANYZAT1,DIALOG_STYLE_LIST,"Adózási rendszer","Ingatlan\nJármû\nIpar\nÁFA\nSZJA","OK","Mégse");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:gotosf(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		SetPlayerPos(playerid,-2009 ,150, 28);
		SetPlayerVirtualWorld(playerid,0);
		SetPlayerInterior(playerid,0);
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:fuelcar(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			VehicleInfo[GetPlayerVehicleID(playerid)][uzemanyag] = vehdata[GetVehicleModel(GetPlayerVehicleID(playerid))-400][0];
			SendClientMessage(playerid,-1,"(( Üzemanyag feltöltve ))");
		}
		else
		{
			SendClientMessage(playerid,COLOR_RED,"(( Nem ülsz kocsiban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:gov(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] > 0 && PlayerInfo[playerid][ftipus] == 1)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(isnull(params)) return SendClientMessage(playerid,-1,"(( Használat: /gov [szöveg] ))");
			new string[256];
			switch(PlayerInfo[playerid][frakcio])
			{
				case 1 .. 5:
				{
					format(string,sizeof(string),"%s %s %s: %s",PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],Nev(playerid),params);
					SendClientMessageToAll(COLOR_BLUE, "|____________ Rendõrségi felhívás _____________|");
					SendClientMessageToAll(COLOR_BLUE, string);
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy legális szervezet tagja! ))");
	}
	return 1;
}
CMD:atvesz(playerid,params[])
{
	new rendszam[64];
	if(sscanf(params, "s[64]", rendszam))return SendClientMessage(playerid, -1, "(( Használat: /atvesz [rendszám] ))");
	new i;
	new bool:talal;
	while(i<MAX_VEHICLES && talal == false)
	{
		if(CompareEx(VehicleInfo[i][Plate],rendszam))
		{
			talal = true;
		}
		else
		{
			talal = false;
			i++;
		}
	}
	if(talal == true)
	{
		if(VehicleInfo[i][lefoglalva] == 2)
		{
			new Float:tav,Float:x,Float:y,Float:z;
			GetPlayerPos(playerid,x,y,z);
			tav = GetVehicleDistanceFromPoint(i,x,y,z);
			if(tav < 6.0)
			{
				if(VehicleInfo[i][Owner] == PlayerInfo[playerid][dbid] || PlayerInfo[playerid][frakcio] == VehicleInfo[i][vfrakcio] || (VehicleInfo[i][vfrakcio] == 1 && PlayerInfo[playerid][frakcio] < 6))
				{
					new string[128];
					format(string,sizeof(string),"Szerelés teljes költsége: %d FT !",VehicleInfo[i][ktartozas]);
					atveszkocsid[playerid] = i;
					ShowPlayerDialog(playerid,DIALOG_KOCSIATVESZ,DIALOG_STYLE_MSGBOX,"Szerelési lap",string,"Átvesz","Mégse");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Túl messze vagy a kocsitól! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nincs szerelés alatt! ))");
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Rendszám nem található az adatbázisban! ))");
	}
	return 1;
}
CMD:teles(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1)
	{
		new string[256];
		new dlg[512];
		new sql[256];
		new rows;
		mysql_format(mysql,sql,sizeof(sql),"SELECT `nev` FROM `teleport`");
		new Cache:res = mysql_query(mysql,sql);
		cache_get_row_count(rows);
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name(i,"nev",string,sizeof(string));
			format(string,sizeof(string),"\n%s",string);
			strcat(dlg,string);
		}
		cache_delete(res);
		ShowPlayerDialog(playerid,DIALOG_TELEPORT,DIALOG_STYLE_LIST,"Teleport",dlg,"OK","Mégse");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:teleadd(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		new string[256];
		new sql[256];
		if(sscanf(params,"s[256]",string)) return SendClientMessage(playerid,-1,"(( Használat: /teleadd [név] ))");
		else
		{
			new Float:x,Float:y,Float:z;
			new tint,tvw;
			GetPlayerPos(playerid,x,y,z);
			tvw = GetPlayerVirtualWorld(playerid);
			tint = GetPlayerInterior(playerid);
			mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `teleport`(`nev`,`x`,`y`,`z`,`vw`,`int`) VALUES ('%s','%f','%f','%f','%d','%d')",string,x,y,z,tvw,tint);
			new Cache:res = mysql_query(mysql,sql);
			cache_delete(res);
			SendClientMessage(playerid,-1,"(( Teleport elmentve! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:nyit(playerid,params[])
{
	for(new i; i<MAX_MOVEOB;i++)
	{
		if(IsPlayerInRangeOfPoint(playerid,7.0,KapuInfo[i][kx],KapuInfo[i][ky],KapuInfo[i][kz]))
		{
			if(KapuInfo[i][rnyitva] == false)
			{
				if(KapuInfo[i][kfrakcio] == 0 || (KapuInfo[i][kfrakcio] == PlayerInfo[playerid][frakcio]))
				{
					//SetDynamicObjectRot(KapuInfo[i][kid], KapuInfo[i][knx], KapuInfo[i][kny], KapuInfo[i][knz]);
					MoveDynamicObject(KapuInfo[i][kid],KapuInfo[i][kx],KapuInfo[i][ky],KapuInfo[i][kz]+0.0001, 0.0001, KapuInfo[i][knx], KapuInfo[i][kny], KapuInfo[i][knz]);
					KapuInfo[i][rnyitva] = true;
					return 1;
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem nyithatod! ))");
					return 1;
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Már nyitva! ))");
				return 1;
			}
		}
	}
	return 1;
}
CMD:zar(playerid,params[])
{
	for(new i; i<MAX_MOVEOB;i++)
	{
		if(IsPlayerInRangeOfPoint(playerid,7.0,KapuInfo[i][kx],KapuInfo[i][ky],KapuInfo[i][kz]))
		{
			if(KapuInfo[i][rnyitva] == true)
			{
				MoveDynamicObject(KapuInfo[i][kid],KapuInfo[i][kx],KapuInfo[i][ky],KapuInfo[i][kz]-0.0001, 0.0001, KapuInfo[i][rkx], KapuInfo[i][rky], KapuInfo[i][rkz]);
				//SetDynamicObjectRot(KapuInfo[i][kid], KapuInfo[i][rkx], KapuInfo[i][rky], KapuInfo[i][rkz]);
				KapuInfo[i][rnyitva] = false;
				return 1;
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Már zárva! ))");
				return 1;
			}
		}
	}
	return 1;
}
CMD:ceg(playerid,params[])
{
	if(PlayerInfo[playerid][cegleader] == 1)
	{
		ShowPlayerDialog(playerid,DIALOG_CEG1,DIALOG_STYLE_LIST,"Cég","Felvétel\nAlkalmazottak\nFizetés","OK","Mégse");
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:cegkocsi(playerid,params[])
{
	if(PlayerInfo[playerid][cegleader] == 1)
	{
		new plate[64];
		new i = 1;
		new bool:talal;
		if(sscanf(params,"s[64]",plate)) return SendClientMessage(playerid,-1,"(( Használat: /cegkocsi [Rendszám]");
		else
		{
			while(i < MAX_VEHICLES && talal == false)
			{
				if(CompareEx(plate,VehicleInfo[i][Plate]))
				{
					talal = true;
				}
				else
				{
					talal = false;
					i++;
				}
			}
			if(talal == true)
			{
				if(VehicleInfo[i][Owner] == PlayerInfo[playerid][dbid])
				{
					if(VehicleInfo[i][vceg] == 0)
					{
						VehicleInfo[i][vceg] = PlayerInfo[playerid][pceg];
						SendClientMessage(playerid,-1,"(( Jármû átállítva céges jármûre! ))");
					}
					else
					{
						VehicleInfo[i][vceg] = 0;
						SendClientMessage(playerid,-1,"(( Jármû átállítva normál jármûre! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Ez nem a te kocsid! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs ilyen rendszám! ))");
			}
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:meroszalag(playerid,params[])
{
	if(PlayerInfo[playerid][Admin] > 1)
	{
		new Float:x,Float:y,Float:z,Float:distant;
		if(sscanf(params,"fff",x,y,z)) return SendClientMessage(playerid,-1,"(( Használat: /meroszalag [x] [y] [z]");
		else
		{
			new string[128];
			distant = GetPlayerDistanceFromPoint(playerid,x,y,z);
			format(string,sizeof(string),"(( %0.2f méterre vagy kijejölt helytõl! ))",distant);
			SendClientMessage(playerid,-1,string);
		}
	}
	else
	{
		return 1;
	}
	return 1;
}
CMD:kamion(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 2)
	{
		new tipus[128];
		if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /kamion [felpakol] [lepakol] [feltölt] [leereszt] [lecsatol] ");
		else
		{
			if( strcmp( tipus, "felpakol", true,8 ) == 0 )
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 514 || GetVehicleModel(GetPlayerVehicleID(playerid)) == 403)
				{
					if(IsPlayerInRangeOfPoint(playerid,10.0,2401.668, -2540.585, 13.354))
					{
						if(IsTrailerAttachedToVehicle(GetPlayerVehicleID(playerid)))
						{
							ShowPlayerDialog(playerid,DIALOG_KAMION1,DIALOG_STYLE_LIST,"Fuvarozó","Romlandó\nNem romlandó","OK","Mégse");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs csatlakoztatva az utánfutó ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nem vagy rakodó helyen! ))");
						SetPlayerCheckpoint(playerid,2401.668, -2540.585, 13.354,5.0);
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ûlsz kamionban! ))");
				}
			}
			if( strcmp( tipus, "lepakol", true,7 ) == 0 )
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 514)
				{
					if(IsTrailerAttachedToVehicle(GetPlayerVehicleID(playerid)))
					{
						new vehid;
						vehid = GetVehicleTrailer(GetPlayerVehicleID(playerid));
						if(kfelpakolva[vehid] == true)
						{
							if(IsPlayerInRangeOfPoint(playerid,10.0,-520.608 ,-500.630, 25.000))
							{
								SetTimerEx("kamionlepakol",60000,false,"ud",playerid,1);
								GameTextForPlayer(playerid,"Lerakodás folyamatban ...",60000,4);
								TogglePlayerControllable(playerid,false);
							}
							else if(IsPlayerInRangeOfPoint(playerid,10.0,2346.506,2754.582,10.820))
							{
								SetTimerEx("kamionlepakol",60000,false,"ud",playerid,2);
								GameTextForPlayer(playerid,"Lerakodás folyamatban ...",60000,4);
								TogglePlayerControllable(playerid,false);
							}
							else if(IsPlayerInRangeOfPoint(playerid,10.0, -1733.823, 187.411 ,3.259))
							{
								SetTimerEx("kamionlepakol",60000,false,"ud",playerid,3);
								GameTextForPlayer(playerid,"Lerakodás folyamatban ...",60000,4);
								TogglePlayerControllable(playerid,false);
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Nem vagy kirakodó helyen! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Pótkocsi nincs felpakolva! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Pótkocsi nincs csatlakoztatva! ))");
					}
				}
			}
			if( strcmp( tipus, "feltolt", true,7 ) == 0 )
			{
				
			}
			if( strcmp( tipus, "leereszt", true,8 ) == 0 )
			{
				
			}
			if( strcmp( tipus, "lecsatol", true,8 ) == 0 )
			{
				DetachTrailerFromVehicle(GetPlayerVehicleID(playerid));
			}
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy kamionos! ))");
	}
	return 1;
}
CMD:pizzafutar(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 4)
	{

		new tipus[128];
		if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /pizzafutar [ruha] [munka] [lead] [vesz] ))");
		else
		{
			if( strcmp( tipus, "ruha", true,4 ) == 0 )
			{
				if(GetPlayerSkin(playerid) != 155)
				{
					if(IsPlayerInRangeOfPoint(playerid,20,373.947, -127.631, 1001.499))
					{
						SetPlayerSkin(playerid,155);
						Cselekves(playerid, "átöltözik.");
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Itt nem tudsz átõltözni! ))");
					}
				}
				else
				{
					if(IsPlayerInRangeOfPoint(playerid,20,373.947, -127.631, 1001.499))
					{
						SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
						Cselekves(playerid, "átöltözik.");
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Itt nem tudsz átõltözni! ))");
					}
				}
			}
			if( strcmp( tipus, "munka", true,5 ) == 0 )
			{
				if(GetPlayerSkin(playerid) == 155)
				{
					if(pizzamunka[playerid] == false)
					{
						if(IsPlayerInAnyVehicle(playerid))
						{
							if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 448)
							{
								new bool:talal = false;
								new rand;
								while(talal ==false)
								{
									rand = random(MAX_HOUSE);
									if(HouseInfo[rand][hvan] == true)
									{
										talal = true;
									}
									else
									{
										talal = false;
									}
								}
								pizzamunka[playerid] = true;								
								SetPlayerCheckpoint(playerid, HouseInfo[rand][px],HouseInfo[rand][py],HouseInfo[rand][pz],5.0);
								SendClientMessage(playerid,-1, "(( Munkát megkezted! ))");
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Nem vagy motoron! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nem ûlsz kocsiban! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Már dolgozol! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem öltöztél át! ))");
				}
			}
			if( strcmp( tipus, "lead", true,4 ) == 0 )
			{
				if(GetPlayerSkin(playerid) == 155)
				{
					if(pizzamunka[playerid] == true)
					{
						if(IsPlayerInCheckpoint(playerid))
						{
							pizzamunka[playerid] = false;
							new str[128];
							new rand;
							rand = random(1500) + 1000;
							PlayerInfo[playerid][pfizetes] += rand;
							DisablePlayerCheckpoint(playerid);
							format(str,sizeof(str),"(( %d FT hozzáadva a fizetésedhez! ))",rand);
							SendClientMessage(playerid,-1,str);
							Cselekves(playerid, "leadta a pizzát.");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nem vagy a checkpoint-ban !))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Még nem dolgozol! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem öltöztél át! ))");
				}
			}
			if( strcmp( tipus, "vesz", true,4 ) == 0 )
			{
				if(GetPlayerSkin(playerid) == 155)
				{
					if(IsPlayerInAnyVehicle(playerid))
					{
						if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 448)
						{
							if(PlayerInfo[playerid][Money] >= 1800)
							{
								PlayerInfo[playerid][Money] -=1800;
								Cselekves(playerid, "vásárolt egy pizzát.");
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nem ûlsz motoron! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nem vagy jármûben! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem öltöztél át! ))");
				}
			}
		}
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem vagy pizzafutár! ))");
	}
	return 1;
}
//fegyvercsempesz
CMD:dobozki(playerid,params[])
{
	if(PlayerInfo[playerid][pmunka] == 3)
	{
		if(dobozpakolhato == true)
		{
			if(IsPlayerInRangeOfPoint(playerid,5.0,fegyverspawn[dobozspawnid][0],fegyverspawn[dobozspawnid][1],fegyverspawn[dobozspawnid][2]))
			{
				SetTimerEx("dobozkipakol",2500,false,"u",playerid);
				ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,1,0,0,1,2500,1);
				GameTextForPlayer(playerid,"Dobozok kipakolás",2500 * 5,4);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy a dobozoknál! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Még nem lehet dobozt kipakolni! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
//Rendõr parancsok
CMD:csipogo(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(ExistItem(playerid,"Csipogo",1))
			{
				new string[256];
				if(csipogo[playerid] == false)
				{
					csipogo[playerid] = true;
					for(new i;i<MAX_PLAYERS;i++)
					{
						if(IsPlayerCop(playerid))
						{
							SetPlayerMarkerForPlayer(i,playerid,COLOR_BLUE);
						}
					}
					SendClientMessage(playerid,-1,"(( Csipogó bekapcsolva! ))");
					format(string,sizeof(string), "[rádió] %s %s %s: CSIPOGÓJA BEKAPCSOLT! ,vége.", PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],Nev(playerid));
					SendToRadio(playerid,0xff8400AA,string);
				}
				else
				{
					csipogo[playerid] = false;
					for(new i;i<MAX_PLAYERS;i++)
					{
						if(IsPlayerCop(playerid))
						{
							SetPlayerMarkerForPlayer(i,playerid,0xFFFFFF00);
						}
					}
					format(string,sizeof(string), "[rádió] %s %s %s: CSIPOGÓJA KIKAPCSOLT! ,vége.", PlayerInfo[playerid][pfrakcio],PlayerInfo[playerid][prang],Nev(playerid));
					SendToRadio(playerid,0xff8400AA,string);
					SendClientMessage(playerid,-1,"(( Csipogó kikapcsolva! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs nállad csipogó! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:bilincsel(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(ExistItem(playerid,"Bilincs",1))
			{
				if(!IsPlayerInAnyVehicle(playerid))
				{
					new ids = GetClosestPlayer(playerid);
					if(playerid == ids) return 1;
					if(ProxDetectorS(3.0,playerid,ids))
					{
						if(bilincsben[ids] ==0)
						{
							bilincsben[ids] =1;
							SetPlayerSpecialAction(ids,SPECIAL_ACTION_CUFFED);
							RemoveItem(playerid,"Bilincs",0,1);
							SendClientMessage(ids,-1,"(( Megbilincseltek ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Már megvan bilincselve! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Kocsiban nem lehet! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs bilincsed ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:visz(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new ids = GetClosestPlayer(playerid);
			if(playerid == ids) return 1;
			if(ProxDetectorS(3.0,playerid,ids))
			{
				if(bilincsben[ids] ==1)
				{
					if(pvisz[ids]==0)
					{
						pvisz[ids]= 1;
						kivisz[ids] = playerid;
						SendClientMessage(playerid,-1,"(( Viszed ))");
						SendClientMessage(ids,-1,"(( Valaki visz téged ))");
					}
					else if(pvisz[ids]==1 && kivisz[ids] == playerid)
					{
						pvisz[ids]= 0;
						SendClientMessage(playerid,-1,"(( Már nem viszed! ))");
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Már viszi valaki! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs megbilincselve ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:elenged(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new ids = GetClosestPlayer(playerid);
			if(playerid == ids) return 1;
			if(ProxDetectorS(3.0,playerid,ids))
			{
				if(bilincsben[ids] ==1)
				{
					pvisz[ids]= 0;
					bilincsben[ids] =0;
					RemovePlayerFromVehicle(ids);
					SetPlayerSpecialAction(ids,SPECIAL_ACTION_NONE);
					TogglePlayerControllable(ids,1);
					SendClientMessage(ids,-1,"(( Elengedtek ))");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs megbilincselve ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:beultet(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new ids = GetClosestPlayer(playerid);
			if(playerid == ids) return 1;
			if(ProxDetectorS(3.0,playerid,ids))
			{
				if(bilincsben[ids] ==1)
				{
					if(pvisz[ids]==0)
					{
						new vehicleid = GetClosestVehicle(playerid,3.0);
						if(vehicleid != -1)
						{
							new hely;
							if(sscanf(params,"d",hely)) return SendClientMessage(playerid,-1,"(( Használat: /beultet [1 Bal hátsó] [2 Hobb hátsó] ))");
							else if(hely > 2) return SendClientMessage(playerid,-1,"(( Használat: /beultet [1 Bal hátsó] [2 Jobb hátsó] ))");
							else if(hely < 1) return SendClientMessage(playerid,-1,"(( Használat: /beultet [1 Bal hátsó] [2 Jobb hátsó] ))");
							else
							{
								if(hely == 1)
								{
									PutPlayerInVehicle(ids,vehicleid,2);
									TogglePlayerControllable(ids,0);
								}
								if(hely == 2)
								{
									PutPlayerInVehicle(ids,vehicleid,3);
									TogglePlayerControllable(ids,0);
								}
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs kocsi a közeledben! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Elõszõr engedd el (/visz) ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs megbilincselve ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:sokkolo(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(sokkolo[playerid] ==0 && sokkolop[playerid] ==0)
			{
				sokkolo[playerid] =1;
				sokkolop[playerid] =0;
				SetPlayerAttachedObject(playerid, 0, 18642, 6, 0.06, 0.01, 0.08, 180.0, 0.0, 0.0);
				SendClientMessage(playerid,-1,"(( Sokkolót elõveted ))");
			}
			else
			{
				sokkolo[playerid] =0;
				sokkolop[playerid] =0;
				SendClientMessage(playerid,-1,"(( Sokkolót elraktad ))");
				RemovePlayerAttachedObject(playerid,0);
				SetPlayerAmmo(playerid,2,0);
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:sokkolopisztoly(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(sokkolo[playerid] ==1)
			{
				if(sokkolo[playerid] ==1 && sokkolop[playerid] ==0)
				{
					sokkolop[playerid] =1;
					GivePlayerWeapon(playerid,22,2);
					SetPlayerSkillLevel(playerid,WEAPONSKILL_PISTOL,500);
					SendClientMessage(playerid,-1,"(( Sokkolót elõveted ))");
				}
				else
				{
					sokkolo[playerid] =0;
					sokkolop[playerid] =0;
					SendClientMessage(playerid,-1,"(( Sokkolót elraktad ))");
					RemovePlayerAttachedObject(playerid,0);
					SetPlayerAmmo(playerid,2,0);
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"((Nem vetted elõ a sokkolót!))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:blokadlerak(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new tipus;
			new Float:x,Float:y,Float:z,Float:face;
			GetPlayerPos(playerid,x,y,z);
			GetPlayerFacingAngle(playerid,face);
			if(sscanf(params,"d",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /blokadlerak 1[Kicsi útzár] 2[Közepes útzár] 3[Nagy útzár] 4[Útelterelés Jelzés] 5[Figyelmeztetõ tábla] 6[Sáv lezárás jelzés]  ))");
			else
			{
				if(tipus ==1)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(1459, x, y, z-0.9, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else if(tipus ==2)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(978, x, y, z-0.9+0.6, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else if(tipus ==3)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(981, x, y, z-0.9+0.9, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else if(tipus ==4)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(1238, x, y, z-0.9+0.2, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else if(tipus ==5)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(1425, x, y, z-0.9+0.6, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else if(tipus ==6)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] ==0)
						{
							utzar[i] = CreateDynamicObject(3091, x, y, z-0.9+0.5, 0, 0, face);
							SendClientMessage(playerid,-1,"(( Blokád lerakva! ))");
							return 1;
						}
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Hibás típus! ))");
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:blokadtorol(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new tipus;
			new bool:talal = false;
			new Float:x,Float:y,Float:z,Float:face;
			new Float:ox,Float:oy,Float:oz;
			GetPlayerPos(playerid,x,y,z);
			GetPlayerFacingAngle(playerid,face);
			if(sscanf(params,"d",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /blokadtorol 1[Közelben] 2[Mindegyik] ))");
			else
			{
				if(tipus ==1)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						if(utzar[i] !=0)
						{
							GetDynamicObjectPos(utzar[i], ox, oy, oz);
							if(IsPlayerInRangeOfPoint(playerid,3,ox,oy,oz))
							{
								DestroyDynamicObject(utzar[i]);
								utzar[i] = 0;
								SendClientMessage(playerid,-1,"(( Blokád törölve! ))");
								talal = true;
								return 1;
							}
						}
					}
					if(talal == false)
					{
						SendClientMessage(playerid,-1,"(( Nincs közeledben blokád! ))");
					}
				}
				else if(tipus ==2)
				{
					for(new i=0;i<MAX_UTZAR;i++)
					{
						DestroyDynamicObject(utzar[i]);
						utzar[i] = 0;
					}
					SendClientMessage(playerid,-1,"(( Össze blokád törölve! ))");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Hibás típus! ))");
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:itelet(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInRangeOfPoint(playerid,2,226.623,114.437,999.015))
			{
				new ido,penz,id;
				new indok[128];
				new string[128];
				new sql[256];
				new idostr[128];
				if(sscanf(params,"udds[64]",id,ido,penz,indok)) return SendClientMessage(playerid,-1,"(( Használat: /itelet ID Idõ(perc), Óvadék/Pénzbírság, Indok ))");
				else
				{
					if(penz < 250000)
					{
						PlayerInfo[id][btipus] = 1;
						PlayerInfo[id][pborton] = ido * 60;
						if(PlayerInfo[id][Money] >= penz)
						{
							PlayerInfo[id][Money] -= penz;
						}
						else
						{
							PlayerInfo[id][ado] += penz;
						}
						bortonbe(id);
						format(string,sizeof(string),"(( %s berakott %d percre a börtönbe, óvadék/pénzbírság: %d indok: %s ))",Nev(playerid),ido,penz,indok);
						SendClientMessage(id,-1,string);
						SendClientMessage(playerid,-1,"(( Sikeresen beraktad a börtönbe ))");
						new Hour, Minute, Second;
						new Year, Month, Day;
						gettime(Hour, Minute, Second);
						getdate(Year, Month, Day);
						format(idostr,sizeof(idostr),"%02d-%02d-%d %02d:%02d:%02d",Year,Month,Day,Hour,Minute,Second);
						mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `lecsuknaplo`(`ki`, `kit`, `mennyire`, `birsag`, `indok`, `mikor`) VALUES ('%s','%s','%d','%d','%s','%s')",Nev(playerid),Nev(id),ido,penz,indok,idostr);
						new Cache:res = mysql_query(mysql,sql);
						cache_delete(res);
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Maximum 250.000FT ! ))");
					}
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy a börtön közelében! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:trafipax(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{	
			if(IsPlayerInAnyVehicle(playerid))
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 596 || GetVehicleModel(GetPlayerVehicleID(playerid)) ==597 ||GetVehicleModel(GetPlayerVehicleID(playerid)) ==598)
				{
					new engine, lights, alarm, doors, bonnet, boot, objective;
					GetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, lights, alarm, doors, bonnet, boot, objective);
					if(engine == 0)
					{
						new tipus[64];
						new limit;
						if(sscanf(params,"s[64]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /trafipax [be] [ki] [limit] ))");
						else
						{
							if( strcmp( tipus, "be", true ) == 0 )
							{
								if(PlayerInfo[playerid][trafilim] != 0)
								{
									if(PlayerInfo[playerid][trafipaxo] == false)
									{
										new Float:x,Float:y,Float:z;
										GetPlayerPos(playerid,x,y,z);
										GetXYInFrontOfPlayer(playerid, x, y, 20);
										PlayerInfo[playerid][trx] = x;
										PlayerInfo[playerid][try] = y;
										PlayerInfo[playerid][trz] = z;
										PlayerInfo[playerid][trafipaxo] = true;
										SetPlayerCheckpoint(playerid,PlayerInfo[playerid][trx],PlayerInfo[playerid][try],PlayerInfo[playerid][trz],8);
										PlayerInfo[playerid][trafitimer] = SetTimerEx("trafitimerr",400,1,"u",playerid);
										SendClientMessage(playerid,-1,"(( Trafipax aktív! ))");
									}
									else
									{
										SendClientMessage(playerid,-1,"(( Már bevan kapcsolva! ))");
									}
								}
								else
								{
									SendClientMessage(playerid,-1,"(( Elöbb állíts limitet! ))");
								}
							}
							else if( strcmp( tipus, "ki", true ) == 0 )
							{
								if(PlayerInfo[playerid][trafipaxo] == true)
								{
									PlayerInfo[playerid][trafipaxo] = false;
									KillTimer(PlayerInfo[playerid][trafitimer]);
									SendClientMessage(playerid,-1,"(( Trafipax kikapcsolva! ))");
								}
								else
								{
									SendClientMessage(playerid,-1,"(( Trafipax nincs bekapcsolva! ))");
								}
							}
							else if( strcmp( tipus, "limit", true,5 ) == 0 )
							{
								new string[128];
								if(sscanf(params,"s[64]d",tipus,limit)) return SendClientMessage(playerid,-1,"(( Használat: /trafipax [limit] [sebesség]");
								else
								{
									PlayerInfo[playerid][trafilim] = limit;
									format(string,sizeof(string),"(( Limit beállítva %d KM/H -ra ))",limit);
									SendClientMessage(playerid,-1,string);
								}
							}
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Elöbb állísd le a motort! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ülsz szolgálati kocsiban! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban ! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:m(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{	
			if(IsPlayerInAnyVehicle(playerid))
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 596 || GetVehicleModel(GetPlayerVehicleID(playerid)) ==597 ||GetVehicleModel(GetPlayerVehicleID(playerid)) ==598)
				{
					new msg[128];
					new string[256];
					if(sscanf(params,"s[128]",msg)) return SendClientMessage(playerid,-1,"(( Használat: /m[egaphone] [szöveg] ))");
					else
					{
						new Float:x,Float:y,Float:z;
						GetPlayerPos(playerid,x,y,z);
						for(new i; i<MAX_PLAYERS;i++)
						{
							if(IsPlayerInRangeOfPoint(i,20,x,y,z))
							{
								format(string,sizeof(string),"%s megaphone <o: %s",PlayerInfo[playerid][pfrakcio],msg);
								SendClientMessage(i,0xffe100AA,string);
							}
						}
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ülsz szolgálati kocsiban! ))");
				}	
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban ! ))");
			}	
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:birsag(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new ids,osssz;
			new msg[128];
			if(sscanf(params,"uds[128]",ids,osssz,msg)) return SendClientMessage(playerid,-1,"(( Használat: /birsag [ID] [Összeg] [OKA] ))");
			else
			{
				if(ProxDetectorS(3.0,playerid,ids))
				{
					new string[128];
					PlayerInfo[ids][ado] += osssz;
					format(string,sizeof(string),"(( %s megbírságolt %d FT-ra oka: %s ))",Nev(playerid),osssz,msg);
					SendClientMessage(ids,-1,string);
					format(string,sizeof(string),"(( Megbírságoltad %s %d FT-ra okat:%s ))",Nev(ids),osssz,msg);
					SendClientMessage(playerid,-1,string);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( A játékos nincs a közeledben! ))");
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:rlk(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInAnyVehicle(playerid))
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 596 || GetVehicleModel(GetPlayerVehicleID(playerid)) ==597 ||GetVehicleModel(GetPlayerVehicleID(playerid)) ==598)
				{
					if(sscanf(params,"s[64]",PlayerInfo[playerid][prlk])) return SendClientMessage(playerid,-1,"(( Használat: /rlk [Rendszám] ))");
					else
					{
						SetTimerEx("prlkv",7000,false,"u",playerid);
						GameTextForPlayer(playerid, "Lekérdezés folyamatban!", 7000, 4);
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ülsz szolgálati kocsiban! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban ! ))");
			}
		}	
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:civilruha(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			SetPlayerSkin(playerid,PlayerInfo[playerid][skin]);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban ! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:muhold(playerid,params[])
{
	if(IsPlayerCop(playerid))
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new tipus[128];
			if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /muhold [be] [ki] ))");
			else
			{
				if( strcmp( tipus, "be", true) == 0 )
				{
					TogglePlayerControllable(playerid,0);
					SetPlayerInterior(playerid,0);
					SetPlayerVirtualWorld(playerid,0);
					SetPlayerPos(playerid,0,0,100);
					SetPlayerCameraPos(playerid,0 ,0, 100);
					SetPlayerCameraLookAt(playerid,0 ,0, 0);
					muholdpoz[playerid][0] = 0;
					muholdpoz[playerid][1] = 0;
					muholdpoz[playerid][2] = 100;
					muholdban[playerid] = true;
				}
				if( strcmp( tipus, "ki", true) == 0 )
				{
					SetCameraBehindPlayer(playerid);
					SetPlayerPos(playerid,-2009 ,150, 28);
					muholdban[playerid] = false;
					TogglePlayerControllable(playerid,1);
				}
			}
			
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:felszereles(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 4)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInRangeOfPoint(playerid,5, 264.222, 110.534, 1004.617))
			{
				if(felszerelesben[playerid] == false)
				{
					ShowPlayerDialog(playerid,DIALOG_ORFK1,DIALOG_STYLE_LIST,"ORFK TEK","Támogatólövész\nMesterlövész\nTûzszerész\nLövész","OK","Mégse");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Többször nem veheted fel! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy szolg helyen! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
//OMSZ parancsok
CMD:vizsgal(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				if(animban[id] == 1)
				{
					GameTextForPlayer(playerid,"Sérült állapotának felmérése folyamatban!",10000,4);
					GameTextForPlayer(id,"Egy mentõs jelenleg vizsgál!",10000,4);
					SetTimerEx("omszvizsgal",10000,false,"uu",playerid,id);
					Cselekves(playerid,"megvizsgálja a beteget.");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs semmi baja! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:dfb(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				new Float:hp;
				GetPlayerHealth(id,hp);
				if(floatround(hp) < 10)
				{
					SetPlayerHealth(id,hp+5.0);
					Cselekves(playerid," defibrillálja a beteget.");
					ApplyAnimation(playerid,"MEDIC","CPR",4.1,0,0,0,1,11000,1);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs szüksége DFB-re! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:adrinj(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				new Float:hp;
				GetPlayerHealth(id,hp);
				if(floatround(hp) > 10 && floatround(hp) < 20)
				{
					SetPlayerHealth(id,hp+8.5);
					Cselekves(playerid,"bead egy adrenalin injekciót.");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs szüksége adrenalinra! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:bszallit(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInAnyVehicle(playerid))
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 416)
				{
					new id = GetClosestPlayer(playerid);
					if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
					if(ProxDetectorS(5.5,playerid,id))
					{
						new Float:hp;
						GetPlayerHealth(id,hp);
						if(floatround(hp) > 15)
						{
							PutPlayerInVehicle(id,GetPlayerVehicleID(playerid),3);
							TogglePlayerControllable(playerid,0);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Állapota súlyos, nem szállíthatod! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ülsz szolgálati kocsiban! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:blead(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInAnyVehicle(playerid))
			{
				if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 416)
				{
					for(new i =1;i<MAX_PLAYERS;i++)
					{
						if(GetPlayerVehicleID(playerid) == GetPlayerVehicleID(i))
						{
							if(GetPlayerVehicleSeat(i) == 3)
							{
								SetPlayerPos(i,1167.998, -1320.587, -4.884);
								SetPlayerVirtualWorld(i,1);
								SetPlayerInterior(i,1);
								SetPlayerFacingAngle(i,183.146);
								PlayerInfo[playerid][pborton] = 240;
								PlayerInfo[playerid][btipus] =5;
								return 1;
							}
						}
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem ülsz szolgálati kocsiban! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ülsz kocsiban! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:lottellat(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				if(serulestipus[id] == 2)
				{
					if(lottellatva[id] == false)
					{
						GameTextForPlayer(playerid,"Ellátás folyamatban!",60000,4);
						SendClientMessage(playerid,-1,"(( 1 percet vesz igénybe ))");
						SendClientMessage(id,-1,"(( Valaki ellátja a sérülésedet! ))");
						SetTimerEx("omszlottellat",60000,false,"uu",playerid,id);
						lottellatva[id] = true;
						Cselekves(playerid,"ellátja a beteget.");
						ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,60000,1);
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Többször nem láthatod el! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincsen lõtt sérülése! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:mutet(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				new tipus[128];
				new tipus2[128];
				new tipus3[128];
				if(sscanf(params,"s[128]",tipus)) return SendClientMessage(playerid,-1,"(( Használat: /mutet [altat] [elkezd] [befejez] [ebreszt] ))");
				else
				{
					if( strcmp( tipus, "altat", true,5 ) == 0 )
					{
						if(altatva[id] == false)
						{
							TogglePlayerControllable(id,0);
							Cselekves(playerid,"beadj az altató injekciót.");
							SendClientMessage(id,-1,"(( Elaltattak! ))");
							altatva[id] = true;
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Már el van alaltva! ))");
						}
					}
					if( strcmp( tipus, "kezd", true,4 ) == 0 )
					{
						if(sscanf(params,"s[128]s[128]",tipus,tipus2)) return SendClientMessage(playerid,-1,"(( Használat: /mutet kezd [golyo] ))");
						else
						{
							if( strcmp( tipus2, "golyo", true,5 ) == 0 )
							{
								if(sscanf(params,"s[128]s[128]s[128]",tipus,tipus2,tipus3)) return SendClientMessage(playerid,-1,"(( Használat: /mutet golyo [tisztit] [feszit] [kiszed] [tisztit] [osszevar] ))");
								else
								{
									if( strcmp( tipus3, "tisztit", true,7 ) == 0 )
									{
										if(mutetfolyamat[playerid] == 0 || mutetfolyamat[playerid] == 3)
										{
											Cselekves(playerid,"kitisztítja a sebet.");
											mutetfolyamat[playerid] += 1;
										}
										else
										{
											SendClientMessage(playerid,-1,"(( Haladj sorban! ))" );
										}
									}
									if( strcmp( tipus3, "feszit", true,6 ) == 0 )
									{
										if(mutetfolyamat[playerid] == 1)
										{
											Cselekves(playerid,"szétfeszíti a sebet.");
											mutetfolyamat[playerid] += 1;
										}
										else
										{
											SendClientMessage(playerid,-1,"(( Haladj sorban! ))" );
										}
									}
									if( strcmp( tipus3, "kiszed", true,6 ) == 0 )
									{
										if(mutetfolyamat[playerid] == 2)
										{
											Cselekves(playerid,"kiszedi a golyót.");
											mutetfolyamat[playerid] += 1;
										}
										else
										{
											SendClientMessage(playerid,-1,"(( Haladj sorban! ))" );
										}
									}
									if( strcmp( tipus3, "osszevar", true,6 ) == 0 )
									{
										if(mutetfolyamat[playerid] == 4)
										{
											Cselekves(playerid,"összevarja a sebet.");
											mutetfolyamat[playerid] += 1;
											KillTimer(elverzest[id]);
											lotttimerbe[playerid] = false;
										}
										else
										{
											SendClientMessage(playerid,-1,"(( Haladj sorban! ))" );
										}
									}
								}
							}
						}
					}
					if( strcmp( tipus, "befejez", true,7 ) == 0 )
					{
						mutetfolyamat[playerid] =0;
						SendClientMessage(playerid,-1,"(( Befejezted a mûtétet! ))");
						Cselekves(playerid,"befejezi a mûtétet.");
					}
					if( strcmp( tipus, "ebreszt", true,7 ) == 0 )
					{
							TogglePlayerControllable(id,1);
							Cselekves(playerid,"felkelti a beteget.");
							SendClientMessage(id,-1,"(( Felébredtél! ))");
							altatva[id] = false;
					}
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:segit(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id = GetClosestPlayer(playerid);
			if(playerid == id) return SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			if(ProxDetectorS(3.0,playerid,id))
			{
				animban[id] = 0;
				ClearAnimations(id);
				SetPlayerDrunkLevel(id,0);
				SendClientMessage(id,-1,"(( Felsegítettek ! ))");
				Cselekves(playerid,"felsegíti a sérültet.");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs senki a közeledben! ))");
			}
			
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:ellat(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 7)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new id;
			if(sscanf(params,"u",id)) return SendClientMessage(playerid,-1,"(( Használat: /ellat [ID/NÉV] ))");
			else
			{
				if(ProxDetectorS(5.0,playerid,id))
				{
					if(animban[id] == 0)
					{
						SetPlayerHealth(id,100);
						Cselekves(playerid,"ellátja a beteget.");
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Animban van, nem adhatsz neki HP-t ))" );
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs a közeledben! ))");
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}

//otp parancsok
CMD:kocsihelyszinel(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 8)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new vehicleid = GetClosestVehicle(playerid,2.5);
			if(vehicleid != -1)
			{
				Cselekves(playerid,"felméri a jármû állapotát.");
				ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,30000,1);
				SetTimerEx("otptimerek",30000,false,"udd",playerid,vehicleid,1);
				GameTextForPlayer(playerid,"Jármû állapotának felmérése folyamatban!",30000,4);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs kocsi a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}	
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:szmentesit(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 8)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new vehicleid = GetClosestVehicle(playerid,2.5);
			if(vehicleid != -1)
			{
				if(uszivargas[vehicleid] == true)
				{
					Cselekves(playerid,"elállítja a szivárgást.");
					ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,60000,1);
					SetTimerEx("otptimerek",60000,false,"udd",playerid,vehicleid,2);
					GameTextForPlayer(playerid,"Szivárgás mentesítés folyamatban!",60000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem szivárog az üzemanyag! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs kocsi a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}	
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:muszakiment(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 8)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new vehicleid = GetClosestVehicle(playerid,2.5);
			if(vehicleid != -1)
			{
				if(GetPlayerWeapon(playerid) == 9)
				{
					if(uszivargas[vehicleid] == false)
					{
						Cselekves(playerid,"elkezdi levágni az ajtókat.");
						ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,60000,1);
						SetTimerEx("otptimerek",60000,false,"udd",playerid,vehicleid,3);
						GameTextForPlayer(playerid,"Mûszakimentés folyamatban!!",60000,4);
					}
					else
					{
						new Float:x, Float:y, Float:z;
						GetVehiclePos(vehicleid,x,y,z);
						SetVehicleHealth(vehicleid,250);
						CreateExplosion(x, y, z, 1, 10.0);
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Láncfûrésznek kell a kezedben lennie! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs kocsi a közeledben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}	
	}
	else
	{
		return 0;
	}
	return 1;
}

//Szerelõ parancsok
CMD:munkalap(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 9)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			new pid;
			new rendszam[64];
			new koltseg;
			if(sscanf(params,"uds[64]",pid,koltseg,rendszam)) return SendClientMessage(playerid,-1,"(( Használat: /munkalap [ID/NÉV] [Öszköltség] [Rendszám] ))");
			else
			{
				if(ProxDetectorS(3.0,playerid,pid))
				{
					new i;
					new bool:talal;
					new Float:tav,Float:x,Float:y,Float:z;
					while(i<MAX_VEHICLES && talal == false)
					{
						if(CompareEx(VehicleInfo[i][Plate],rendszam))
						{
							talal = true;
						}
						else
						{
							talal = false;
							i++;
						}
					}
					if(talal == true)
					{
						GetPlayerPos(playerid,x,y,z);
						tav = GetVehicleDistanceFromPoint(i,x,y,z);
						if(tav < 6.0)
						{
							new string[128];
							format(string,sizeof(string),"(( Felajánlották a(z): %s rendszámû kocsit munkalapra ! ))",VehicleInfo[i][Plate]);
							SendClientMessage(pid,-1,string);
							format(string,sizeof(string),"(( Szerelés összege: %d FT ))",koltseg);
							SendClientMessage(pid,-1,string);
							SendClientMessage(pid,-1,"(( /elfogad munkalap a véglegesítéshez ))");
							munkalapfel[pid] = true;
							munkalapid[pid] = i;
							szerelkolts[pid] = koltseg;
							PlayerInfo[pid][felajan] = 1;
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Túl messze vagy a jármûtõl! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( A rendszám nem található az adatbázisban! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs a játékos a közeledben! ))");
				}
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}	
	}
	else
	{
		return 0;
	}
	return 1;		
}
CMD:szerel(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 9)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInRangeOfPoint(playerid,7,2174.391, -2165.140, 13.569))
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				if(vehicleid != -1)
				{
					if(VehicleInfo[vehicleid][lefoglalva] == 2)
					{
						// piros {ff0000}                zöld {00ff00}                   sárga {f2ff00}
						new dlg[512];
						new string[128];
						new PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper,DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight,LFirst, LSecond, LThird, LFourth,TFrontLeft, TFrontRight, TRearLeft, TRearRight;
						GetVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper);
						GetVehicleDoorsDamageStatus(vehicleid, DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight);
						GetVehicleLightsDamageStatus(vehicleid, LFirst, LSecond, LThird, LFourth);
						GetVehicleTiresDamageStatus(vehicleid, TFrontLeft, TFrontRight, TRearLeft, TRearRight);
						if(PFrontLeft ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal elsõ lemez\n");
							strcat(dlg,string);
						}
						else if(PFrontLeft <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Bal elsõ lemez\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal elsõ lemez\n");
							strcat(dlg,string);
						}
						if(PFrontRight ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb elsõ lemez\n");
							strcat(dlg,string);
						}
						else if(PFrontRight <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Jobb elsõ lemez\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb elsõ lemez\n");
							strcat(dlg,string);
						}
						if(PRearLeft ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal hátsó lemez\n");
							strcat(dlg,string);
						}
						else if(PRearLeft <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Bal hátsó lemez\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal hátsó lemez\n");
							strcat(dlg,string);
						}
						if(PRearRight ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb hátsó lemez\n");
							strcat(dlg,string);
						}
						else if(PRearRight <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Jobb hátsó lemez\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb hátsó lemez\n");
							strcat(dlg,string);
						}
						if(PWindShield ==0)
						{
							format(string,sizeof(string),"{00ff00}Szélvédõ\n");
							strcat(dlg,string);
						}
						else if(PWindShield <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Szélvédõ\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Szélvédõ\n");
							strcat(dlg,string);
						}
						if(PFrontBumper ==0)
						{
							format(string,sizeof(string),"{00ff00}Elsõ lökhárító\n");
							strcat(dlg,string);
						}
						else if(PFrontBumper <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Elsõ lökhárító\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Elsõ lökhárító\n");
							strcat(dlg,string);
						}
						if(PRearBumper ==0)
						{
							format(string,sizeof(string),"{00ff00}Hátsó lökhárító\n");
							strcat(dlg,string);
						}
						else if(PRearBumper <= 2)
						{
							format(string,sizeof(string),"{f2ff00}Hátsó lökhárító\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Hátsó lökhárító\n");
							strcat(dlg,string);
						}
						if(DBonnet ==0)
						{
							format(string,sizeof(string),"{00ff00}Motorháztetõ\n");
							strcat(dlg,string);
						}
						else if(DBonnet <= 3)
						{
							format(string,sizeof(string),"{f2ff00}Motorháztetõ\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Motorháztetõ\n");
							strcat(dlg,string);
						}
						if(DBoot ==0)
						{
							format(string,sizeof(string),"{00ff00}Csomagtartó\n");
							strcat(dlg,string);
						}
						else if(DBoot <= 3)
						{
							format(string,sizeof(string),"{f2ff00}Csomagtartó\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Csomagtartó\n");
							strcat(dlg,string);
						}
						if(DFrontLeft ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal elsõ ajtó\n");
							strcat(dlg,string);
						}
						else if(DFrontLeft <= 3)
						{
							format(string,sizeof(string),"{f2ff00}Bal elsõ ajtó\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal elsõ ajtó\n");
							strcat(dlg,string);
						}
						if(DFrontRight ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb elsõ ajtó\n");
							strcat(dlg,string);
						}
						else if(DFrontRight <= 3)
						{
							format(string,sizeof(string),"{f2ff00}Jobb elsõ ajtó\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb elsõ ajtó\n");
							strcat(dlg,string);
						}
						if(LFirst ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal elsõ lámpa\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal elsõ lámpa\n");
							strcat(dlg,string);
						}
						if(LSecond ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb elsõ lámpa\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb elsõ lámpa\n");
							strcat(dlg,string);
						}
						if(LThird ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal hátsó lámpa\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal hátsó lámpa\n");
							strcat(dlg,string);
						}
						if(LFourth ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb hátsó lámpa\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb hátsó lámpa\n");
							strcat(dlg,string);
						}
						if(TFrontLeft ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal elsõ kerék\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal elsõ kerék\n");
							strcat(dlg,string);
						}
						if(TFrontRight ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb elsõ kerék\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb elsõ kerék\n");
							strcat(dlg,string);
						}
						if(TRearLeft ==0)
						{
							format(string,sizeof(string),"{00ff00}Bal hátsó kerék\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Bal hátsó kerék\n");
							strcat(dlg,string);
						}
						if(TRearRight ==0)
						{
							format(string,sizeof(string),"{00ff00}Jobb hátsó kerék\n");
							strcat(dlg,string);
						}
						else
						{
							format(string,sizeof(string),"{ff0000}Jobb hátsó kerék\n");
							strcat(dlg,string);
						}
						ShowPlayerDialog(playerid,DIALOG_SZERELO1,DIALOG_STYLE_LIST,"Lakatos",dlg,"Szerel","Mégse");
					}
					else
					{	
						SendClientMessage(playerid,-1,"(( A jármû nincs munkalapon! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs közeledben jármû ))");
				}
			}
			else if(IsPlayerInRangeOfPoint(playerid,7,2160.347, -2179.066, 13.569))
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				if(vehicleid != -1)
				{
					if(VehicleInfo[vehicleid][lefoglalva] == 2)
					{
						if(VehicleInfo[vehicleid][uzemanyagt] == 0)
						{
							if(szerelesben[playerid] == false)
							{
								SetTimerEx("pmotorszerel",6000,false,"ud",playerid,vehicleid);
								Cselekves(playerid,"elkezdi szerelni a jármûvet.");
								szerelesben[playerid] = true;
								ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,0,6000,1);
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Már szerelsz! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt itt nem szerelheted! ))");
						}
					}
					else
					{	
						SendClientMessage(playerid,-1,"(( A jármû nincs munkalapon! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs közeledben jármû ))");
				}
			}
			else if(IsPlayerInRangeOfPoint(playerid,7,2148.495, -2190.342, 13.639))
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				if(vehicleid != -1)
				{
					if(VehicleInfo[vehicleid][lefoglalva] == 2)
					{
						if(VehicleInfo[vehicleid][uzemanyagt] == 1)
						{	
							if(szerelesben[playerid] == false)
							{
								SetTimerEx("pmotorszerel",6000,false,"ud",playerid,vehicleid);
								Cselekves(playerid,"elkezdi szerelni a jármûvet.");
								szerelesben[playerid] = true;
								ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,0,6000,1);
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Már szerelsz! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt itt nem szerelheted! ))");
						}
					}
					else
					{	
						SendClientMessage(playerid,-1,"(( A jármû nincs munkalapon! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs közeledben jármû ))");
				}
			}
			else if(IsPlayerInRangeOfPoint(playerid,5,-1491.941 ,782.998 ,7.239))
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				if(vehicleid != -1)
				{
					if(VehicleInfo[vehicleid][lefoglalva] == 2)
					{
						if(VehicleInfo[vehicleid][uzemanyagt] == 3)
						{
							if(szerelesben[playerid] == false)
							{
								SetTimerEx("pmotorszerel",6000,false,"ud",playerid,vehicleid);
								Cselekves(playerid,"elkezdi szerelni a jármûvet.");
								szerelesben[playerid] = true;
								ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,0,6000,1);
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Már szerelsz! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt itt nem szerelheted! ))");
						}
					}
					else
					{	
						SendClientMessage(playerid,-1,"(( A jármû nincs munkalapon! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs közeledben jármû ))");
				}
			}
			else if(IsPlayerInRangeOfPoint(playerid,5,-1491.793,755.404,7.239))
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				if(vehicleid != -1)
				{
					if(VehicleInfo[vehicleid][lefoglalva] == 2)
					{
						if(VehicleInfo[vehicleid][uzemanyagt] == 3)
						{
							if(szerelesben[playerid] == false)
							{
								SetTimerEx("pmotorszerel",6000,false,"ud",playerid,vehicleid);
								Cselekves(playerid,"elkezdi szerelni a jármûvet.");
								szerelesben[playerid] = true;
								ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,0,6000,1);
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Már szerelsz! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt itt nem szerelheted! ))");
						}
					}
					else
					{	
						SendClientMessage(playerid,-1,"(( A jármû nincs munkalapon! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs közeledben jármû ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy mûhelyben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}	
	}
	else
	{
		return 0;
	}
	return 1;	
}
CMD:festes(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio]==9)
	{
		if(PlayerInfo[playerid][pszolg] == 1)
		{
			
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:jrendel(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 13)
	{
		if(PlayerInfo[playerid][pszolg] == 1)
		{
			if(IsPlayerInRangeOfPoint(playerid,5.0,-1665.052,1206.348,7.254))
			{
				ShowPlayerDialog(playerid,DIALOG_SZERELO3,DIALOG_STYLE_LIST,"Kereskedõ","Repülõgép/Helikopter\nMotor\nKabrió\nIpari\nLowrider\nOff Road\nSzalon\nSport\nKombi\nHajó","OK","Mégse");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy a kereskedõ pontnál ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:jelad(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 13)
	{
		if(PlayerInfo[playerid][pszolg] == 1)
		{
			if(IsPlayerInRangeOfPoint(playerid,5.0,-1665.052,1206.348,7.254))
			{
				new ids,amounts;
				new plate[64];
				new str[128];
				if(sscanf(params, "us[64]d", ids,plate, amounts))return SendClientMessage(playerid, -1, "(( Használat: /jelad [ID] [Rendszám] [Pénz]");
				else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
				else
				{
					new bool:talal;
					new i=0;
					while(i < MAX_VEHICLES && talal==false)
					{
						if(CompareEx(VehicleInfo[i][Plate],plate))
						{
							talal= true;
						}
						else
						{
							talal= false;
							i++;
						}
					}
					if(talal == true)
					{
						if(VehicleInfo[i][vfrakcio] == 13)
						{
							jfelajid[ids] = i;
							jfelajossz[ids] = amounts;
							jfelajtip[ids] =1;
							PlayerInfo[ids][felajan] = 1;
							format(str,sizeof(str),"(( Kereskedõ: Felajánlották a(z) %s rendszámú jármûvet %d FT-ért! ))",plate,amounts);
							SendClientMessage(ids,-1,str);
							SendClientMessage(ids,-1,"(( /elfogad kereskedo parancsal megvehted a jármûvet! ))");
							SendClientMessage(playerid,-1,"(( Sikeresen felajánlottad a jármûvet eladásra! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt nem adhatod el! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs ilyen rendszám! ))");
					}
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy a kereskedõ pontnál ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:jvesz(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 13)
	{
		if(PlayerInfo[playerid][pszolg] == 1)
		{
			if(IsPlayerInRangeOfPoint(playerid,5.0,-1665.052,1206.348,7.254))
			{
				new ids,amounts;
				new plate[64];
				new str[128];
				if(sscanf(params, "us[64]d", ids,plate, amounts))return SendClientMessage(playerid, -1, "(( Használat: /jvesz [ID] [Rendszám] [Pénz]");
				else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
				else
				{
					new bool:talal;
					new i=0;
					while(i < MAX_VEHICLES && talal==false)
					{
						if(CompareEx(VehicleInfo[i][Plate],plate))
						{
							talal= true;
						}
						else
						{
							talal= false;
							i++;
						}
					}
					if(talal == true)
					{
						if(VehicleInfo[i][vfrakcio] == 0 && VehicleInfo[i][vmunka] == 0)
						{
							if(VehicleInfo[i][Owner] == PlayerInfo[ids][dbid])
							{
								new sql[256];
								new azon,bpenz;
								mysql_format(mysql,sql,sizeof(sql),"SELECT `azonosito`,`penz` FROM `bank` WHERE `frakcio`='13'");
								new Cache:res = mysql_query(mysql,sql);
								cache_get_value_name_int(0,"azonosito",azon);
								cache_get_value_name_int(0,"penz",bpenz);
								cache_delete(res);
								if(bpenz >= amounts)
								{
									jfelajid[ids] = i;
									jfelajossz[ids] = amounts;
									jfelajtip[ids] =2;
									PlayerInfo[ids][felajan] = 1;
									format(str,sizeof(str),"(( Kereskedõ: Felajánlották a(z) %s rendszámú jármûvet %d FT-ért! ))",plate,amounts);
									SendClientMessage(ids,-1,str);
									SendClientMessage(ids,-1,"(( /elfogad kereskedo parancsl eladhatod a jármûvet! ))");
									SendClientMessage(playerid,-1,"(( Sikeresen felajánlottad a jármûvet megvételre ! ))");
								}
								else
								{
									SendClientMessage(playerid,-1,"(( Nincs elég pénz a bankszámlán! ))");
								}
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Tulajdonos nem egyezik! ))");
							}
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Ezt nem veheted meg! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs ilyen rendszám! ))");
					}
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem vagy a kereskedõ pontnál ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
CMD:jarmufelmer(playerid,params[])
{
	if(PlayerInfo[playerid][frakcio] == 9)
	{
		if(PlayerInfo[playerid][pszolg] ==1)
		{
			if(IsPlayerInAnyVehicle(playerid))
			{
				karfelmer(playerid,GetPlayerVehicleID(playerid));
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nem ûlsz jármáben! ))");
			}
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Nem vagy szolgálatban! ))");
		}
	}
	else
	{
		return 0;
	}
	return 1;
}
public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	GetVehicleHealth(vehicleid, vhp);
	vhealth[playerid] = vhp;
	rkocsiid[playerid] = GetPlayerVehicleID(playerid);
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	if(beszorulva[playerid] == true)
	{	
		ClearAnimations(playerid);
		new st;
		st = GetPlayerVehicleSeat(playerid);
		PutPlayerInVehicle(playerid,vehicleid,st);
		GameTextForPlayer(playerid,"Be vagy szorulva a kocsiba!",5000,4);
	}
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
    if(GetPlayerState(playerid) == 2)
    {
		GetVehicleHealth(GetPlayerVehicleID(playerid), vhp);
		vhealth[playerid] = vhp;
		rkocsiid[playerid] = GetPlayerVehicleID(playerid);
		TextDrawShowForPlayer(playerid, sebessegmero[GetPlayerVehicleID(playerid)]);
		TextDrawShowForPlayer(playerid, kmmero[GetPlayerVehicleID(playerid)]);
		
		uzemanyagb[playerid]=CreatePlayerProgressBar(playerid, 568.000000, 435.000000, 50.0, 4.0, COLOR_YELLOW, 100.0, BAR_DIRECTION_RIGHT);
		ShowPlayerProgressBar(playerid, uzemanyagb[playerid]);
		
		GetVehiclePos(GetPlayerVehicleID(playerid),tmp_vkilometer[GetPlayerVehicleID(playerid)][0],tmp_vkilometer[GetPlayerVehicleID(playerid)][1],tmp_vkilometer[GetPlayerVehicleID(playerid)][2]);
		timerkmhez[playerid] = SetTimerEx("kmszamlalo", 2500, true, "iu", GetPlayerVehicleID(playerid),playerid);
		if(VehicleInfo[GetPlayerVehicleID(playerid)][lefoglalva] == 0)
		{
			if(VehicleInfo[GetPlayerVehicleID(playerid)][vmunka]>0 && PlayerInfo[playerid][pmunka] != VehicleInfo[GetPlayerVehicleID(playerid)][vmunka])
			{
				RemovePlayerFromVehicle(playerid);
				SendClientMessage(playerid, -1, "(( Ez egy munka kocsi! ))");
			}
			if(VehicleInfo[GetPlayerVehicleID(playerid)][vfrakcio]>0 && PlayerInfo[playerid][frakcio] != VehicleInfo[GetPlayerVehicleID(playerid)][vfrakcio])
			{
				if(VehicleInfo[GetPlayerVehicleID(playerid)][vfrakcio] == 1 && PlayerInfo[playerid][frakcio] < 6 )
				{
					return 1;
				}
				else if(PlayerInfo[playerid][frakcio] != VehicleInfo[GetPlayerVehicleID(playerid)][vfrakcio])
				{
					RemovePlayerFromVehicle(playerid);
					SendClientMessage(playerid, -1, "(( Ez egy frakciós kocsi! ))");
				}
			}
			if(PlayerInfo[playerid][dbid] != VehicleInfo[GetPlayerVehicleID(playerid)][Owner])
			{
				if(VehicleInfo[GetPlayerVehicleID(playerid)][vfrakcio] == 0 && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] == 0)
				{
					SendClientMessage(playerid, -1, "(( Ez nem a te kocsid! ))");
				}
			}
			if( VehicleInfo[GetPlayerVehicleID(playerid)][vceg] != 0 && (VehicleInfo[GetPlayerVehicleID(playerid)][vceg] != PlayerInfo[playerid][pceg]))
			{
				RemovePlayerFromVehicle(playerid);
				SendClientMessage(playerid, -1, "(( Ez egy céges kocsi! ))");
			}
		}
		else if(VehicleInfo[GetPlayerVehicleID(playerid)][lefoglalva] == 2 && PlayerInfo[playerid][frakcio] == 9 && PlayerInfo[playerid][pszolg] == 1)
		{
			return 1;
		}
		else
		{
			RemovePlayerFromVehicle(playerid);
			SendClientMessage(playerid,COLOR_RED,"(( Nem elvihetõ! ))");
		}
    }
	if(GetPlayerState(playerid) == 1)
	{
		if(PlayerInfo[playerid][trafipaxo] == true)
		{
			PlayerInfo[playerid][trafipaxo] = false;
			KillTimer(PlayerInfo[playerid][trafitimer]);
			SendClientMessage(playerid,-1,"(( Trafipax kikapcsolva! ))");
		}
	}
    return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	if(tisztit[playerid] == 1)
	{
		if(IsPlayerInAnyVehicle(playerid) && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] ==1 && GetVehicleModel(GetPlayerVehicleID(playerid)) == 574)
		{
			new rand;
			DisablePlayerCheckpoint(playerid);
			rand = random(sizeof(tisztitocp));
			SetPlayerCheckpoint(playerid, tisztitocp[rand][0],tisztitocp[rand][1],tisztitocp[rand][2],5.0);
			PlayerInfo[playerid][pfizetes] += 500;
		}
	}
	if(funyirasban[playerid] == true)
	{
		if(IsPlayerInAnyVehicle(playerid) && VehicleInfo[GetPlayerVehicleID(playerid)][vmunka] ==5 && GetVehicleModel(GetPlayerVehicleID(playerid)) == 572)
		{
			if(47 != funyirocpsz[playerid])
			{
				GameTextForPlayer(playerid,"Fûnyírás folyamatban!",5000,4);
				SetTimerEx("funyirotimer",5000,false,"u",playerid);
				TogglePlayerControllable(playerid,false);
			}
			else
			{
				new rnd;
				new str[128];
				TogglePlayerControllable(playerid,false);
				DestroyPlayerObject(playerid,funyirofu[playerid]);
				funyirasban[playerid] = false;
				rnd = random(4000) + 8000;
				PlayerInfo[playerid][pfizetes] += rnd;
				format(str,sizeof(str),"(( %d FT hozzáadva a fizetésedhez! ))",rnd);
				SendClientMessage(playerid,-1,str);
			}
		}
	}
	if(kreszgyakorlasban[playerid]==true)
	{
		kreszgyakorlasban[playerid] = false;
		kreszkilometer[playerid] = floatround(VehicleInfo[GetPlayerVehicleID(playerid)][megkilometer]);
		SendClientMessage(playerid,-1,"((Befejezted a gyakorlást!))");
		if(kreszkilometer[playerid] >= 80)
		{
			SendClientMessage(playerid,-1,"(( Sikeresen befejezted a gyakorlást! ))");
			kreszv[playerid][0][2] = true;
			VehicleInfo[GetPlayerVehicleID(playerid)][Vvan] = false;
			DestroyVehicle(GetPlayerVehicleID(playerid));
		}
		else
		{
			VehicleInfo[GetPlayerVehicleID(playerid)][Vvan] = false;
			DestroyVehicle(GetPlayerVehicleID(playerid));
			SendClientMessage(playerid,-1,"(( Még gyakorolnod kell! ))");
		}
		DisablePlayerCheckpoint(playerid);
	}
	return 1;
}

public OnPlayerLeaveCheckpoint(playerid)
{
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	if(kreszvizsgaban[playerid] == true)
	{	
		if(IsPlayerInAnyVehicle(playerid))
		{
			if(kreszvcp[playerid]+1 <= 33)
			{
				SetPlayerRaceCheckpoint(playerid,0,kreszvizsgacp[kreszvcp[playerid]][0],kreszvizsgacp[kreszvcp[playerid]][1],kreszvizsgacp[kreszvcp[playerid]][2],kreszvizsgacp[kreszvcp[playerid]+1][0],kreszvizsgacp[kreszvcp[playerid]+1][1],kreszvizsgacp[kreszvcp[playerid]+1][2],3.0);
				kreszvcp[playerid]++;
			}
			else
			{
				SetPlayerRaceCheckpoint(playerid,1,-2068.798,-83.864,34.742,-2068.798,-83.864,34.742,3.0);
				kreszvcp[playerid]++;
			}	
		}
		if(kreszvcp[playerid] > 34)
		{
			DisablePlayerRaceCheckpoint(playerid);
			new Float:vhlt;
			GetVehicleHealth(GetPlayerVehicleID(playerid),vhlt);
			if(vhlt == 1000.0)
			{
				new kat[2];
				new str[64];
				format(kat,sizeof(kat),"A");
				VehicleInfo[GetPlayerVehicleID(playerid)][Vvan] = false;
				DestroyVehicle(GetPlayerVehicleID(playerid));
				SendClientMessage(playerid,-1,"(( Sikeres vizsga! ))");
				kreszv[playerid][0][3] = true;
				if(strcmp( PlayerInfo[playerid][jogosit], "NINCS", true ) == 0)
				{
					format(PlayerInfo[playerid][jogosit],2," ");
				}
				sscanf(PlayerInfo[playerid][jogosit],"s[63]",str);
				strcat(str,kat);
				format(PlayerInfo[playerid][jogosit],2,"%s",str);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Sikertelen vizsga! ))");
			}
		}
	}
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	return 1;
}

public OnRconCommand(cmd[])
{
	if(!strcmp(cmd, "hello", true))
    {
        SendClientMessageToAll(0xFFFFFFAA, "Hello World!");
        print("You said hello to the world."); // This will appear to the player who typed the rcon command in the chat in white
        return 1;
    }
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	SendClientMessage(playerid,-1,"OnPlayerRequestSpawn");
	return 0;
}

public OnObjectMoved(objectid)
{
	return 1;
}

public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}
public OnPlayerPickUpPickup(playerid, pickupid)
{
	for(new hid;hid<MAX_HOUSE;hid++)
	{
		if(pickupid == HouseInfo[hid][housepick])
		{
			if(pickupba[playerid] == 0)
			{
				if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
				{
					if(HouseInfo[hid][Owner] == 0)
					{
						ShowPlayerDialog(playerid, DIALOG_VHOUSE, DIALOG_STYLE_LIST, "Eladó ház!", "Információ\nVásárlás", "Mehet", "Mégse");
					}
					if(HouseInfo[hid][Owner] != 0)
					{
						new string[128];
						format(string, sizeof(string), "%s háza!", HouseInfo[hid][Ownername]);
						if(HouseInfo[hid][Owner] == PlayerInfo[playerid][dbid])
						{
							if(HouseInfo[hid][nyitzar] == 0)
							{
								ShowPlayerDialog(playerid, DIALOG_MHOUSEZ, DIALOG_STYLE_LIST, string, "Információ\nBelépés\nNyit\nElad", "Mehet", "Mégse");
							}
							else
							{
								ShowPlayerDialog(playerid, DIALOG_MHOUSENY, DIALOG_STYLE_LIST, string, "Információ\nBelépés\nZár\nElad", "Mehet", "Mégse");
							}
						}
						else
						{
							ShowPlayerDialog(playerid, DIALOG_MHOUSEI, DIALOG_STYLE_LIST, string, "Információ\nBelépés", "Mehet", "Mégse");
						}
					}
				}
				SetTimerEx("hazpicnull",5000,false,"u",playerid);
				pickupba[playerid] =1;
				break;
			}
		}
	}
	for(new bid;bid<MAX_BIZZ;bid++)
	{
		if(pickupid == BizzInfo[bid][bizzpick])
		{
			
			if(pickupba[playerid] == 0)
			{
				if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
				{
					if(BizzInfo[bid][Owner] == 0)
					{
						ShowPlayerDialog(playerid, DIALOG_VBIZZ, DIALOG_STYLE_LIST, "Eladó Bizznisz!", "Információ\nVásárlás\nBelépés", "Mehet", "Mégse");
					}
					if(BizzInfo[bid][Owner] != 0)
					{
						new string[128];
						format(string, sizeof(string), "%s bizznisze!", BizzInfo[bid][Ownername]);
						if(BizzInfo[bid][Owner] == PlayerInfo[playerid][dbid])
						{
							ShowPlayerDialog(playerid, DIALOG_SBIZZ, DIALOG_STYLE_LIST, string, "Információ\nBelépés\nKassza\nElad", "Mehet", "Mégse");
						}
						else
						{
							ShowPlayerDialog(playerid, DIALOG_MBIZZ, DIALOG_STYLE_LIST, string, "Információ\nBelépés", "Mehet", "Mégse");
						}
					}
				}
				SetTimerEx("hazpicnull",5000,false,"u",playerid);
				pickupba[playerid] =1;
				break;
			}
		}
	}
	for(new i;i<MAX_RENTH;i++)
	{
		if(pickupid == RentInfo[i][pickid])
		{
			if(pickupba[playerid] == 0)
			{
				if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
				{
					if(RentInfo[i][owner] == 0)
					{
						ShowPlayerDialog(playerid,DIALOG_RENTH1,DIALOG_STYLE_LIST,"Bérelhetõ ház","Információ\nBérlés","OK","Mégse");
					}
					if(RentInfo[i][owner] !=0)
					{
						new str[128];
						format(str,sizeof(str),"%s háza",RentInfo[i][ownername]);
						if(RentInfo[i][owner] == PlayerInfo[playerid][dbid])
						{
							if(RentInfo[i][rzarvar] == 0)
							{
								ShowPlayerDialog(playerid,DIALOG_RENTH2,DIALOG_STYLE_LIST,str,"Információ\nBelépés\nZár\nBérlés hosszabítása\nLemond","OK","Mégse");
							}
							else
							{
								ShowPlayerDialog(playerid,DIALOG_RENTH2,DIALOG_STYLE_LIST,str,"Információ\nBelépés\nNyit\nBérlés hosszabítása\nLemond","OK","Mégse");
							}
						}
						else
						{
							ShowPlayerDialog(playerid,DIALOG_RENTH3,DIALOG_STYLE_LIST,str,"Információ\nBelépés","OK","Mégse");
						}
					}
				}
				SetTimerEx("hazpicnull",5000,false,"u",playerid);
				pickupba[playerid] =1;
				break;
			}
		}
	}
	for(new i;i<MAX_CEG;i++)
	{
		if(pickupid == CegInfo[i][cegpick])
		{
			if(pickupba[playerid] == 0)
			{
				if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
				{
					if(CegInfo[i][cowner] == 0)
					{
						ShowPlayerDialog(playerid,DIALOG_CEG6,DIALOG_STYLE_LIST,"Eladó cég","Információ\nVásárlás","OK","Mégse");
					}
					else
					{
						new string[128];
						format(string, sizeof(string), "%s cége!", CegInfo[i][cownername]);
						if(CegInfo[i][cowner] == PlayerInfo[playerid][dbid])
						{
							ShowPlayerDialog(playerid,DIALOG_CEG7,DIALOG_STYLE_LIST,string,"Információ\nElad","OK","Mégse");
						}
						else
						{
							ShowPlayerDialog(playerid,DIALOG_CEG8,DIALOG_STYLE_LIST,string,"Információ","OK","Mégse");
						}
					}
				}
				SetTimerEx("hazpicnull",5000,false,"u",playerid);
				pickupba[playerid] =1;
				break;
			}
		}
	}
	for(new i;i<MAX_RAKTAR;i++)
	{
		if(pickupid == RaktarInfo[i][rapickupid])
		{
			if(pickupba[playerid] == 0)
			{
				if(RaktarInfo[i][owner] == 0)
				{
					ShowPlayerDialog(playerid,DIALOG_RAKTAR1,DIALOG_STYLE_LIST,"Eladó raktár","Információ\nVásárlás","OK","Mégse");
				}
				else
				{
					if(RaktarInfo[i][owner] == PlayerInfo[playerid][dbid])
					{
						ShowPlayerDialog(playerid,DIALOG_RAKTAR2,DIALOG_STYLE_LIST,"Raktár","Belépés\nÁtalakítás\nJelszóváltás\nEladás","OK","Mégse");
					}
					else
					{
						ShowPlayerDialog(playerid,DIALOG_RAKTAR3,DIALOG_STYLE_LIST,"Raktár","Belépés","OK","Mégse");
					}
				}
				SetTimerEx("hazpicnull",5000,false,"u",playerid);
				pickupba[playerid] =1;
				break;
			}
		}
	}
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	if(PRESSED(KEY_ANALOG_RIGHT) && ruhaskin[playerid] ==1)
	{
		new rand;
		rand = kivalaszt[playerid];
		SetPlayerSkin(playerid,ruhaskinek[rand][0]);
		kivalaszt[playerid] = kivalaszt[playerid]+1;
	}
	if(PRESSED(KEY_ANALOG_LEFT) && ruhaskin[playerid] ==1)
	{
		new rand;
		rand = kivalaszt[playerid]-1;
		SetPlayerSkin(playerid,ruhaskinek[rand][0]);
		kivalaszt[playerid] = kivalaszt[playerid]-1;
	}
	if (PRESSED( KEY_FIRE ) && ruhaskin[playerid] ==1)
	{
		new sskin;
		sskin = GetPlayerSkin(playerid);
		SetPlayerSkin(playerid, sskin);
		PlayerInfo[playerid][skin] = sskin;
		ruhaskin[playerid] = 0;
		TogglePlayerControllable(playerid,1);
		SetCameraBehindPlayer(playerid);
		KillTimer(ruhatimer);
		kivalaszt[playerid] = 0;
	}
	if (PRESSED( KEY_AIM ) && ruhaskin[playerid] ==1)
	{
		SetPlayerSkin(playerid, PlayerInfo[playerid][skin]);
		ruhaskin[playerid] = 0;
		TogglePlayerControllable(playerid,1);
		SetCameraBehindPlayer(playerid);
		KillTimer(ruhatimer);
		kivalaszt[playerid] = 0;
	}
	if(PRESSED(KEY_AIM) && sokkolop[playerid] ==1)
	{
		new wep,amm;
		GetPlayerWeaponData(playerid,2,wep,amm);
		if(wep == 22)
		{
			if(amm !=2)
			{
				SetPlayerAmmo(playerid,WEAPON_COLT45,2);
			}
		}
		
	}
	if(HOLDING(KEY_HANDBRAKE) && HOLDING(KEY_SUBMISSION))
	{
		autoinditas(playerid);
	}
	if(HOLDING(KEY_SUBMISSION))
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, lights, alarm, doors, bonnet, boot, objective);
				if(lights == 0)
				{
					SetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, 1, alarm, doors, bonnet, boot, objective);
				}
				else
				{
					SetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, 0, alarm, doors, bonnet, boot, objective);
				}
			}
			else
			{
				SendClientMessage(playerid, -1, "(( Nem te vagy a vezetõ! ))");
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "(( Nem ülsz kocsiban! ))");
		}
	}
	if(HOLDING(KEY_FIRE) && lancfuresz[playerid] == true && GetPlayerWeapon(playerid) == 9)
	{
		if(favagoszamlalo[playerid] <= 200)
		{
			for(new i;i<sizeof(vaghatofa);i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,10,vaghatofa[i][0],vaghatofa[i][1],vaghatofa[i][2]))
				{
					favagoszamlalo[playerid]++;
				}
			}
		}
		else if(favagoszamlalo[playerid] > 200)
		{
			SendClientMessage(playerid,-1,"(( Pakold át a fát az autóba! ))");
		}
	}
	/*if(PRESSED(KEY_FIRE)||PRESSED(KEY_AIM)||HOLDING( KEY_FIRE ))
	{
		new wep,wepf,ammo;
		new weaponss[13][2];
		wep = GetPlayerWeapon(playerid);
		for(new i = 0; i < 13; i++)
		{
			GetPlayerWeaponData(playerid,i,wepf,ammo);
			if(wepf==wep)
			{
				break;
			}
		}
		if(ammo == 1)
		{
			for (new i = 0; i < 13; i++)
			{
				GetPlayerWeaponData(playerid, i, weaponss[i][0], weaponss[i][1]);
			}
			ResetPlayerWeapons(playerid);
			for(new i = 0; i < 13; i++)
			{
				GivePlayerWeapon(playerid,weaponss[i][0],weaponss[i][1]);
			}
		}
	}*/
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	if(!success)
	{
		new pip[16];
		new string[64];
		new sendername[64];
		for(new i=0; i<MAX_PLAYERS; i++)
		{	
			GetPlayerIp(i, pip, sizeof(pip));
			if(!strcmp(ip, pip, true))
			{
				GetPlayerName(i, sendername, sizeof(sendername));
				format(string, sizeof(string),"SCD: %s megpróbált belépni RCON-ba IP:%s",sendername,pip);
			}
		}
		SendToAdmins(COLOR_RED,string);
	}
	else
	{
		new pip[16];
		new string[64];
		new sendername[64];
		for(new i=0; i<MAX_PLAYERS; i++)
		{	
			GetPlayerIp(i, pip, sizeof(pip));
			if(!strcmp(ip, pip, true))
			{
				GetPlayerName(i, sendername, sizeof(sendername));
				format(string, sizeof(string),"SCD: %s belépett RCON-ba IP:%s",sendername,pip);
			}
		}
		SendToDevelopment(COLOR_RED,string);
	}
	return 1;
}

public OnPlayerUpdate(playerid)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		GetVehicleHealth(GetPlayerVehicleID(playerid), vhp);
		new vehid = GetPlayerVehicleID(playerid);
		if(vhp < vhealth[playerid])
		{
			OnVehicleLoseHealth(playerid,vehid,floatround(vhealth[playerid]-vhp));
			vhealth[playerid] = vhp;
		}
	}
	if(IsPlayerInAnyVehicle(playerid))
	{
		new string[16];
		format(string, 16, "%d KM/H", GetPlayerSpeed(playerid,true));
		TextDrawSetString(sebessegmero[GetPlayerVehicleID(playerid)], string);
	}
	if(!IsPlayerInAnyVehicle(playerid))
	{
		TextDrawHideForPlayer(playerid, sebessegmero[rkocsiid[playerid]]);
		TextDrawHideForPlayer(playerid, kmmero[rkocsiid[playerid]]);
		HidePlayerProgressBar(playerid, uzemanyagb[playerid]);
		KillTimer(timerkmhez[playerid]);
		Audio_Stop(playerid, 1);
	}
	if(animban[playerid] == 1)
	{
		if((animtipus[playerid] == 2) && (GetPlayerAnimationIndex(playerid) != 1537))
		{
			ApplyAnimation(playerid, "SWEET", "Sweet_injuredloop",4.1,0,0,0,1,0,1);
		}
		if((animtipus[playerid] == 3) && (GetPlayerAnimationIndex(playerid) != 746))
		{
			if(!IsPlayerInAnyVehicle(playerid))
			{
				ApplyAnimation(playerid, "KNIFE", "KILL_Knife_Ped_Die",4.1,0,0,0,1,0,1);
			}
		}
	}
	if(muholdban[playerid] == true)
	{
		new keys,ud,lr;
		GetPlayerKeys(playerid,keys,ud,lr);
		if(ud==KEY_UP)
		{
			muholdpoz[playerid][1] +=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		if(ud==KEY_DOWN)
		{
			muholdpoz[playerid][1] -=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		if(lr==KEY_LEFT)
		{
			muholdpoz[playerid][0] -=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		if(lr==KEY_RIGHT)
		{
			muholdpoz[playerid][0] +=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		if(keys == KEY_JUMP  )
		{
			muholdpoz[playerid][2] +=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		if(keys == KEY_WALK )
		{
			muholdpoz[playerid][2] -=3.0;
			SetPlayerCameraLookAt(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], 0);
			SetPlayerCameraPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1], muholdpoz[playerid][2]);
		}
		SetPlayerPos(playerid,muholdpoz[playerid][0],muholdpoz[playerid][1],-3);
	}	
	if(PlayerInfo[playerid][frakcio] == 8)
	{
		if(tuzeset == true)
		{
			tuzupdate(playerid);
		}
	}
	
	if(PlayerIsLoggedOn[playerid] == true)
	{
		if(halalk[playerid] == false || halalban[playerid] == false)
		{
			new Float:Php;
			GetPlayerHealth(playerid,Php);
			if(Php <= 10 && halalban[playerid] == false)
			{
				animban[playerid] = 1;
				animtipus[playerid] = 2;
				halalk[playerid] = true;
			}
		}
	}
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	ShowPlayerNameTagForPlayer(playerid,forplayerid,false);
	if(aszolgban[playerid] == 1)
	{
		ShowPlayerNameTagForPlayer(playerid,forplayerid,true);
	}
	else
	{
		for(new bari;bari<MAX_BARAT;bari++)
		{
			if(barat[playerid][bari] == PlayerInfo[forplayerid][dbid])
			{
				ShowPlayerNameTagForPlayer(playerid,forplayerid,true);
			}
			else
			{
				//ShowPlayerNameTagForPlayer(playerid,forplayerid,false);
			}
		}
	}
	if(!IsPlayerCop(forplayerid) || csipogo[playerid] == false)
	{
		SetPlayerMarkerForPlayer(forplayerid,playerid,0xFFFFFF00);
	}
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	if(!IsPlayerCop(forplayerid) ||	csipogo[playerid] == false)
	{
		SetPlayerMarkerForPlayer(forplayerid,playerid,0xFFFFFF00);
	}
	ShowPlayerNameTagForPlayer(playerid,forplayerid,false);
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}

stock KAddItem(vehicleid,itemname[],itemammount)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname))
	{
		new ddb,rows;
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `kesztyutarto` WHERE `dbid` = '%d' AND `nev` ='%s';",VehicleInfo[vehicleid][Id],itemname);
		res = mysql_query(mysql,query);		
		if(cache_get_row_count(rows))
		{
			cache_get_value_name_int(0,"mennyiseg",ddb);
			itemammount += ddb;
			mysql_format(mysql,query,sizeof(query),"UPDATE `kesztyutarto` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",itemammount,VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);

		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"INSERT INTO `kesztyutarto`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
			res = mysql_query(mysql,query);
		}
		
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"INSERT INTO `kesztyutarto`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	cache_delete(res);
}

stock CSAddItem(vehicleid,itemname[],itemammount)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname))
	{
		new ddb,rows;
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `csomagtarto` WHERE `dbid` = '%d' AND `nev` ='%s';",VehicleInfo[vehicleid][Id],itemname);
		res = mysql_query(mysql,query);		
		if(cache_get_row_count(rows))
		{
			cache_get_value_name_int(0,"mennyiseg",ddb);
			itemammount += ddb;
			mysql_format(mysql,query,sizeof(query),"UPDATE `csomagtarto` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",itemammount,VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);

		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"INSERT INTO `csomagtarto`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
			res = mysql_query(mysql,query);
		}
		
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"INSERT INTO `csomagtarto`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	cache_delete(res);
}


public AddItem(playerid,itemname[],itemammount)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname))
	{
		new ddb,rows;
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `inventory` WHERE `dbid` = '%d' AND `nev` ='%s';",PlayerInfo[playerid][dbid],itemname);
		res = mysql_query(mysql,query);		
		if(cache_get_row_count(rows))
		{
			cache_get_value_name_int(0,"mennyiseg",ddb);
			itemammount += ddb;
			mysql_format(mysql,query,sizeof(query),"UPDATE `inventory` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",itemammount,PlayerInfo[playerid][dbid],itemname);
			res = mysql_query(mysql,query);

		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"INSERT INTO `inventory`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",PlayerInfo[playerid][dbid],itemname,itemammount);
			res = mysql_query(mysql,query);
		}
		
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"INSERT INTO `inventory`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",PlayerInfo[playerid][dbid],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	cache_delete(res);
	return 1;
}

stock RAddItem(raktarid,itemname[],itemammount)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname))
	{
		new ddb,rows;
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `raktartartalom` WHERE `dbid` = '%d' AND `nev` ='%s';",RaktarInfo[raktarid][dbid],itemname);
		res = mysql_query(mysql,query);		
		if(cache_get_row_count(rows))
		{
			cache_get_value_name_int(0,"mennyiseg",ddb);
			itemammount += ddb;
			mysql_format(mysql,query,sizeof(query),"UPDATE `raktartartalom` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",itemammount,RaktarInfo[raktarid][dbid],itemname);
			res = mysql_query(mysql,query);

		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"INSERT INTO `raktartartalom`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",RaktarInfo[raktarid][dbid],itemname,itemammount);
			res = mysql_query(mysql,query);
		}
		
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"INSERT INTO `raktartartalom`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','%s','%d');",RaktarInfo[raktarid][dbid],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	cache_delete(res);
}
public bool:osszevonhato(itemname[])
{
	new i = 0;
	while(sizeof(fegyvernev) > i)
	{
		if(CompareEx(itemname,fegyvernev[i]) || (strfind(itemname, "Jármûkulcs", true) != -1))
		{
			return false;
		}
		else
		{
			i++;
		}
	}
	return true;
}
stock UseItems(playerid,itemname[],itemammount)
{
	if(CompareEx(itemname,"Pizza"))
	{
		new Float:hhp;
		GetPlayerHealth(playerid,hhp);
		hhp += 10.0;
		SetPlayerHealth(playerid,hhp);
		RemoveItem(playerid,"Pizza",itemammount,1);
	}
	for(new i = 0; i < sizeof(fegyvernev);i++)
	{
		if(CompareEx(itemname,fegyvernev[i]))
		{
			GivePlayerWeapon(playerid,i+1,itemammount);
			RemoveItem(playerid,itemname,itemammount,itemammount);
		}
	}
	if(strfind(itemname, "Jármûkulcs", true) != -1)
	{
		strdel(itemname,0,11);
		sscanf(itemname,"s[32]",PlayerInfo[playerid][kulcs]);
		for(new vehid;vehid<MAX_VEHICLES;vehid++)
		{
			if(PlayerInfo[playerid][kulcs] == VehicleInfo[vehid][Plate])
			{
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
				if(doors == 1)
				{
					ShowPlayerDialog(playerid,DIALOG_KULCS1,DIALOG_STYLE_LIST,"Kulcs","Parkol\nNyit","OK","Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid,DIALOG_KULCS2,DIALOG_STYLE_LIST,"Kulcs","Parkol\nZár","OK","Mégse");
				}
				return 1;
			}
		}
	}
	if(strfind(itemname, "Forgalmi Engedély", true) != -1)
	{
		new string[128];
		strdel(itemname,0,18);
		sscanf(itemname,"s[32]",string);
		new day, month, year,sa,as,ssa;
		for(new vehid;vehid<MAX_VEHICLES;vehid++)
		{
			if(CompareEx(string,VehicleInfo[vehid][Plate]))
			{
				format(string,sizeof(string),"(( Forgalmi Engedély ))");
				chats(30.0,playerid,string,COLOR_CHAT1,false);
				format(string,sizeof(string),"((Tulajdonos: %s ))",VehicleInfo[vehid][Ownername]);
				chats(30.0,playerid,string,COLOR_CHAT1,false);
				format(string,sizeof(string),"((Rendszám: %s ))",VehicleInfo[vehid][Plate]);
				chats(30.0,playerid,string,COLOR_CHAT1,false);
				format(string,sizeof(string),"((Model: %s ))",GetVehicleModelName(VehicleInfo[vehid][Model],false));
				chats(30.0,playerid,string,COLOR_CHAT1,false);
				datemk(VehicleInfo[vehid][forgalmi],day, month, year,sa,as,ssa);
				if(VehicleInfo[vehid][forgalmi] > gettime())
				{
					format(string,sizeof(string),"(({00ff00} Érvényesség: %d - %d - %d {ffffff}))",year,month,day);
				}
				else
				{
					format(string,sizeof(string),"(({FF0000} Érvényesség: %d - %d - %d {ffffff}))",year,month,day);
				}
				chats(30.0,playerid,string,COLOR_CHAT1,false);
				return 1;
			}
		}
	}
	return 1;
}
stock RemoveItem(playerid,itemname[],itemammount,remove)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname) == false || itemammount == remove)
	{
		mysql_format(mysql,query,sizeof(query),"DELETE FROM `inventory` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` = '%d');",PlayerInfo[playerid][dbid],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `inventory` WHERE `dbid` = '%d' AND `nev` = '%s';",PlayerInfo[playerid][dbid],itemname);
		res = mysql_query(mysql,query);
		cache_get_value_name_int(0,"mennyiseg",itemammount);
		if(itemammount-remove > 0)
		{
			mysql_format(mysql,query,sizeof(query),"UPDATE `inventory` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",(itemammount-remove),PlayerInfo[playerid][dbid],itemname);
			res = mysql_query(mysql,query);
		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"DELETE FROM `inventory` WHERE `dbid` = '%d' AND `nev` = '%s' ;",PlayerInfo[playerid][dbid],itemname);
			res = mysql_query(mysql,query);
		}
	}
	cache_delete(res);
}
stock RRemoveItem(raktarid,itemname[],itemammount,remove)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname) == false || itemammount == remove)
	{
		mysql_format(mysql,query,sizeof(query),"DELETE FROM `raktartartalom` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` = '%d');",RaktarInfo[raktarid][dbid],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `raktartartalom` WHERE `dbid` = '%d' AND `nev` = '%s';",RaktarInfo[raktarid][dbid],itemname);
		res = mysql_query(mysql,query);
		cache_get_value_name_int(0,"mennyiseg",itemammount);
		if(itemammount-remove > 0)
		{
			mysql_format(mysql,query,sizeof(query),"UPDATE `raktartartalom` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",(itemammount-remove),RaktarInfo[raktarid][dbid],itemname);
			res = mysql_query(mysql,query);
		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"DELETE FROM `raktartartalom` WHERE `dbid` = '%d' AND `nev` = '%s' ;",RaktarInfo[raktarid][dbid],itemname);
			res = mysql_query(mysql,query);
		}
	}
	cache_delete(res);
}
stock CSRemoveItem(vehicleid,itemname[],itemammount,remove)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname) == false || itemammount == remove)
	{
		mysql_format(mysql,query,sizeof(query),"DELETE FROM `csomagtarto` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` = '%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `csomagtarto` WHERE `dbid` = '%d' AND `nev` = '%s';",VehicleInfo[vehicleid][Id],itemname);
		res = mysql_query(mysql,query);
		cache_get_value_name_int(0,"mennyiseg",itemammount);
		if(itemammount-remove > 0)
		{
			mysql_format(mysql,query,sizeof(query),"UPDATE `csomagtarto` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",(itemammount-remove),VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);
		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"DELETE FROM `csomagtarto` WHERE `dbid` = '%d' AND `nev` = '%s' ;",VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);
		}
	}
	cache_delete(res);
}
stock KRemoveItem(vehicleid,itemname[],itemammount,remove)
{
	new query[1024];
	new Cache:res;
	if(osszevonhato(itemname) == false || itemammount == remove)
	{
		mysql_format(mysql,query,sizeof(query),"DELETE FROM `kesztyutarto` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` = '%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
		res = mysql_query(mysql,query);
	}
	else
	{
		mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `kesztyutarto` WHERE `dbid` = '%d' AND `nev` = '%s';",VehicleInfo[vehicleid][Id],itemname);
		res = mysql_query(mysql,query);
		cache_get_value_name_int(0,"mennyiseg",itemammount);
		if(itemammount-remove > 0)
		{
			mysql_format(mysql,query,sizeof(query),"UPDATE `kesztyutarto` SET `mennyiseg` = '%d' WHERE `dbid` = '%d' AND `nev` = '%s';",(itemammount-remove),VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);
		}
		else
		{
			mysql_format(mysql,query,sizeof(query),"DELETE FROM `kesztyutarto` WHERE `dbid` = '%d' AND `nev` = '%s' ;",VehicleInfo[vehicleid][Id],itemname);
			res = mysql_query(mysql,query);
		}
	}
	cache_delete(res);
}
stock ExistItem(playerid,itemname[],itemammount)
{
	new query[1024];
	new rows;
	mysql_format(mysql,query,sizeof(query),"SELECT `id` FROM `inventory` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` >= '%d');",PlayerInfo[playerid][dbid],itemname,itemammount);
	new Cache:res = mysql_query(mysql,query);
	if(cache_get_row_count(rows))
	{
		cache_delete(res);
		return true;
	}
	cache_delete(res);
	return false;
}
stock KExistItem(vehicleid,itemname[],itemammount)
{
	new query[1024];
	new rows;
	mysql_format(mysql,query,sizeof(query),"SELECT `id` FROM `kesztyutarto` WHERE `dbid` = '%d' AND (`nev` = '%s' AND `mennyiseg` >= '%d');",VehicleInfo[vehicleid][Id],itemname,itemammount);
	new Cache:res = mysql_query(mysql,query);
	if(cache_get_row_count(rows))
	{
		cache_delete(res);
		return true;
	}
	cache_delete(res);
	return false;
}
stock ItemAmmounts(playerid,itemname[])
{
	new query[1024];
	new db=0;
	mysql_format(mysql,query,sizeof(query),"SELECT `mennyiseg` FROM `inventory` WHERE `dbid` = '%d' AND `nev` = '%s';",PlayerInfo[playerid][dbid],itemname);
	new Cache:res = mysql_query(mysql,query);
	cache_get_value_name_int(0,"mennyiseg",db);
	cache_delete(res);
	return db;
}
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid == 2)
	{
		LoginPlayer(playerid,inputtext);
		return 1;
	}
	if(dialogid == DIALOG_TASKA1)
	{
		if(response)
		{
			new Cache:res;
			new sql[256];
			mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `inventory` WHERE `dbid` = '%d' LIMIT %d,1;",PlayerInfo[playerid][dbid],listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name(0,"nev",TskKiNev[playerid],128);
			cache_get_value_name_int(0,"mennyiseg",TskKiAmount[playerid]);
			cache_delete(res);
			ShowPlayerDialog(playerid,DIALOG_TASKA2,DIALOG_STYLE_LIST,"Táska","Használ\nÁtad\nEldob","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_TASKA2)
	{
		if(response)
		{
			if(listitem == 0)
			{
				UseItems(playerid,TskKiNev[playerid],TskKiAmount[playerid]);
			}
			else if(listitem == 1)
			{
				ShowPlayerDialog(playerid,DIALOG_TASKA3,DIALOG_STYLE_INPUT,"Táska","Játékos neve vagy ID","OK","Mégse");
			}
			else
			{
				if(TskKiAmount[playerid] == 1)
				{
					ShowPlayerDialog(playerid,DIALOG_TASKA4,DIALOG_STYLE_MSGBOX,"Táska","Biztos el szeretnéd dobni?","Igen","Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid,DIALOG_TASKA5,DIALOG_STYLE_INPUT,"Táska","Mennyit szeretnél eldobni?","Igen","Mégse");
				}
			}
		}
	}
	if(dialogid == DIALOG_TASKA3)
	{
		if(response)
		{	
			new ids;
			if(sscanf(inputtext, "u", ids))return SendClientMessage(playerid, COLOR_RED, "(( Szegmentális hiba! ))");
			else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Játékos nem ONLINE!  ))");
			else
			{
				new Float:ttx,Float:tty,Float:ttz;
				GetPlayerPos(playerid,ttx,tty,ttz);
				if(IsPlayerInRangeOfPoint(ids,3.0,ttx,tty,ttz))
				{
					RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
					AddItem(ids,TskKiNev[playerid],TskKiAmount[playerid]);
					SendClientMessage(playerid,COLOR_GREEN,"(( Sikeresen átadtad! ))");
					SendClientMessage(ids,COLOR_GREEN,"(( Átadtak valamit! ))");
					Cselekves(playerid,"átadott valamit.");
				}
				else
				{
					SendClientMessage(playerid,COLOR_RED,"(( Játékos nincs a közeledben! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_TASKA4)
	{
		if(response)
		{
			RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
			SendClientMessage(playerid,COLOR_GREEN,"(( Sikeresen eldobtad! ))");
		}
	}
	if( dialogid == DIALOG_TASKA5)
	{
		if(response)
		{
			new ddb = strval(inputtext);
			RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],ddb);
		}
	}	
	if(dialogid == DIALOG_CSOMAGTARTO1)
	{
		sscanf(inputtext, "p<\t>s[32]d", TskKiNev[playerid],TskKiAmount[playerid]);
		if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
		{
			AddItem(playerid,TskKiNev[playerid],TskKiAmount[playerid]);
			CSRemoveItem(pcsomag[playerid],TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
			SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
		}
		else
		{
			ShowPlayerDialog(playerid,DIALOG_CSOMAGTARTO2,DIALOG_STYLE_INPUT,"Táska","Mennyit szeretnél kivenni?","Igen","Mégse");
		}
	}
	if(dialogid == DIALOG_CSOMAGTARTO2)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				AddItem(playerid,TskKiNev[playerid],mennyis);
				CSRemoveItem(pcsomag[playerid],TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
			}
		}
	}
	if(dialogid == DIALOG_CSOMAGTARTO3)
	{
		if(response)
		{
			sscanf(inputtext, "p<\t>s[32]d", TskKiNev[playerid],TskKiAmount[playerid]);
			if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
			{
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
				CSAddItem(pcsomag[playerid],TskKiNev[playerid],TskKiAmount[playerid]);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
			else
			{
				ShowPlayerDialog(playerid,DIALOG_CSOMAGTARTO4,DIALOG_STYLE_INPUT,"Táska","Mennyit szeretnél berakni?","Igen","Mégse");
			}
		
		}
	}
	if(dialogid == DIALOG_CSOMAGTARTO4)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				CSAddItem(pcsomag[playerid],TskKiNev[playerid],mennyis);
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
		}
	}
	if(dialogid == DIALOG_KESZTYUTARTO1)
	{
		if(response)
		{
			sscanf(inputtext, "p<\t>s[32]d", TskKiNev[playerid],TskKiAmount[playerid]);
			if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
			{
				AddItem(playerid,TskKiNev[playerid],TskKiAmount[playerid]);
				KRemoveItem(pkesztyu[playerid],TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
				SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
			}
			else
			{
				ShowPlayerDialog(playerid,DIALOG_KESZTYUTARTO2,DIALOG_STYLE_INPUT,"Táska","Mennyit szeretnél kivenni?","Igen","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_KESZTYUTARTO2)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				AddItem(playerid,TskKiNev[playerid],mennyis);
				KRemoveItem(pkesztyu[playerid],TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
			}
		}
	}
	if(dialogid == DIALOG_KESZTYUTARTO3)
	{
		if(response)
		{
			sscanf(inputtext, "p<\t>s[32]d", TskKiNev[playerid],TskKiAmount[playerid]);
			if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
			{
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
				KAddItem(pkesztyu[playerid],TskKiNev[playerid],TskKiAmount[playerid]);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
			else
			{
				ShowPlayerDialog(playerid,DIALOG_KESZTYUTARTO4,DIALOG_STYLE_INPUT,"Táska","Mennyit szeretnél berakni?","Igen","Mégse");
			}
		
		}
	}
	if(dialogid == DIALOG_KESZTYUTARTO4)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				KAddItem(pkesztyu[playerid],TskKiNev[playerid],mennyis);
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
		}
	}
	if(dialogid == DIALOG_MHOUSEZ)
	{
		if(response)
		{	
			if(listitem == 0)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						new string[128];
						new string2[256];
						format(string,sizeof(string), "Házszám: %d", HouseInfo[hid][Id]);
						format(string2,sizeof(string2),"Tulaj: %s",HouseInfo[hid][Ownername]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						if(HouseInfo[hid][nyitzar] == 1)
						{
							SetPlayerPos(playerid,HouseInfo[hid][enterx],HouseInfo[hid][entery],HouseInfo[hid][enterz]);
							SetPlayerInterior(playerid,HouseInfo[hid][interior]);
							SetPlayerVirtualWorld(playerid, HouseInfo[hid][hvw]);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Zárva! ))");
						}
					}
				}
			}
			if(listitem == 2)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						HouseInfo[hid][nyitzar] = 1;
						SendClientMessage(playerid,-1,"((Kinyitottad a házadat!))");
					}
				}
			}
			if(listitem ==3)
			{
				new string[32];
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						hazelad[playerid] = HouseInfo[hid][Cost]/10;
						hazelad[playerid] = hazelad[playerid]*8;
						format(string,sizeof(string),"Ház eladási ára: $ %d",hazelad[playerid]);
						ShowPlayerDialog(playerid,DIALOG_HAZELAD,DIALOG_STYLE_MSGBOX,"Ház eladás",string,"Elad","Mégse");
					}
				}
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_MHOUSENY)
	{
		if(response)
		{	
			if(listitem == 0)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						new string[128];
						new string2[256];
						format(string,sizeof(string), "Házszám: %d", HouseInfo[hid][Id]);
						format(string2,sizeof(string2),"Tulaj: %s",HouseInfo[hid][Ownername]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						if(HouseInfo[hid][nyitzar] == 1)
						{
							SetPlayerPos(playerid,HouseInfo[hid][enterx],HouseInfo[hid][entery],HouseInfo[hid][enterz]);
							SetPlayerInterior(playerid,HouseInfo[hid][interior]);
							SetPlayerVirtualWorld(playerid, HouseInfo[hid][hvw]);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Zárva! ))");
						}
					}
				}
			}
			if(listitem == 2)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						HouseInfo[hid][nyitzar] = 0;
						SendClientMessage(playerid,-1,"((Bezártad a házadat!))");
					}
				}
			}
			if(listitem ==3)
			{
				new string[32];
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						hazelad[playerid] = HouseInfo[hid][Cost]/10;
						hazelad[playerid] = hazelad[playerid]*8;
						format(string,sizeof(string),"Ház eladási ára: %d FT",hazelad[playerid]);
						ShowPlayerDialog(playerid,DIALOG_HAZELAD,DIALOG_STYLE_MSGBOX,"Ház eladás",string,"Elad","Mégse");
					}
				}
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_HAZELAD)
	{
		if(response)
		{
			for(new hid; hid<MAX_HOUSE;hid++)
			{
				if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
				{
					HouseInfo[hid][Owner]  = 0;
					HouseInfo[hid][Ownername] = 0;
					DestroyPickup(HouseInfo[hid][housepick]);
					CreatePickup(1273, 1, HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz], -1);
					PlayerInfo[playerid][Money] += hazelad[playerid];
					hazelad[playerid] =0;
					SendClientMessage(playerid,-1,"(( Sikeresen elatad a házadat'! ))");
				}
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_MHOUSEI)
	{
		if(response)
		{	
			if(listitem == 0)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						new string[128];
						new string2[256];
						format(string,sizeof(string), "Házszám: %d", HouseInfo[hid][Id]);
						format(string2,sizeof(string2),"Tulaj: %s",HouseInfo[hid][Ownername]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						if(HouseInfo[hid][nyitzar] == 1)
						{
							SetPlayerPos(playerid,HouseInfo[hid][enterx],HouseInfo[hid][entery],HouseInfo[hid][enterz]);
							SetPlayerInterior(playerid,HouseInfo[hid][interior]);
							SetPlayerVirtualWorld(playerid, HouseInfo[hid][hvw]);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Zárva! ))");
						}
					}
				}
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_VHOUSE)
	{	
		if(response)
		{	
			if(listitem == 0)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						new string[128];
						new string2[129];
						format(string,sizeof(string), "Házszám: %d", HouseInfo[hid][Id]);
						format(string2,sizeof(string2),"Ára: %d",HouseInfo[hid][Cost]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new hid; hid<MAX_HOUSE;hid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, HouseInfo[hid][px],HouseInfo[hid][py],HouseInfo[hid][pz]))
					{
						if(HouseInfo[hid][Cost] <= PlayerInfo[playerid][Money])
						{
							new pname[128];
							GetPlayerName(playerid, pname,sizeof(pname));
							HouseInfo[hid][Owner]  = PlayerInfo[playerid][dbid];
							HouseInfo[hid][Ownername] = pname;
							PlayerInfo[playerid][Money] -= HouseInfo[hid][Cost];
							DestroyPickup(HouseInfo[hid][housepick]);
							CreatePickup(1239, 1, HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz], -1);
							SendClientMessage(playerid, -1, "((Sikeresen megveted a házadat!))");
						}
						else
						{
							SendClientMessage(playerid, -1, "((Nincs elég pénzed!))");
						}
					}
				}
			}
		}
		return 1;
	}
	if(dialogid == DIALOG_JELENTES)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid, DIALOG_BUG, DIALOG_STYLE_INPUT, "Jelentés", "Jelentés elküldése", "OK", "Mégse");
			}
			if(listitem == 1)
			{
				ShowPlayerDialog(playerid, DIALOG_GAME, DIALOG_STYLE_INPUT, "Jelentés", "Jelentés elküldése", "OK", "Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid, DIALOG_NONRP, DIALOG_STYLE_INPUT, "Jelentés", "Jelentés elküldése", "OK", "Mégse");
			}
			if(listitem == 3)
			{
				SendClientMessage(playerid, COLOR_RED, "((Vészhelyzetet elküldtük az adminoknak!))");
				new string[128];
				format(string,sizeof(string),"Jelentés:%s (%i) vészhelyzetet jelentett!",Nev(playerid),playerid);
				SendToAdmins(0xF2FF00FF, string);
			}
		}
	}
	if(dialogid == DIALOG_BUG)
	{
		if(response == 1)
		{
			new string[128];
			format(string, sizeof(string), "Jelentés BUG %s (%i): %s", Nev(playerid), playerid, inputtext);
			SendToReport(0xF2FF00FF, string);
			format(string,sizeof(string), "Jelentésedet elküldtük BUG:%s",inputtext);
			SendClientMessage(playerid, 0xF2FF00FF,string);
		}
	}
	if(dialogid == DIALOG_GAME)
	{
		if(response == 1)
		{
			new string[128];
			format(string, sizeof(string), "Jelentés Segitség %s (%i): %s", Nev(playerid), playerid, inputtext);
			SendToReport(0xF2FF00FF, string);
			format(string,sizeof(string), "Jelentésedet elküldtük Segitség:%s",inputtext);
			SendClientMessage(playerid, 0xF2FF00FF,string);
		}
	}
	if(dialogid == DIALOG_NONRP)
	{
		if(response == 1)
		{
			new string[128];
			format(string, sizeof(string), "Jelentés Cheater/szabályszegés %s (%i): %s", Nev(playerid), playerid, inputtext);
			SendToReport(0xF2FF00FF, string);
			format(string,sizeof(string), "Jelentésedet elküldtük Cheater/szabályszegés:%s",inputtext);
			SendClientMessage(playerid, 0xF2FF00FF,string);
		}
	}
	if(dialogid == DIALOG_LEADER)
	{
		if(response)
		{
			if(listitem == 1)
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER1, DIALOG_STYLE_INPUT,"Felvétel","Játékos neve vagy ID","OK","Mégse");
			}
			if(listitem == 0)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new ids,rows;
				mysql_format(mysql,query,sizeof(query),"SELECT `Name` FROM `user` WHERE `frakcio` = '%d'",PlayerInfo[playerid][frakcio]);
				new Cache:res = mysql_query(mysql,query);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"Name",lnevek,sizeof(lnevek));
					sscanf(lnevek, "u", ids);
					if(IsPlayerConnected(ids))
					{
						format(string,sizeof(string),"\n{00ff00}%s",lnevek);
						if(PlayerInfo[ids][pleader] ==1)
						{
							format(string,sizeof(string),"\n{ff0000}%s",lnevek);
						}
					}
					else
					{
						format(string,sizeof(string),"\n%s",lnevek);
					}
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_LEADER2, DIALOG_STYLE_LIST, "Vezetõség", dlg, "OK", "Mégse");
			}
		}
	}
	if(dialogid == DIALOG_LEADER1)
	{
		if(response)
		{
			new ids;
			if(sscanf(inputtext, "u", ids))return SendClientMessage(playerid, -1, "(( Nem atad meg a nevet/id! ))");
			else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
			else
			{
				if(PlayerInfo[ids][frakcio] != 0)
				{
					SendClientMessage(playerid,-1,"((  Frakció tagja! ))");
				}
				else
				{
					SendClientMessage(ids,-1,"((  Felajánlottak egy frakciót! ))");
					SendClientMessage(ids,-1,"((  /elfogad frakcio ))");
					PlayerInfo[ids][elfogad] = PlayerInfo[playerid][frakcio];
					PlayerInfo[ids][rang] = 1;
					PlayerInfo[ids][felajan] =1;
				}
			}
		}
	}
	if(dialogid == DIALOG_LEADER2/* && response*/)
	{
		if(response)
		{
			if(!listitem)
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				ShowPlayerDialog(playerid, DIALOG_LEADER3,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nElbocsájtás","OK","Mégse");
			}
			else
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				ShowPlayerDialog(playerid, DIALOG_LEADER3,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nElbocsájtás","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_LEADER3)
	{
		if(response)
		{
			if(listitem == 0)
			{
				new sql[128];
				new asql[256];
				new mrang[129];
				new string[128];
				new szrang;
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql), "SELECT `frang` FROM `user` WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"frang",szrang);
				mysql_format(mysql,asql, sizeof(asql), "SELECT `%d` FROM `frakcio` WHERE `Id` = '%d'", szrang,PlayerInfo[playerid][frakcio]);
				res = mysql_query(mysql,asql);
				cache_get_value_index(0,0,mrang,sizeof(mrang));
				format(string,sizeof(string), "Név:%s\nRang:%s",PlayerInfo[playerid][kival],mrang);
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_LEADER4,DIALOG_STYLE_MSGBOX,"Adatok",string,"OK","");
			}
			if(listitem == 1)
			{
				new query[256];
				new string[128];
				new dlg[256];
				new frakok[512];
				new Cache:res;
				for(new i = 1; i < MAX_RANGOK; i++)
				{
					mysql_format(mysql,query,sizeof(query),"SELECT `%d` FROM `frakcio` WHERE `Id` = '%d'",i,PlayerInfo[playerid][frakcio]);
					res = mysql_query(mysql,query);
					cache_get_value_index(0,0,frakok,sizeof(frakok));
					format(string,sizeof(string),"\n%s",frakok);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_LEADER5, DIALOG_STYLE_LIST, "Rangok", dlg, "OK", "Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER6,DIALOG_STYLE_MSGBOX,"Elbocsájtás","Biztos kiakarod rugni?","Igen","Nem");
			}
		}
	}
	if(dialogid == DIALOG_LEADER6)
	{
		if(response)
		{
			new ids;
			if(sscanf(PlayerInfo[playerid][kival], "u", ids))return SendClientMessage(playerid, COLOR_RED, "(( Szegmentális hiba! ))");
			else if(IsPlayerConnected(ids))
			{//fentvan
				new civ[32] = "Civil";
				PlayerInfo[ids][frakcio] =0;
				PlayerInfo[ids][rang] =0;
				PlayerInfo[ids][pleader] =0;
				sscanf(civ,"s[32]",PlayerInfo[ids][pfrakcio]);
				sscanf(civ,"s[32]",PlayerInfo[ids][prang]);
				SendClientMessage(ids, COLOR_RED, "(( A fõnököd kirúgott! ))");
				SendClientMessage(playerid, COLOR_RED, "(( Kirugva!))");
				saveplayer(ids);
			}
			else
			{//nincs fent
				new Cache:res;
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `frakcio`='0',`frang`='0',`leader`='0' WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
				res = mysql_query(mysql,sql);
				SendClientMessage(playerid, COLOR_RED, "(( Kirúgva!))");
				cache_delete(res);
			}
		}
	}
	if(dialogid == DIALOG_LEADER5)
	{
		if(response)
		{
			if(!listitem)
			{
				new ids;
				if(sscanf(PlayerInfo[playerid][kival], "u", ids))return SendClientMessage(playerid, COLOR_RED, "(( Szegmentális hiba! ))");
				else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( A játékos nincs  ))");
				else
				{
					PlayerInfo[ids][rang] = listitem+1;
					SendClientMessage(playerid,-1,"(( A játékost elõléptetted!))");
					SendClientMessage(ids,-1,"(( Elõléptettek))");
					new sql[256];
					mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
					mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
				}
			}
			else
			{
				new ids;
				if(sscanf(PlayerInfo[playerid][kival], "u", ids))return SendClientMessage(playerid, COLOR_RED, "(( Szegmentális hiba! ))");
				else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( A játékos nincs  ))");
				else
				{
					PlayerInfo[ids][rang] = listitem+1;
					SendClientMessage(playerid,-1,"(( A játékost elõléptetted!))");
					SendClientMessage(ids,-1,"(( Elõléptettek))");
					new sql[256];
					mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
					mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
				}

			}
		
		}
	}
	if(dialogid == DIALOG_BANK1)
	{
		if(response)
		{
			if(listitem==0)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK2,DIALOG_STYLE_INPUT,"Bank","Írd be a számlaszámodat","OK","Mégse");
			}
			if(listitem == 1)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new Cache:res;
				new rows;
				mysql_format(mysql,query,sizeof(query),"SELECT `azonosito` FROM `bank` WHERE `tulaj` = '%d'",PlayerInfo[playerid][dbid]);
				res = mysql_query(mysql,query);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"azonosito",lnevek,sizeof(lnevek));
					format(string,sizeof(string),"\n%s",lnevek);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_BANK2, DIALOG_STYLE_LIST, "Bank", dlg, "OK", "Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK9,DIALOG_STYLE_INPUT,"Bank","Írj be egy jelszót (csak szám!)!","OK","Mégse");
			}
			if(listitem == 3)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK10,DIALOG_STYLE_INPUT,"Bank","Adja meg a bankszámla számot!","OK","Mégse");
			}
			if(listitem == 4)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK14,DIALOG_STYLE_LIST,"Bank","Fizetés felvétel(Ha nem bankszámlára megy)\nFizetési bankszámlaszám megadása","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_BANK2)
	{
		if(response)
		{
			banksz[playerid] = strval(inputtext);
			ShowPlayerDialog(playerid,DIALOG_BANK3,DIALOG_STYLE_INPUT,"Bank","Írd be a számlaszám jelszavát","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_BANK3)
	{
		if(response)
		{
			new query[512];
			new rows;
			new Cache:res;
			bankj[playerid] = strval(inputtext);
			mysql_format(mysql,query,sizeof(query),"SELECT * FROM `bank` WHERE `azonosito` = '%d' AND `jelszo` = '%d' ",banksz[playerid],bankj[playerid]);
			res = mysql_query(mysql,query);
			if(cache_get_row_count(rows))
			{
				ShowPlayerDialog(playerid,DIALOG_BANK4,DIALOG_STYLE_LIST,"Bank","Pénz felvétel\nPénz befizetés\nPénz küldése\nJelszó módosítása\nInformáció","OK","Mégse");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Hibás számlaszám vagy jelszó! ))");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_BANK4)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK5,DIALOG_STYLE_INPUT,"Bank","Mennyi pénzt szeretnél felvenni?","OK","Mégse");
			}
			if(listitem == 1)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK6,DIALOG_STYLE_INPUT,"Bank","Mennyi pénzt szeretnél befizetni?","OK","Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK12,DIALOG_STYLE_INPUT,"Bank","Adja meg a bankszámlaszámot!","OK","Mégse");
			}
			if(listitem == 3)
			{
				ShowPlayerDialog(playerid,DIALOG_BANK7,DIALOG_STYLE_INPUT,"Bank","Add meg az új jelszavadat","OK","Mégse");
			}
			if(listitem == 4)
			{
				new query[256];
				new osszeg;
				new string[128];
				new Cache:res;
				mysql_format(mysql,query,sizeof(query),"SELECT `penz` FROM `bank` WHERE `azonosito` = '%d'",banksz[playerid]);
				res = mysql_query(mysql,query);
				cache_get_value_name_int(0,"penz",osszeg);
				format(string,sizeof(string),"Jelenleg: %d FT van a bankszámládon!",osszeg);
				ShowPlayerDialog(playerid,DIALOG_BANK8,DIALOG_STYLE_MSGBOX,"Bank",string,"OK","");
				cache_delete(res);
			}
		}
	}
	if(dialogid == DIALOG_BANK5)
	{
		if(response)
		{
			new query[256];
			new sql[250];
			new string[128];
			new osszeg;
			new befizet;
			new Cache:res;
			befizet = strval(inputtext);
			mysql_format(mysql,query,sizeof(query),"SELECT `penz` FROM `bank` WHERE `azonosito` = '%d'",banksz[playerid]);
			res = mysql_query(mysql,query);
			cache_get_value_name_int(0,"penz",osszeg);
			if(befizet <= osszeg)
			{
				new ujp = osszeg-befizet;
				PlayerInfo[playerid][Money] += befizet;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`='%d' WHERE `azonosito` = '%d'",ujp,banksz[playerid]);
				res = mysql_query(mysql,sql);
				format(string,sizeof(string),"%d felvéve a %d számláról",befizet,banksz[playerid]);
				SendClientMessage(playerid,-1,string);
			}
			else
			{
				format(string,sizeof(string),"%d %d",osszeg,befizet);
				SendClientMessage(playerid,-1,string);
				SendClientMessage(playerid,-1,"(( Nem tudsz ennyi pénzt kivenni ))");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_BANK6)
	{
		if(response)
		{
			new osszeg = strval(inputtext);
			if(osszeg <= PlayerInfo[playerid][Money])
			{
				new query[256];
				new string[128];
				new Cache:res;
				mysql_format(mysql,query,sizeof(query),"UPDATE `bank` SET `penz`='%d'+`penz` WHERE `azonosito` = '%d'",osszeg,banksz[playerid]);
				res = mysql_query(mysql,query);
				format(string,sizeof(string),"%d összeg befizetve a %d bankszámlára",osszeg,banksz[playerid]);
				SendClientMessage(playerid,-1,string);
				cache_delete(res);
			}
			else
			{
				SendClientMessage(playerid,-1,"((Nincs elég pénzed! ))");
			}
		}
	}
	if(dialogid == DIALOG_BANK7)
	{
		if(response)
		{
			new jelsz;
			new query[256];
			new string[128];
			jelsz = strval(inputtext);
			mysql_format(mysql,query,sizeof(query),"UPDATE `bank` SET `jelszo`='%d' WHERE `azonosito` = '%d'",jelsz,banksz[playerid]);
			new Cache:res = mysql_query(mysql,query);
			cache_delete(res);
			format(string,sizeof(string),"A jelszavad megváltoztatva %d", jelsz);
			SendClientMessage(playerid,-1,string);
		}
	}
	if(dialogid == DIALOG_BANK9)
	{
		if(response)
		{
			new string[128],query[512],sql[512];
			new rand,rows;
			new Cache:res;
			bankj[playerid]= strval(inputtext);
			rand = random(888888888)+111111111;
			banksz[playerid] = rand;
			mysql_format(mysql,query,sizeof(query),"SELECT * FROM `bank` WHERE `azonosito` = '%d'",banksz[playerid]);
			res = mysql_query(mysql,query);
			if(cache_get_row_count(rows))
			{
				return response;
			}
			else
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `bank`(azonosito,jelszo,tulaj) VALUES ('%d','%d','%d')",banksz[playerid],bankj[playerid],PlayerInfo[playerid][dbid]);
				res = mysql_query(mysql,sql);
				format(string,sizeof(string),"(( Sikeresen létrahoztad a %d számlaszámot %d jelszóval ))",banksz[playerid],bankj[playerid]);
				SendClientMessage(playerid,-1,string);
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_BANK10)
	{
		if(response)
		{
			banksz[playerid] = strval(inputtext);
			ShowPlayerDialog(playerid,DIALOG_BANK11,DIALOG_STYLE_INPUT,"Bank","Adja meg a kívánt összeget!","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_BANK11)
	{
		if(response)
		{
			new osszeg = strval(inputtext);
			new query[512],string[128];
			new Cache:res;
			if(osszeg <= PlayerInfo[playerid][Money])
			{
				mysql_format(mysql,query,sizeof(query),"UPDATE `bank` SET `penz`='%d'+`penz` WHERE `azonosito` = '%d'",osszeg,banksz[playerid]);
				res = mysql_query(mysql,query);
				PlayerInfo[playerid][Money] -= osszeg;
				format(string,sizeof(string),"(( %d FT sikeresen befizetve a %d bankszámlára! ))",osszeg,banksz[playerid]);
				SendClientMessage(playerid,-1,string);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_BANK12)
	{
		if(response)
		{
			bankssz[playerid] = strval(inputtext);
			ShowPlayerDialog(playerid,DIALOG_BANK13,DIALOG_STYLE_INPUT,"Bank","Adja meg az összeget!","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_BANK13)
	{
		if(response)
		{
			new keres[128];
			new Cache:res;
			new rows;
			mysql_format(mysql,keres,sizeof(keres),"SELECT * FROM `bank` WHERE `azonosito` = '%d'",bankssz[playerid]);
			res = mysql_query(mysql,keres);
			if(cache_get_row_count(rows))
			{
				new osszeg,ujj,uj,van,vann,query[128],querya[128],sql[128],sqla[128],string[128];
				osszeg = strval(inputtext);
				mysql_format(mysql,query,sizeof(query),"SELECT `penz` FROM `bank` WHERE `azonosito` = '%d'",banksz[playerid]);
				res = mysql_query(mysql,query);
				cache_get_value_name_int(0,"penz",van);
				mysql_format(mysql,querya,sizeof(querya),"SELECT `penz` FROM `bank` WHERE `azonosito` = '%d'",bankssz[playerid]);
				res = mysql_query(mysql,querya);
				cache_get_value_name_int(0,"penz",vann);
				if(osszeg <= van)
				{
					uj = van-osszeg;
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`='%d' WHERE `azonosito` = '%d'",uj,banksz[playerid]);
					res = mysql_query(mysql,sql);
					ujj = vann+osszeg;
					mysql_format(mysql,sqla,sizeof(sqla),"UPDATE `bank` SET `penz`='%d' WHERE `azonosito` = '%d'",ujj,bankssz[playerid]);
					res = mysql_query(mysql,sqla);
					format(string,sizeof(string),"(( %d FT átutalva a %d bankszámlaszámra új egyenlege %d FT))",osszeg,bankssz[playerid],uj);
					SendClientMessage(playerid,-1,string);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs ennyi pénz a számládon! ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Hibás bankszámlaszám! ))");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_BANK14)
	{
		if(response)
		{
			if(listitem ==0)
			{
				if(PlayerInfo[playerid][fiztipus] ==0)
				{
					new penz,string[128];
					PlayerInfo[playerid][felfiz] = penz;
					PlayerInfo[playerid][Money] += PlayerInfo[playerid][felfiz];
					PlayerInfo[playerid][felfiz] =0;
					format(string,sizeof(string),"(( Felvetted a fizetésedet! %d FT ))",penz);
					SendClientMessage(playerid,-1,string);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Fizetésedet bankszámlára kapod! ))");
				}
			}
			if(listitem ==1)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new Cache:res;
				new rows;
				mysql_format(mysql,query,sizeof(query),"SELECT `azonosito` FROM `bank` WHERE `tulaj` = '%d'",PlayerInfo[playerid][dbid]);
				res = mysql_query(mysql,query);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"azonosito",lnevek,sizeof(lnevek));
					format(string,sizeof(string),"\n%s",lnevek);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_BANK15, DIALOG_STYLE_LIST, "Bank", dlg, "OK", "Mégse");
			}
		}
	}
	if(dialogid == DIALOG_BANK15)
	{
		if(response)
		{
			new szaml,pname[64],sql[128],string[128];
			new Cache:res;
			szaml = strval(inputtext);
			GetPlayerName(playerid,pname,sizeof(pname));
			mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `fizszam` = '%d', `fiztipus` = '1' WHERE `Name` = '%s'",szaml,pname);
			res = mysql_query(mysql,sql);
			PlayerInfo[playerid][fiztipus] = 1;
			cache_delete(res);
			format(string,sizeof(string),"(( Fizetésedet mostantól a %d számlaszámra kapod! ))",szaml);
			SendClientMessage(playerid,-1,string);
		}
	}
	/*if(dialogid == DIALOG_ADMIN1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				TogglePlayerSpectating(playerid, 1);
				PlayerSpectatePlayer(playerid, admindia[playerid]);
				SetPlayerInterior(playerid,GetPlayerInterior(admindia[playerid]));
			}
			if(listitem ==1)
			{
			
			}
			if(listitem ==2)
			{
				new query[256];
				new lnevek;
				new string[128];
				new dlg[256];
				new penz;
				new plate[16];
				format(query,sizeof(query),"SELECT `Id`,`model`,`Plate` FROM `vehicle` WHERE `Owner` = '%d'",PlayerInfo[admindia[playerid]][dbid]);
				mysql_query(query);
				mysql_store_result();
				while(mysql_fetch_row(query))
				{
					sscanf(query, "p<|>dds[16]",lnevek,penz,plate);
					format(string,sizeof(string),"\nID: %d\tModel:%s\tRendszám: %s",lnevek,GetVehicleModelName(penz,false),plate);
					strcat(dlg,string);
					ShowPlayerDialog(playerid, DIALOG_ADMIN3, DIALOG_STYLE_MSGBOX, "Admin", dlg, "OK", "Mégse");
				}
			}
			if(listitem ==3)
			{
				new query[256];
				new lnevek;
				new string[128];
				new dlg[256];
				new penz;
				format(query,sizeof(query),"SELECT `Id`,`Cost` FROM `house` WHERE `Owner` = '%d'",PlayerInfo[admindia[playerid]][dbid]);
				mysql_query(query);
				mysql_store_result();
				while(mysql_fetch_row(query))
				{
					sscanf(query, "p<|>dd",lnevek,penz);
					format(string,sizeof(string),"\nID: %d\tÉrtéke:%d",lnevek,penz);
					strcat(dlg,string);
					ShowPlayerDialog(playerid, DIALOG_ADMIN4, DIALOG_STYLE_MSGBOX, "Admin", dlg, "OK", "Mégse");
				}
			}		
			if(listitem ==4)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new penz;
				format(query,sizeof(query),"SELECT `azonosito`,`penz` FROM `bank` WHERE `tulaj` = '%d'",PlayerInfo[admindia[playerid]][dbid]);
				mysql_query(query);
				mysql_store_result();
				while(mysql_fetch_row(query))
				{
					sscanf(query, "p<|>s[64]d",lnevek,penz);
					format(string,sizeof(string),"\n%s - %d FT",lnevek,penz);
					strcat(dlg,string);
					ShowPlayerDialog(playerid, DIALOG_ADMIN5, DIALOG_STYLE_MSGBOX, "Admin", dlg, "OK", "Mégse");
				}
			}			
		}
	}
	if(dialogid == DIALOG_MOBIL1)
	{
		if(response)
		{
			telefonsz[playerid] = strval(inputtext);
			ShowPlayerDialog(playerid,DIALOG_MOBIL2,DIALOG_STYLE_LIST,"Mobil","Hívás\nSMS\nEgyenleg\nKikapcsol\nTelefonszám rejtése","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_MOBIL2)
	{
		if(response)
		{
			if(listitem ==0)
			{
				ShowPlayerDialog(playerid,DIALOG_MOBIL3,DIALOG_STYLE_INPUT,"Mobil","Adja meg a telefonszámot!","OK","Mégse");
			}
			if(listitem ==1)
			{
				ShowPlayerDialog(playerid,DIALOG_MOBIL4,DIALOG_STYLE_LIST,"Mobil","Új üzenet\nBejövõ SMS\nElküldött SMS","OK","Mégse");
			}
			if(listitem ==2)
			{
				new sql[256],string[256],egynl;
				format(sql,sizeof(sql),"SELECT `egyenleg` FROM `mobil` WHERE `telefonszam` = '%d'",telefonsz[playerid]);
				mysql_query(sql);
				mysql_store_result();
				mysql_fetch_row(sql);
				sscanf(sql,"p<|>d",egynl);
				format(string,sizeof(string),"((%d telefonszámon jelenleg %d FT egyneleg van))",telefonsz[playerid],egynl);
				SendClientMessage(playerid,-1,string);
			}
		}
	}
	if(dialogid == DIALOG_MOBIL3)
	{
		if(response)
		{
			new hivsz,hivszdbid;
			new sql[256],pnev[64];
			hivsz=strval(inputtext);
			format(sql,sizeof(sql),"SELECT `tulaj` FROM `mobil` WHERE `telefonszam`='%d'",hivsz);
			mysql_query(sql);
			mysql_store_result();
			if(mysql_num_rows() != 0)
			{
				mysql_fetch_row(sql);
				sscanf(sql,"p<|>d",hivszdbid);
				for(new i;i<MAX_PLAYERS;i++)
				{
					if(PlayerInfo[i][dbid] == hivszdbid)
					{
						if(phivasba[i] ==0)
						{
							hivta[playerid] =1;
							SendClientMessage(i,-1,"(( Csörög a telefonod (/felvesz) ))");
							GetPlayerName(i,pnev,sizeof(pnev));
							csorogsz[i] = 1;
							akihivo[playerid] = i;
							sscanf(pnev,"u",PlayerInfo[playerid][kithivsz]);
							hivo[i] = playerid;
						}
						else
						{
							SendClientMessage(playerid,-1,"((( Foglalt ))");
						}
					}
				}
			}
			else if(hivsz==107)
			{
				SendClientMessage(playerid,-1,"Ön az ORFK-t hívta");
				hivta[playerid] =1;
				phivasba[playerid] =1;
				bejelentes[playerid] =1;
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Ismeretlen telefonszám! ))");
			}
		}
	}
	if(dialogid == DIALOG_MOBIL4)
	{
		if(response)
		{
			if(listitem ==0)
			{
				ShowPlayerDialog(playerid,DIALOG_MOBIL5,DIALOG_STYLE_INPUT,"Mobil","Adja meg a telefonszámot!","OK","Mégse");
			}
			if(listitem ==1)
			{
				new sql[512];
				format(sql,sizeof(sql),"");
			}
		}
	}
	if(dialogid == DIALOG_MOBIL5)
	{
		if(response)
		{
			new sql[512];
			smssz[playerid]=strval(inputtext);
			format(sql,sizeof(sql),"SELECT * FROM `mobil` WHERE `telefonszam`='%d'",smssz[playerid]);
			mysql_query(sql);
			mysql_store_result();
			if(mysql_num_rows() != 0)
			{
				smssz[playerid] = strval(inputtext);
				ShowPlayerDialog(playerid,DIALOG_MOBIL6,DIALOG_STYLE_INPUT,"Mobil","Adja meg a szöveget","OK","Mégse");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Ismeretlen telefonszám! ))");
			}
		}
	}
	if(dialogid == DIALOG_MOBIL6)
	{
		if(response)
		{
			if(isnull(inputtext))return SendClientMessage(playerid,-1,"(( Nem adott meg szöveget! ))");
			new sql[512],asql[128];
			new string2[256];
			new hivszdbid;
			new year,mounth,day,hour,minute,second;
			getdate(year, mounth, day);
			gettime(hour,minute,second);
			format(string2,sizeof(string2),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
			format(sql,sizeof(sql),"INSERT INTO `sms` (kuldo,fogado,text,mikor) VALUES ('%d','%d','%s','%s')",telefonsz[playerid],smssz[playerid],inputtext,string2);
			mysql_query(sql);
			SendClientMessage(playerid,-1,"(( Üzenet sikeresen elküldve! ))");
			format(asql,sizeof(asql),"SELECT `tulaj` FROM `mobil` WHERE `telefonszam`='%d'",smssz[playerid]);
			mysql_query(asql);
			mysql_store_result();
			mysql_fetch_row(asql);
			sscanf(asql,"p<|>d",hivszdbid);
			for(new i;i<MAX_PLAYERS;i++)
			{
				if(PlayerInfo[i][dbid] == hivszdbid)
				{
					SendClientMessage(i,-1,"(( Önnek új üzenete érkezett! ))");
					break;
				}
			}
		}
	}*/
	if(dialogid == DIALOG_KULCS1)
	{
		if(response)
		{
			if(listitem ==0)
			{
				for(new vehid;vehid<MAX_VEHICLES;vehid++)
				{
					if(PlayerInfo[playerid][kulcs] == VehicleInfo[vehid][Plate])
					{
						new Float:vvx,Float:vvy,Float:vvz;
						GetVehiclePos(vehid,vvx,vvy,vvz);
						if(IsPlayerInRangeOfPoint(playerid,3.0,vvx,vvy,vvz))
						{
							new Float:vx,Float:vy,Float:vz;
							GetVehiclePos(vehid,vx,vy,vz);
							VehicleInfo[vehid][PosX] = vx;
							VehicleInfo[vehid][PosY] = vy;
							VehicleInfo[vehid][PosZ] = vz;
							SendClientMessage(playerid,-1,"(( Kocsi sikeresen leparkolva! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Túl messze vagy a kocsitól! ))");
						}
					}
				}
			}
			if(listitem ==1)
			{
				for(new vehid;vehid<MAX_VEHICLES;vehid++)
				{
					if(PlayerInfo[playerid][kulcs] == VehicleInfo[vehid][Plate])
					{
						new Float:vvx,Float:vvy,Float:vvz;
						GetVehiclePos(vehid,vvx,vvy,vvz);
						if(IsPlayerInRangeOfPoint(playerid,3.0,vvx,vvy,vvz))
						{
							new engine, lights, alarm, doors, bonnet, boot, objective;
							GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
							SetVehicleParamsEx(vehid, engine, lights, alarm, 0, bonnet, boot, objective);
							SendClientMessage(playerid,-1,"(( Kinyitotad a kocsit! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Túl messze vagy a kocsitól! ))");
						}
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_KULCS2)
	{
		if(response)
		{
			if(listitem ==0)
			{
				for(new vehid;vehid<MAX_VEHICLES;vehid++)
				{
					if(PlayerInfo[playerid][kulcs] == VehicleInfo[vehid][Plate])
					{
						new Float:vvx,Float:vvy,Float:vvz;
						GetVehiclePos(vehid,vvx,vvy,vvz);
						if(IsPlayerInRangeOfPoint(playerid,3.0,vvx,vvy,vvz))
						{
							new Float:vx,Float:vy,Float:vz;
							GetVehiclePos(vehid,vx,vy,vz);
							VehicleInfo[vehid][PosX] = vx;
							VehicleInfo[vehid][PosY] = vy;
							VehicleInfo[vehid][PosZ] = vz;
							SendClientMessage(playerid,-1,"(( Kocsi sikeresen leparkolva! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Túl messze vagy a kocsitól! ))");
						}
					}
				}
			}
			if(listitem ==1)
			{
				for(new vehid;vehid<MAX_VEHICLES;vehid++)
				{
					if(PlayerInfo[playerid][kulcs] == VehicleInfo[vehid][Plate])
					{
						new Float:vvx,Float:vvy,Float:vvz;
						GetVehiclePos(vehid,vvx,vvy,vvz);
						if(IsPlayerInRangeOfPoint(playerid,3.0,vvx,vvy,vvz))
						{
							new engine, lights, alarm, doors, bonnet, boot, objective;
							GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
							SetVehicleParamsEx(vehid, engine, lights, alarm, 1, bonnet, boot, objective);
							SendClientMessage(playerid,-1,"(( Bezártad a kocsit! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Túl messze vagy a kocsitól! ))");
						}
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_VBIZZ)
	{
		if(response)
		{
			if(listitem ==0)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						new string[128];
						format(string,sizeof(string),"Neve: %s",BizzInfo[bid][bizname]);
						SendClientMessage(playerid,-1,string);
						format(string,sizeof(string),"Ár: %d FT",BizzInfo[bid][Cost]);
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			if(listitem ==1)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						if(BizzInfo[bid][Cost] <= PlayerInfo[playerid][Money])
						{
							new pname[128];
							GetPlayerName(playerid, pname,sizeof(pname));
							BizzInfo[bid][Owner]  = PlayerInfo[playerid][dbid];
							BizzInfo[bid][Ownername] = pname;
							PlayerInfo[playerid][Money] -= BizzInfo[bid][Cost];
							SendClientMessage(playerid, -1, "((Sikeresen megveted a bizzniszt!))");
						}
						else
						{
							SendClientMessage(playerid, -1, "((Nincs elég pénzed!))");
						}
					}
				}
			}
			if(listitem ==2)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						SetPlayerPos(playerid,BizzInfo[bid][enterx],BizzInfo[bid][entery],BizzInfo[bid][enterz]);
						SetPlayerInterior(playerid,BizzInfo[bid][interior]);
						SetPlayerVirtualWorld(playerid, BizzInfo[bid][bvw]);
						playerinbolt[playerid] = true;
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_MBIZZ)
	{
		if(response)
		{
			if(listitem==0)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						new string[128];
						format(string,sizeof(string),"Neve: %s",BizzInfo[bid][bizname]);
						SendClientMessage(playerid,-1,string);
						format(string,sizeof(string),"Tulaj: %s",BizzInfo[bid][Ownername]);
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			if(listitem ==1)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						SetPlayerPos(playerid,BizzInfo[bid][enterx],BizzInfo[bid][entery],BizzInfo[bid][enterz]);
						SetPlayerInterior(playerid,BizzInfo[bid][interior]);
						SetPlayerVirtualWorld(playerid, BizzInfo[bid][bvw]);
						playerinbolt[playerid] = true;
					}
				}
			}
		}
	}
	if(dialogid ==DIALOG_SBIZZ)
	{
		if(response)
		{
			if(listitem ==0)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						new string[128];
						format(string,sizeof(string),"Neve: %s",BizzInfo[bid][bizname]);
						SendClientMessage(playerid,-1,string);
						format(string,sizeof(string),"Tulaj: %s",BizzInfo[bid][Ownername]);
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			if(listitem ==1)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						SetPlayerPos(playerid,BizzInfo[bid][enterx],BizzInfo[bid][entery],BizzInfo[bid][enterz]);
						SetPlayerInterior(playerid,BizzInfo[bid][interior]);
						SetPlayerVirtualWorld(playerid, BizzInfo[bid][bvw]);
						playerinbolt[playerid] = true;
					}
				}
			}
			if(listitem==2)
			{
				ShowPlayerDialog(playerid,DIALOG_BIZ1,DIALOG_STYLE_LIST,"Bizznisz kassza","Információ\nKivesz","OK","Mégse");
			}
			if(listitem ==3)
			{
				new string[32];
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						bizelad[playerid] = BizzInfo[bid][Cost]/10;
						bizelad[playerid] = bizelad[playerid]*8;
						format(string,sizeof(string),"Bizznisz eladási ára: %d FT",bizelad[playerid]);
						ShowPlayerDialog(playerid,DIALOG_BIZELAD,DIALOG_STYLE_MSGBOX,"Bizznisz eladás",string,"Elad","Mégse");
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_BIZ1)
	{
		if(response)
		{
			if(listitem ==0)
			{
				for(new bid;bid<MAX_BIZZ;bid++)
				{
					if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
					{
						new string[128];
						format(string,sizeof(string),"Kasszában jelenleg %s FT van.",cformat(BizzInfo[bid][kassza]));
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			if(listitem ==1)
			{
				ShowPlayerDialog(playerid,DIALOG_BIZ2,DIALOG_STYLE_INPUT,"Bizznisz kassza","Mennyit szeretnél kivenni?","OK","Mégse");
			}
		}
	}
	if(dialogid==DIALOG_BIZ2)
	{
		for(new bid;bid<MAX_BIZZ;bid++)
		{
			if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
			{
				new osszeg;
				new string[128];
				osszeg = strval(inputtext);
				if(BizzInfo[bid][kassza] >= osszeg)
				{
					BizzInfo[bid][kassza] -= osszeg;
					PlayerInfo[playerid][Money] += osszeg;
					format(string,sizeof(string),"(( %s FT sikeresen kivettél a kasszából! ))",cformat(osszeg));
					SendClientMessage(playerid,-1,string);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs ennyi pénz a kasszában! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_BIZELAD)
	{
		if(response)
		{
			for(new bid;bid<MAX_BIZZ;bid++)
			{
				if(IsPlayerInRangeOfPoint(playerid, 2.0, BizzInfo[bid][px],BizzInfo[bid][py],BizzInfo[bid][pz]))
				{
					BizzInfo[bid][Owner]  = 0;
					BizzInfo[bid][Ownername] = 0;
					PlayerInfo[playerid][Money] += bizelad[playerid];
					bizelad[playerid] =0;
					SendClientMessage(playerid,-1,"(( Sikeresen elatad a Bizzniszed! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_ALLEADER1)
	{
		if(response)
		{
			new Cache:res;
			if(listitem == 0)
			{
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Id` FROM `frakcio` WHERE `Hneve` = '%s'",inputtext);
				res = mysql_query(mysql,sql);
				cache_get_value_index_int(0,0,aleaderk[playerid]);
				if(aleaderk[playerid]==3)
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER2, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája\nFelvétel", "OK", "Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER2, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája", "OK", "Mégse");
				}
			}
			else
			{
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Id` FROM `frakcio` WHERE `Hneve` = '%s'",inputtext);
				res = mysql_query(mysql,sql);
				cache_get_value_index_int(0,0,aleaderk[playerid]);
				if(aleaderk[playerid]==3)
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER2, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája\nFelvétel", "OK", "Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER2, DIALOG_STYLE_LIST, "Vezetõség", "Alkalmazottak listája", "OK", "Mégse");
				}
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_ALLEADER2)
	{
		if(response)
		{
			if(listitem ==0)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new ids,rows;
				new Cache:res;
				mysql_format(mysql,query,sizeof(query),"SELECT `Name` FROM `user` WHERE `frakcio` = '%d'",aleaderk[playerid]);
				res = mysql_query(mysql,query);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"Name",lnevek,sizeof(lnevek));
					sscanf(lnevek, "u", ids);
					if(IsPlayerConnected(ids))
					{
						format(string,sizeof(string),"\n{00ff00}%s",lnevek);
						if(PlayerInfo[ids][pleader] ==1)
						{
							format(string,sizeof(string),"\n{ff0000}%s",lnevek);
						}
					}
					else
					{
						format(string,sizeof(string),"\n%s",lnevek);
					}
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_ALLEADER3, DIALOG_STYLE_LIST, "Vezetõség", dlg, "OK", "Mégse");
			}
			if(listitem ==1)
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER1, DIALOG_STYLE_INPUT,"Felvétel","Játékos neve vagy ID","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_ALLEADER3)
	{
		if(response)
		{
			new Cache:res;
			new sql[128],isaleader;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `aleader` FROM `user` WHERE `Name` = '%s'",inputtext);
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"aleader",isaleader);
			if(!listitem)
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				if(isaleader == 0)
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER4,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nÁthelyezés\nElbocsájtás\nElõléptetés fõosztály vezetõvé","OK","Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER4,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nÁthelyezés\nElbocsájtás\nFõosztály vezetõi rang megvonása","OK","Mégse");
				}
			}
			else
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				if(isaleader == 0)
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER4,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nÁthelyezés\nElbocsájtás\nElõléptetés fõosztály vezetõvé","OK","Mégse");
				}
				else
				{
					ShowPlayerDialog(playerid, DIALOG_ALLEADER4,DIALOG_STYLE_LIST,"Vezetõség","Információ\nElõléptetés/lefokozás\nÁthelyezés\nElbocsájtás\nFõosztály vezetõi rang megvonása","OK","Mégse");
				}
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_ALLEADER4)
	{
		if(response)
		{
			if(listitem == 0)
			{
				new sql[128];
				new asql[256];
				new mrang[129];
				new string[128];
				new szrang;
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql), "SELECT `frang` FROM `user` WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"frang",szrang);
				mysql_format(mysql,asql, sizeof(asql), "SELECT `%d` FROM `frakcio` WHERE `Id` = '%d'", szrang,aleaderk[playerid]);
				res = mysql_query(mysql,asql);
				cache_get_value_index(0,0,mrang,sizeof(mrang));
				format(string,sizeof(string), "Név:%s\nRang:%s",PlayerInfo[playerid][kival],mrang);
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_LEADER4,DIALOG_STYLE_MSGBOX,"Adatok",string,"OK","");
			}
			if(listitem == 1)
			{
				new query[256];
				new string[128];
				new dlg[256];
				new frakok[512];
				new Cache:res;
				for(new i = 1; i < MAX_RANGOK; i++)
				{
					mysql_format(mysql,query,sizeof(query),"SELECT `%d` FROM `frakcio` WHERE `Id` = '%d'",i,aleaderk[playerid]);
					res = mysql_query(mysql,query);
					cache_get_value_index(0,0,frakok,sizeof(frakok));
					format(string,sizeof(string),"\n%s",frakok);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_LEADER5, DIALOG_STYLE_LIST, "Rangok", dlg, "OK", "Mégse");
			}
			if(listitem ==2)
			{
				new sql[128];
				new string[128];
				new dlg[256];
				new Cache:res;
				new rows;
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve` FROM `frakcio` WHERE `Alosztaly` = '%d'",PlayerInfo[playerid][alosztaly]);
				res = mysql_query(mysql,sql);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"Hneve",string,sizeof(string));
					format(string,sizeof(string),"\n%s",string);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid,DIALOG_ALLEADER5,DIALOG_STYLE_LIST,"Vezetõség",dlg,"OK","Mégse");
			}
			if(listitem == 3)
			{
				ShowPlayerDialog(playerid, DIALOG_LEADER6,DIALOG_STYLE_MSGBOX,"Elbocsájtás","Biztos kiakarod rugni?","Igen","Nem");
			}
			if(listitem == 4)
			{
				new sql[128],isaleader,ids;
				new Cache:res;
				sscanf(PlayerInfo[playerid][kival], "u", ids);
				mysql_format(mysql,sql,sizeof(sql),"SELECT `aleader` FROM `user` WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"aleader",isaleader);
				if(isaleader == 0)
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `aleader` = '1' WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
					if(IsPlayerConnected(ids))
					{
						PlayerInfo[ids][paleader] = 1;
						SendClientMessage(ids,-1,"(( Elõléptettek fõosztály vezetõvé ))");
					}
				}
				else
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `aleader` = '0' WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
					if(IsPlayerConnected(ids))
					{
						PlayerInfo[ids][paleader] = 0;
						SendClientMessage(ids,-1,"(( Megvonták a fõosztály vezetõi rangod ))");
					}
				}
				cache_delete(res);
			}
		}
	}
	if(dialogid == DIALOG_ALLEADER5)
	{
		if(response)
		{
			if(listitem ==0)
			{
				new sql[256];
				new kiv;
				new ids;
				new Cache:res;
				sscanf(PlayerInfo[playerid][kival], "u", ids);
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Id` FROM `frakcio` WHERE `Hneve` = '%s'",inputtext);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"Id",kiv);
				if(IsPlayerConnected(ids))
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `frakcio`='%d' WHERE `Name` = '%s'",kiv,PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
					PlayerInfo[ids][frakcio]=kiv;
					SendClientMessage(playerid,-1,"(( Sikeresen áthelyezted ))");
					SendClientMessage(ids,-1,"(( Átlettél helyezve! ))");
					mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
					mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
				}
				else
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `frakcio`='%d' WHERE `Name` = '%s'",kiv,PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
				}
				cache_delete(res);
			}
			else
			{
				new sql[256];
				new kiv;
				new ids;
				new Cache:res;
				sscanf(PlayerInfo[playerid][kival], "u", ids);
				mysql_format(mysql,sql,sizeof(sql),"SELECT `Id` FROM `frakcio` WHERE `Hneve` = '%s'",inputtext);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"Id",kiv);
				if(IsPlayerConnected(ids))
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `frakcio`='%d' WHERE `Name` = '%s'",kiv,PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
					PlayerInfo[ids][frakcio]=kiv;
					SendClientMessage(playerid,-1,"(( Sikeresen áthelyezted ))");
					SendClientMessage(ids,-1,"(( Átlettél helyezve! ))");
					mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[ids][rang],PlayerInfo[ids][rang],PlayerInfo[ids][frakcio]);
					mysql_tquery(mysql, sql, "LoadFrakcio", "d", ids);
				}
				else
				{
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `frakcio`='%d' WHERE `Name` = '%s'",kiv,PlayerInfo[playerid][kival]);
					res = mysql_query(mysql,sql);
				}
				cache_delete(res);
			}
		}
	}
	if(dialogid == DIALOG_FELRAK1)
	{
		if(response)
		{
			new str[128];
			new mennyis;
			sscanf(inputtext, "p<\t>s[32]d", str,mennyis);
			new bool:talal;
			new i;
			while(i < sizeof(fegyvernev) && talal == false)
			{
				if(CompareEx(fegyvernev[i],str))
				{
					talal = true;
				}
				else
				{
					i++;
				}
			}
			if(talal == true)
			{
				AddItem(playerid,str,mennyis);
				weapons[playerid][listitem][0] = 0;
				weapons[playerid][listitem][1] = 0;
				ResetPlayerWeapons(playerid);
				for (new x = 0; x < 13; x++)
				{
					GivePlayerWeapon(playerid,weapons[playerid][x][0],weapons[playerid][x][1]);
				}
			}
		}
	}
	if(dialogid == DIALOG_SZERELO1)
	{
		if(response)
		{
			if(szerelesben[playerid] == false)
			{
				new vehicleid = GetClosestVehicle(playerid,2.5);
				SetTimerEx("pszerel",6000,false,"udd",playerid,vehicleid,listitem);
				Cselekves(playerid,"elkezdi szerelni a jármûvet.");
				szerelesben[playerid] = true;
				ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,0,6000,1);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Már szerelsz! ))");
			}
		}
	}
	if(dialogid == DIALOG_SZERELO3)
	{
		if(response)
		{
			if(listitem == 0)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO4,DIALOG_STYLE_LIST,"Kereskedés","Leviathan\nSkimmer\nSparrow\nRustler\nMaverick\nBeagle\nCropduster\nStuntplane\nShamal\nCargobob\nNevada\nAT-400\nAndromada\nDodo","OK","Mégse");
				new jkocsik1[] = {417,460,469,476,487,511,512,513,519,548,553,577,592,593};
				ShowModelSelectionMenuEx(playerid, jkocsik1, sizeof(jkocsik1), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 1)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO5,DIALOG_STYLE_LIST,"Kereskedés","Faggio\nBF-400\nNRG-500\nPCJ-600\nFCR-900\nFreeway\nWayfarer\nSanchez\nQuad","OK","Mégse");
				new jkocsik2[] = {462,581,522,461,521,463,586,468,471};
				ShowModelSelectionMenuEx(playerid, jkocsik2, sizeof(jkocsik2), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 2)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO6,DIALOG_STYLE_LIST,"Kereskedés","Comet\nFeltzer\nStallion\nWindsor","OK","Mégse");
				new jkocsik3[] = {480,533,439,555};
				ShowModelSelectionMenuEx(playerid, jkocsik3, sizeof(jkocsik3), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 3)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO7,DIALOG_STYLE_LIST,"Kereskedés","Benson\nBobcat\nBurrito\nBoxville\nBoxburg\nDFT-30\nFlatbed\nLinerunner\nMule\nPetrol Tanker\nPicador\nPony\nRoadtrain\nRumpo\nSadler\nTopfun\nTopfun\nWalton\nYankee\nYosemite","OK","Mégse");
				new jkocsik4[] = {499,422,482,498,609,578,455,403,414,514,600,413,515,440,543,459,478,456,554};
				ShowModelSelectionMenuEx(playerid, jkocsik4, sizeof(jkocsik4), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 4)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO8,DIALOG_STYLE_LIST,"Kereskedés","Blade\nBroadway\nRemington\nSavanna\nSlamvan\nTahoma\nTornado\nVoodoo","OK","Mégse");
				new jkocsik5[] = {536,575,534,567,535,566,576,412};
				ShowModelSelectionMenuEx(playerid, jkocsik5, sizeof(jkocsik5), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem==5)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO9,DIALOG_STYLE_LIST,"Kereskedés","Bandito\nBF Injection\nHuntley\nLandstalker\nMesa\nPatriot\nRancher\nSandking","OK","Mégse");
				new jkocsik6[] = {568,424,579,400,500,470,489,495};
				ShowModelSelectionMenuEx(playerid, jkocsik6, sizeof(jkocsik6), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 6)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO10,DIALOG_STYLE_LIST,"Kereskedés","Admiral\nBloodring Banger\nBravura\nBuccaneer\nCadrona\nClover\nElegant\nElegy\nEmperor\nEsperanto\nFortune\nGlendale\nGreenwood\nHermes\nIntruder\nMajestic\nManana\nMerit\nNebula\nOceanic\nPremier\nPrevion\nPrimo\nSentinel\nStafford\nSultan\nSunrise\nTampa\nVincent\nVirgo\nWillard\nWashington","OK","Mégse");
				new jkocsik7[] = {445,504,401,518,527,542,507,562,585,419,526,466,492,474,546,517,410,551,516,467,426,436,547,405,580,560,550,549,540,491,529,421};
				ShowModelSelectionMenuEx(playerid, jkocsik7, sizeof(jkocsik7), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 7)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO11,DIALOG_STYLE_LIST,"Kereskedés","Alpha\nBanshee\nBlista Compact\nBuffalo\nBullet\nCheetah\nClub\nEuros\nFlash\nHotring Racer\nInfernus\nJester\nPhoenix\nSabre\nSuper GT\nTurismo\nUranus\nZR-350","OK","Mégse");
				new jkocsik8[] = {602,429,496,402,541,415,589,587,565,494,411,559,603,475,506,451,558,477};
				ShowModelSelectionMenuEx(playerid, jkocsik8, sizeof(jkocsik8), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
			if(listitem == 8)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO12,DIALOG_STYLE_LIST,"Kereskedés","Moonbeam\nPerenniel\nRegina\nSolair\nStratum","OK","Mégse");
				new jkocsik9[] = {418,404,479,458,561};
				ShowModelSelectionMenuEx(playerid, jkocsik9, sizeof(jkocsik9), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}			
			if(listitem == 9)
			{
				//ShowPlayerDialog(playerid,DIALOG_SZERELO13,DIALOG_STYLE_LIST,"Kereskedés","Dinghy\nJetmax\nLaunch\nMarquis\nReefer\nSpeeder\nSquallo\nTropic","OK","Mégse");
				new jkocsik10[] = {473,493,595,484,453,452,446,454};
				ShowModelSelectionMenuEx(playerid, jkocsik10, sizeof(jkocsik10), "Válassz egy jármûvet!", JARMURENDEL_MENU, 16.0, 0.0, -55.0);
			}
		}
	}
	if(dialogid == DIALOG_TELEPORT)
	{
		if(response)
		{
			new sql[128];
			new Float:x,Float:y,Float:z;
			new tint,tvw;
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `x`,`y`,`z`,`vw`,`int` FROM `teleport` WHERE `nev` = '%s'",inputtext);
			res = mysql_query(mysql,sql);
			cache_get_value_name_float(0,"x",x);
			cache_get_value_name_float(0,"y",y);
			cache_get_value_name_float(0,"z",z);
			cache_get_value_name_int(0,"vw",tvw);
			cache_get_value_name_int(0,"int",tint);
			SetPlayerPos(playerid,x,y,z);
			SetPlayerInterior(playerid,tint);
			SetPlayerVirtualWorld(playerid,tvw);
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_SZERELO4)
	{
		if(response)
		{
			new jkocsik1[] = {417,460,469,476,487,511,512,513,519,548,553,577,592,593};
			jrendelmodel[playerid] = jkocsik1[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO5)
	{
		if(response)
		{
			new jkocsik2[] = {462,581,522,461,521,463,586,468,471};
			jrendelmodel[playerid] = jkocsik2[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO6)
	{
		if(response)
		{
			new jkocsik3[] = {480,533,439,555};
			jrendelmodel[playerid] = jkocsik3[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO7)
	{
		if(response)
		{
			new jkocsik4[] = {499,422,482,498,609,578,455,403,414,514,600,413,515,440,543,459,478,456,554};
			jrendelmodel[playerid] = jkocsik4[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO8)
	{
		if(response)
		{
			new jkocsik5[] = {536,575,534,567,535,566,576,412};
			jrendelmodel[playerid] = jkocsik5[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO9)
	{
		if(response)
		{
			new jkocsik6[] = {568,424,579,400,500,470,489,495};
			jrendelmodel[playerid] = jkocsik6[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO10)
	{
		if(response)
		{
			new jkocsik7[] = {445,504,401,518,527,542,507,562,585,419,526,466,492,474,546,517,410,551,516,467,426,436,547,405,580,560,550,549,540,491,529,421};
			jrendelmodel[playerid] = jkocsik7[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO11)
	{
		if(response)
		{
			new jkocsik8[] = {602,429,496,402,541,415,589,587,565,494,411,559,603,475,506,451,558,477};
			jrendelmodel[playerid] = jkocsik8[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO12)
	{
		if(response)
		{
			new jkocsik9[] = {418,404,479,458,561};
			jrendelmodel[playerid] = jkocsik9[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO13)
	{
		if(response)
		{
			new jkocsik10[] = {473,493,595,484,453,452,446,454};
			jrendelmodel[playerid] = jkocsik10[listitem];
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO14)
	{
		if(response)
		{
			jszin1[playerid] = strval(inputtext);
			ShowPlayerDialog(playerid,DIALOG_SZERELO15,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 2", "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_SZERELO15)
	{
		if(response)
		{
			jszin1[playerid] = strval(inputtext);
			if(GetVehicleType(jrendelmodel[playerid]) != HELICOPTER && GetVehicleType(jrendelmodel[playerid]) != BOAT)
			{
				ShowPlayerDialog(playerid,DIALOG_SZERELO16,DIALOG_STYLE_LIST,"Kereskedés", "Benzin\nDiesel", "OK", "Mégse");
			}
			if(GetVehicleType(jrendelmodel[playerid]) == HELICOPTER && GetVehicleType(jrendelmodel[playerid]) == BOAT)
			{
				ShowPlayerDialog(playerid,DIALOG_SZERELO16,DIALOG_STYLE_LIST,"Kereskedés", "Kerozin", "OK", "Mégse");
			}
		}
	}
	if(dialogid == DIALOG_SZERELO16)
	{
		if(response)
		{
			jszin2[playerid] = strval(inputtext);
			new sql[256];
			new ados,afas,bpenz;
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `penz` FROM `bank` WHERE `frakcio`='13'");
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"penz",bpenz);
			if(bpenz >= vehdata[jrendelmodel[playerid]-400][2])
			{
				afaado(vehdata[jrendelmodel[playerid]-400][2],afas,ados);
				allamibevetel(afas);
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`=`penz`-'%d' WHERE `frakcio`='13'",ados);
				res = mysql_query(mysql,sql);

				new Float:uzem;
				new plate[32];
				new Query[512];
				new amount = jrendelmodel[playerid];
				new colork = jszin1[playerid];
				new colorkk= jszin2[playerid];
				new owne=0;
				new ownen[64];
				new vsmunka=0;
				new vsfrakcio=13;
				new Float:x,Float:y,Float:z,Float:face;
				x= 2497.508;
				y= ((2559+random(52)) * -1);
				z= 13.356;
				face = 90.994;
				format(plate,sizeof(plate),"%c%c%c-%d%d%d",random(25)+65,random(25)+65,random(25)+65,random(9),random(9),random(9));
				format(ownen,sizeof(ownen),"NINCS");
				uzem = vehdata[amount-400][0];
				mysql_format(mysql,Query,sizeof(Query),"INSERT INTO `vehicle`(Owner,Ownername,Model,PosX,PosY,PosZ,PosFace,Color1,Color2,Plate,munka,frakcio,uzemanyagt) VALUES ('%d','%s','%d','%f','%f','%f','%f','%d','%d','%s','%d','%d','%d')",owne,ownen,amount,x,y,z,face,colork,colorkk,plate,vsmunka,vsfrakcio,uzem);
				res = mysql_query(mysql,Query);
				cache_delete(res);
				mysql_format(mysql,Query, sizeof(Query), "SELECT `Id`,`Plate` FROM `vehicle` ORDER BY `Id` DESC LIMIT 1");
				res = mysql_query(mysql,Query);
				new plates[64],idss;
				cache_get_value_name_int(0,"Id",idss);
				cache_get_value_name(0,"Plate",plates,sizeof(plates));
				format(Query,sizeof(Query),"INSERT INTO `kesztyutarto`(`dbid`, `nev`, `mennyiseg`) VALUES ('%d','Jármûkulcs %s','1')",idss,plates);
				res = mysql_query(mysql,Query);
				res = mysql_query(mysql,Query);
				cache_delete(res);
				mysql_format(mysql,Query, sizeof(Query), "SELECT * FROM `vehicle` ORDER BY `Id` DESC LIMIT 1");
				mysql_tquery(mysql,Query,"LoadVehicle");
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Jármû leszállítva az LS dokkokhoz! ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs elég pénz a bankszámlán! ))");
			}
		}
	}
	if(dialogid == DIALOG_RENTH1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						new string[128];
						new string2[129];
						format(string,sizeof(string), "Házszám: %d", RentInfo[i][dbid]);
						format(string2,sizeof(string2),"Ára: %d",RentInfo[i][rcost]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						if(PlayerInfo[playerid][Money] >= RentInfo[i][rcost])
						{
							PlayerInfo[playerid][Money] -= RentInfo[i][rcost];
							RentInfo[i][owner] = PlayerInfo[playerid][dbid];
							sscanf(Nev(playerid),"s[64]",RentInfo[i][ownername]);
							new year, month, day, hour, minute,second;
							new date;
							getdate(year, month, day);
							gettime( hour, minute,second);
							new btime = day+1;
							date = mktime(hour, minute, second, btime, month, year);
							RentInfo[i][rido] = date;
							DestroyPickup(RentInfo[i][pickid]);
							RentInfo[i][pickid] = CreatePickup(1239, 1, RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz],RentInfo[i][rvw]);
							SendClientMessage(playerid,-1,"(( Sikeresen kibérelted a házat 1 napra! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
						}
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_RENTH2)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						new string[128];
						new string2[129];
						format(string,sizeof(string), "Házszám: %d", RentInfo[i][dbid]);
						format(string2,sizeof(string2),"Tulajdonos: %s",RentInfo[i][ownername]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
						new day, month, year, hour, minute, second;
						datemk(RentInfo[i][rido], day, month, year, hour, minute, second);
						format(string,sizeof(string),"Bérlési idõ: %d-%d-%d",year,month,day);
						SendClientMessage(playerid,-1,string);
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						if(RentInfo[i][rzarvar] == 0)
						{
							SetPlayerPos(playerid,266.500, 305.075, 999.148);
							SetPlayerInterior(playerid,2);
							SetPlayerFacingAngle(playerid,270.256);
							SetPlayerVirtualWorld(playerid,i);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Zárva van! ))");
						}
					}
				}
			}
			if(listitem == 2)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						if(RentInfo[i][rzarvar] == 0)
						{
							RentInfo[i][rzarvar] = 1;
							SendClientMessage(playerid,-1,"(( Bezártad az ajtót ! ))");
						}
						else
						{
							RentInfo[i][rzarvar] = 0;
							SendClientMessage(playerid,-1,"(( Kinyitotad az ajtót ! ))");
						}
					}
				}
			}
			if(listitem == 3)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						ShowPlayerDialog(playerid,DIALOG_RENTH4,DIALOG_STYLE_INPUT,"Bérlés","Hány napra szeretnéd meghosszabítani a bérlést?","OK","Mégse");
					}
				}
			}
			if(listitem == 4)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						RentInfo[i][owner] = 0;
						DestroyPickup(RentInfo[i][pickid]);
						RentInfo[i][pickid] = CreatePickup(1272, 1, RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz],RentInfo[i][rvw]);
						SendClientMessage(playerid,-1,"(( Sikeresen lemontad a bérlést ! ))");
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_RENTH3)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						new string[128];
						new string2[129];
						format(string,sizeof(string), "Házszám: %d", RentInfo[i][dbid]);
						format(string2,sizeof(string2),"Tulajdonos: %s",RentInfo[i][ownername]);
						SendClientMessage(playerid,-1,string);
						SendClientMessage(playerid,-1,string2);
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_RENTH;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
					{
						if(RentInfo[i][rzarvar] == 0)
						{
							SetPlayerPos(playerid,266.500, 305.075, 999.148);
							SetPlayerInterior(playerid,2);
							SetPlayerFacingAngle(playerid,270.256);
							SetPlayerVirtualWorld(playerid,i);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Zárva van! ))");
						}
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_RENTH4)
	{
		if(response)
		{
			for(new i;i<MAX_RENTH;i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,2.0,RentInfo[i][rx],RentInfo[i][ry],RentInfo[i][rz]))
				{
					new rday,btam,btam2;
					new ossz1,ossz2;
					new string[128];
					rday = strval(inputtext);
					new f_day, f_month, f_year, f_hour, f_min, f_sec;
					new day, month, year, hour, minute, second;
					datemk(gettime(), f_day, f_month, f_year, f_hour, f_min, f_sec);
					datemk(RentInfo[i][rido], day, month, year, hour, minute, second);
					btam = day+rday;
					btam2 = f_day+15;
					ossz1 = mktime(hour, minute, second, btam, month, year);
					ossz2 = mktime(f_hour, f_min, f_sec, btam2, f_month, f_year);
					if(ossz1 <= ossz2)
					{
						if(PlayerInfo[playerid][Money] >= RentInfo[i][rcost]*rday)
						{
							PlayerInfo[playerid][Money] -= RentInfo[i][rcost]*rday;
							RentInfo[i][rido] = ossz1;
							format(string,sizeof(string),"(( Sikeresen meghosszabítottad a bérlést %d napra))",rday);
							SendClientMessage(playerid,-1,string);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Maximum 15 nap! ))");
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_CEG1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid, DIALOG_CEG2, DIALOG_STYLE_INPUT,"Felvétel","Játékos neve vagy ID","OK","Mégse");
			}
			if(listitem == 1)
			{
				new query[256];
				new lnevek[512];
				new string[128];
				new dlg[256];
				new Cache:res;
				new rows;
				mysql_format(mysql,query,sizeof(query),"SELECT `Name` FROM `user` WHERE `ceg` = '%d'",PlayerInfo[playerid][pceg]);
				res = mysql_query(mysql,query);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"Name",lnevek,sizeof(lnevek));
					format(string,sizeof(string),"%s\n",lnevek);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid, DIALOG_CEG3, DIALOG_STYLE_LIST, "Cég", dlg, "OK", "Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid, DIALOG_CEG4, DIALOG_STYLE_INPUT,"Cég","Fizetés összege","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_CEG2)
	{
		if(response)
		{
			new ids;
			if(sscanf(inputtext, "u", ids))return SendClientMessage(playerid, -1, "(( Nem atad meg a nevet/id! ))");
			else if(!IsPlayerConnected(ids))return SendClientMessage(playerid, COLOR_RED, "(( Hibás ID ))");
			else
			{
				if(PlayerInfo[ids][pceg] != 0)
				{
					SendClientMessage(playerid,-1,"((  Cég tagja! ))");
				}
				else
				{
					SendClientMessage(ids,-1,"((  Felajánlottak egy céget! ))");
					SendClientMessage(ids,-1,"((  /elfogad ceg ))");
					PlayerInfo[ids][elfogad] = PlayerInfo[playerid][pceg];
					PlayerInfo[ids][rang] = 1;
					PlayerInfo[ids][felajan] =1;
				}
			}
		}
	}
	if(dialogid == DIALOG_CEG3)
	{
		if(response)
		{
			if(!listitem)
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				ShowPlayerDialog(playerid, DIALOG_CEG5,DIALOG_STYLE_LIST,"Cég","Elbocsájtás","OK","Mégse");
			}
			else
			{
				sscanf(inputtext, "s[32]", PlayerInfo[playerid][kival]);
				ShowPlayerDialog(playerid, DIALOG_CEG5,DIALOG_STYLE_LIST,"Cég","Elbocsájtás","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_CEG4)
	{
		if(response)
		{
			new cosszeg;
			cosszeg = strval(inputtext);
			new sql[128];
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"UPDATE `ceg` SET `fizetes`='%d'",cosszeg);
			res = mysql_query(mysql,sql);
			cache_delete(res);
			SendClientMessage(playerid,-1,"(( Sikeresen átálítottad a fizetést ))");
		}
	}
	if(dialogid == DIALOG_CEG5)
	{
		if(response)
		{
			if(listitem == 0)
			{
				new sql[128];
				new ids;
				if(sscanf(PlayerInfo[playerid][kival], "u", ids))return SendClientMessage(playerid, COLOR_RED, "(( Szegmentális hiba! ))");
				else
				{
					if(IsPlayerConnected(ids))
					{
						PlayerInfo[ids][pceg] = 0;
						SendClientMessage(ids, COLOR_RED, "(( A fõnököd kirúgott! ))");
						SendClientMessage(playerid, COLOR_RED, "(( Kirúgva!))");
					}
					else
					{
						new Cache:res;
						mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `ceg`='0' WHERE `Name` = '%s'",PlayerInfo[playerid][kival]);
						res = mysql_query(mysql,sql);
						cache_delete(res);
						SendClientMessage(playerid, COLOR_RED, "(( Kirúgva!))");
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_CEG6)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_CEG;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
					{
						new str[128];
						format(str,sizeof(str),"Neve: %s",CegInfo[i][cnev]);
						SendClientMessage(playerid,-1,str);
						format(str,sizeof(str),"Ára: %d",CegInfo[i][ccost]);
						SendClientMessage(playerid,-1,str);
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_CEG;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
					{
						if(PlayerInfo[playerid][Money] >= CegInfo[i][ccost])
						{
							PlayerInfo[playerid][Money] -= CegInfo[i][ccost];
							CegInfo[i][cowner] = PlayerInfo[playerid][dbid];
							sscanf(Nev(playerid),"s[64]",CegInfo[i][cownername]);
							PlayerInfo[playerid][cegleader] = 1;
							PlayerInfo[playerid][pceg] = CegInfo[i][dbid];
							SendClientMessage(playerid,-1,"(( Sikeresen megveted a céget! ))");
							saveplayer(playerid);
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
						}
					}
				}	
			}
		}
	}
	if(dialogid == DIALOG_CEG7)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_CEG;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
					{
						new str[128];
						format(str,sizeof(str),"Neve: %s",CegInfo[i][cnev]);
						SendClientMessage(playerid,-1,str);
						format(str,sizeof(str),"Ára: %d",CegInfo[i][ccost]);
						SendClientMessage(playerid,-1,str);
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_CEG;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
					{
						new string[128];
						format(string,sizeof(string),"Cég eladási ára: %d FT",CegInfo[i][ccost]);
						ShowPlayerDialog(playerid,DIALOG_CEG9,DIALOG_STYLE_MSGBOX,"Bizznisz eladás",string,"Elad","Mégse");
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_CEG8)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_CEG;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
					{
						new str[128];
						format(str,sizeof(str),"Neve: %s",CegInfo[i][cnev]);
						SendClientMessage(playerid,-1,str);
						format(str,sizeof(str),"Ára: %d",CegInfo[i][ccost]);
						SendClientMessage(playerid,-1,str);
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_CEG9)
	{
		if(response)
		{
			for(new i;i<MAX_CEG;i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,2.0,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]))
				{
					CegInfo[i][cowner] = 0;
					PlayerInfo[playerid][Money] += CegInfo[i][ccost];
					PlayerInfo[playerid][pceg] = 0;
					PlayerInfo[playerid][cegleader] = 0;
					saveplayer(playerid);
					SendClientMessage(playerid,-1,"(( Sikeresen eladtad a cégedet! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_KAMION1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				new vehid;
				vehid = GetVehicleTrailer(GetPlayerVehicleID(playerid));
				if(GetVehicleModel(vehid) == 591)
				{
					SetTimerEx("kamionbepakol",60000,false,"u",playerid);
					TogglePlayerControllable(playerid,false);
					GameTextForPlayer(playerid,"Berakodás folyamatban ...",60000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem megfelelõ pótkocsi! ))");
				}
			}
			if(listitem == 1)
			{
				new vehid;
				vehid = GetVehicleTrailer(GetPlayerVehicleID(playerid));
				if(GetVehicleModel(vehid) == 435)
				{
					SetTimerEx("kamionbepakol",60000,false,"u",playerid);
					TogglePlayerControllable(playerid,false);
					GameTextForPlayer(playerid,"Berakodás folyamatban ...",60000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nem megfelelõ pótkocsi! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_3DLABEL1)
	{
		if(response)
		{
			sscanf(inputtext,"s[512]",create3dsz[playerid]);
			ShowPlayerDialog(playerid,DIALOG_3DLABEL2,DIALOG_STYLE_INPUT,"3D LABEL CREATOR","Szöveg színe","Tovább","Mégse");
		}
	}
	if(dialogid == DIALOG_3DLABEL2)
	{
		if(response)
		{
			sscanf(inputtext,"h",create3dc[playerid]);
			ShowPlayerDialog(playerid,DIALOG_3DLABEL3,DIALOG_STYLE_INPUT,"3D LABEL CREATOR","Szöveg látótávolság","Tovább","Mégse");
		}
	}
	if(dialogid == DIALOG_3DLABEL3)
	{
		if(response)
		{
			new Float:X, Float:Y, Float:Z,virtualworld;
			new query[512];
			sscanf(inputtext,"f",create3dt[playerid]);
			
			GetPlayerPos(playerid, X, Y, Z);
			virtualworld = GetPlayerVirtualWorld(playerid);
			mysql_format(mysql,query,sizeof(query),"INSERT INTO `3dtextlabel` (text,color,x,y,z,distance,vw,testLOS) VALUES ('%s','%h','%.3f','%.3f','%.3f','%.3f','%d','0')",create3dsz[playerid],create3dc[playerid],X,Y,Z,create3dt[playerid],virtualworld);
			new Cache:res = mysql_query(mysql,query);
			cache_delete(res);
			Create3DTextLabel(create3dsz[playerid],create3dc[playerid],X,Y,Z,create3dt[playerid],virtualworld,0);
		}
	}
	if(dialogid == DIALOG_KOCSIATVESZ)
	{
		if(response)
		{
			if((PlayerInfo[playerid][Money]-VehicleInfo[atveszkocsid[playerid]][ktartozas])>=0 && VehicleInfo[atveszkocsid[playerid]][vfrakcio] == 0)
			{
				if(VehicleInfo[atveszkocsid[playerid]][vfrakcio] == 0)
				{
					new adobe,adozott;
					new sql[256];
					new Cache:res;
					afaado(VehicleInfo[atveszkocsid[playerid]][ktartozas],adobe,adozott);
					allamibevetel(adobe);
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`='%d'+`penz` WHERE `frakcio`='9'",adozott);
					res = mysql_query(mysql,sql);
					PlayerInfo[playerid][Money] -= VehicleInfo[atveszkocsid[playerid]][ktartozas];
					VehicleInfo[atveszkocsid[playerid]][ktartozas] = 0;
					VehicleInfo[atveszkocsid[playerid]][lefoglalva] = 0;
					cache_delete(res);
					SendClientMessage(playerid,-1,"(( Sikersen átvetted a kocsit! ))");
				}
			}
			else if(VehicleInfo[atveszkocsid[playerid]][vfrakcio] != 0)
			{
				new Cache:res;
				new azon,bpenz,adobe,adozott;
				new sql[256];
				mysql_format(mysql,sql,sizeof(sql),"SELECT `azonosito`,`penz` FROM `bank` WHERE `frakcio`='%d'",VehicleInfo[atveszkocsid[playerid]][vfrakcio]);
				res = mysql_query(mysql,sql);
				cache_get_value_name_int(0,"azonosito",azon);
				cache_get_value_name_int(0,"penz",bpenz);
				if((bpenz-VehicleInfo[atveszkocsid[playerid]][ktartozas])>=0)
				{
					afaado(VehicleInfo[atveszkocsid[playerid]][ktartozas],adobe,adozott);
					allamibevetel(adobe);
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`=`penz`-'%d' WHERE `azonosito`='%d'",adozott,azon);
					res = mysql_query(mysql,sql);
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz`='%d'+`penz` WHERE `frakcio`='9'",adozott);
					res = mysql_query(mysql,sql);
					VehicleInfo[atveszkocsid[playerid]][ktartozas] = 0;
					VehicleInfo[atveszkocsid[playerid]][lefoglalva] = 0;
					SendClientMessage(playerid,-1,"(( Sikersen átvetted a kocsit! ))");
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs elég pénz a bankszámlán! ))");
				}
				cache_delete(res);
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
			}
		}
	}
	if(dialogid == DIALOG_ORFK1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				SetPlayerSkin(playerid,285);
				SetPlayerArmour(playerid,100);
				SetPlayerHealth(playerid,100);
				GivePlayerWeapon(playerid,31,500);
				GivePlayerWeapon(playerid,24,50);
				GivePlayerWeapon(playerid,17,1);
				PlayerInfo[playerid][pszolg] = 1;
				Cselekves(playerid, "szolgálatba lépett");
				fkradio[playerid] = 1;
				felszerelesben[playerid] = true;
			}
			if(listitem == 1)
			{
				SetPlayerSkin(playerid,285);
				SetPlayerArmour(playerid,100);
				SetPlayerHealth(playerid,100);
				GivePlayerWeapon(playerid,34,50);
				GivePlayerWeapon(playerid,24,50);
				GivePlayerWeapon(playerid,17,1);
				PlayerInfo[playerid][pszolg] = 1;
				Cselekves(playerid, "szolgálatba lépett");
				fkradio[playerid] = 1;
				felszerelesben[playerid] = true;
			}
			if(listitem == 2)
			{
				SetPlayerSkin(playerid,285);
				SetPlayerArmour(playerid,100);
				SetPlayerHealth(playerid,100);
				GivePlayerWeapon(playerid,29,500);
				GivePlayerWeapon(playerid,24,50);
				GivePlayerWeapon(playerid,17,1);
				PlayerInfo[playerid][pszolg] = 1;
				Cselekves(playerid, "szolgálatba lépett");
				fkradio[playerid] = 1;
				felszerelesben[playerid] = true;
			}
			if(listitem == 3)
			{
				SetPlayerSkin(playerid,285);
				SetPlayerArmour(playerid,100);
				SetPlayerHealth(playerid,100);
				GivePlayerWeapon(playerid,27,100);
				GivePlayerWeapon(playerid,24,50);
				GivePlayerWeapon(playerid,17,1);
				PlayerInfo[playerid][pszolg] = 1;
				Cselekves(playerid, "szolgálatba lépett");
				fkradio[playerid] = 1;
				felszerelesben[playerid] = true;
			}
		}
	}
	if(dialogid == DIALOG_KRESZ1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid,DIALOG_KRESZ2,DIALOG_STYLE_LIST,"KRESZ","Elméleti anyag\nElméleti vizsga\nGyakorlat\nForgalmi vizsga","OK","Mégse");
			}
			if(listitem == 1)
			{
				ShowPlayerDialog(playerid,DIALOG_KRESZ3,DIALOG_STYLE_LIST,"KRESZ","Elméleti anyag\nElméleti vizsga\nGyakorlat\nForgalmi vizsga","OK","Mégse");
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid,DIALOG_KRESZ4,DIALOG_STYLE_LIST,"KRESZ","Elméleti anyag\nElméleti vizsga\nGyakorlat\nForgalmi vizsga","OK","Mégse");
			}
			if(listitem == 3)
			{
				ShowPlayerDialog(playerid,DIALOG_KRESZ5,DIALOG_STYLE_LIST,"KRESZ","Elméleti anyag\nElméleti vizsga\nGyakorlat\nForgalmi vizsga","OK","Mégse");
			}
			if(listitem == 4)
			{
				ShowPlayerDialog(playerid,DIALOG_KRESZ6,DIALOG_STYLE_LIST,"KRESZ","Elméleti anyag\nElméleti vizsga\nGyakorlat\nForgalmi vizsga","OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_KRESZ2)
	{
		if(response)
		{
			if(listitem == 0)
			{
				if(kreszv[playerid][0][0] == false)
				{
					if((PlayerInfo[playerid][Money] - 20000) >= 0)
					{
						ShowPlayerDialog(playerid,DIALOG_KRESZ7,DIALOG_STYLE_MSGBOX,"KRESZ","Feltöltés alatt!","OK","");
						kreszv[playerid][0][0] = true;
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs elég pénzed! 20.000ft !))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Neked már megvan az elméleti tananyag! ))");
				}
			}
			if(listitem == 1)
			{
				if(kreszv[playerid][0][1] == false)
				{
					if(kreszv[playerid][0][0] == true)
					{
						if((PlayerInfo[playerid][Money] - 4600) >= 0)
						{
							new rnd = random(20);
							PlayerVizsgakerdes[playerid] = 0;
							Playervizsgarnd[playerid][PlayerVizsgakerdes[playerid]] = rnd;
							ShowPlayerDialog(playerid,DIALOG_KRESZ8,DIALOG_STYLE_LIST,kerdesA[rnd][0],kerdesA[rnd][1],"OK","");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs elég pénzed! 4.600ft !))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs meg az elméleti anyag! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Neked már megvan az elméleti vizsga! ))");
				}
			}
			if(listitem == 2)
			{
				if(kreszv[playerid][0][2] == false)
				{
					if(kreszv[playerid][0][1] == true)
					{
						SendClientMessage(playerid,-1,"(( 80KM kell levezetned jármû sérülés nélkül! ))");
						new vid=1;
						new bool:talal;
						while( vid < MAX_VEHICLES && talal == false)
						{
							if(VehicleInfo[vid][Vvan] == false)
							{
								talal = true;
							}
							else
							{
								talal = false;
								vid++;
							}
						}
						if(talal == true)
						{
							new string[7];
							format(string,sizeof(string),"Oktato");
							CreateVehicle(461,-2064.579,-84.558,35.164,0.0,3,3,-1,0);
							SetVehicleNumberPlate(vid,string);
							VehicleInfo[vid][PosX] = -2064.579;
							VehicleInfo[vid][PosY] = -84.558;
							VehicleInfo[vid][PosZ] = 35.164;
							VehicleInfo[vid][vmunka] = 0;
							VehicleInfo[vid][vfrakcio] = 0;
							VehicleInfo[vid][megkilometer] = kreszkilometer[playerid];
							VehicleInfo[vid][uzemanyag] = 100;
							sscanf(string, "s[32]",VehicleInfo[vid][Plate]);
							SetVehicleToRespawn(vid);
							VehicleInfo[vid][Vvan] = true;
							SetPlayerPos(playerid,-2064.579,-84.558,35.164);
							SetPlayerInterior(playerid,0);
							SetPlayerVirtualWorld(playerid,0);
							PutPlayerInVehicle(playerid,vid,0);
							new engine, lights, alarm, doors, bonnet, boot, objective;
							GetVehicleParamsEx(vid, engine, lights, alarm, doors, bonnet, boot, objective);
							SetVehicleParamsEx(vid, 1, lights, alarm, doors, bonnet, boot, objective);
						}
						SendClientMessage(playerid,-1,"((Ha beakarod fejezni a vezetést menj bele a checkpoint-ba és a KM-ed mentésre kerül!))");
						kreszgyakorlasban[playerid] = true;
						SetPlayerCheckpoint(playerid, -2045.771,-97.671,33.164, 3.0);
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs meg az elméleti vizsga! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Neked már megvan a gyakorlat! ))");
				}
			}
			if(listitem ==3)
			{
				if(kreszv[playerid][0][3] == false)
				{
					if(kreszv[playerid][0][2] == true)
					{
						SendClientMessage(playerid,-1,"(( Elkezted a vizsgát! ))");
						kreszvizsgaban[playerid] = true;
						
						new vid=1;
						new bool:talal;
						while( vid < MAX_VEHICLES && talal == false)
						{
							if(VehicleInfo[vid][Vvan] == false)
							{
								talal = true;
							}
							else
							{
								talal = false;
								vid++;
							}
						}
						if(talal == true)
						{
							new string[7];
							format(string,sizeof(string),"Oktato");
							CreateVehicle(461,-2064.579,-84.558,35.164,0.0,3,3,-1,0);
							SetVehicleNumberPlate(vid,string);
							VehicleInfo[vid][PosX] = -2064.579;
							VehicleInfo[vid][PosY] = -84.558;
							VehicleInfo[vid][PosZ] = 35.164;
							VehicleInfo[vid][vmunka] = 0;
							VehicleInfo[vid][vfrakcio] = 0;
							VehicleInfo[vid][megkilometer] = kreszkilometer[playerid];
							VehicleInfo[vid][uzemanyag] = 100;
							sscanf(string, "s[32]",VehicleInfo[vid][Plate]);
							SetVehicleToRespawn(vid);
							VehicleInfo[vid][Vvan] = true;
							SetPlayerPos(playerid,-2064.579,-84.558,35.164);
							SetPlayerInterior(playerid,0);
							SetPlayerVirtualWorld(playerid,0);
							PutPlayerInVehicle(playerid,vid,0);
							new engine, lights, alarm, doors, bonnet, boot, objective;
							GetVehicleParamsEx(vid, engine, lights, alarm, doors, bonnet, boot, objective);
							SetVehicleParamsEx(vid, 1, lights, alarm, doors, bonnet, boot, objective);
						}						
						SetPlayerRaceCheckpoint(playerid,0,kreszvizsgacp[kreszvcp[playerid]][0],kreszvizsgacp[kreszvcp[playerid]][1],kreszvizsgacp[kreszvcp[playerid]][2],kreszvizsgacp[kreszvcp[playerid]+1][0],kreszvizsgacp[kreszvcp[playerid]+1][1],kreszvizsgacp[kreszvcp[playerid]+1][2],3.0);	
						kreszvcp[playerid]++;						
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs meg a gyakorlat! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Neked már megvan a forglami vizsga! ))");
				}
			}
		}
	}
	if(dialogid == DIALOG_KRESZ8)
	{
		if(PlayerVizsgakerdes[playerid] < 10)
		{
			if(strval(kerdesA[Playervizsgarnd[playerid][PlayerVizsgakerdes[playerid]]][2]) == listitem)
			{
				kreszjovalasz[playerid]++;
			}
			new rnd = random(20);
			new i = 0;
			while(i < 11)
			{
				if(Playervizsgarnd[playerid][i] == rnd)
				{
					rnd = random(20);
					i=0;
				}
				else
				{
					i++;
				}
			}
			ShowPlayerDialog(playerid,DIALOG_KRESZ8,DIALOG_STYLE_LIST,kerdesA[rnd][0],kerdesA[rnd][1],"OK","");
			PlayerVizsgakerdes[playerid]++;		
			Playervizsgarnd[playerid][PlayerVizsgakerdes[playerid]] = rnd;			
		}
		else
		{
			if(kreszjovalasz[playerid] >= 8)
			{
				SendClientMessage(playerid,-1,"(( Sikeres Vizsga! ))");
				kreszv[playerid][0][1] = true;
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Sikertelen Vizsga! ))");
				kreszv[playerid][0][1] = false;
			}
		}
	}
	if(dialogid == DIALOG_BENZINKUT1)
	{
		if(response)
		{
			new bizid;
			new benzincucc[10][2];
			new bnzcc;
			bizid = GetClosestBenzinbiz(playerid);
			for(new i;i<sizeof(benzinar);i++)
			{
				if(benzinar[i][0] == bizid)
				{
					benzincucc[bnzcc][0]=benzinar[i][1];
					benzincucc[bnzcc][1]=benzinar[i][3];
					bnzcc++;
				}
			}
			playerbenzin[playerid][0]=benzincucc[listitem][0];
			playerbenzin[playerid][1]=benzincucc[listitem][1];
			ShowPlayerDialog(playerid,DIALOG_BENZINKUT2,DIALOG_STYLE_INPUT,"Benzinkút","Hogy kíván tankolni?","Pénz","Liter");
		}
	}
	if(dialogid == DIALOG_BENZINKUT2)
	{
		new upenz;
		new liter;	
		new vehid;
		new vissza1;
		new vissza2;
		new bool:joe;
		vehid = GetClosestVehicle(playerid,1.8);	
		if(response)
		{
			upenz = strval(inputtext);
			liter = upenz/playerbenzin[playerid][1];
			if( (liter+floatround(VehicleInfo[vehid][uzemanyag])) <= vehdata[VehicleInfo[vehid][Model]-400][0])
			{
				if( upenz <= PlayerInfo[playerid][Money])
				{
					VehicleInfo[vehid][uzemanyag] += float(liter);
					if(VehicleInfo[vehid][uzemanyagt] == playerbenzin[playerid][0])
					{
						joe = true;
					}
					else
					{
						joe	= false;
					}
					SetTimerEx("tankoltimer",liter * 1000,false,"uddb",playerid,upenz,vehid,joe);
					VehicleInfo[vehid][tankolasban] = true;
					GameTextForPlayer(playerid,"Tankolás folyamatban!",liter * 1000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
				}
			}
			else
			{
				vissza1 = (liter+floatround(VehicleInfo[vehid][uzemanyag])) - vehdata[VehicleInfo[vehid][Model]-400][0];
				vissza2 = vissza1 * playerbenzin[playerid][1];
				upenz -= vissza2;
				liter -= vissza1;
				if( upenz <= PlayerInfo[playerid][Money])
				{
					VehicleInfo[vehid][uzemanyag] += float(liter);
					if(VehicleInfo[vehid][uzemanyagt] == playerbenzin[playerid][0])
					{
						joe = true;
					}
					else
					{
						joe	= false;
					}
					SetTimerEx("tankoltimer",liter * 1000,false,"uddb",playerid,upenz,vehid,joe);
					VehicleInfo[vehid][tankolasban] = true;
					GameTextForPlayer(playerid,"Tankolás folyamatban!",liter * 1000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
				}
			}
		}
		else
		{
			liter = strval(inputtext);
			upenz = liter*playerbenzin[playerid][1];
			if( (liter+floatround(VehicleInfo[vehid][uzemanyag])) <= vehdata[VehicleInfo[vehid][Model]-400][0])
			{
				if( upenz <= PlayerInfo[playerid][Money])
				{
					VehicleInfo[vehid][uzemanyag] += float(liter);
					if(VehicleInfo[vehid][uzemanyagt] == playerbenzin[playerid][0])
					{
						joe = true;
					}
					else
					{
						joe	= false;
					}
					SetTimerEx("tankoltimer",liter * 1000,false,"uddb",playerid,upenz,vehid,joe);
					VehicleInfo[vehid][tankolasban] = true;
					GameTextForPlayer(playerid,"Tankolás folyamatban!",liter * 1000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
				}
			}
			else
			{
				vissza1 = (liter+floatround(VehicleInfo[vehid][uzemanyag])) - vehdata[VehicleInfo[vehid][Model]-400][0];
				vissza2 = vissza1 * playerbenzin[playerid][1];
				upenz -= vissza2;
				liter -= vissza1;
				if( upenz <= PlayerInfo[playerid][Money])
				{
					VehicleInfo[vehid][uzemanyag] += float(liter);
					if(VehicleInfo[vehid][uzemanyagt] == playerbenzin[playerid][0])
					{
						joe = true;
					}
					else
					{
						joe	= false;
					}
					SetTimerEx("tankoltimer",liter * 1000,false,"uddb",playerid,upenz,vehid,joe);
					VehicleInfo[vehid][tankolasban] = true;
					GameTextForPlayer(playerid,"Tankolás folyamatban!",liter * 1000,4);
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs nállad elég pénz! ))");
				}
			}
		}
		
	}
	if(dialogid == DIALOG_RAKTAR1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_RAKTAR;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
					{
						new string[128];
						format(string,sizeof(string),"(( Raktár Ára: %d FT !))",RaktarInfo[i][ar]);
						SendClientMessage(playerid,-1,string);
						break;
					}
				}
			}
			if(listitem == 1)
			{
				for(new i;i<MAX_RAKTAR;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
					{
						if(PlayerInfo[playerid][Money] >= RaktarInfo[i][ar])
						{
							PlayerInfo[playerid][Money] -= RaktarInfo[i][ar];
							RaktarInfo[i][owner] = PlayerInfo[playerid][dbid];
							SendClientMessage(playerid,-1,"(( Sikeresen megvetted a raktárat! ))");
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Nincs elég pénzed! ))");
						}
						break;
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR2)
	{
		if(response)
		{
			if(listitem == 0)
			{
				for(new i;i<MAX_RAKTAR;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
					{
						if(RaktarInfo[i][fajta] == 1)
						{
							SetPlayerPos(playerid,1606.584,-1624.856,416.389);
							SetPlayerVirtualWorld(playerid,i);
							break;
						}
					}
				}
			}
			if(listitem == 1)
			{
				//késõbbR AKTAR4
			}
			if(listitem == 2)
			{
				ShowPlayerDialog(playerid,DIALOG_RAKTAR5,DIALOG_STYLE_INPUT,"Raktár","Adja meg az új jelszót!\n CSAK SZÁM! ","OK","Mégse");
			}
			if(listitem == 3)
			{
				for(new i;i<MAX_RAKTAR;i++)
				{
					if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
					{
						new string[128];
						new raktarar = RaktarInfo[i][ar] / 10;
						raktarar = raktarar * 8;
						format(string,sizeof(string),"Raktár eladási ára: %d FT\nBiztos el akarod adni?",raktarar);
						ShowPlayerDialog(playerid,DIALOG_RAKTAR6,DIALOG_STYLE_MSGBOX,"Raktár",string,"Igen","Mégse");
						break;
					}
				}
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR3)
	{
		if(response)
		{
			ShowPlayerDialog(playerid,DIALOG_RAKTAR7,DIALOG_STYLE_PASSWORD,"Raktár","Adja meg a raktár jelszavát!","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_RAKTAR7)
	{
		if(response)
		{
			for(new i;i<MAX_RAKTAR;i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
				{
					if(RaktarInfo[i][jelszo] == strval(inputtext))
					{
						SetPlayerPos(playerid,1606.584,-1624.856,416.389);
						SetPlayerVirtualWorld(playerid,i);
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Helytelen jelszó! ))");
					}
					break;
				}
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR5)
	{
		if(response)
		{
			for(new i;i<MAX_RAKTAR;i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
				{
					RaktarInfo[i][jelszo] = strval(inputtext);
					SendClientMessage(playerid,-1,"(( Jelszó sikeresen megváltoztatva! ))");
					break;
				}
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR6)
	{
		if(response)
		{
			for(new i;i<MAX_RAKTAR;i++)
			{
				if(IsPlayerInRangeOfPoint(playerid,2,RaktarInfo[i][rax],RaktarInfo[i][ray],RaktarInfo[i][raz]))
				{
					new raktarar = RaktarInfo[i][ar] / 10;
					raktarar = raktarar * 8;
					PlayerInfo[playerid][Money] += raktarar;
					RaktarInfo[i][owner] = 0;
					SendClientMessage(playerid,-1,"(( Sikeresen eladtad a raktáradat! ))");
					break;
				}
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR8)
	{
		if(response)
		{
			new sql[512];
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`, `mennyiseg` FROM `raktartartalom` WHERE `dbid` = '%d' LIMIT %d,1",RaktarInfo[GetPlayerVirtualWorld(playerid)][dbid],listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name(0,"nev",TskKiNev[playerid],128);
			cache_get_value_name_int(0,"mennyiseg",TskKiAmount[playerid]);
			if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
			{
				AddItem(playerid,TskKiNev[playerid],TskKiAmount[playerid]);
				RRemoveItem(GetPlayerVirtualWorld(playerid),TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
				SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
			}
			else
			{
				ShowPlayerDialog(playerid,DIALOG_RAKTAR9,DIALOG_STYLE_INPUT,"Raktár","Mennyit szeretnél kivenni?","Igen","Mégse");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_RAKTAR9)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				AddItem(playerid,TskKiNev[playerid],mennyis);
				RRemoveItem(GetPlayerVirtualWorld(playerid),TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen kivetted! ))");
			}
		}
	}
	if(dialogid == DIALOG_RAKTAR10)
	{
		if(response)
		{
			new sql[512];
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`, `mennyiseg` FROM `inventory` WHERE `dbid` = '%d' LIMIT %d,1",PlayerInfo[playerid][dbid],listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name(0,"nev",TskKiNev[playerid],128);
			cache_get_value_name_int(0,"mennyiseg",TskKiAmount[playerid]);
			if(TskKiAmount[playerid] == 1 || osszevonhato(TskKiNev[playerid]) == false)
			{
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],TskKiAmount[playerid]);
				RAddItem(GetPlayerVirtualWorld(playerid),TskKiNev[playerid],TskKiAmount[playerid]);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
			else
			{
				ShowPlayerDialog(playerid,DIALOG_RAKTAR11,DIALOG_STYLE_INPUT,"Raktár","Mennyit szeretnél berakni?","Igen","Mégse");
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_RAKTAR11)
	{
		if(response)
		{
			new mennyis = strval(inputtext);
			if(TskKiAmount[playerid] >= mennyis)
			{
				RAddItem(GetPlayerVirtualWorld(playerid),TskKiNev[playerid],mennyis);
				RemoveItem(playerid,TskKiNev[playerid],TskKiAmount[playerid],mennyis);
				SendClientMessage(playerid,-1,"(( Sikeresen beraktad! ))");
			}
		}
	}
	if(dialogid == DIALOG_NOVENY1)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid,DIALOG_NOVENY2,DIALOG_STYLE_LIST,"Növény","Marihuána\nKokacserje","Ültet","Mégse");
			}
			if(listitem == 1)
			{
				new string[256];
				new raktido;
				new raktnov;
				new raktfolyamat;
				new dlg[2048];
				new sql[256];
				new Cache: res;
				new rows;
				mysql_format(mysql,sql,sizeof(sql),"SELECT `noveny`, `folyamat`, `ido` FROM `noveny` WHERE `raktar` = '%d';",RaktarInfo[GetPlayerVirtualWorld(playerid)][dbid]);
				res = mysql_query(mysql,sql);
				format(string,sizeof(string),"Név\tFolyamat\tIdõ\n");
				strcat(dlg,string);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name_int(i,"noveny",raktnov);
					cache_get_value_name_int(i,"folyamat",raktfolyamat);
					cache_get_value_name_int(i,"ido",raktido);
					new ora,perc,masodperc,buff;
					buff = (raktido-gettime());
					ora =  buff / 3600;
					perc = (buff - ora * 3600) / 60;
					masodperc = buff - (ora * 3600 + perc * 60);
					if(raktnov == 0)
					{
						if(gettime() < raktido)
						{
							format(string,sizeof(string),"{f2ff00}Marihuána\t%s\t%d:%d:%d\n",fufolyamat[raktfolyamat][0],ora,perc,masodperc);
						}
						else
						{
							format(string,sizeof(string),"{00ff00}Marihuána\t%s\t00:00:00\n",fufolyamat[raktfolyamat][0]);
						}
					}
					else
					{
						if(gettime() > raktido)
						{
							format(string,sizeof(string),"{00ff00}Koka\t%s\t%d\n",fufolyamat[raktfolyamat][0],raktido);
						}
						else
						{
							format(string,sizeof(string),"{f2ff00}Koka\t%s\t%d\n",fufolyamat[raktfolyamat][0],raktido);
						}
					}
					strcat(dlg,string);
					ShowPlayerDialog(playerid,DIALOG_NOVENY5,DIALOG_STYLE_TABLIST_HEADERS,"Növények",dlg,"OK","Mégse");
				}
				cache_delete(res);
			}
		}
	}
	if(dialogid == DIALOG_NOVENY2)
	{
		if(response)
		{
			if(listitem == 0)
			{
				ShowPlayerDialog(playerid,DIALOG_NOVENY3,DIALOG_STYLE_INPUT,"Növény","Mennyit szeretnél ültetni?","Ültet","Mégse");
			}
			if(listitem == 1)
			{
				ShowPlayerDialog(playerid,DIALOG_NOVENY4,DIALOG_STYLE_INPUT,"Növény","Mennyit szeretnél ültetni?","Ültet","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_NOVENY3)
	{
		if(response)
		{
			if(ExistItem(playerid,"Marihuána mag",strval(inputtext)))
			{
				if(strval(inputtext) <= 20)
				{
					SetTimerEx("novenyultet",1000 * strval(inputtext),false,"udd",playerid,0,strval(inputtext));
					Cselekves(playerid,"elkezdett ültetni.");
					ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,1000 * strval(inputtext),1);
					GameTextForPlayer(playerid,"Ültetés folyamatban!",1000 * strval(inputtext),4);
				}
				else
				{
					SetTimerEx("novenyultet",1000 * 20,false,"udd",playerid,0,strval(inputtext));
					Cselekves(playerid,"elkezdett ültetni.");
					ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,1000 * 20,1);
					GameTextForPlayer(playerid,"Ültetés folyamatban!",1000 * 20,4);
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs nállad elég mag! ))");
			}
		}
	}
	if(dialogid == DIALOG_NOVENY4)
	{
		if(response)
		{
			if(ExistItem(playerid,"Koka mag",strval(inputtext)))
			{
				if(strval(inputtext) <= 20)
				{
					SetTimerEx("novenyultet",1000 * strval(inputtext),false,"udd",playerid,1,strval(inputtext));
					Cselekves(playerid,"elkezdett ültetni.");
					ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,1000 * strval(inputtext),1);
					GameTextForPlayer(playerid,"Ültetés folyamatban!",1000 * strval(inputtext),4);
				}
				else
				{
					SetTimerEx("novenyultet",1000 * 20,false,"udd",playerid,1,strval(inputtext));
					Cselekves(playerid,"elkezdett ültetni.");
					ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,0,0,0,1,1000 * 20,1);
					GameTextForPlayer(playerid,"Ültetés folyamatban!",1000 * 20,4);
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Nincs nállad elég mag! ))");
			}
		}
	}
	if(dialogid == DIALOG_NOVENY5)
	{
		if(response)
		{
			new sql[256];
			new string[128];
			new raktnov,raktfolyamat,raktido,novid;
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `ID`,`noveny`, `folyamat`, `ido` FROM `noveny` WHERE `raktar` = '%d' LIMIT %d,1",RaktarInfo[GetPlayerVirtualWorld(playerid)][dbid],listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"ID",novid);
			cache_get_value_name_int(0,"noveny",raktnov);
			cache_get_value_name_int(0,"folyamat",raktfolyamat);
			cache_get_value_name_int(0,"ido",raktido);
			if(raktnov == 0)
			{
				if(RaktarInfo[GetPlayerVirtualWorld(playerid)][paratlanito] == true)
				{
					if(RaktarInfo[GetPlayerVirtualWorld(playerid)][ontozorendszer] == true)
					{
						if(raktido < gettime())
						{
							format(string,sizeof(string),"%s folyamatban!",fufolyamat[raktfolyamat][0]);
							GameTextForPlayer(playerid,string,1000 * 5,4);
							SetTimerEx("novenyfejlodes",1000 * 5,false,"uddd",playerid,novid,raktnov,raktfolyamat);
							ApplyAnimation(playerid, "BOMBER","BOM_Plant_Loop",4.1,1,0,0,1,1000 * 20,1);	
						}
						else
						{
							SendClientMessage(playerid,-1,"(( Még nem telt le az idõ! ))");
						}
					}
					else
					{
						SendClientMessage(playerid,-1,"(( Nincs öntözõrendszered! ))");
					}
				}
				else
				{
					SendClientMessage(playerid,-1,"(( Nincs párátlanítód! ))");
				}
			}
			cache_delete(res);
		}
	}
	if(dialogid == DIALOG_DOBOZ1)
	{
		if(response)
		{
			new sql[512];
			new fid;
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `ID`,`nev`, `mennyiseg` FROM `dobozok` LIMIT %d,1",listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"ID",fid);
			cache_get_value_name(0,"nev",TskKiNev[playerid],128);
			cache_get_value_name_int(0,"mennyiseg",TskKiAmount[playerid]);
			AddItem(playerid,TskKiNev[playerid],TskKiAmount[playerid]);
			cache_delete(res);
			mysql_format(mysql,sql,sizeof(sql),"DELETE FROM `dobozok` WHERE `ID` = '%d'",fid);
			res = mysql_query(mysql,sql);
			cache_delete(res);
			SendClientMessage(playerid,-1,"(( Fegyver kivéve! ))");
		}
	}
	if(dialogid == DIALOG_ONKORMANYZAT1)
	{
		if(response)
		{
			if(listitem == 3)
			{
				ShowPlayerDialog(playerid,DIALOG_ONKORMANYZAT3,DIALOG_STYLE_INPUT,"Adózási rendszer","Adja meg az ÁFA százalékát!","OK","Mégse");
			}
			if(listitem == 4)
			{
				ShowPlayerDialog(playerid,DIALOG_ONKORMANYZAT5,DIALOG_STYLE_INPUT,"Adózási rendszer","Adja meg az SZJA százalékát!","OK","Mégse");
			}
			else
			{
				new string[256];
				new dlg[512];
				new sql[256];
				new namess[64];
				new mettol;
				new meddig;
				new Float:ados;
				new Cache:res;
				new rows;
				GetPlayerName(playerid,namess,sizeof(namess));
				mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mettol`,`meddig`,`ado` FROM `ado` WHERE `fajta` = '%d';",listitem);
				res = mysql_query(mysql,sql);
				format(string,sizeof(string),"Név\tMettõl\tMeddig\tADÓ\n");
				strcat(dlg,string);
				cache_get_row_count(rows);
				for(new i=0;i<rows;i++)
				{
					cache_get_value_name(i,"nev",string,sizeof(string));
					cache_get_value_name_int(i,"mettol",mettol);
					cache_get_value_name_int(i,"meddig",meddig);
					cache_get_value_name_float(i,"ado",ados);
					format(string,sizeof(string),"%s\t%s\t%s\t%.2f\n",string,cformat(mettol),cformat(meddig),ados);
					strcat(dlg,string);
				}
				cache_delete(res);
				ShowPlayerDialog(playerid,DIALOG_ONKORMANYZAT2,DIALOG_STYLE_TABLIST_HEADERS,"Adózási rendszer",dlg,"OK","Mégse");
			}
		}
	}
	if(dialogid == DIALOG_ONKORMANYZAT2)
	{
		if(response)
		{
			new string[256];
			new sql[256];
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `ID` FROM `ado` WHERE `nev` = '%s';",inputtext);
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"ID",adomodosit[playerid]);
			format(string,sizeof(string),"Adja meg a(z) %s kategória adóértékét!",inputtext);
			cache_delete(res);
			ShowPlayerDialog(playerid,DIALOG_ONKORMANYZAT4,DIALOG_STYLE_INPUT,"Adózási rendszer",string,"OK","Mégse");
		}
	}
	if(dialogid == DIALOG_ONKORMANYZAT3)
	{
		if(response)
		{
			new Float:ados = floatstr(inputtext);//strval(inputtext);
			if( 0.0 < ados && ados < 50.0)
			{
				new sql[256];
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `ado` SET `ado` = '%f' WHERE `ID` = '15'",ados);
				res = mysql_query(mysql,sql);
				AFA = floatstr(inputtext);
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Sikeresen módosítva!))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Minimum 1% maximum 50% !))");
			}
		}
	}
	if(dialogid == DIALOG_ONKORMANYZAT4)
	{
		if(response)
		{
			new Float:ados = floatstr(inputtext);//strval(inputtext);
			if( 0.0 < ados && ados < 50.0)
			{
				new sql[256];
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `ado` SET `ado` = '%f' WHERE `ID` = '%d'",ados,adomodosit[playerid]);
				res = mysql_query(mysql,sql);
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Sikeresen módosítva!))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Minimum 1% maximum 50% !))");
			}
		}
	}
	if(dialogid == DIALOG_ONKORMANYZAT5)
	{
		if(response)
		{
			new Float:ados = floatstr(inputtext);//strval(inputtext);
			if( 0.0 < ados && ados < 50.0)
			{
				new sql[256];
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `ado` SET `ado` = '%f' WHERE `ID` = '16'",ados);
				res = mysql_query(mysql,sql);
				SZJA = floatstr(inputtext);
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Sikeresen módosítva!))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Minimum 1% maximum 50% !))");
			}
		}
	}
	if(dialogid == DIALOG_FIZETES1)
	{
		if(response)
		{
			fizmodrang[playerid] = listitem+1;
			ShowPlayerDialog(playerid,DIALOG_FIZETES2,DIALOG_STYLE_INPUT,"Fizetés","Adja meg a fizetés összegét!","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_FIZETES2)
	{
		if(response)
		{
			new fizes = strval(inputtext);
			if(fizes >= 0)
			{
				new sql[256];
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `frakcio` SET `%df` = '%d' WHERE Id = '%d'",fizmodrang[playerid],fizes,PlayerInfo[playerid][frakcio]);
				res = mysql_query(mysql,sql);
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Fizetés módosítva! ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Csak pozitív lehet! ))");
			}
		}
	}
	if(dialogid == DIALOG_FIZETES3)
	{
		if(response)
		{
			new query[128];
			new dlg[512];
			new frakok[64];
			new string[64];
			new fizus;
			new Cache:res;
			format(string,sizeof(string),"Rang\tFizetés\n",frakok);
			strcat(dlg,string);
			mysql_format(mysql,query,sizeof(query),"SELECT `Id` FROM `frakcio` WHERE `Hneve` = '%s'",inputtext);
			res = mysql_query(mysql,query);
			cache_get_value_name_int(0,"Id",fizmodfrak[playerid]);
			cache_delete(res);
			for(new i = 1; i < MAX_RANGOK; i++)
			{
				mysql_format(mysql,query,sizeof(query),"SELECT `%d`,`%df` FROM `frakcio` WHERE LENGTH(`%d`) > 1 AND `Hneve` = '%s'",i,i,i,inputtext);
				res = mysql_query(mysql,query);
				cache_get_value_index(0,0,frakok,sizeof(frakok));
				cache_get_value_index_int(0,1,fizus);
				format(string,sizeof(string),"%s\t%s\n",frakok,cformat(fizus));
				strcat(dlg,string);
			}
			cache_delete(res);
			ShowPlayerDialog(playerid, DIALOG_FIZETES4, DIALOG_STYLE_TABLIST_HEADERS, "Fizetés", dlg, "OK", "Mégse");
		}
	}
	if(dialogid == DIALOG_FIZETES4)
	{
		if(response)
		{
			fizmodrang[playerid] = listitem+1;
			ShowPlayerDialog(playerid,DIALOG_FIZETES5,DIALOG_STYLE_INPUT,"Fizetés","Adja meg a fizetés összegét!","OK","Mégse");
		}
	}
	if(dialogid == DIALOG_FIZETES5)
	{
		if(response)
		{
			new fizes = strval(inputtext);
			if(fizes >= 0)
			{
				new sql[256];
				new Cache:res;
				mysql_format(mysql,sql,sizeof(sql),"UPDATE `frakcio` SET `%df` = '%d' WHERE Id = '%d'",fizmodrang[playerid],fizes,fizmodfrak[playerid]);
				res = mysql_query(mysql,sql);
				cache_delete(res);
				SendClientMessage(playerid,-1,"(( Fizetés módosítva! ))");
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Csak pozitív lehet! ))");
			}
		}
	}
	if(dialogid == DIALOG_VASAROL)
	{
		if(response)
		{
			new sql[256];
			new arv,meny,idb;
			new Cache:res;
			mysql_format(mysql,sql,sizeof(sql),"SELECT `ID`,`ar`,`mennyiseg` FROM `termekek` WHERE `dbid` = '%d' ORDER BY `nev` ASC LIMIT %d,1",BizzInfo[GetPlayerVirtualWorld(playerid)][Id],listitem);
			res = mysql_query(mysql,sql);
			cache_get_value_name_int(0,"ID",idb);
			cache_get_value_name_int(0,"ar",arv);
			cache_get_value_name_int(0,"mennyiseg",meny);
			cache_delete(res);
			if(meny != 0)
			{
				if(PlayerInfo[playerid][Money] - arv >= 0)
				{
					new afas,adozott;
					AddItem(playerid,inputtext,1);
					mysql_format(mysql,sql,sizeof(sql),"UPDATE `termekek` SET `mennyiseg` = `mennyiseg` - '1' WHERE `ID` = '%d'",idb);
					res = mysql_query(mysql,sql);
					afaado(arv,afas,adozott);
					allamibevetel(afas);
					BizzInfo[GetPlayerVirtualWorld(playerid)][kassza] += adozott;
					cache_delete(res);
					format(sql,sizeof(sql),"(( Vásároltál egy %s ))",inputtext);
					SendClientMessage(playerid,-1,sql);				
				}
			}
		}
	}
	return 0;
}

public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	if(PlayerInfo[playerid][Admin] >=1)
	{
		ShowPlayerDialog(playerid,DIALOG_ADMIN1,DIALOG_STYLE_LIST,"Admin","ATV\nPlayer adatok\nKocsik\nHázak\nBankszmála","OK","Mégse");
		admindia[playerid] = clickedplayerid;
    }
	return 1;
}

public OnPlayerCommandPerformed(playerid, cmdtext[], success)
{
	if(!success)
	{
		new string[128];
		format(string, sizeof(string), "(( Ismeretlen parancs! ))");
		SendClientMessage(playerid,COLOR_RED, string);
	}
	else
	{
		new sql[256],string[128];
		new Cache:res;
		new year,mounth,day,hour,minute,second;
		getdate(year, mounth, day);
		gettime(hour,minute,second);
		format(string,sizeof(string),"%02d-%02d-%d %02d:%02d:%02d",year,mounth,day,hour,minute,second);
		mysql_format(mysql,sql,sizeof(sql), "INSERT INTO `commandlog`(ido,nev,parancs) VALUES ('%s','%s','%s')",string,Nev(playerid),cmdtext);
		res = mysql_query(mysql,sql);
		cache_delete(res);
	}
	return 1;
}

stock Nev(playerid)
{
	new Neve[MAX_PLAYER_NAME];
	GetPlayerName(playerid, Neve, sizeof(Neve));
	str_replace(Neve, '_', ' ');
	return Neve;
}

stock SendToAS(color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][Admin] >= 1 || ahelp[i] == 1)
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}

stock SendToModerator(color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][Admin] >= 1)
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}

stock SendToAdmins(color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][Admin] >= 2)
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}

stock SendToReport(color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][Admin] >= 1 || ahelp[i] == 1)
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}

stock SendToDevelopment(color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][Admin] >= 1337)
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}
stock SendToRadio(playerid,color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if((fkradio[i] == fkradio[playerid]) && fkradio[playerid] !=0 )
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}
stock SendToRadioD(playerid,color, text[])
{
	for(new i; i < MAX_PLAYERS; i++)
	{
		if( PlayerInfo[i][pszolg] == 1 && (PlayerInfo[playerid][ftipus] == PlayerInfo[i][ftipus]))
		{
			SendClientMessage(i, color, text);
		}
	}
	return 1;
}
stock loadkiegcar()
{	
	for(new i;i<MAX_VEHICLES;i++)
	{
		TextDrawDestroy(sebessegmero[i]);
		TextDrawDestroy(kmmero[i]);
	}
	for(new i;i<MAX_VEHICLES;i++)
	{
		sebessegmero[i] = TextDrawCreate(568.000000, 410.000000, "0 KM/H");
		TextDrawBackgroundColor(sebessegmero[i], 255);
		TextDrawFont(sebessegmero[i], 2);
		TextDrawLetterSize(sebessegmero[i], 0.270000, 1.000000);
		TextDrawColor(sebessegmero[i], -1);
		TextDrawSetOutline(sebessegmero[i], 1);
		TextDrawSetProportional(sebessegmero[i], 1);
		
		kmmero[i] = TextDrawCreate(568.000000, 425.000000, "0 KM");
		TextDrawBackgroundColor(kmmero[i], 255);
		TextDrawFont(kmmero[i], 2);
		TextDrawLetterSize(kmmero[i], 0.270000, 1.000000);
		TextDrawColor(kmmero[i], -1);
		TextDrawSetOutline(kmmero[i], 1);
		TextDrawSetProportional(kmmero[i], 1);
	}
}

public LoadRentHouse()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new rid=0;
			new bool:talal = false;
			while( rid < MAX_RENTH && talal == false)
			{
				if(RentInfo[rid][rvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					rid++;
				}
			}
			cache_get_value_name_int(i,"ID",RentInfo[rid][dbid]);
			cache_get_value_name_int(i,"Owner",RentInfo[rid][owner]);
			cache_get_value_name(i,"Ownername",RentInfo[rid][ownername],64);
			cache_get_value_name_int(i,"cost",RentInfo[rid][rcost]);
			cache_get_value_name_int(i,"ido",RentInfo[rid][rido]);
			cache_get_value_name_int(i,"zarva",RentInfo[rid][rzarvar]);
			cache_get_value_name_float(i,"x",RentInfo[rid][rx]);
			cache_get_value_name_float(i,"y",RentInfo[rid][ry]);
			cache_get_value_name_float(i,"z",RentInfo[rid][rz]);
			cache_get_value_name_int(i,"vw",RentInfo[rid][rvw]);
			
			if(RentInfo[rid][owner] == 0)
			{
				new string[128];
				format(string,sizeof(string),"Házszám : %d",RentInfo[rid][dbid]);
				RentInfo[rid][rlabel] = Create3DTextLabel(string ,0x00FF00AA,RentInfo[rid][rx],RentInfo[rid][ry],RentInfo[rid][rz]+0.5,25, RentInfo[rid][rvw], 1);
				RentInfo[rid][pickid] = CreatePickup(1272, 1, RentInfo[rid][rx],RentInfo[rid][ry],RentInfo[rid][rz],RentInfo[rid][rvw]);
			}
			else
			{
				new string[128];
				format(string,sizeof(string),"Házszám : %d",RentInfo[rid][dbid]);
				RentInfo[rid][rlabel] = Create3DTextLabel(string ,0x00FF00AA,RentInfo[rid][rx],RentInfo[rid][ry],RentInfo[rid][rz]+0.5,25, RentInfo[rid][rvw], 1);
				RentInfo[rid][pickid] = CreatePickup(1239, 1, RentInfo[rid][rx],RentInfo[rid][ry],RentInfo[rid][rz],RentInfo[rid][rvw]);
			}
			RentInfo[rid][rvan]= true;
		}
	}
}

public LoadRaktar()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new rid=0;
			new bool:talal = false;
			while( rid < MAX_RAKTAR && talal == false)
			{
				if(RaktarInfo[rid][rkvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					rid++;
				}
			}
			cache_get_value_name_int(i,"ID",RaktarInfo[rid][dbid]);
			cache_get_value_name_int(i,"tulaj",RaktarInfo[rid][owner]);
			cache_get_value_name_float(i,"x",RaktarInfo[rid][rax]);
			cache_get_value_name_float(i,"y",RaktarInfo[rid][ray]);
			cache_get_value_name_float(i,"z",RaktarInfo[rid][raz]);
			cache_get_value_name_int(i,"ar",RaktarInfo[rid][ar]);
			cache_get_value_name_int(i,"jelszo",RaktarInfo[rid][jelszo]);
			cache_get_value_name_int(i,"fajta",RaktarInfo[rid][fajta]);
			cache_get_value_name_int(i,"ontozorendszer",RaktarInfo[rid][ontozorendszer]);
			cache_get_value_name_int(i,"paratlanito",RaktarInfo[rid][paratlanito]);
			cache_get_value_name_int(i,"parasito",RaktarInfo[rid][parasito]);
			cache_get_value_name_int(i,"benzin",RaktarInfo[rid][benzin]);
			new string[128];
			format(string,sizeof(string),"Raktár : %d",RaktarInfo[rid][dbid]);
			RaktarInfo[rid][ralabel] = Create3DTextLabel(string,0x00F00AA,RaktarInfo[rid][rax],RaktarInfo[rid][ray],RaktarInfo[rid][raz]+0.5,25,0,1);
			RaktarInfo[rid][rapickupid] = CreatePickup(1239, 1,RaktarInfo[rid][rax],RaktarInfo[rid][ray],RaktarInfo[rid][raz],0);
			
			RaktarInfo[rid][rkvan] = true;
		}
	}
}
public LoadBizz()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new bid=0;
			new bool:talal = false;
			while( bid < MAX_BIZZ && talal == false)
			{
				if(BizzInfo[bid][bvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					bid++;
				}
			}
			cache_get_value_name_int(i,"Id",BizzInfo[bid][Id]);
			cache_get_value_name_float(i,"x",BizzInfo[bid][px]);
			cache_get_value_name_float(i,"y",BizzInfo[bid][py]);
			cache_get_value_name_float(i,"z",BizzInfo[bid][pz]);
			cache_get_value_name_float(i,"ex",BizzInfo[bid][enterx]);
			cache_get_value_name_float(i,"ey",BizzInfo[bid][entery]);
			cache_get_value_name_float(i,"ez",BizzInfo[bid][enterz]);
			cache_get_value_name_int(i,"interior",BizzInfo[bid][interior]);
			cache_get_value_name_int(i,"owner",BizzInfo[bid][Owner]);
			cache_get_value_name(i,"ownername",BizzInfo[bid][Ownername],128);
			cache_get_value_name(i,"bizname",BizzInfo[bid][bizname],128);
			cache_get_value_name_int(i,"tipus",BizzInfo[bid][btipus]);
			cache_get_value_name_int(i,"cost",BizzInfo[bid][Cost]);
			cache_get_value_name_int(i,"kasszapw",BizzInfo[bid][kasszapw]);
			cache_get_value_name_int(i,"kassza",BizzInfo[bid][kassza]);
			
			BizzInfo[bid][mapicon] = CreateDynamicMapIcon(BizzInfo[bid][px], BizzInfo[bid][py], BizzInfo[bid][pz], 52, 0, -1, -1, -1, 100.0);
			new string[128];
			format(string,sizeof(string),"%s",BizzInfo[bid][bizname]);
			BizzInfo[bid][bizlabel] = Create3DTextLabel(string ,0x00FF00AA,BizzInfo[bid][px], BizzInfo[bid][py], BizzInfo[bid][pz]+0.5,25, 0, 1);
			BizzInfo[bid][bizzpick] = CreatePickup(1274, 1, BizzInfo[bid][px], BizzInfo[bid][py], BizzInfo[bid][pz], -1);
			BizzInfo[bid][bvw] = bid;
			BizzInfo[bid][bvan] = true;
		}
	}
}

public LoadCeg()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name_int(i,"ID",CegInfo[i][dbid]);
			cache_get_value_name_int(i,"cost",CegInfo[i][ccost]);
			cache_get_value_name(i,"nev",CegInfo[i][cnev],64);
			cache_get_value_name_int(i,"owner",CegInfo[i][cowner]);
			cache_get_value_name(i,"ownername",CegInfo[i][cownername],64);
			cache_get_value_name_float(i,"x",CegInfo[i][cex]);
			cache_get_value_name_float(i,"y",CegInfo[i][cey]);
			cache_get_value_name_float(i,"z",CegInfo[i][cez]);
			CreateDynamicMapIcon(CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez], 52, 0, -1, -1, -1, 100.0);
			new string[128];
			format(string,sizeof(string),"%s",CegInfo[i][cnev]);
			Create3DTextLabel(string ,0x00FF00AA,CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez]+0.5,25, 0, 1);
			CegInfo[i][cegpick] = CreatePickup(1274, 1, CegInfo[i][cex],CegInfo[i][cey],CegInfo[i][cez], -1);
		}
	}
}
public LoadDoor()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{	
			cache_get_value_name_int(i,"Id",AjtoInfo[i][Id]);
			cache_get_value_name_float(i,"x",AjtoInfo[i][ax]);
			cache_get_value_name_float(i,"y",AjtoInfo[i][ay]);
			cache_get_value_name_float(i,"z",AjtoInfo[i][az]);
			cache_get_value_name_float(i,"ax",AjtoInfo[i][fx]);
			cache_get_value_name_float(i,"ay",AjtoInfo[i][fy]);
			cache_get_value_name_float(i,"az",AjtoInfo[i][fz]);
			cache_get_value_name_float(i,"face",AjtoInfo[i][face2]);
			cache_get_value_name_float(i,"faces",AjtoInfo[i][face1]);
			cache_get_value_name_int(i,"int",AjtoInfo[i][inter]);
			cache_get_value_name_int(i,"vw",AjtoInfo[i][virt]);
		}
	}
}
public LoadPlayer(playerid)
{
	new rows;
	if(cache_get_row_count(rows))
	{
		new sql[512];
		cache_get_value_name_int(0,"Id",PlayerInfo[playerid][dbid]);
		cache_get_value_name_int(0,"Money",PlayerInfo[playerid][Money]);
		cache_get_value_name_int(0,"Score",PlayerInfo[playerid][Score]);
		cache_get_value_name_int(0,"Admin",PlayerInfo[playerid][Admin]);
		cache_get_value_name_int(0,"skin",PlayerInfo[playerid][skin]);
		cache_get_value_name(0,"szuletes",PlayerInfo[playerid][szuletes],128);
		cache_get_value_name(0,"neme",PlayerInfo[playerid][neme],128);
		cache_get_value_name(0,"allamp",PlayerInfo[playerid][allamp],128);
		cache_get_value_name(0,"okm",PlayerInfo[playerid][okm],128);
		cache_get_value_name_int(0,"frakcio",PlayerInfo[playerid][frakcio]);
		cache_get_value_name_int(0,"frang",PlayerInfo[playerid][rang]);
		cache_get_value_name_int(0,"leader",PlayerInfo[playerid][pleader]);
		cache_get_value_name_int(0,"connecttime",PlayerInfo[playerid][pconnecttime]);
		cache_get_value_name_int(0,"munka",PlayerInfo[playerid][pmunka]);
		cache_get_value_name_int(0,"fizetes",PlayerInfo[playerid][pfizetes]);
		cache_get_value_name(0,"hazas",PlayerInfo[playerid][phazas],128);
		cache_get_value_name(0,"reged",PlayerInfo[playerid][preg],128);
		cache_get_value_name_int(0,"ado",PlayerInfo[playerid][ado]);
		cache_get_value_name_int(0,"aleader",PlayerInfo[playerid][paleader]);
		cache_get_value_name_int(0,"ceg",PlayerInfo[playerid][pceg]);
		cache_get_value_name_int(0,"cegleader",PlayerInfo[playerid][cegleader]);
		cache_get_value_name_int(0,"maonline",PlayerInfo[playerid][maonline]);
		cache_get_value_name_int(0,"borton",PlayerInfo[playerid][pborton]);
		cache_get_value_name_int(0,"btipus",PlayerInfo[playerid][btipus]);
		PlayerInfo[playerid][pconnecttime] = gettime() - PlayerInfo[playerid][pconnecttime];
		if(PlayerInfo[playerid][frakcio] != 0 && PlayerInfo[playerid][rang] != 0)
		{
			mysql_format(mysql,sql,sizeof(sql),"SELECT `Hneve`, `Neve`, `tipus`,`Alosztaly`,`%d`,`%ds` FROM `frakcio` WHERE `Id` = '%d'",PlayerInfo[playerid][rang],PlayerInfo[playerid][rang],PlayerInfo[playerid][frakcio]);
			mysql_tquery(mysql, sql, "LoadFrakcio", "d", playerid);
		}
		mysql_format(mysql,sql,sizeof(sql),"SELECT `lvl`,`lvlexp`,`connecttime` FROM `user` WHERE `Id` ='%d'",PlayerInfo[playerid][dbid]);
		mysql_tquery(mysql, sql, "PlayerScore", "d", playerid);

		mysql_format(mysql,sql,sizeof(sql),"SELECT `kinek` FROM `barat` WHERE `ki` = '%d'",PlayerInfo[playerid][dbid]);
		mysql_tquery(mysql, sql, "LoadFriend", "d", playerid);

		mysql_format(mysql,sql,sizeof(sql),"SELECT `sorozatszam`, `megszerzett`, `km`, `00`, `01`, `02`, `03`, `10`, `11`, `12`, `13`, `20`, `21`, `22`, `23`, `30`, `31`, `32`, `33`, `40`, `41`, `42`, `43` FROM `jogositvany` WHERE `Owner` = '%d'",PlayerInfo[playerid][dbid]);
		mysql_tquery(mysql, sql, "LoadLicens", "d", playerid);
		
		new string[64];
		format(string, sizeof(string), "%s SQL betöltés lezajlott!", Nev(playerid));
		print(string);
	}
}

public LoadFrakcio(playerid)
{
	new rows;
	if(cache_get_row_count(rows))
	{
		cache_get_value_name(0,"Hneve",PlayerInfo[playerid][phfrakcio],128);
		cache_get_value_name(0,"Neve",PlayerInfo[playerid][pfrakcio],128);
		cache_get_value_name_int(0,"tipus",PlayerInfo[playerid][ftipus]);
		cache_get_value_name_int(0,"Alosztaly",PlayerInfo[playerid][alosztaly]);
		cache_get_value_index(0,4,PlayerInfo[playerid][prang],128);
		cache_get_value_index_int(0,5,PlayerInfo[playerid][fksin]);
	}
}

public LoadFriend(playerid)
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			cache_get_value_index_int(i,0,barat[playerid][i]);
		}

	}
}
stock bortonbe(playerid)
{
	if(PlayerInfo[playerid][pborton] > 1)
	{
		switch(PlayerInfo[playerid][btipus])
		{
			case 1:{SetPlayerPos(playerid,227.768,110.943,999.015);SetPlayerInterior(playerid,10);SetPlayerVirtualWorld(playerid,0);}
			case 2:{}
			case 3:{}
			case 4:{}
			case 5:{SetPlayerPos(playerid,1158.5031,-1311.9374,-9.6531);SetPlayerInterior(playerid,1);SetPlayerVirtualWorld(playerid,1);new Float:x,Float:y,Float:z;GetPlayerPos(playerid,x,y,z);Streamer_UpdateEx(playerid,x,y,z,GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));}
		}
	}
}
public Load3DLabel()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new text[128], color, Float:X, Float:Y, Float:Z, Float:DrawDistance, virtualworld, testLOS;
			cache_get_value_name(i,"text",text,sizeof(text));
			cache_get_value_name_int(i,"color",color);
			cache_get_value_name_float(i,"x",X);
			cache_get_value_name_float(i,"y",Z);
			cache_get_value_name_float(i,"z",Y);
			cache_get_value_name_float(i,"distance",DrawDistance);
			cache_get_value_name_int(i,"vw",virtualworld);
			cache_get_value_name_int(i,"testLOS",testLOS);
			global3d[i] = Create3DTextLabel(text,color,X,Y,Z,DrawDistance,virtualworld,testLOS);
		}
	}
}

public LoginPlayer(playerid,const password[])
{
	//new EscapedText[60];
	new Query[256];
	new pName[68];
	new buf[129];
	new Cache:res;
	new rows;
	//mysql_real_escape_string(password, EscapedText);
	GetPlayerName(playerid,pName,128);
	WP_Hash(buf, sizeof (buf), password);
	mysql_format(mysql,Query,sizeof(Query),"SELECT * FROM `user` WHERE `Name` = '%s' AND `Password` = '%s' ",pName,buf);
	res = mysql_query(mysql,Query);
	cache_get_row_count(rows);
	if(rows != 0)
	{
		if(sikertelenbe[playerid] != 0)
		{
			sikertelenbe[playerid] =0;
			OnPlayerRequestClass( playerid, 0 );
		}
		new ban,oka[128],sql[512];
		mysql_format(mysql,sql,sizeof(sql),"SELECT `Ban`, `oka` FROM `user` WHERE `Name` = '%s'",pName);
		res = mysql_query(mysql,sql);
		cache_get_value_name_int(0,"Ban",ban);
		cache_get_value_name(0,"oka",oka,sizeof(oka));
		if( gettime() < ban)
		{
			new string2[77];
			format(string2,sizeof(string2),"Bannolva vagy a szerverrõl!\n\rOka:%s\n\rEddig:%s",oka,ban);
			KickWithMessage(playerid, COLOR_RED, string2);
		}
		else
		{
			TogglePlayerSpectating(playerid, false);
			mysql_format(mysql,sql,sizeof(sql),"SELECT * FROM `user` WHERE `Name` = '%s'",pName);
			mysql_tquery(mysql,sql,"LoadPlayer","d",playerid);
			SendClientMessage(playerid, -1, "Sikeres bejelentkezés");
			ajtokbett[playerid] = SetTimerEx("ajtokbe",2000,1,"u",playerid);
			//SetTimerEx("playertimer",1,1,"u",playerid);
		}
	}
	else
	{
		if(sikertelenbe[playerid] ==3)
		{
			KickWithMessage(playerid, COLOR_RED, "Sikertelen bejelentkezés");
			sikertelenbe[playerid] =0;
		}
		else
		{
			sikertelenbe[playerid] += 1;
			ShowPlayerDialog(playerid, 2, DIALOG_STYLE_PASSWORD, "Belépés", "Sikertelen belépés!\nÍrd be a jelszavad!", "Rendben", "Mégse");
		}
	}
	cache_delete(res);
}

public saveplayer(playerid)
{	
	new Query[512];
	new pName[128];
	new string[128];
	new contime;
	GetPlayerName(playerid,pName,128);
	contime = gettime()- PlayerInfo[playerid][pconnecttime];
	format(Query,sizeof(Query),"UPDATE `user` SET `Money`='%d',`Admin`='%d',`skin`='%d',`frakcio`='%d',`frang`='%d',`leader`='%d',`fksin`='%d',`connecttime`='%d',`borton`='%d', `btipus`='%d', `fizetes`='%d', `ado`='%d', `ceg` = '%d', `cegleader` = '%d',`maonline` = '%d' WHERE `Name` = '%s'",PlayerInfo[playerid][Money],PlayerInfo[playerid][Admin],PlayerInfo[playerid][skin],PlayerInfo[playerid][frakcio],PlayerInfo[playerid][rang],PlayerInfo[playerid][pleader],PlayerInfo[playerid][fksin],
	contime,PlayerInfo[playerid][pborton],PlayerInfo[playerid][btipus],PlayerInfo[playerid][pfizetes],PlayerInfo[playerid][ado],PlayerInfo[playerid][pceg],PlayerInfo[playerid][cegleader],PlayerInfo[playerid][maonline],pName);
	SendClientMessage(playerid,-1,Query);
	new Cache:result = mysql_query(mysql,Query);
	cache_delete(result);
	format(string, sizeof(string), "%s SQL mentése lezajlott!", pName);
	print(string);
	return 1;
}

stock savehouse()
{	
	for(new hid; hid < MAX_HOUSE; hid++)
	{
		new Query[1024];
		mysql_format(mysql,Query, sizeof(Query), "UPDATE `house` SET `Owner` = '%d',`Ownername` = '%s',`zarva`='%d' WHERE `id` = '%d'", HouseInfo[hid][Owner],HouseInfo[hid][Ownername],HouseInfo[hid][nyitzar],HouseInfo[hid][Id]);
		mysql_tquery(mysql,Query);
	}
}
stock saverenth()
{
	for(new i;i<MAX_RENTH;i++)
	{
		new sql[1024];
		mysql_format(mysql,sql,sizeof(sql),"UPDATE `renthouse` SET `Owner`='%d',`Ownername`='%s', `ido`='%d',`zarva`='%d' WHERE `ID`='%d'",RentInfo[i][owner],RentInfo[i][ownername],RentInfo[i][rido],RentInfo[i][rzarvar],RentInfo[i][dbid]);
		mysql_tquery(mysql,sql);
	}
}
stock saveceg()
{
	for(new i;i<MAX_CEG;i++)
	{
		new sql[1024];
		mysql_format(mysql,sql,sizeof(sql),"UPDATE `ceg` SET `owner`='%d',`ownername`='%s' WHERE ID='%d'",CegInfo[i][cowner],CegInfo[i][cownername],CegInfo[i][dbid]);
		mysql_tquery(mysql,sql);
	}
}
public ClearChatbox(playerid, lines)
{
	if (IsPlayerConnected(playerid))
	{
		for(new i=0; i<lines; i++)
		{
			SendClientMessage(playerid, -1, " ");
		}
	}
	return 1;
}

/*new BigEar[MAX_PLAYERS];
public ProxDetector(Float:radi, playerid, string[],col1,col2,col3,col4,col5)
{
	if(IsPlayerConnected(playerid))
	{
		new Float:posx, Float:posy, Float:posz;
		new Float:oldposx, Float:oldposy, Float:oldposz;
		new Float:tempposx, Float:tempposy, Float:tempposz;
		GetPlayerPos(playerid, oldposx, oldposy, oldposz);
		for(new i = 0; i < MAX_PLAYERS; i++)
		{
			if(IsPlayerConnected(i) && (GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)))
			{
				if(!BigEar[i])
				{
					GetPlayerPos(i, posx, posy, posz);
					tempposx = (oldposx -posx);
					tempposy = (oldposy -posy);
					tempposz = (oldposz -posz);
					if (((tempposx < radi/16) && (tempposx > -radi/16)) && ((tempposy < radi/16) && (tempposy > -radi/16)) && ((tempposz < radi/16) && (tempposz > -radi/16)))
					{
						SendClientMessage(i, col1, string);
					}
					else if (((tempposx < radi/8) && (tempposx > -radi/8)) && ((tempposy < radi/8) && (tempposy > -radi/8)) && ((tempposz < radi/8) && (tempposz > -radi/8)))
					{
						SendClientMessage(i, col2, string);
					}
					else if (((tempposx < radi/4) && (tempposx > -radi/4)) && ((tempposy < radi/4) && (tempposy > -radi/4)) && ((tempposz < radi/4) && (tempposz > -radi/4)))
					{
						SendClientMessage(i, col3, string);
					}
					else if (((tempposx < radi/2) && (tempposx > -radi/2)) && ((tempposy < radi/2) && (tempposy > -radi/2)) && ((tempposz < radi/2) && (tempposz > -radi/2)))
					{
						SendClientMessage(i, col4, string);
					}
					else if (((tempposx < radi) && (tempposx > -radi)) && ((tempposy < radi) && (tempposy > -radi)) && ((tempposz < radi) && (tempposz > -radi)))
					{
						SendClientMessage(i, col5, string);
					}
				}
				else
				{
					SendClientMessage(i, col1, string);
				}
			}
		}
	}
	return 1;
}*/

public chats(Float:radi,playerid,string[],color,bool:nev)
{
	new Float:x,Float:y,Float:z;
	new sztring[128];
	new bool:talal;
	new bari;
	GetPlayerPos(playerid,x,y,z);
	for(new i=0;i<MAX_PLAYERS;i++)
	{
		if(IsPlayerConnected(i) &&(GetPlayerVirtualWorld(playerid) == GetPlayerVirtualWorld(i)))
		{
			if(IsPlayerInRangeOfPoint(i,radi,x,y,z))
			{
				if(nev == true)
				{
					if(playerid == i)
					{
						format(sztring,sizeof(sztring),"Te%s",string);
						SendClientMessage(i,color,sztring);
					}
					else
					{
						while( bari < MAX_BARAT && talal == false)
						{
							if(PlayerInfo[playerid][dbid] == barat[i][bari])
							{
								talal = true;
							}
							else
							{
								talal = false;
								bari++;
							}
						}
						if(talal == true)
						{
							format(sztring,sizeof(sztring),"%s%s",Nev(playerid),string);
							SendClientMessage(i,color,sztring);
						}
						else
						{
							format(sztring,sizeof(sztring),"Valaki%s",string);
							SendClientMessage(i,color,sztring);
						}
					}
				}
				else
				{
					SendClientMessage(i,color,string);
				}
			}
		}
		chatfix[i] =0;
	}
	return 1;
}
stock str_replace(string[], find, replace)
{
	new len = strlen(string);
	while(len--)
	{
		if(string[len] == find) string[len] = replace;
	}
	return string;
}

stock Cselekves(playerid, cselekves[])
{
	new string[128];
	format(string, sizeof(string), " %s", cselekves);
	chats(30.0,playerid,string,COLOR_FADE1,true);
	return 1;
}
stock GetDateAndTimeOfMs(millisecond, &year, &month, &day, &hour, &minute, &second)
{
    new accuracity;
    tickcount(accuracity);
    while(millisecond > accuracity-1)
    {
        millisecond = millisecond-accuracity;
        second++;
    }
    while(second > 59)
    {
        second = second-60;
        minute++;
    }
    while(minute > 59)
    {
        minute = minute-60;
        hour++;
    }
    while(hour > 23)
    {
        hour = hour-24;
        day++;
    }
    while(day > 31)
    {
        day = day-32;
        month++;
    }
    while(month > 11)
    {
        month = month-12;
        year++;
    }
}
stock GetDateAndTimeOfh(millisecond, &hour, &minute, &second)
{
    new accuracity;
    tickcount(accuracity);
    while(millisecond > accuracity-1)
    {
        millisecond = millisecond-accuracity;
        second++;
    }
    while(second > 59)
    {
        second = second-60;
        minute++;
    }
    while(minute > 59)
    {
        minute = minute-60;
        hour++;
    }
   /* while(hour > 23)
    {
        hour = hour-24;
        day++;
    }*/
}
public jail()
{
	for(new i = 0; i < MAX_PLAYERS; i++)
	{
		if(PlayerInfo[i][pborton] > 0)
		{
			PlayerInfo[i][pborton]--;
			if(PlayerInfo[i][pborton] ==1)
			{
				switch(PlayerInfo[i][btipus])
				{
					case 1:{SetPlayerPos(i,246.553,112.513,1003.218);SetPlayerInterior(i,10);SetPlayerVirtualWorld(i,0);PlayerInfo[i][pborton]=0;PlayerInfo[i][btipus]=0;}
					case 2:{}
					case 3:{}
					case 4:{}
					case 5:{}
				
				}
			}
		}
	}
}
/*stock GetPSpeed(playerid, bool:kmh) // by misco
{
    new Float:Vx,Float:Vy,Float:Vz,Float:rtn;
    if(IsPlayerInAnyVehicle(playerid)) GetVehicleVelocity(GetPlayerVehicleID(playerid),Vx,Vy,Vz); else GetPlayerVelocity(playerid,Vx,Vy,Vz);
    rtn = floatsqroot(floatabs(floatpower(Vx + Vy + Vz,2)));
    return kmh?floatround(rtn * 100 * 1.61):floatround(rtn * 100);
}*/
public timerekhez()
{
	new Hour, Minute, Second;
	gettime(Hour, Minute, Second);
	if( Hour == 22 && Minute == 0 && Second == 0)
	{
		for(new i;i<MAX_PLAYERS;i++)
		{
			if(IsPlayerConnected(i))
			{
				saveplayer(i);
			}
		}
		savebiz();
		savehouse();
		savecar();
		new sql[256];
		mysql_format(mysql,sql,sizeof(sql),"SELECT `Id`,`frakcio`,`frang`,`fizetes`,`fiztipus`,`felfiz`,`fizszam`,`maonline` FROM `user`");
		mysql_tquery(mysql,sql,"fizetes");
	}
	if((Minute == 30 && Second == 0) || (Minute == 00 && Second == 0))
	{
		new rand = random(11);
		if(rand == 8)
		{
			rand ++;
		}
		SetWeather(rand);
	}
	if( Minute == 00 && Second == 0 )
	{
//		mysql_set_character_set("UTF8");
		mysql_tquery(mysql,"SET NAMES latin2");
		SetWorldTime(Hour);
	}
	for(new i;i<MAX_VEHICLES;i++)
	{
		new engine, lights, alarm, doors, bonnet, boot, objective;
		new Float:vvhp;
		GetVehicleParamsEx(i, engine, lights, alarm, doors, bonnet, boot, objective);
		if((VehicleInfo[i][uzemanyag] <0) && (engine ==1))
		{
			SetVehicleParamsEx(i, 0, lights, alarm, doors, bonnet, boot, objective);
		}
		if((VehicleInfo[i][uzemanyag] != 0) && (engine == 1) && (GetVehicleSpeed(i) < 10))
		{
			VehicleInfo[i][uzemanyag] -= 0.0041;
		}
		GetVehicleHealth(i,vvhp);
		if(vvhp <= 250)
		{
			SetVehicleHealth(i,251);
		}
		if(VehicleInfo[i][lefoglaltido] > 0)
		{
			VehicleInfo[i][lefoglaltido] --;
		}
	}
	if(gettime() > dobozido && dobozpakolhato == false && elsoinditas == false)
	{
		dobozspawnid = random(sizeof(fegyverspawn));
		dobozgeneral(dobozspawnid);
		dobozpakolhato = true;
		dobozpakolasiido = gettime()+900;
	}
	
	if((gettime() > dobozpakolasiido && dobozpakolhato == true) || elsoinditas == true)
	{
		elsoinditas = false;
		dobozpakolhato = false;
		new rndido = random(3)+3;
		dobozido = gettime() + ((rndido * 60) * 60);
	}
	return 1;
}
public playertimer(playerid)
{
	if(pvisz[playerid]==1)
	{
		if(IsPlayerConnected(kivisz[playerid]))
		{
			new Float:x,Float:y,Float:z,int,vvw;
			GetPlayerPos(kivisz[playerid],x,y,z);
			int = GetPlayerInterior(kivisz[playerid]);
			vvw = GetPlayerVirtualWorld(kivisz[playerid]);
			SetPlayerPos(playerid,x+1,y+1,z);
			SetPlayerInterior(playerid,int);
			SetPlayerVirtualWorld(playerid,vvw);
		}
		else
		{
			SetPlayerSpecialAction(playerid,SPECIAL_ACTION_NONE);
			SendClientMessage(playerid,-1,"(( Kiszabadultál ))");
		}
	}
	if(PlayerInfo[playerid][pakolhat] != 0)
	{
		PlayerInfo[playerid][pakolhat] --;
	}
	PlayerInfo[playerid][maonline]++;
}
public ajtokbe(playerid)
{
	for(new aid;aid< MAX_AJTO;aid++)
	{
		if(IsPlayerInRangeOfPoint(playerid,1.5,AjtoInfo[aid][ax],AjtoInfo[aid][ay],AjtoInfo[aid][az]))
		{
			if(PlayerInfo[playerid][pborton] > 0 && PlayerInfo[playerid][btipus] == 5)
			{
				SendClientMessage(playerid,COLOR_RED,"(( Kórház egyik kezelése alatt áll! ))");
			}
			else
			{
				//SetPlayerPos(playerid,AjtoInfo[aid][fx],AjtoInfo[aid][fy],AjtoInfo[aid][fz]);
				new Float:nx, Float:ny;
				GetXYInFrontOfPlayerAjto(nx, ny, AjtoInfo[aid][fx],AjtoInfo[aid][fy], AjtoInfo[aid][face1],3);
				SetPlayerVirtualWorld(playerid,AjtoInfo[aid][virt]);
				SetPlayerInterior(playerid,AjtoInfo[aid][inter]);
				SetPlayerFacingAngle(playerid,AjtoInfo[aid][face1]);
				SetPlayerPos(playerid,nx,ny,AjtoInfo[aid][fz]);
				new Float:x,Float:y,Float:z;
				GetPlayerPos(playerid,x,y,z);
				Streamer_UpdateEx(playerid,x,y,z,GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));
			}
		}
		if(IsPlayerInRangeOfPoint(playerid,1.5,AjtoInfo[aid][fx],AjtoInfo[aid][fy],AjtoInfo[aid][fz]))
		{
			if(PlayerInfo[playerid][pborton] > 0 && PlayerInfo[playerid][btipus] == 5)
			{
				SendClientMessage(playerid,COLOR_RED,"(( Kórház egyik kezelése alatt áll! ))");
			}
			else
			{
				//SetPlayerPos(playerid,AjtoInfo[aid][ax],AjtoInfo[aid][ay],AjtoInfo[aid][az]);
				new Float:nx, Float:ny;
				GetXYInFrontOfPlayerAjto(nx, ny, AjtoInfo[aid][ax],AjtoInfo[aid][ay], AjtoInfo[aid][face2],3);
				SetPlayerVirtualWorld(playerid,0);
				SetPlayerInterior(playerid,0);
				SetPlayerFacingAngle(playerid,AjtoInfo[aid][face2]);
				SetPlayerPos(playerid,nx,ny,AjtoInfo[aid][az]);
				new Float:x,Float:y,Float:z;
				GetPlayerPos(playerid,x,y,z);
				Streamer_UpdateEx(playerid,x,y,z,GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));
			}
		}
	}
	return 1;
}
public ruhavalaszto(playerid)
{
	new Float:fca;
	GetPlayerFacingAngle(playerid,fca);
	SetPlayerFacingAngle(playerid,fca+5);
	return 1;
}
public OnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart)
{

	if(IsPlayerConnected(issuerid))
	{
		if(((weaponid > 21) && (weaponid < 37)) && amount < 30)
		{
			if(bodypart == 3)
			{
				new Float:armor;
				GetPlayerArmour(playerid,armor);
				if(armor > 1)
				{
					Cselekves(playerid,"megvédte a golyóálló mellény.");
					SetPlayerDrunkLevel(playerid,2500);
				}
				else
				{
					Cselekves(playerid,"golyót kapott a testébe.");
					animban[playerid] = 1;
					animtipus[playerid] = 2;
					if(lotttimerbe[playerid] == false)
					{
						elverzest[playerid] = SetTimerEx("elverzes",300000,false,"u",playerid);
						lotttimerbe[playerid] = true;
					}
				}
			}
			if((bodypart == 5) || (bodypart == 6))
			{
				Cselekves(playerid,"golyót kapott a karjába.");
				animban[playerid] = 1;
				animtipus[playerid] = 2;
				if(lotttimerbe[playerid] == false)
				{
					elverzest[playerid] = SetTimerEx("elverzes",300000,false,"u",playerid);
					lotttimerbe[playerid] = true;
				}
			}
			if((bodypart == 7) || (bodypart == 8))
			{
				Cselekves(playerid,"golyót kapott a lábába.");
				animban[playerid] = 1;
				animtipus[playerid] = 2;
				if(lotttimerbe[playerid] == false)
				{
					elverzest[playerid] = SetTimerEx("elverzes",300000,false,"u",playerid);
					lotttimerbe[playerid] = true;
				}
			}
			if(bodypart == 4)
			{
				Cselekves(playerid,"golyót kapott az ágyékába.");
				animban[playerid] = 1;
				animtipus[playerid] = 2;
				if(lotttimerbe[playerid] == false)
				{
					elverzest[playerid] = SetTimerEx("elverzes",300000,false,"u",playerid);
					lotttimerbe[playerid] = true;
				}
			}
			if(bodypart == 9)
			{
				SetPlayerHealth(playerid,0);
				Cselekves(playerid,"golyót kapott a fejébe.");
			}
		}
	}
	if((issuerid == INVALID_PLAYER_ID) && (weaponid == 54))
	{
		if(amount > 30)
		{
			Cselekves(playerid,"nagyot esett");
			ApplyAnimation(playerid, "SWEET", "Sweet_injuredloop",4.1,1,0,0,1,0,0);
			//animban[playerid] = 1;
			//animtipus[playerid] = 1;
		}
	}
	if((issuerid == INVALID_PLAYER_ID) && (weaponid == 51))
	{
		if(amount > 30)
		{
			Cselekves(playerid,"robbanás áldozatává vált");
			SetPlayerHealth(playerid,15);
			ApplyAnimation(playerid, "SWEET", "Sweet_injuredloop",4.1,1,0,0,1,0,0);
			//animban[playerid] = 1;
			//animtipus[playerid] = 1;
		}
	}
	if((issuerid == INVALID_PLAYER_ID) && (weaponid == 49))
	{
		if(amount > 15)
		{
			Cselekves(playerid,"elütötte egy kocsi");
			ApplyAnimation(playerid, "SWEET", "Sweet_injuredloop",4.1,1,0,0,1,0,0);
			//animban[playerid] = 1;
			//animtipus[playerid] = 1;
		}
	}
	if(sokkolo[issuerid] ==1 && weaponid ==0)
	{
		ApplyAnimation(playerid, "CRACK", "crckdeth2", 4.0, 0, 0, 15000, 1, 0);
		GameTextForPlayer(playerid,"Sokkolva lettél!",9000,4);
	}
	if(sokkolop[issuerid] ==1 && weaponid ==22)
	{
		ApplyAnimation(playerid, "CRACK", "crckdeth2", 4.0, 0, 0, 15000, 1, 0);
		GameTextForPlayer(playerid,"Sokkolva lettél!",9000,4);
	}
}
public OnVehicleLoseHealth(playerid, vehid, health)
{
	new Float:php;
	switch(health)
	{
		case 100 .. 140:
		{
			GetPlayerHealth(playerid, php);
			SetPlayerHealth(playerid, php-10);
			SetPlayerDrunkLevel(playerid,3500);
			Cselekves(playerid,"könnyeben megsérült.");
		}
		case 141 .. 250:
		{
			new engine, lights, alarm, doors, bonnet, boot, objective;
			new rnd[2];
			GetPlayerHealth(playerid, php);
			SetPlayerHealth(playerid, php-35);
			SetPlayerDrunkLevel(playerid,5000);
			GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
			SetVehicleParamsEx(vehid, 0, lights, alarm, doors, bonnet, boot, objective);
			rnd[0] = random(3);
			rnd[1] = random(3);
			if(rnd[0] == 1)
			{
				uszivargas[vehid] = true;
			}
			if(rnd[1] == 1)
			{
				beszorulva[playerid] = true;
			}
			animban[playerid] = 1;
			Cselekves(playerid,"súlyosan megsérült.");
			animtipus[playerid] = 3;
		}
		case 251 .. 999:
		{	
			new engine, lights, alarm, doors, bonnet, boot, objective;
			GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
			SetVehicleParamsEx(vehid, 0, lights, alarm, doors, bonnet, boot, objective);
			Cselekves(playerid,"szörnyethalt.");
			SetPlayerHealth(playerid, -1.0);
		}
	}
	if(kreszgyakorlasban[playerid] == true)
	{
		if(health > 3)
		{
			VehicleInfo[vehid][Vvan] = false;
			DestroyVehicle(vehid);			
			SetPlayerPos(playerid,-2030.5,-90.207,35.320);
			SendClientMessage(playerid,-1,"(( Megtörted a kocsit, az eddig megtett km nem kerül mentésre! ))");
		}
	}
	return 1;
}

public ProxDetectorS(Float:radi, playerid, targetid)
{
    if(IsPlayerConnected(playerid)&&IsPlayerConnected(targetid))
	{
		new Float:posx, Float:posy, Float:posz;
		new Float:oldposx, Float:oldposy, Float:oldposz;
		new Float:tempposx, Float:tempposy, Float:tempposz;
		GetPlayerPos(playerid, oldposx, oldposy, oldposz);
		//radi = 2.0; //Trigger Radius
		GetPlayerPos(targetid, posx, posy, posz);
		tempposx = (oldposx -posx);
		tempposy = (oldposy -posy);
		tempposz = (oldposz -posz);
		//printf("DEBUG: X:%f Y:%f Z:%f",posx,posy,posz);
		if (((tempposx < radi) && (tempposx > -radi)) && ((tempposy < radi) && (tempposy > -radi)) && ((tempposz < radi) && (tempposz > -radi)))
		{
			return 1;
		}
	}
	return 0;
}
public PosLog(string[])
{
	new entry[256];
	format(entry, sizeof(entry), "%s\n",string);
	new File:hFile;
	hFile = fopen("posok.log", io_append);
	fwrite(hFile, entry);
	fclose(hFile);
}
public LoadServerPro()
{
	new query[128],string[128],string2[128],nev[128],verzio[64],sznev[128],szjelszo[128],string3[128];

	cache_get_value_name(0,"sznev",sznev,128);
	cache_get_value_name(0,"nev",nev,128);
	cache_get_value_name(0,"verzio",verzio,128);
	cache_get_value_name(0,"jelszo",szjelszo,128);
	format(string,sizeof(string),"%s %s",nev,verzio);
	SetGameModeText(string);
	format(string2,sizeof(string2),"hostname %s",sznev);
	SendRconCommand(string2);
	format(string3,sizeof(string3),"password %s",szjelszo);
	SendRconCommand(string3);
	
	new Cache:res;
	mysql_format(mysql,query,sizeof(query),"SELECT `ado` FROM `ado` WHERE `ID` = '15'");
	res = mysql_query(mysql,query);
	cache_get_value_name_float(0,"ado",AFA);
	cache_delete(res);

	format(query,sizeof(query),"SELECT `ado` FROM `ado` WHERE `ID` = '16'");
	res = mysql_query(mysql,query);
	cache_get_value_name_float(0,"ado",SZJA);
	cache_delete(res);
}

public fizetes()
{	
	new rows;
	new Cache:mentett = cache_save();
	cache_get_row_count(rows);
	for(new i=0;i<rows;i++)
	{
		
		new dbi,fiz,fizt,felf,fizsz,fizrang,fizfrak,mao,tmp_fiz,tmp_frakciopenz,tmp_fizado,tmp_hazado,tmp_rezsi,tmp_jarmuado,tmp_iparado,tmp_fizetes;
		
		cache_set_active(mentett);
		cache_get_value_name_int(i,"Id",dbi);
		cache_get_value_name_int(i,"frakcio",fizfrak);
		cache_get_value_name_int(i,"frang",fizrang);
		cache_get_value_name_int(i,"fizetes",fiz);
		cache_get_value_name_int(i,"fiztipus",fizt);
		cache_get_value_name_int(i,"felfiz",felf);
		cache_get_value_name_int(i,"fizszam",fizsz);
		cache_get_value_name_int(i,"maonline",mao);
		
		// online ellenõrzés
		new k = 0;
		new bool:online = false;
		while(k < MAX_PLAYERS && online == false)
		{
			if(PlayerInfo[k][dbid] == dbi)
			{
				online = true;
			}
			else
			{
				k++;
			}
		}
		
		if(mao > 3600)
		{
			new query[256];
			new Cache:res;
			new Cache:tmp_res;
			new rowe;
			if(fizfrak != 0)
			{
				//frakciós fizetés 
				mysql_format(mysql,query,sizeof(query),"SELECT `%df` FROM `frakcio` WHERE `Id` = '%d'",fizrang,fizfrak);
				mysql_query(mysql,query);
				cache_get_value_index_int(0,0,tmp_fiz);
				//cache_delete(res);
				
				//frakciós bankszámla
				mysql_format(mysql,query,sizeof(query),"SELECT `penz` FROM `bank` WHERE `frakcio` = '%d'",fizfrak);
				res = mysql_query(mysql,query);
				cache_get_value_name_int(0,"penz",tmp_frakciopenz);
				//cache_delete(res);
				if(tmp_frakciopenz - tmp_fiz >= 0)
				{
					mysql_format(mysql,query,sizeof(query),"UPDATE `bank` SET `penz`=`penz`-'%d' WHERE `frakcio` = '%d'",tmp_fiz,fizfrak);
					mysql_tquery(mysql,query);
					fiz += tmp_fiz;
				}
			}
			//fizetés adózása
			tmp_fizetes = fiz;
			tmp_fizado = floatround((fiz * SZJA) / 100);
			fiz -= tmp_fizado;
			allamibevetel(tmp_fizado);
			
			//házak
			new aquery[256];
			new tmp_hazertek;
			mysql_format(mysql,query,sizeof(query),"SELECT `Cost` FROM `house` WHERE `Owner` = '%d'",dbi);
			res = mysql_query(mysql,query);
			cache_get_row_count(rowe);
			for(new j=0;j < rowe;j++)
			{
				new Float:adokulcs;
				cache_get_value_name_int(j,"Cost",tmp_hazertek);
				mysql_format(mysql,aquery,sizeof(aquery),"SELECT `ado` FROM `ado` WHERE  '%d' > `mettol` AND `meddig` >= '%d' AND `fajta` = '0'",tmp_hazertek,tmp_hazertek);
				tmp_res = mysql_query(mysql,aquery);
				cache_get_value_name_float(0,"ado",adokulcs);
				//cache_delete(tmp_res);
				tmp_hazado += floatround((tmp_hazertek * adokulcs) / 100);
				tmp_rezsi += (tmp_hazertek * 2) / 100;
			}
			//cache_delete(res);
			//ház adózása
			if(fiz - tmp_hazado >= 0)
			{
				fiz -= tmp_hazado;
				allamibevetel(tmp_hazado);
			}
			else
			{
				if(online == true)
				{
					PlayerInfo[k][ado] += tmp_hazado;
				}
				else
				{
					mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `ado`= `ado` + %d WHERE `Id` = '%d'",tmp_hazado,dbi);
					mysql_tquery(mysql,query);
				}
			}
			//ház rezsi
			if(fiz - tmp_rezsi >= 0)
			{
				fiz -= tmp_rezsi;
			}
			else
			{
				if(online == true)
				{
					PlayerInfo[k][ado] += tmp_rezsi;
				}
				else
				{
					mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `ado`= `ado` + %d WHERE `Id` = '%d'",tmp_rezsi,dbi);
					mysql_tquery(mysql,query);
				}
			}
			//jármûvek
			new tmp_modelid;
			mysql_format(mysql,query,sizeof(query),"SELECT `Model` FROM `vehicle` WHERE `Owner` = '%d'",dbi);
			res = mysql_query(mysql,query);
			cache_get_row_count(rowe);
			for(new j=0;j<rowe;j++)
			{
				new Float:adokulcs;
				cache_get_value_name_int(j,"Model",tmp_modelid);
				mysql_format(mysql,aquery,sizeof(aquery),"SELECT `ado` FROM `ado` WHERE  '%d' > `mettol` AND `meddig` >= '%d' AND `fajta` = '1'",vehdata[tmp_modelid-400][2],vehdata[tmp_modelid-400][2]);
				tmp_res = mysql_query(mysql,aquery);
				cache_get_value_name_float(0,"ado",adokulcs);
				//cache_delete(tmp_res);
				tmp_jarmuado += floatround((vehdata[tmp_modelid-400][2] * adokulcs) / 100);
			}
			//cache_delete(res);
			
			//jármû adózása
			if(fiz - tmp_jarmuado >= 0)
			{
				fiz -= tmp_jarmuado;
				allamibevetel(tmp_jarmuado);
			}
			else
			{
				if(online == true)
				{
					PlayerInfo[k][ado] += tmp_jarmuado;
				}
				else
				{
					mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `ado`= `ado` + %d WHERE `Id` = '%d'",tmp_jarmuado,dbi);
					mysql_tquery(mysql,query);
				}
			}
			
			//bizniszek
			new tmp_bizertek;
			mysql_format(mysql,query,sizeof(query),"SELECT `cost` FROM `biznisz` WHERE `owner` = '%d'",dbi);
			res = mysql_query(mysql,query);
			cache_get_row_count(rowe);
			for(new j=0;j<rowe;j++)
			{
				new Float:adokulcs;
				cache_get_value_name_int(j,"cost",tmp_bizertek);
				mysql_format(mysql,aquery,sizeof(aquery),"SELECT `ado` FROM `ado` WHERE  '%d' > `mettol` AND `meddig` >= '%d' AND `fajta` = '2'",tmp_bizertek,tmp_bizertek);
				tmp_res = mysql_query(mysql,aquery);
				cache_get_value_name_float(0,"ado",adokulcs);
				tmp_iparado += floatround((tmp_bizertek * adokulcs) / 100);
				//cache_delete(tmp_res);
			}
			//cache_delete(res);
			//biz adozas
			if(online == true)
			{
				PlayerInfo[k][ado] += tmp_iparado;
			}
			else
			{
				mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `ado`= `ado` + %d WHERE `Id` = '%d'",tmp_iparado,dbi);
				mysql_tquery(mysql,query);
			}
			
			//fizetés utalás
			if(fizt == 0)
			{
				mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `fizetes` = '0', `felfiz` = `felfiz` + '%d' WHERE `Id`='%d'",fiz,dbi);
				mysql_tquery(mysql,query);
			}
			else
			{
				mysql_format(mysql,query,sizeof(query),"UPDATE `bank` SET `penz`='%d' + `penz` WHERE `azonosito` = '%d'",fiz,fizsz);
				mysql_tquery(mysql,query);
				mysql_format(mysql,query,sizeof(query),"UPDATE `user` SET `fizetes` = '0' WHERE `Id`='%d'",dbi);
				mysql_tquery(mysql,query);
			}
			//online gondok
			if(online == true)
			{
				new string[128];
				format(string,sizeof(string),"((Fizetés folyamatban!))");
				SendClientMessage(k,-1,string);
				format(string,sizeof(string),"((Bruttó Fizetésed:%s SZJA:%s))",cformat(tmp_fizetes),cformat(tmp_fizado));
				SendClientMessage(k,-1,string);
				format(string,sizeof(string),"((Ingatlan adó:%s Rezsi:%s))",cformat(tmp_hazado),cformat(tmp_rezsi));
				SendClientMessage(k,-1,string);
				format(string,sizeof(string),"((Jármû adó:%s))",cformat(tmp_jarmuado));
				SendClientMessage(k,-1,string);
				format(string,sizeof(string),"((Iparûzési adó:%s))",cformat(tmp_iparado));
				SendClientMessage(k,-1,string);
				format(string,sizeof(string),"((Nettó fizetésed:%s ))",cformat(fiz));
				SendClientMessage(k,-1,string);
				PlayerInfo[k][pfizetes] = 0;
			}
			cache_delete(res);
			cache_delete(tmp_res);
		}
		else
		{
			if(online == true)
			{
				SendClientMessage(k,-1,"(( Nem dolgoztál eleget! ))");
			}
		}
		
		//biznisz fizetés
		for(new j;j<MAX_BIZZ;j++)
		{
			BizzInfo[j][kassza] += floatround((BizzInfo[j][Cost] * 0.8) / 100);
		}
	}
}

public PlayerScore(playerid)
{
	new lvl,lvlexp,idos;
	new hour, minute, millisecond,contime;
	new sql[128];
	cache_get_value_name_int(0,"lvl",lvl);
	cache_get_value_name_int(0,"lvlexp",lvlexp);
	cache_get_value_name_int(0,"connecttime",idos);
	contime =  gettime() - PlayerInfo[playerid][pconnecttime];
	GetDateAndTimeOfh(millisecond, hour, minute, contime);
	if((hour/4)-lvlexp > lvl)
	{
		mysql_format(mysql,sql,sizeof(sql),"UPDATE `user` SET `lvlexp`='%d',`lvl`='%d' WHERE `Id` = '%d'",lvlexp+lvl,lvl+1,PlayerInfo[playerid][dbid]);
		new Cache:res = mysql_query(mysql,sql);
		SetPlayerScore(playerid,lvl+1);
		PlayerInfo[playerid][level] = lvl+1;
		cache_delete(res);
	}
	else
	{
		if(lvl != 0)
		{
			SetPlayerScore(playerid,lvl);
			PlayerInfo[playerid][level] = lvl;
		}
		else
		{
			SetPlayerScore(playerid,1);
		}
	}
}
public IsVehicleOccupied(vehicleid)
{
	for(new i;i<MAX_PLAYERS;i++)
		if(IsPlayerInVehicle(i,vehicleid)) return 1;
	return 0;
}
stock GetClosestVehicle(playerid, Float:distance)
{
    new
        Float:xX,
        Float:yY,
        Float:zZ,
        retElement = -1
        ;
    for(new i = 1; i < MAX_VEHICLES; i++)
    {
        GetVehiclePos(i, xX, yY, zZ);
        new Float:odist = GetPlayerDistanceFromPoint(playerid, xX, yY, zZ);
		/*if (retElement == -1)
        {
            retElement = i;
            distance = odist;
        }*/
		if(IsPlayerInAnyVehicle(playerid))
		{
			if (odist < distance && i != GetPlayerVehicleID(playerid))
			{
				retElement = i;
				distance = odist;
			}
		}
		else
		{
			if (odist < distance)
			{
				retElement = i;
				distance = odist;
			}
		}
    }
    return retElement;
}

public hazpicnull(playerid)
{
	pickupba[playerid] =0;
}
public Audio_OnClientConnect(playerid)
{
	Audio_TransferPack(playerid);
}

public GetClosestPlayer(p1)
{
	new x,Float:dis,Float:dis2,player;
	player = -1;
	dis = 99999.99;
	for (x=0;x<MAX_PLAYERS;x++)
	{
		if(IsPlayerConnected(x))
		{
			if(x != p1)
			{
				dis2 = GetDistanceBetweenPlayers(x,p1);
				if(dis2 < dis && dis2 != -1.00)
				{
					dis = dis2;
					player = x;
				}
			}
		}
	}
	return player;
}

public Float:GetDistanceBetweenPlayers(p1,p2)
{
	new Float:x1,Float:y1,Float:z1,Float:x2,Float:y2,Float:z2;
	if(!IsPlayerConnected(p1) || !IsPlayerConnected(p2))
	{
		return -1.00;
	}
	GetPlayerPos(p1,x1,y1,z1);
	GetPlayerPos(p2,x2,y2,z2);
	return floatsqroot(floatpower(floatabs(floatsub(x2,x1)),2)+floatpower(floatabs(floatsub(y2,y1)),2)+floatpower(floatabs(floatsub(z2,z1)),2));
}
public Float:GetDistanceBetweenPoints(Float:rx1,Float:ry1,Float:rz1,Float:rx2,Float:ry2,Float:rz2)
{
    return floatadd(floatadd(floatsqroot(floatpower(floatsub(rx1,rx2),2)),floatsqroot(floatpower(floatsub(ry1,ry2),2))),floatsqroot(floatpower(floatsub(rz1,rz2),2)));
}
stock GetVehicleModelName(id, bool:carid = false)
{
	new str[64];
	if(!carid)
	{
		if(id >= 400)
			id -= 400;
		format(str,64, "%s", vehName[id]);
	}
	else
		format(str,64, "%s", vehName[GetVehicleModel(id) - 400]);
	return str;
}
/*public OnMysqlError(error[], errorid, MySQL:handle)
{
	new sqlerror[92];
	format(sqlerror,sizeof(sqlerror), "MySQL Hiba történt. ErrorID: %d. Hiba:", errorid);
	SendToDevelopment(COLOR_RED,sqlerror);
	format(sqlerror,sizeof(sqlerror), "%s", error);
	SendToDevelopment(COLOR_RED,sqlerror);
}*/
public OnQueryError(errorid, const error[], const callback[], const query[], MySQL:handle)
{
	new sqlerror[92];
	format(sqlerror,sizeof(sqlerror), "MySQL Hiba történt. ErrorID: %d. Hiba:", errorid);
	SendToDevelopment(COLOR_RED,sqlerror);
	format(sqlerror,sizeof(sqlerror), "%s", error);
	SendToDevelopment(COLOR_RED,sqlerror);
}

public IsPlayerCop(playerid)
{
	if(PlayerInfo[playerid][frakcio] !=0 && PlayerInfo[playerid][frakcio] < 7)
	{
		return true;
	}
	else
	{
		return false;
	}
}
stock savecar()
{
	new sql[1024];
	for(new i=1;i<MAX_VEHICLES;i++)
	{	
		if(VehicleInfo[i][Id] != 0)
		{
			new panels,doors,lights,tires;
			new Float:heal;
			GetVehicleDamageStatus(i,panels,doors,lights,tires);
			GetVehicleHealth(i,heal);
			mysql_format(mysql,sql,sizeof(sql),"UPDATE `vehicle` SET `Owner`='%d', `Ownername`='%s', `PosX`='%f', `PosY`='%f', `PosZ`='%f', `PosFace`='%f', `Color1`='%d', `Color2`='%d', `Plate`='%s', `health`='%f',`panels`='%d',`doors`='%d',`lights`='%d',`tires`='%d',`uzemanyag`='%f', `km`='%f', `lefoglalva`='%d', `tartozas`='%d', `ceg`='%d' WHERE `Id`='%d'",VehicleInfo[i][Owner],VehicleInfo[i][Ownername],VehicleInfo[i][PosX],VehicleInfo[i][PosY],VehicleInfo[i][PosZ],VehicleInfo[i][PosFace],
			VehicleInfo[i][Color1],VehicleInfo[i][Color2],VehicleInfo[i][Plate],heal,panels,doors,lights,tires,VehicleInfo[i][uzemanyag],VehicleInfo[i][megkilometer],VehicleInfo[i][lefoglalva],VehicleInfo[i][ktartozas],VehicleInfo[i][vceg],VehicleInfo[i][Id]);
			mysql_tquery(mysql,sql);
		}
	}
}
public kmszamlalo(vehicleid,playerid)
{
	new string[16];
	new Float:uzemsz;
	if(GetVehicleSpeed(vehicleid) > 10)
	{
		new Float:distant;
		distant = GetVehicleDistanceFromPoint(vehicleid,tmp_vkilometer[vehicleid][0],tmp_vkilometer[vehicleid][1],tmp_vkilometer[vehicleid][2]);
		distant /= 1000.0;
		GetVehiclePos(vehicleid,tmp_vkilometer[vehicleid][0],tmp_vkilometer[vehicleid][1],tmp_vkilometer[vehicleid][2]);
		if(distant < 250.0)
		{
			VehicleInfo[vehicleid][megkilometer] += distant;
		}
		format(string,sizeof(string),"%.1f KM",VehicleInfo[vehicleid][megkilometer]);
		if(GetVehicleModel(vehicleid) == 403 || GetVehicleModel(vehicleid) == 514 || GetVehicleModel(vehicleid) == 515)
		{
			PlayerInfo[playerid][ptchm] += floatdiv(GetVehicleSpeed(vehicleid), 300.0);
		}
		if(VehicleInfo[vehicleid][uzemanyagt] == 0)
		{
			switch(GetVehicleSpeed(vehicleid))
			{
				case 10 .. 70:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.56;
				}
				case 71 .. 110:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.2;
				}
				case 111 .. 125:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.32;
				}
				case 126 .. 150:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.68;
				}
				case 151 .. 300:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.8;
				}
			}
		}
		if(VehicleInfo[vehicleid][uzemanyagt] == 1)
		{
			switch(GetVehicleSpeed(vehicleid))
			{
				case 10 .. 70:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.04;
				}
				case 71 .. 110:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 0.8;
				}
				case 111 .. 125:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 0.88;
				}
				case 126 .. 150:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.12;
				}
				case 151 .. 300:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.2;
				}
			}
		}	
		if(VehicleInfo[vehicleid][uzemanyagt] == 2)
		{
			switch(GetVehicleSpeed(vehicleid))
			{
				case 10 .. 70:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.95;
				}
				case 71 .. 110:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.6;
				}
				case 111 .. 125:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.65;
				}
				case 126 .. 150:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 2.1;
				}
				case 151 .. 300:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 2.25;
				}
			}
		}	
		if(VehicleInfo[vehicleid][uzemanyagt] == 3)
		{
			switch(GetVehicleSpeed(vehicleid))
			{
				case 10 .. 70:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.69;
				}
				case 71 .. 110:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.3;
				}
				case 111 .. 125:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.43;
				}
				case 126 .. 150:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.82;
				}
				case 151 .. 300:
				{
					VehicleInfo[vehicleid][uzemanyag] -= floatdiv(vehdata[GetVehicleModel(vehicleid)-400][1],100) * floatdiv(GetVehicleSpeed(vehicleid), 300.0) * 1.95;
				}
			}
		}	
		uzemsz = floatdiv(VehicleInfo[vehicleid][uzemanyag],vehdata[GetVehicleModel(vehicleid)-400][0]) * 100;
		TextDrawSetString(kmmero[vehicleid], string);
		SetPlayerProgressBarValue(playerid, uzemanyagb[playerid], uzemsz);
	}
	
}
stock GetVehicleSpeed(vehicleid)
{
	new Float: ST[4];
	GetVehicleVelocity(vehicleid, ST[0], ST[1], ST[2]);
	ST[3] = floatsqroot(floatpower(floatabs(ST[0]), 2.0) + floatpower(floatabs(ST[1]), 2.0) + floatpower(floatabs(ST[2]), 2.0)) * 180;
	return floatround(ST[3]);
}
stock mktime(m_hour, m_minute, m_second, m_day, m_month, m_year)
{
	new timestamp2;
	timestamp2 = m_second + (m_minute * 60) + (m_hour * 3600);
	new days_of_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	if(((m_year % 4 == 0) && (m_year % 100 != 0)) || (m_year % 400 == 0))
	{
		days_of_month[1] = 29;
	}
	else
	{
		days_of_month[1] = 28;
	}
	new days_this_year = 0;
	days_this_year = --m_day;
	if(m_month > 1)
	{
		for(new i = 0; i < m_month - 1; i++)
		{
			days_this_year += days_of_month[i];
		}
	}
	timestamp2 += days_this_year * 86400;   
	for(new j = 1970; j < m_year; j++)
	{
		timestamp2 += 31536000;
		if(((j % 4 == 0) && (j % 100 != 0)) || (j % 400 == 0))  timestamp2 += 86400;
	}
	return timestamp2;
}
stock datemk(timestamp, &f_day, &f_month, &f_year, &f_hour, &f_min, &f_sec)
{
	new s_year=1970, s_day=0, s_month=0, s_hour=0, s_mins=0;
	new days_of_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	while(timestamp > 31622400)
	{
		timestamp -= 31536000;
		if(((s_year % 4 == 0) && (s_year % 100 != 0)) || (s_year % 400 == 0))  timestamp -= 86400;
		s_year++;
	}
	if(((s_year % 4 == 0) && (s_year % 100 != 0)) || (s_year % 400 == 0))
	{
		days_of_month[1] = 29;
	}
	else
	{
		days_of_month[1] = 28;
	} 
	while(timestamp > 86400)
	{
		timestamp -= 86400, s_day++;
		if(s_day == days_of_month[s_month]) s_day=0, s_month++;
	}
	while(timestamp>60)
	{
		timestamp -= 60, s_mins++;
		if(s_mins == 60) s_mins=0, s_hour++;
	}
	f_day = s_day + 1;
	f_month = s_month + 1;
	f_year = s_year;
	f_hour = s_hour;
	f_min = s_mins;
	f_sec = timestamp;
 
	return true;
}
public LoadMoveObject()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			cache_get_value_name_int(i,"ID",KapuInfo[i][dbid]);
			cache_get_value_name_int(i,"model",KapuInfo[i][model]);
			cache_get_value_name_int(i,"munka",KapuInfo[i][kmunka]);
			cache_get_value_name_int(i,"frakcio",KapuInfo[i][kfrakcio]);
			cache_get_value_name_float(i,"x",KapuInfo[i][kx]); 
			cache_get_value_name_float(i,"y",KapuInfo[i][ky]); 
			cache_get_value_name_float(i,"z",KapuInfo[i][kz]); 
			cache_get_value_name_float(i,"rx",KapuInfo[i][rkx]); 
			cache_get_value_name_float(i,"ry",KapuInfo[i][rky]); 
			cache_get_value_name_float(i,"rz",KapuInfo[i][rkz]); 
			cache_get_value_name_float(i,"nx",KapuInfo[i][knx]); 
			cache_get_value_name_float(i,"ny",KapuInfo[i][kny]); 
			cache_get_value_name_float(i,"nz",KapuInfo[i][knz]); 
			KapuInfo[i][kid] = CreateDynamicObject(KapuInfo[i][model],KapuInfo[i][kx],KapuInfo[i][ky],KapuInfo[i][kz],KapuInfo[i][rkx],KapuInfo[i][rky],KapuInfo[i][rkz]);
		}
	}
}
CMD:spanicszoba(playerid,params[])
{
	if(IsPlayerInRangeOfPoint(playerid,3,2537.3000500,-1456.0000000,1058.5000000))
	{
		if(seanpanicszoba[1] == 0)
		{
			MoveDynamicObject(seanpanicszoba[0],2530.3000500,-1456.0000000,1058.5000000,1);
			seanpanicszoba[1] = 1;
		}
		else
		{
			MoveDynamicObject(seanpanicszoba[0],2537.3000500,-1456.0000000,1058.5000000,1);
			seanpanicszoba[1] = 0;
		}
	}
	return 1;
}
public playervaltnull(playerid)
{
	PlayerInfo[playerid][Admin] = 0;
	PlayerInfo[playerid][Score] = 0;
	PlayerInfo[playerid][skin] = 0;
	PlayerInfo[playerid][Money] = 0;
	PlayerInfo[playerid][dbid] = 0;
	PlayerInfo[playerid][szuletes] = 0;
	PlayerInfo[playerid][neme] = 0;
	PlayerInfo[playerid][allamp] = 0;
	PlayerInfo[playerid][okm] = 0;
	PlayerInfo[playerid][frakcio] = 0;
	PlayerInfo[playerid][rang] = 0;
	PlayerInfo[playerid][pfrakcio] = 0;
	PlayerInfo[playerid][prang] = 0;
	PlayerInfo[playerid][pleader] = 0;
	PlayerInfo[playerid][pconnecttime] = 0;
	PlayerInfo[playerid][pszolg] = 0;
	PlayerInfo[playerid][fksin] = 0;
	PlayerInfo[playerid][pmunka] = 0;
	PlayerInfo[playerid][pborton] = 0;
	PlayerInfo[playerid][btipus] = 0;
	PlayerInfo[playerid][elfogad] = 0;
	PlayerInfo[playerid][felajan] = 0;
	PlayerInfo[playerid][kival] = 0;
	PlayerInfo[playerid][iitems] = 0;
	PlayerInfo[playerid][iamounts] = 0;
	PlayerInfo[playerid][pfizetes] = 0;
	PlayerInfo[playerid][felfiz] = 0;
	PlayerInfo[playerid][fiztipus] = 0;
	PlayerInfo[playerid][level] = 0;
	PlayerInfo[playerid][phazas] = 0;
	PlayerInfo[playerid][preg] = 0;
	PlayerInfo[playerid][kithivsz] = 0;
	PlayerInfo[playerid][ftipus] = 0;
	PlayerInfo[playerid][phfrakcio] = 0;
	PlayerInfo[playerid][alosztaly] = 0;
	PlayerInfo[playerid][ado] = 0;
	PlayerInfo[playerid][paleader] = 0;
	tisztit[playerid] = 0;
	ruhaskin[playerid] = 0;
	kivalaszt[playerid] = 0;
	Pfreeze[playerid] = 0;
	ahelp[playerid] = 0;
	aszolgban[playerid] = 0;
	anhp[playerid] = 0;
	vhealth[playerid] = 0;
	hazelad[playerid] = 0;
	banksz[playerid] = 0;
	bankj[playerid] = 0;
	forcess[playerid] = 0;
	bankssz[playerid] = 0;
	pickupba[playerid] = 0;
	admindia[playerid] = 0;
	sikertelenbe[playerid] = 0;
	telefonsz[playerid] = 0;
	phivasba[playerid] = 0;
	hivta[playerid] = 0;
	akihivo[playerid] = 0;
	hivo[playerid] = 0;
	csorogsz[playerid] = 0;
	smssz[playerid] = 0;
	pcsomag[playerid] = 0;
	berakcsomag[playerid] = 0;
	bilincsben[playerid] = 0;
	pvisz[playerid] = 0;
	kivisz[playerid] = 0;
	sokkolo[playerid] = 0;
	sokkolop[playerid] = 0;
	for(new i;i<MAX_BARAT;i++)
	{
		barat[playerid][i] = 0;
	}
	betoltb[playerid] = 0;
	chatfix[playerid] = 0;
	bizelad[playerid] = 0;
	aleaderk[playerid] = 0;
	bejelentes[playerid] = 0;
	animban[playerid] = 0;
	animtipus[playerid] = 0;
	rkocsiid[playerid] = 0;
	timerkmhez[playerid] = 0;
	fkradio[playerid] = 0;
	ajtokbett[playerid] = 0;
	muholdban[playerid] = false;
	return 1;
}
stock GetXYInFrontOfPlayer(playerid, &Float:x, &Float:y, Float:distance)
{
	new Float:a;
	GetPlayerPos(playerid, x, y, a);
	GetPlayerFacingAngle(playerid, a);
	if (GetPlayerVehicleID(playerid))
	{
	    GetVehicleZAngle(GetPlayerVehicleID(playerid), a);
	}
	x += (distance * floatsin(-a, degrees));
	y += (distance * floatcos(-a, degrees));
}
stock GetXYInFrontOfPlayerAjto(&Float:nx, &Float:ny, Float:x, Float:y, Float:a, Float:distance)
{
	nx = x+(distance * floatsin(-a, degrees));
	ny = y+(distance * floatcos(-a, degrees));
}
public trafitimerr(playerid)
{
	if(PlayerInfo[playerid][trafipaxo] == true)
	{
		for(new i; i<MAX_PLAYERS;i++)
		{
			if(IsPlayerInRangeOfPoint(i,8,PlayerInfo[playerid][trx],PlayerInfo[playerid][try],PlayerInfo[playerid][trz]) && IsPlayerInAnyVehicle(playerid))
			{
				if(GetPlayerSpeed(i,true) >= PlayerInfo[playerid][trafilim])
				{
					new string[128];
					SendClientMessage(playerid,-1,"(( |____________Trafipax____________| ))");
					format(string,sizeof(string),"(( Jármû rendszáma: %s ))",VehicleInfo[GetPlayerVehicleID(i)][Plate]);
					SendClientMessage(playerid,-1,string);
					format(string,sizeof(string),"(( Jármû típusa: %s ))",GetVehicleModelName(VehicleInfo[GetPlayerVehicleID(i)][Model],false));
					SendClientMessage(playerid,-1,string);
					format(string,sizeof(string),"(( Jármû sebessége: %d ))",GetPlayerSpeed(i,true));
					SendClientMessage(playerid,-1,string);
				}
			}
		}
	}
}
public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	if(PlayerInfo[playerid][Admin] >= 1337)
	{
		if(IsPlayerInAnyVehicle(playerid))
		{
			SetVehiclePos(GetPlayerVehicleID(playerid),fX, fY, fZ);
		}
		else
		{
			SetPlayerPosFindZ(playerid, fX, fY, fZ);
		}
	}
    return 1;
}
stock RemoveHexColorFromString(string[])
{
    new
        pos[2];
    pos[0] = strfind(string,"{",true,0);
	pos[1] = strfind(string,"}",true,0);
	for(new i;i < strlen(string); i++)
	{
	    if(pos[0]+7 == pos[1])
	    {
	        strdel(string, pos[0], pos[1]+1);
	        pos[0] = strfind(string,"{",true,0);
			pos[1] = strfind(string,"}",true,0);
		}
	}
	return 1;
}

stock GetNamei(playerid)
{
	new n[MAX_PLAYER_NAME];
	GetPlayerName(playerid,n,sizeof(n));
	return n;
}

stock CompareEx(comp[], with[]) //By: Fl0rian
{
	new LenghtComp = strlen(comp);
	new LenghtWith = strlen(with);
	new Character;

	if( LenghtComp != LenghtWith ) return false;
	
	for( new i = 0; i < LenghtComp; i++ )
	{
	    if( comp[i] == with[i] )
	    {
	        Character++;
		}
	}
	
	if( LenghtComp == Character ) return true;

	return false;
}

stock egyesitem(item[])
{	
	new bool:talal;
	new i=0;
	while(i < sizeof(fegyvernev) && talal==false)
	{
		if( (CompareEx(item,fegyvernev[i])) || (strfind(item, "Jármûkulcs", true) != -1)|| (strfind(item, "lõszer", true) != -1))
		{
			talal= true;
		}
		else
		{
			talal= false;
			i++;
		}
	}
	if(talal == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}
public prlkv(playerid)
{
	new bool:talal;
	new i=0;
	while(i < MAX_VEHICLES && talal==false)
	{
		if(CompareEx(VehicleInfo[i][Plate],PlayerInfo[playerid][prlk]))
		{
			talal= true;
		}
		else
		{
			talal= false;
			i++;
		}
	}
	if(talal == true)
	{
		new string[128];
		SendClientMessage(playerid,-1,"(( |_____Lekérdezés_____| ))");
		format(string,sizeof(string),"(( Jármû típusa: %s ))",GetVehicleModelName(VehicleInfo[i][Model],false));
		SendClientMessage(playerid,-1,string);
		format(string,sizeof(string),"(( Jármû rendszáma: %s ))",VehicleInfo[i][Plate]);
		SendClientMessage(playerid,-1,string);
		format(string,sizeof(string),"(( Jármû színe: %d %d ))",VehicleInfo[i][Color1],VehicleInfo[i][Color2]);
		SendClientMessage(playerid,-1,string);
		format(string,sizeof(string),"(( Jármû tulajdonos: %s ))",VehicleInfo[i][Ownername]);
		SendClientMessage(playerid,-1,string);
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nincs találat! ))");
	}
	return 1;
}
public startmotor(playerid,vehicleid)
{
	new engine, lights, alarm, doors, bonnet, boot, objective;
	if(inditmen[playerid] == true)
	{
		GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
		SetVehicleParamsEx(vehicleid, 1, lights, alarm, doors, bonnet, boot, objective);
		SendClientMessage(playerid, -1, "(( Indítva ))");
		folyindit[playerid] = false;
		inditmen[playerid] = false;
	}
	else
	{
		SendClientMessage(playerid, -1, "(( A motor lefulladt! Nemártana elvinned a szerelõhöz! ))");
		folyindit[playerid] = false;
		inditmen[playerid] = false;
	}
	return 1;
}
public omszvizsgal(playerid,id)
{
	new string[128];
	new tipus[128];
	new allapot[128];
	new ellatas[64];
	new Float:hp;
	GetPlayerHealth(id,hp);
	switch(serulestipus[id])
	{
		case 0:
		{
			sscanf("Ismeretlen", "s[128]", tipus);
		}
		case 1:
		{
			sscanf("Baleset", "s[128]", tipus);
		}
		case 2:
		{
			sscanf("Lõtt sérülés", "s[128]", tipus);
			sscanf("Igen", "s[64]", ellatas);
		}
	}
	switch(floatround(hp))
	{
		case 0 .. 9:
		{
			sscanf("Életveszélyes", "s[128]", allapot);
			sscanf("Igen", "s[64]", ellatas);
		}
		case 10 .. 19:
		{
			sscanf("Válságos", "s[128]", allapot);
			sscanf("Igen", "s[64]", ellatas);
		}
		case 20 .. 29:
		{
			sscanf("Súlyos", "s[128]", allapot);
			sscanf("Igen", "s[64]", ellatas);
		}
		case 30 .. 49:
		{	
			sscanf("Könnyebb", "s[128]", allapot);
			if(serulestipus[id] == 2)
			{
				sscanf("Igen", "s[64]", ellatas);
			}
			else
			{
				sscanf("Nem", "s[64]", ellatas);
			}
		}
		case 50 .. 100:
		{
			sscanf("Normál", "s[128]", allapot);
			if(serulestipus[id] == 2)
			{
				sscanf("Igen", "s[64]", ellatas);
			}
			else
			{
				sscanf("Nem", "s[64]", ellatas);
			}
		}
	}
	if(floatround(hp) < 15)
	{
		format(string,sizeof(string),"Sérülés tipusa: %s \n Sérült állapota: %s \n Szállítható:Nem \n Korházi ellátás: %s",tipus,allapot,ellatas);
	}
	else
	{
		format(string,sizeof(string),"Sérülés tipusa: %s \n Sérült állapota: %s \n Szállítható:Igen \n Korházi ellátás: %s",tipus,allapot,ellatas);
	}
	ShowPlayerDialog(playerid,DIALOG_OMSZ1,DIALOG_STYLE_MSGBOX,"OMSZ",string,"OK","");
	return 1;
}
public elverzes(playerid)
{
	new Float:hp;
	GetPlayerHealth(playerid,hp);
	SetPlayerHealth(playerid,hp-1);
	return 1;
}
public omszlottellat(playerid,id)
{
	SetPlayerHealth(id,100);
	SendClientMessage(playerid,-1,"(( Nyertél egy kis idõt siess! ))");
	return 1;
}
public OnVehicleDamageStatusUpdate(vehicleid, playerid)
{
	return 1;
}

stock GetVehiclePanelsDamageStatus(vehicleid, &FrontLeft, &FrontRight, &RearLeft, &RearRight, &WindShield, &FrontBumper, &RearBumper)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	FrontLeft = Panels & 15;
	FrontRight = Panels >> 4 & 15;
	RearLeft = Panels >> 8 & 15;
	RearRight = Panels >> 12 & 15;
	WindShield = Panels >> 16 & 15;
	FrontBumper = Panels >> 20 & 15;
	RearBumper = Panels >> 24 & 15;
	return true;
}

stock GetVehicleDoorsDamageStatus(vehicleid, &Bonnet, &Boot, &FrontLeft, &FrontRight, &RearLeft, &RearRight)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	Bonnet = Doors & 7;
	Boot = Doors >> 8 & 7;
	FrontLeft = Doors >> 16 & 7;
	FrontRight = Doors >> 24 & 7;
	RearLeft = Doors >> 32 & 7;
	RearRight = Doors >> 40 & 7;
	return true;
}

stock GetVehicleLightsDamageStatus(vehicleid, &First, &Second, &Third, &Fourth)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	First = Lights & 1;
	Second = Lights >> 1 & 1;
	Third = Lights >> 2 & 1;
	Fourth = Lights >> 3 & 1;
	return true;
}

stock GetVehicleTiresDamageStatus(vehicleid, &FrontLeft, &FrontRight, &RearLeft, &RearRight)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	if(GetVehicleType(vehicleid) == MOTORBIKE || GetVehicleType(vehicleid) == BIKE) FrontLeft = Tires >> 1 & 1, FrontRight = Tires & 1;
	else
	{
		RearRight = Tires & 1;
		FrontRight = Tires >> 1 & 1;
		RearLeft = Tires >> 2 & 1;
		FrontLeft = Tires >> 3 & 1;
	}
	return true;
}

stock UpdateVehiclePanelsDamageStatus(vehicleid, FrontLeft, FrontRight, RearLeft, RearRight, WindShield, FrontBumper, RearBumper)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	return UpdateVehicleDamageStatus(vehicleid, FrontLeft | (FrontRight << 4) | (RearLeft << 8) | (RearRight << 12) | (WindShield << 16) | (FrontBumper << 20) | (RearBumper << 24), Doors, Lights, Tires);
}

stock UpdateVehicleDoorsDamageStatus(vehicleid, Bonnet, Boot, FrontLeft, FrontRight, RearLeft, RearRight)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	return UpdateVehicleDamageStatus(vehicleid, Panels, Bonnet | (Boot << 8) | (FrontLeft << 16) | (FrontRight << 24) | (RearLeft << 32) | (RearRight << 40), Lights, Tires);
}

stock UpdateVehicleLightsDamageStatus(vehicleid, First, Second, Third, Fourth)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	return UpdateVehicleDamageStatus(vehicleid, Panels, Doors, First | (Second << 1) | (Third << 2) | (Fourth << 3), Tires);
}

stock GetVehicleType(vehicleid)
{
	switch(GetVehicleModel(vehicleid))
	{
		case 400 .. 416, 418 .. 424, 426 .. 429, 431 .. 434, 436 .. 440, 442 .. 445, 451, 455 .. 459, 466, 467, 470, 471, 474, 475, 477 .. 480, 482, 483, 485, 486, 489 .. 492, 494 .. 496, 498 .. 500, 502 .. 508, 514 .. 518, 524 .. 536, 539 .. 547, 549 .. 552, 554 .. 562, 565 .. 568, 571 .. 576, 578 .. 580, 582, 583, 585, 587 .. 589, 596 .. 605, 609: return CAR;
		case 481, 509, 510: return BIKE;
		case 448, 461 .. 463, 468, 521 .. 523, 581, 586: return MOTORBIKE;
		case 430, 446, 452 .. 454, 472, 473, 484, 493, 595: return BOAT;
		case 460, 476, 511 .. 513, 519, 520, 553, 577, 592, 593: return PLANE;
		case 441, 464, 465, 501, 564, 594: return RC;
		case 449, 537, 538, 569, 570, 590: return TRAIN;
		case 435, 450, 584, 591, 606 .. 608, 610, 611: return TRAILER;
		case 417, 425, 447, 469, 487, 488, 497, 548, 563: return HELICOPTER;
	}
	return -1;
}

stock UpdateVehicleTiresDamageStatus(vehicleid, FrontLeft, FrontRight, RearLeft, RearRight)
{
	new Panels, Doors, Lights, Tires;
	GetVehicleDamageStatus(vehicleid, Panels, Doors, Lights, Tires);
	if(GetVehicleType(vehicleid) == MOTORBIKE || GetVehicleType(vehicleid) == BIKE) return UpdateVehicleDamageStatus(vehicleid, Panels, Doors, Lights, FrontRight | (FrontLeft << 1));
	else return UpdateVehicleDamageStatus(vehicleid, Panels, Doors, Lights, RearRight | (FrontRight << 1) | (RearLeft << 2) | (FrontLeft << 3));
}

public otptimerek(playerid,vehicleid,tipus)
{
	if(tipus == 1)
	{
		if(uszivargas[vehicleid] == true)
		{
			SendClientMessage(playerid,-1,"(( Üzemanyag szivárgás: van! ))");
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Üzemanyag szivárgás: nincs! ))");
		}
	}
	if(tipus == 2)
	{
		uszivargas[vehicleid] = false;
		SendClientMessage(playerid,-1,"(( Szivárgás elállítva! ))");
	}
	if(tipus == 3)
	{
		new Bonnet, Boot, FrontLeft, FrontRight, RearLeft, RearRight;
		GetVehicleDoorsDamageStatus(vehicleid, Bonnet, Boot, FrontLeft, FrontRight, RearLeft, RearRight);
		UpdateVehicleDoorsDamageStatus(vehicleid,Bonnet, Boot, 4, 4, RearLeft, RearRight);
		for(new i;i<MAX_PLAYERS;i++)
		{
			if(GetPlayerVehicleID(i) == vehicleid)
			{
				beszorulva[i] = false;
			}
		}
	}
	return 1;
}
public OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ)
{
	if(hittype == BULLET_HIT_TYPE_VEHICLE)
	{
		if(weaponid <= 22 && weaponid >= 34)
		{
			if(IsValidVehicle(hitid))
			{
				new bool:talal;
				new i;
				while(i < MAX_PLAYERS && talal == false)
				{
					if(GetPlayerVehicleID(i) == hitid)
					{
						if(GetPlayerVehicleSeat(playerid) == 0)
						{
							talal = true;
						}
						else
						{
							i++;
						}
					}
					else
					{
						i++;
					}
				}
				if(talal == true)
				{
					new Float:vehip;
					GetVehicleHealth(hitid,vehip);
					SetVehicleHealth(hitid,vehip-50.0);
					return 0;
				}
			}
		}
	}
	if(hittype == BULLET_HIT_TYPE_PLAYER)
	{
		if(halalk[hitid] == true)
		{
			if(halalban[hitid] == false)
			{
				halalban[hitid] = true;
				SetPlayerHealth(hitid,100);
				animban[hitid] = 1;
				animtipus[hitid] = 3;
				TogglePlayerControllable(hitid,false);
				elverzest[hitid] = SetTimerEx("elverzes",1000,true,"u",hitid);
				return 0;
			}
		}
	}
    return 1;
}

Float:DistanceCameraTargetToLocation(Float:CamX, Float:CamY, Float:CamZ,   Float:ObjX, Float:ObjY, Float:ObjZ,   Float:FrX, Float:FrY, Float:FrZ) 
{
	new Float:TGTDistance;
	TGTDistance = floatsqroot((CamX - ObjX) * (CamX - ObjX) + (CamY - ObjY) * (CamY - ObjY) + (CamZ - ObjZ) * (CamZ - ObjZ));
	new Float:tmpX, Float:tmpY, Float:tmpZ;
	tmpX = FrX * TGTDistance + CamX;
	tmpY = FrY * TGTDistance + CamY;
	tmpZ = FrZ * TGTDistance + CamZ;
	return floatsqroot((tmpX - ObjX) * (tmpX - ObjX) + (tmpY - ObjY) * (tmpY - ObjY) + (tmpZ - ObjZ) * (tmpZ - ObjZ));
}

stock PlayerFaces(playerid, Float:x, Float:y, Float:z, Float:radius)
{
    new Float:cx,Float:cy,Float:cz,Float:ffx,Float:ffy,Float:ffz;
    GetPlayerCameraPos(playerid, cx, cy, cz);
    GetPlayerCameraFrontVector(playerid, ffx, ffy, ffz);
    return (radius >= DistanceCameraTargetToLocation(cx, cy, cz, x, y, z, ffx, ffy, ffz));
}
stock Float:GetPointAngleToPoint(Float:x2, Float:y2, Float:X, Float:Y) 
{
  new Float:DX, Float:DY;
  new Float:angle;

  DX = floatabs(floatsub(x2,X));
  DY = floatabs(floatsub(y2,Y));

  if (DY == 0.0 || DX == 0.0) {
    if(DY == 0 && DX > 0) angle = 0.0;
    else if(DY == 0 && DX < 0) angle = 180.0;
    else if(DY > 0 && DX == 0) angle = 90.0;
    else if(DY < 0 && DX == 0) angle = 270.0;
    else if(DY == 0 && DX == 0) angle = 0.0;
  }
  else {
    angle = atan(DX/DY);

    if(X > x2 && Y <= y2) angle += 90.0;
    else if(X <= x2 && Y < y2) angle = floatsub(90.0, angle);
    else if(X < x2 && Y >= y2) angle -= 90.0;
    else if(X >= x2 && Y > y2) angle = floatsub(270.0, angle);
  }

  return floatadd(angle, 90.0);
}
stock GetXYInFrontOfPoint(&Float:x, &Float:y, Float:angle, Float:distance) 
{
	x += (distance * floatsin(-angle, degrees));
	y += (distance * floatcos(-angle, degrees));
}
stock IsPlayerAimingAt(playerid, Float:x, Float:y, Float:z, Float:radius)
{
	new Float:camera_x,Float:camera_y,Float:camera_z,Float:vector_x,Float:vector_y,Float:vector_z;
	GetPlayerCameraPos(playerid, camera_x, camera_y, camera_z);
	GetPlayerCameraFrontVector(playerid, vector_x, vector_y, vector_z);

	new Float:vertical, Float:horizontal;

	switch (GetPlayerWeapon(playerid))
	{
		case 34,35,36:
		{
			if(DistanceCameraTargetToLocation(camera_x, camera_y, camera_z, x, y, z, vector_x, vector_y, vector_z) < radius) return true;
			else return false;
		}
		case 30,31: {vertical = 4.0; horizontal = -1.6;}
		case 33: {vertical = 2.7; horizontal = -1.0;}
		default: {vertical = 6.0; horizontal = -2.2;}
	}

	new Float:angle = GetPointAngleToPoint(0, 0, floatsqroot(vector_x*vector_x+vector_y*vector_y), vector_z) - 270.0;
	new Float:resize_x, Float:resize_y, Float:resize_z = floatsin(angle+vertical, degrees);
	GetXYInFrontOfPoint(resize_x, resize_y, GetPointAngleToPoint(0, 0, vector_x, vector_y)+horizontal, floatcos(angle+vertical, degrees));

	if(DistanceCameraTargetToLocation(camera_x, camera_y, camera_z, x, y, z, resize_x, resize_y, resize_z) < radius) return true;
	return false;
}
stock createfire(Float:x, Float:y, Float:z)
{
	new i;
	new bool:talal;
	new tuzid;
	while(i < MAX_TUZ && talal == false)
	{
		if(TuzInfo[i][ures] == false)
		{
			talal = true;
			tuzid = i;
		}
		else
		{
			i++;
		}
	}
	if(talal == true)
	{
		TuzInfo[tuzid][ID] = tuzid;
		TuzInfo[tuzid][ures] = true;
		TuzInfo[tuzid][THP] = 100;
		TuzInfo[tuzid][OID] = CreateDynamicObject(18692, x, y, z-2.5, 0, 0, 0.0);
		TuzInfo[tuzid][TX] = x;
		TuzInfo[tuzid][TY] = y;
		TuzInfo[tuzid][TZ] = z;
	}
}
stock deletefire(id)
{
	TuzInfo[id][ures] = false;
	DestroyDynamicObject(TuzInfo[id][OID]);
}
stock rndfire()
{
	vantuz();
	if(tuzeset == false)
	{
		new intenziv = random(10);
		for(new i;i<intenziv;i++)
		{
			new Float:x,Float:y;
			x = tuzcp[0][0]+(random(6) - random (5));
			y = tuzcp[0][1]+(random(6) - random (5));
			createfire(x,y, tuzcp[0][2]);
		}
		tuzeset = true;
	}
}
stock tuzupdate(playerid)
{
	new newkeys,l,u;
	GetPlayerKeys(playerid, newkeys, l, u);
	if(HOLDING(KEY_FIRE))
	{
		for(new i = 0; i < MAX_TUZ; i++)
		{
			if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 407 && GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
			{
				if(IsPlayerAimingAt(playerid, TuzInfo[i][TX],  TuzInfo[i][TY],  TuzInfo[i][TZ], 2.5) && IsPlayerInRangeOfPoint(playerid, 20.0, TuzInfo[i][TX],  TuzInfo[i][TY],  TuzInfo[i][TZ]))
				{
					TuzInfo[i][THP]-=2;
					if(TuzInfo[i][THP] <= 0)
					{
						deletefire(i);
					}
				}
			}
			else if(GetPlayerWeapon(playerid) == 42 && PlayerFaces(playerid, TuzInfo[i][TX],  TuzInfo[i][TY],  TuzInfo[i][TZ], 1) && IsPlayerInRangeOfPoint(playerid, 4, TuzInfo[i][TX],  TuzInfo[i][TY],  TuzInfo[i][TZ]))
			{
				TuzInfo[i][THP]-=1;
				if(TuzInfo[i][THP] <= 0)
				{
					deletefire(i);
				}
			}
		}
	}
}
stock vantuz()
{
	new i;
	new bool:talal;
	while(i < MAX_TUZ && talal == false)
	{
		if(TuzInfo[i][ures] == true)
		{
			talal = true;
		}
		else
		{
			talal = false;
			i++;
		}
	}
	if(talal == true)
	{
		tuzeset = true;
	}
	else
	{
		tuzeset = false;
	}
}
stock vscarresp(vehicleid)
{
	SetVehiclePos(vehicleid,VehicleInfo[vehicleid][PosX],VehicleInfo[vehicleid][PosY],VehicleInfo[vehicleid][PosZ]);
	SetVehicleZAngle(vehicleid,VehicleInfo[vehicleid][PosFace]);
}
public pszerel(playerid,vehicleid,listitem)
{
	SetVehicleVirtualWorld(vehicleid,1);
	szerelesben[playerid] = false;
	ClearAnimations(playerid);
	new PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper,DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight,LFirst, LSecond, LThird, LFourth,TFrontLeft, TFrontRight, TRearLeft, TRearRight;
	GetVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper);
	GetVehicleDoorsDamageStatus(vehicleid, DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight);
	GetVehicleLightsDamageStatus(vehicleid, LFirst, LSecond, LThird, LFourth);
	GetVehicleTiresDamageStatus(vehicleid, TFrontLeft, TFrontRight, TRearLeft, TRearRight);
	if(listitem == 0)
	{
		if(PFrontLeft != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, 0, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 1)
	{
		if(PFrontRight != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, 0, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 2)
	{
		if(PRearLeft != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, 0, PRearRight, PWindShield, PFrontBumper, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 3)
	{
		if(PRearRight != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, 0, PWindShield, PFrontBumper, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 4)
	{
		if(PWindShield != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, 0, PFrontBumper, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 5)
	{
		if(PFrontBumper != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, 0, PRearBumper);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 6)
	{
		if(PRearBumper != 0)
		{
			UpdateVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 7)
	{
		if(DBonnet != 0)
		{
			UpdateVehicleDoorsDamageStatus(vehicleid, 0, DBoot, DFrontLeft, DFrontRight, 0, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 8)
	{
		if(DBoot != 0)
		{
			UpdateVehicleDoorsDamageStatus(vehicleid, DBonnet, 0, DFrontLeft, DFrontRight, 0, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 9)
	{
		if(DFrontLeft != 0)
		{
			UpdateVehicleDoorsDamageStatus(vehicleid, DBonnet, DBoot, 0, DFrontRight, 0, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 10)
	{
		if(DFrontRight != 0)
		{
			UpdateVehicleDoorsDamageStatus(vehicleid, DBonnet, DBoot, DFrontLeft, 0, 0, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 11)
	{
		if(LFirst != 0)
		{
			UpdateVehicleLightsDamageStatus(vehicleid, 0, LSecond, LThird, LFourth);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 12)
	{
		if(LSecond != 0)
		{
			UpdateVehicleLightsDamageStatus(vehicleid, LFirst, 0, LThird, LFourth);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 13)
	{
		if(LThird != 0)
		{
			UpdateVehicleLightsDamageStatus(vehicleid, LFirst, LSecond, 0, LFourth);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 14)
	{
		if(LFourth != 0)
		{
			UpdateVehicleLightsDamageStatus(vehicleid, LFirst, LSecond, LThird, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 15)
	{
		if(TFrontLeft != 0)
		{
			UpdateVehicleTiresDamageStatus(vehicleid, 0, TFrontRight, TRearLeft, TRearRight);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 16)
	{
		if(TFrontRight != 0)
		{
			UpdateVehicleTiresDamageStatus(vehicleid, TFrontLeft, 0, TRearLeft, TRearRight);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 17)
	{
		if(TRearLeft != 0)
		{
			UpdateVehicleTiresDamageStatus(vehicleid, TFrontLeft, TFrontRight, 0, TRearRight);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	if(listitem == 18)
	{
		if(TRearRight != 0)
		{
			UpdateVehicleTiresDamageStatus(vehicleid, TFrontLeft, TFrontRight, TRearLeft, 0);
		}
		else
		{
			SendClientMessage(playerid,-1,"(( Ennek nincs semmi baja! ))");
		}
	}
	/*new Float:x,Float:y,Float:z,Float:angle;
	GetVehiclePos(vehicleid,x,y,z);
	GetVehicleZAngle(vehicleid,angle);
	SetVehiclePos(vehicleid,0,0,0);
	SetVehiclePos(vehicleid,x,y,z);
	SetVehicleZAngle(vehicleid,angle);*/
	SetTimerEx("kocsivwn",1000,false,"d",vehicleid);
	return 1;
}
public kocsivwn(vehicleid)
{
	SetVehicleVirtualWorld(vehicleid,0);
	return 1;
}
public pmotorszerel(playerid,vehicleid)
{
	new Float:vhpaa;
	GetVehicleHealth(vehicleid,vhpaa);
	if( (vhpaa+150.0) < 1000.0)
	{
		SetVehicleHealth(vehicleid,vhpaa+150);
	}
	else
	{
		SetVehicleHealth(vehicleid,1000);
	}
	szerelesben[playerid] = false;
	return 1;
}
public atvjatekos(playerid,ids)
{
	if(!IsPlayerInAnyVehicle(ids))
	{
		TogglePlayerSpectating(playerid, 1);
		PlayerSpectatePlayer(playerid, ids);
		SetPlayerInterior(playerid,GetPlayerInterior(ids));
	}
	else
	{
		TogglePlayerSpectating(playerid, 1);
		PlayerSpectateVehicle(playerid, GetPlayerVehicleID(ids));
	}
	return 1;
}
public kamionbepakol(playerid)
{
	TogglePlayerControllable(playerid,true);
	new vehid,tip;
	vehid = GetVehicleTrailer(GetPlayerVehicleID(playerid));	
	tip = random(2)+1;
	if(tip == 1)
	{
		SetPlayerCheckpoint(playerid,-520.608 ,-500.630, 25.000,3.0);
	}
	if(tip == 2)
	{
		SetPlayerCheckpoint(playerid,2346.506,2754.582,10.820,3.0);
	}
	if(tip == 3)
	{
		SetPlayerCheckpoint(playerid, -1733.823, 187.411 ,3.259,3.0);
	}
	kfelpakolva[vehid]= true;
	SendClientMessage(playerid,-1,"(( Pótkocsi felpakolva! ))");
	return 1;
}
public kamionlepakol(playerid,hely)
{
	TogglePlayerControllable(playerid,true);
	new vehid;
	vehid = GetVehicleTrailer(GetPlayerVehicleID(playerid));
	new string[128];
	if(hely == 1)
	{
		PlayerInfo[playerid][pfizetes] += 18000;
		format(string,sizeof(string),"(( 18.000 FT hozzáadva a fizetésedhez! ))");
	}
	if(hely == 2)
	{
		PlayerInfo[playerid][pfizetes] += 24500;
		format(string,sizeof(string),"(( 24.500 FT hozzáadva a fizetésedhez! ))");
	}
	if(hely == 3)
	{
		PlayerInfo[playerid][pfizetes] +=20000;
		format(string,sizeof(string),"(( 20.000 FT hozzáadva a fizetésedhez! ))");
	}
	kfelpakolva[vehid]= false;
	SendClientMessage(playerid,-1,"(( Lepakolás sikeres! ))");
	SendClientMessage(playerid,-1,string);
	return 1;
}

public LoadLicens(playerid)
{
		new rows;
		cache_get_row_count(rows);
		if(rows != 0)
		{
			new string[128];
			cache_get_value_name(0,"sorozatszam",PlayerInfo[playerid][jsorszam],16);
			cache_get_value_name(0,"megszerzett",PlayerInfo[playerid][jogosit],16);
			cache_get_value_name_int(0,"km",kreszkilometer[playerid]);
			for(new i=0;i<=4;i++)
			{
				for(new j=0;j<=3;i++)
				{
					format(string,sizeof(string),"%d%d",i,j);
					cache_get_value_name_int(0,string,kreszv[playerid][i][j]);
				}
			}
		}
}

stock jogostivanymentes(playerid)
{
	new sql[512];
	mysql_format(mysql,sql,sizeof(sql),"UPDATE `jogositvany` SET `megszerzett`='%s',`km`='%d',`00`='%d',`01`='%d',`02`='%d',`03`='%d',`10`='%d',`11`='%d',`12`='%d',`13`='%d',`20`='%d',`21`='%d',`22`='%d',`23`='%d',`30`='%d',`31`='%d',`32`='%d',`33`='%d',`40`='%d',`41`='%d',`42`='%d',`43`='%d' WHERE `Owner` = '%d'",
	PlayerInfo[playerid][jogosit],kreszkilometer[playerid],kreszv[playerid][0][0],kreszv[playerid][0][1],kreszv[playerid][0][2],kreszv[playerid][0][3],kreszv[playerid][1][0],kreszv[playerid][1][1],kreszv[playerid][1][2],kreszv[playerid][1][3],kreszv[playerid][2][0],kreszv[playerid][2][1],kreszv[playerid][2][2],
	kreszv[playerid][2][3],kreszv[playerid][3][0],kreszv[playerid][3][1],kreszv[playerid][3][2],kreszv[playerid][3][3],kreszv[playerid][4][0],kreszv[playerid][4][1],kreszv[playerid][4][2],kreszv[playerid][4][3],PlayerInfo[playerid][dbid]);
	mysql_tquery(mysql,sql);
}

public GetClosestBenzinbiz(playerid)
{
	new ids;
	new Float:tav;
	new bool:talal=false;
	new t;
	while(t < MAX_BIZZ && talal == false)
	{
		if(BizzInfo[t][bvan] == true)
		{
			if(BizzInfo[t][btipus] == 1)
			{
				tav = GetPlayerDistanceFromPoint(playerid, BizzInfo[t][px],BizzInfo[t][py],BizzInfo[t][pz]);
				ids = BizzInfo[t][Id];
				talal = true;
			}
			else
			{
				talal = false;
				t++;
			}
		}
		else
		{
			talal = false;
			t++;
		}
	}
	
	for(new i=0;i < MAX_BIZZ; i++)
	{
		if(BizzInfo[i][bvan] == true)
		{
			if(BizzInfo[i][btipus] == 1)
			{
				if( tav > GetPlayerDistanceFromPoint(playerid, BizzInfo[i][px],BizzInfo[i][py],BizzInfo[i][pz]))
				{
					tav = GetPlayerDistanceFromPoint(playerid, BizzInfo[i][px],BizzInfo[i][py],BizzInfo[i][pz]);
					ids = BizzInfo[i][Id];
				}
			}
		}
	}
	return ids;
}

public tankoltimer(playerid,levonando,vehid,bool:joe)
{
	new Float:x,Float:y,Float:z;
	GetVehiclePos(vehid,x,y,z);
	VehicleInfo[vehid][tankolasban] = false;
	new string[128];
	new engine, lights, alarm, doors, bonnet, boot, objective;
	format(string,sizeof(string),"(( Sikeresen megtankoltad a jármûvet %d FT-ért! ))",levonando);
	SendClientMessage(playerid,-1,string);
	PlayerInfo[playerid][Money] -= levonando;
	if(joe == false)
	{
		SetVehicleHealth(vehid,251);
	}
	GetVehicleParamsEx(vehid, engine, lights, alarm, doors, bonnet, boot, objective);
	if(engine == 1)
	{
		SetVehicleHealth(vehid,251);
		UpdateVehiclePanelsDamageStatus(vehid, 1, 1, 1, 1, 1, 1, 1);
		UpdateVehicleDoorsDamageStatus(vehid, 1, 1, 1, 1, 1, 1);
		UpdateVehicleLightsDamageStatus(vehid, 1, 1, 1, 1);
		UpdateVehicleTiresDamageStatus(vehid, 1, 1, 1, 1);
		CreateExplosion(x, y, z, 1, 10.0);
	}
	return 1;
}
public funyirotimer(playerid)
{
	DestroyPlayerObject(playerid,funyirofu[playerid]);
	SetPlayerCheckpoint(playerid, funyirocp[funyirocpsz[playerid]][0],funyirocp[funyirocpsz[playerid]][1],funyirocp[funyirocpsz[playerid]][2],5.0);
	funyirofu[playerid] = CreatePlayerObject(playerid, 822, funyirocp[funyirocpsz[playerid]][0],funyirocp[funyirocpsz[playerid]][1],funyirocp[funyirocpsz[playerid]][2], 0, 0, 96, 300.0);
	funyirocpsz[playerid]++;
	TogglePlayerControllable(playerid,true);
	return 1;
}
public GetPlayerSpeed(playerid, get3d)
{
	new Float:Vx, Float:Vy, Float:Vz;
    new Float:rtn;
	if (IsPlayerInAnyVehicle (playerid))
	{
		GetVehicleVelocity(GetPlayerVehicleID (playerid), Vx, Vy, Vz);
	}
	else
	{
		GetPlayerVelocity(playerid, Vx, Vy, Vz);
	}
    rtn = floatsqroot(floatpower(Vx*100,2) + floatpower(Vy*100,2));
    rtn = floatsqroot(floatpower(rtn,2) + floatpower(Vz*100,2));
    return floatround(rtn);
}

public autoinditas(playerid)
{
	if(IsPlayerInAnyVehicle(playerid))
	{
		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
		{
			if(VehicleInfo[GetPlayerVehicleID(playerid)][tankolasban] == false)
			{
				if(VehicleInfo[GetPlayerVehicleID(playerid)][lefoglalva] == 2 && PlayerInfo[playerid][frakcio] == 9 && PlayerInfo[playerid][pszolg] == 1)
				{
					SetTimerEx("startmotor",100,false,"ud",playerid,GetPlayerVehicleID(playerid));
					folyindit[playerid] = true;
					inditmen[playerid] = true;
					return 1;
				}
				new engine, lights, alarm, doors, bonnet, boot, objective,string[64];
				format(string,sizeof(string),"Jármûkulcs %s",VehicleInfo[GetPlayerVehicleID(playerid)][Plate]);
				if(ExistItem(playerid,string,1) || KExistItem(GetPlayerVehicleID(playerid),string,1))
				{
					if(VehicleInfo[GetPlayerVehicleID(playerid)][uzemanyag] >0.0)
					{
						GetVehicleParamsEx(GetPlayerVehicleID(playerid), engine, lights, alarm, doors, bonnet, boot, objective);
						if(engine == 0)
						{
							new Float:vhealths;
							GetVehicleHealth(GetPlayerVehicleID(playerid),vhealths);
							if(folyindit[playerid] == false)
							{
								if(vhealths == 1000)
								{
									folyindit[playerid] = true;
									inditmen[playerid] = true;
									SetTimerEx("startmotor",2000,false,"ud",playerid,GetPlayerVehicleID(playerid));
									GameTextForPlayer(playerid,"Indítás folyamatban!",2000,4);
								}
								else if(vhealths < 400)
								{
									folyindit[playerid] = true;
									inditmen[playerid] = false;
									GameTextForPlayer(playerid,"Indítás folyamatban!",8000,4);
									SetTimerEx("startmotor",8000,false,"ud",playerid,GetPlayerVehicleID(playerid));
								}
								else
								{
									folyindit[playerid] = true;
									inditmen[playerid] = true;
									SetTimerEx("startmotor",floatround((1000/vhealths) * 3.5) * 1000,false,"ud",playerid,GetPlayerVehicleID(playerid));
									GameTextForPlayer(playerid,"Indítás folyamatban!",floatround((1000/vhealths) * 3.5) * 1000,4);						
								}
							}
							else
							{
								SendClientMessage(playerid,-1,"(( Már indítod! ))");
							}
							if(PlayerInfo[playerid][trafipaxo] == true)
							{
								PlayerInfo[playerid][trafipaxo] = false;
								KillTimer(PlayerInfo[playerid][trafitimer]);
								SendClientMessage(playerid,-1,"(( Trafipax kikapcsolva! ))");
							}
						}
						else
						{
							/*if(IsPlayerInAnyVehicle(playerid))
							{
								if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
								{
									if(engine == 1)
									{
										SetVehicleParamsEx(GetPlayerVehicleID(playerid), 0, lights, alarm, doors, bonnet, boot, objective);
										SendClientMessage(playerid, -1, "(( Leállítva ))");
									}
									else
									{
										SendClientMessage(playerid, -1, "(( Már levan állítva ))");
									}
								}
								else
								{
									SendClientMessage(playerid, -1, "(( Nem te vagy a vezetõ! ))");
								}
							}
							else
							{
								SendClientMessage(playerid, -1, "(( Nem ülsz kocsiban! ))");
							}*/
						}
					}
					else
					{
						SendClientMessage(playerid,COLOR_RED,"(( Kifogyott az üzemanyag ))");
					}
				}
				else
				{
					SendClientMessage(playerid, -1, "(( Nincs hozzá kulcsod ))");
				}
			}
			else
			{
				SendClientMessage(playerid,-1,"(( Jelenleg tankolsz! ))");
			}
		}
		else
		{
			SendClientMessage(playerid, -1, "(( Nem te vagy a vezetõ! ))");
		}
	}
	else
	{
		SendClientMessage(playerid, -1, "(( Nem ülsz kocsiban! ))");
	}
	return 1;
}

public novenyultet(playerid,noveny,mennyiseg)
{
	new sql[512];
	new darab;
	new Cache:res;
	mysql_format(mysql,sql,sizeof(sql),"SELECT COUNT(*) FROM `noveny` WHERE `noveny` = %d",noveny);
	res = mysql_query(mysql,sql);
	cache_get_value_index_int(0,0,darab);
	cache_delete(res);
	if(darab <= 20)
	{
		if(darab + mennyiseg > 20)
		{
			mennyiseg = 20 - darab;
		}
		RemoveItem(playerid,"Marihuána mag",0,mennyiseg);
		new ido=0;
		if(noveny == 0)
		{
			ido = gettime()+18000;
		}
		else
		{
		 ido = gettime()+12600;
		}
		for(new i;i<mennyiseg;i++)
		{
			mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `noveny`(`raktar`, `noveny`, `folyamat`, `ido`) VALUES ('%d','%d','%d','%d')",RaktarInfo[GetPlayerVirtualWorld(playerid)][dbid],noveny,0,ido);
			mysql_tquery(mysql,sql);
		}
		
	}
	else
	{
		SendClientMessage(playerid,-1,"(( Nem tudsz többet ültetni! ))");
	}
	return 1;
}
public novenyfejlodes(playerid,novenyid,novenyf,fejlodes)
{
	new sql[256];
	if(novenyf == 0)
	{
		if(fejlodes < 7)
		{
			mysql_format(mysql,sql,sizeof(sql),"UPDATE `noveny` SET `folyamat`='%d',`ido`='%d' WHERE `ID` = '%d'",fejlodes+1,gettime()+900,novenyid);
			mysql_tquery(mysql,sql);
		}
		if(fejlodes == 7)
		{
			mysql_format(mysql,sql,sizeof(sql),"DELETE FROM `noveny` WHERE `ID` = '%d'",novenyid);
			mysql_tquery(mysql,sql);
			new darab = random(5)+5;
			AddItem(playerid,"Marihuána",darab);
			AddItem(playerid,"Marihuána mag",5);
			new string[128];
			format(string,sizeof(string),"(( Kaptál: %d gramm Marihuánát! ))",darab);
			SendClientMessage(playerid,COLOR_GREEN,string);
		}
	}
	ClearAnimations(playerid);
	return 1;
}

stock removeobj(playerid)
{
	RemoveBuildingForPlayer(playerid, 3687, 2135.7422, -2186.4453, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3687, 2162.8516, -2159.7500, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3687, 2150.1953, -2172.3594, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2073.8281, -2091.2344, 15.1328, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2051.0547, -2089.6094, 15.1328, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2026.2500, -2093.3906, 15.1328, 0.25);
	RemoveBuildingForPlayer(playerid, 3744, 2101.7891, -2162.5781, 15.1328, 0.25);
	RemoveBuildingForPlayer(playerid, 1531, 2173.5938, -2165.1875, 15.3047, 0.25);
	RemoveBuildingForPlayer(playerid, 3622, 2135.7422, -2186.4453, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3622, 2150.1953, -2172.3594, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2101.7969, -2162.5625, 15.0703, 0.25);
	RemoveBuildingForPlayer(playerid, 3567, 2083.5234, -2159.6172, 13.2578, 0.25);
	RemoveBuildingForPlayer(playerid, 3622, 2162.8516, -2159.7500, 15.6719, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2026.2500, -2093.3984, 15.0703, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2073.8359, -2091.2188, 15.0703, 0.25);
	RemoveBuildingForPlayer(playerid, 3574, 2051.0469, -2089.6016, 15.0703, 0.25);
	RemoveBuildingForPlayer(playerid, 3578, 2026.8359, -2099.4531, 13.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 3578, 2050.5938, -2099.4609, 13.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 3578, 2074.0156, -2099.4453, 13.3203, 0.25);
	RemoveBuildingForPlayer(playerid, 645, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 3509, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 14804, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 626, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 649, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 622, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 625, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 740, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 739, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 718, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 716, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 712, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 711, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 710, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 652, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 648, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 627, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 646, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 621, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 641, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 634, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 633, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 632, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 631, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 630, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 629, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 628, 0,0,0,30000.0);
	RemoveBuildingForPlayer(playerid, 620, 0,0,0,30000.0);
}

public dobozkipakol(playerid)
{
	new string[256];
	new dlg[512];
	new sql[256];
	new menny,rows;
	new Cache:res;
	mysql_format(mysql,sql,sizeof(sql),"SELECT `nev`,`mennyiseg` FROM `dobozok`;");
	res = mysql_query(mysql,sql);
	format(string,sizeof(string),"Név\tDarab\n",string,menny);
	strcat(dlg,string);
	cache_get_row_count(rows);
	for(new i=0;i<rows;i++)
	{
		cache_get_value_name(i,"nev",string,sizeof(string));
		cache_get_value_name_int(i,"mennyiseg",menny);
		format(string,sizeof(string),"%s\t%d\n",string,menny);
		strcat(dlg,string);
		
	}
	cache_delete(res);
	ShowPlayerDialog(playerid,DIALOG_DOBOZ1,DIALOG_STYLE_TABLIST_HEADERS,"Doboz",dlg,"OK","Mégse");
	return 1;
}

stock dobozgeneral(dobozidje)
{
	for(new i;i<21;i++)
	{
		new randa = random(228)+1;
		new sql[256];
		switch(randa)
		{
			case 1 .. 35:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[21],14);
			}
			case 36 .. 55:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[22],14);
			}
			case 56 .. 76:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[23],14);
			}
			case 77 .. 102:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[24],14);
			}
			case 103 .. 106:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[25],14);
			}
			case 107 .. 122:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[26],14);
			}
			case 123 .. 143:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[27],14);
			}
			case 144 .. 159:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[28],14);
			}
			case 160 .. 170:
			{	
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[29],14);
			}
			case 171 .. 181:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[30],14);
			}
			case 182 .. 197:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[31],14);
			}
			case 198 .. 223:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[32],14);
			}
			case 224 .. 228:
			{
				mysql_format(mysql,sql,sizeof(sql),"INSERT INTO `dobozok`(`nev`, `mennyiseg`) VALUES ('%s','%d')",fegyvernev[33],14);
			}
		}
		mysql_tquery(mysql,sql);
	}
}

stock karfelmer(playerid,vehicleid)
{
	new dlg[512];
	new string[128];
	new Float:vhelt;
	new PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper,DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight,LFirst, LSecond, LThird, LFourth,TFrontLeft, TFrontRight, TRearLeft, TRearRight;
	GetVehiclePanelsDamageStatus(vehicleid, PFrontLeft, PFrontRight, PRearLeft, PRearRight, PWindShield, PFrontBumper, PRearBumper);
	GetVehicleDoorsDamageStatus(vehicleid, DBonnet, DBoot, DFrontLeft, DFrontRight, DRearLeft, DRearRight);
	GetVehicleLightsDamageStatus(vehicleid, LFirst, LSecond, LThird, LFourth);
	GetVehicleTiresDamageStatus(vehicleid, TFrontLeft, TFrontRight, TRearLeft, TRearRight);
	if(PFrontLeft ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal elsõ lemez\n");
		strcat(dlg,string);
	}
	else if(PFrontLeft <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Bal elsõ lemez\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal elsõ lemez\n");
		strcat(dlg,string);
	}
	if(PFrontRight ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb elsõ lemez\n");
		strcat(dlg,string);
	}
	else if(PFrontRight <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Jobb elsõ lemez\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb elsõ lemez\n");
		strcat(dlg,string);
	}
	if(PRearLeft ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal hátsó lemez\n");
		strcat(dlg,string);
	}
	else if(PRearLeft <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Bal hátsó lemez\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal hátsó lemez\n");
		strcat(dlg,string);
	}
	if(PRearRight ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb hátsó lemez\n");
		strcat(dlg,string);
	}
	else if(PRearRight <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Jobb hátsó lemez\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb hátsó lemez\n");
		strcat(dlg,string);
	}
	if(PWindShield ==0)
	{
		format(string,sizeof(string),"{00ff00}Szélvédõ\n");
		strcat(dlg,string);
	}
	else if(PWindShield <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Szélvédõ\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Szélvédõ\n");
		strcat(dlg,string);
	}
	if(PFrontBumper ==0)
	{
		format(string,sizeof(string),"{00ff00}Elsõ lökhárító\n");
		strcat(dlg,string);
	}
	else if(PFrontBumper <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Elsõ lökhárító\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Elsõ lökhárító\n");
		strcat(dlg,string);
	}
	if(PRearBumper ==0)
	{
		format(string,sizeof(string),"{00ff00}Hátsó lökhárító\n");
		strcat(dlg,string);
	}
	else if(PRearBumper <= 2)
	{
		format(string,sizeof(string),"{f2ff00}Hátsó lökhárító\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Hátsó lökhárító\n");
		strcat(dlg,string);
	}
	if(DBonnet ==0)
	{
		format(string,sizeof(string),"{00ff00}Motorháztetõ\n");
		strcat(dlg,string);
	}
	else if(DBonnet <= 3)
	{
		format(string,sizeof(string),"{f2ff00}Motorháztetõ\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Motorháztetõ\n");
		strcat(dlg,string);
	}
	if(DBoot ==0)
	{
		format(string,sizeof(string),"{00ff00}Csomagtartó\n");
		strcat(dlg,string);
	}
	else if(DBoot <= 3)
	{
		format(string,sizeof(string),"{f2ff00}Csomagtartó\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Csomagtartó\n");
		strcat(dlg,string);
	}
	if(DFrontLeft ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal elsõ ajtó\n");
		strcat(dlg,string);
	}
	else if(DFrontLeft <= 3)
	{
		format(string,sizeof(string),"{f2ff00}Bal elsõ ajtó\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal elsõ ajtó\n");
		strcat(dlg,string);
	}
	if(DFrontRight ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb elsõ ajtó\n");
		strcat(dlg,string);
	}
	else if(DFrontRight <= 3)
	{
		format(string,sizeof(string),"{f2ff00}Jobb elsõ ajtó\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb elsõ ajtó\n");
		strcat(dlg,string);
	}
	if(LFirst ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal elsõ lámpa\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal elsõ lámpa\n");
		strcat(dlg,string);
	}
	if(LSecond ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb elsõ lámpa\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb elsõ lámpa\n");
		strcat(dlg,string);
	}
	if(LThird ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal hátsó lámpa\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal hátsó lámpa\n");
		strcat(dlg,string);
	}
	if(LFourth ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb hátsó lámpa\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb hátsó lámpa\n");
		strcat(dlg,string);
	}
	if(TFrontLeft ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal elsõ kerék\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal elsõ kerék\n");
		strcat(dlg,string);
	}
	if(TFrontRight ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb elsõ kerék\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb elsõ kerék\n");
		strcat(dlg,string);
	}
	if(TRearLeft ==0)
	{
		format(string,sizeof(string),"{00ff00}Bal hátsó kerék\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Bal hátsó kerék\n");
		strcat(dlg,string);
	}
	if(TRearRight ==0)
	{
		format(string,sizeof(string),"{00ff00}Jobb hátsó kerék\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{ff0000}Jobb hátsó kerék\n");
		strcat(dlg,string);
	}
	GetVehicleHealth(vehicleid,vhelt);
	if(vhelt < 300.0)
	{
		format(string,sizeof(string),"{ff0000}Motor\n");
		strcat(dlg,string);
	}
	else if(vhelt < 1000.0)
	{
		format(string,sizeof(string),"{f2ff00}Motor\n");
		strcat(dlg,string);
	}
	else
	{
		format(string,sizeof(string),"{00ff00}Motor\n");
		strcat(dlg,string);
	}
	strcat(dlg,string);
	ShowPlayerDialog(playerid,DIALOG_AUSZEREL1,DIALOG_STYLE_MSGBOX,"Kárjelentés",dlg,"OK","Mégse");
}

public OnPlayerModelSelectionEx(playerid, response, extraid, modelid)
{
	if(extraid == JARMURENDEL_MENU)
	{
	    if(response)
	    {
			jrendelmodel[playerid] = modelid;
			ShowPlayerDialog(playerid,DIALOG_SZERELO14,DIALOG_STYLE_INPUT,"Kereskedés", "Jármû színe 1", "OK", "Mégse");
		}
	}
	return 1;
}

stock cformat(currency)
{
	new string[15];
	format(string,sizeof(string),"%d",currency);
	for(new i=strlen(string) - 3;0<i;i -= 3)
	{
		strins(string,".",i);
	}
	return string;
}

stock allamibevetel(osszeg)
{
	new sql[256];
	mysql_format(mysql,sql,sizeof(sql),"UPDATE `bank` SET `penz` = `penz` + '%d' WHERE `frakcio` = '10'",osszeg);
	mysql_tquery(mysql,sql);
}
stock savebiz()
{
	for(new i;i<MAX_BIZZ;i++)
	{
		new sql[1024];
		mysql_format(mysql,sql,sizeof(sql),"UPDATE `biznisz` SET `owner`='%d',`ownername`='%s' WHERE `Id`='%d'",BizzInfo[i][Owner],BizzInfo[i][Ownername],BizzInfo[i][Id]);
		mysql_tquery(mysql,sql);
	}
}
public afaado(osszeg,&adoosszeg,&adozott)
{
	adoosszeg = floatround((osszeg * AFA) / 100);
	adozott = osszeg - adoosszeg;
}

public createhulla(playerid,killerid,reason,Float:x,Float:y,Float:z,cvw)
{
	new bool:talal = false;
	new i = 0;
	while(i < MAX_HULLA && talal == false)
	{
		if(HullaInfo[i][hvan] == false)
		{
			talal = true;
		}
		else
		{
			i++;
			talal = false;
		}
	}
	if(talal == true)
	{
		z -= 0.8;
		new wep[128];
		GetWeaponName(reason, wep, sizeof(wep));
		sscanf(Nev(playerid), "s[64]",HullaInfo[i][aldozat]);
		sscanf(Nev(killerid), "s[64]",HullaInfo[i][tettes]);
		format(HullaInfo[i][halalok],128,"Halál oka: %s",wep);
		HullaInfo[i][hobject][0] = CreateDynamicObject(2907, x, y, z, 0, 0, 274, cvw);
		HullaInfo[i][hobject][1] = CreateDynamicObject(2905, x+0.85376, y+0.161621, z-0.01857662, 0, 26, 278, cvw);
		HullaInfo[i][hobject][2] = CreateDynamicObject(2905, x+0.739746, y-0.158935, z+0.006423, 356.68469238281, 145.93511962891, 263.75891113281, cvw);
		HullaInfo[i][hobject][3] = CreateDynamicObject(2906, x-0.081787, y+0.239991, z-0.03601265, 359.30590820313, 273.93939208984, 298.02392578125, cvw);
		HullaInfo[i][hobject][4] = CreateDynamicObject(2906, x-0.024414, y-0.30957, z-0.03601265, 359.01071166992, 339.99688720703, 242.68936157227, cvw);
		HullaInfo[i][hobject][5] = CreateDynamicObject(2908, x-0.5, y, z, 0, 0, 274, cvw);
		HullaInfo[i][huido] = gettime();
		HullaInfo[i][htimer] = SetTimerEx("destroyhulla",HULLAIDO,false,"d",i);
	}	
}

public destroyhulla(i)
{
	HullaInfo[i][hvan] = false;
	DestroyDynamicObject(HullaInfo[i][hobject][0]);
	DestroyDynamicObject(HullaInfo[i][hobject][1]);
	DestroyDynamicObject(HullaInfo[i][hobject][2]);
	DestroyDynamicObject(HullaInfo[i][hobject][3]);
	DestroyDynamicObject(HullaInfo[i][hobject][4]);
	DestroyDynamicObject(HullaInfo[i][hobject][5]);
}

public LoadVehicle()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new vid=1;
			new bool:talal = false;
			while( vid < MAX_VEHICLES && talal == false)
			{
				if(VehicleInfo[vid][Vvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					vid++;
				}
			}
			new string2[8];
			new Float:heal;
			new panels,doors,lights,tires;
			cache_get_value_name_int(i,"Id",VehicleInfo[vid][Id]);
			cache_get_value_name_int(i,"Owner",VehicleInfo[vid][Owner]);
			cache_get_value_name(i,"Ownername",VehicleInfo[vid][Ownername],64);
			cache_get_value_name_int(i,"Model",VehicleInfo[vid][Model]);
			cache_get_value_name_float(i,"PosX",VehicleInfo[vid][PosX]);
			cache_get_value_name_float(i,"PosY",VehicleInfo[vid][PosY]);
			cache_get_value_name_float(i,"PosZ",VehicleInfo[vid][PosZ]);
			cache_get_value_name_float(i,"PosFace",VehicleInfo[vid][PosFace]);
			cache_get_value_name_int(i,"Color1",VehicleInfo[vid][Color1]);
			cache_get_value_name_int(i,"Color2",VehicleInfo[vid][Color2]);
			cache_get_value_name(i,"Plate",VehicleInfo[vid][Plate],8);
			cache_get_value_name_int(i,"munka",VehicleInfo[vid][vmunka]);
			cache_get_value_name_int(i,"frakcio",VehicleInfo[vid][vfrakcio]);
			cache_get_value_name_float(i,"health",heal);
			cache_get_value_name_int(i,"panels",panels);
			cache_get_value_name_int(i,"doors",doors);
			cache_get_value_name_int(i,"lights",lights);
			cache_get_value_name_int(i,"tires",tires);
			cache_get_value_name_float(i,"uzemanyag",VehicleInfo[vid][uzemanyag]);
			cache_get_value_name_int(i,"uzemanyagt",VehicleInfo[vid][uzemanyagt]);
			cache_get_value_name_float(i,"km",VehicleInfo[vid][megkilometer]);
			cache_get_value_name_int(i,"lefoglalva",VehicleInfo[vid][lefoglalva]);
			cache_get_value_name_int(i,"lefoglalido",VehicleInfo[vid][lefoglaltido]);
			cache_get_value_name_int(i,"tartozas",VehicleInfo[vid][ktartozas]);
			cache_get_value_name_int(i,"ceg",VehicleInfo[vid][vceg]);
			cache_get_value_name_int(i,"forgalmi",VehicleInfo[vid][forgalmi]);
			new vehicleid = CreateVehicle(VehicleInfo[vid][Model], VehicleInfo[vid][PosX], VehicleInfo[vid][PosY], VehicleInfo[vid][PosZ], VehicleInfo[vid][PosFace], VehicleInfo[vid][Color1], VehicleInfo[vid][Color2],-1,0);
			format(string2,sizeof(string2), "%s", VehicleInfo[vid][Plate]);
			SetVehicleNumberPlate(vehicleid, string2);
			SetVehicleToRespawn(vehicleid);
			SetVehicleParamsEx(vehicleid, 0, 0, 0, 0, 0, 0, 0);
			UpdateVehicleDamageStatus(vehicleid,panels,doors,lights,tires);
			SetVehicleHealth(vehicleid,heal);
			VehicleInfo[vid][Vvan] = true;
		}
		loadkiegcar();
	}
}

public LoadHouse()
{
	new rows;
	if(cache_get_row_count(rows))
	{
		for(new i=0;i<rows;i++)
		{
			new hid;
			new bool:talal = false;
			while( hid< MAX_HOUSE && talal == false)
			{
				if(HouseInfo[hid][hvan] == false)
				{
					talal = true;
				}
				else
				{
					talal = false;
					hid++;
				}
			}
			cache_get_value_name_int(i,"Id",HouseInfo[hid][Id]);
			cache_get_value_name_float(i,"x",HouseInfo[hid][px]);
			cache_get_value_name_float(i,"y",HouseInfo[hid][py]);
			cache_get_value_name_float(i,"z",HouseInfo[hid][pz]);
			cache_get_value_name_float(i,"ex",HouseInfo[hid][enterx]);
			cache_get_value_name_float(i,"ey",HouseInfo[hid][entery]);
			cache_get_value_name_float(i,"ez",HouseInfo[hid][enterz]);
			cache_get_value_name_int(i,"interior",HouseInfo[hid][interior]);
			cache_get_value_name_int(i,"Owner",HouseInfo[hid][Owner]);
			cache_get_value_name(i,"Ownername",HouseInfo[hid][Ownername],128);
			cache_get_value_name_int(i,"Cost",HouseInfo[hid][Cost]);
			cache_get_value_name_int(i,"zarva",HouseInfo[hid][nyitzar]);
			HouseInfo[hid][hvan] = true;
			HouseInfo[hid][hvw] = hid;
			if(HouseInfo[hid][Owner] == 0)
			{
				new string[128];
				format(string,sizeof(string),"Házszám : %d",HouseInfo[hid][Id]);
				HouseInfo[hid][houselabel] = Create3DTextLabel(string ,0x00FF00AA,HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz]+0.5,25, 0, 1);
				HouseInfo[hid][housepick] = CreatePickup(1273, 1, HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz], -1);
			}
			if(HouseInfo[hid][Owner] != 0)
			{
				new string[128];
				format(string,sizeof(string),"Házszám : %d",HouseInfo[hid][Id]);
				HouseInfo[hid][houselabel] = Create3DTextLabel(string ,0x00FF00AA,HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz]+0.5,25, 0, 1);
				HouseInfo[hid][housepick] = CreatePickup(1239, 1, HouseInfo[hid][px], HouseInfo[hid][py], HouseInfo[hid][pz], -1);
			}
		}
	}
}